/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.restlet.data.MediaType;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import entity.EntityConfig;
import java.io.ByteArrayInputStream;
import helper.Helper;
import org.restlet.engine.io.InputStreamChannel;
import org.restlet.representation.ReadableRepresentation;

/**
 *
 * @author hasan
 */
public class DownloadResource extends ServerResource {

    JSONObject jsonObject;
    JSONParser parser = new JSONParser();
    Cache cacheConfig;
    Helper hlp;
    private final EntityConfig entityConfig;

    public DownloadResource() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("PERSPEBSI");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.hlp = new Helper();
    }

    @Get
    public ReadableRepresentation download() {
        InputStreamChannel inputStreamChannel;
        try {
            inputStreamChannel = new InputStreamChannel(new ByteArrayInputStream(new byte[]{1,2,3,4,5,6,7,8,9,10}));
            return new ReadableRepresentation(inputStreamChannel, MediaType.ALL);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
