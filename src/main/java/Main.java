import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import helper.*;
import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfObject;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.DiskStoreConfiguration;
import net.sf.ehcache.Element;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.restlet.Component;
import org.restlet.Context;
import org.restlet.Server;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.engine.Engine;
import org.restlet.engine.application.Encoder;
import org.restlet.engine.connector.ConnectorHelper;
import org.restlet.ext.oauth.OAuthParameters;
import org.restlet.ext.oauth.OAuthProxy;
import org.restlet.ext.oauth.internal.Client;
import org.restlet.ext.oauth.internal.ClientManager;
import org.restlet.ext.oauth.internal.TokenManager;
import org.restlet.ext.oauth.internal.memory.MemoryClientManager;
import org.restlet.ext.oauth.internal.memory.MemoryTokenManager;
import org.restlet.service.EncoderService;
import org.restlet.util.Series;
import controller.ControllerConfig;
import entity.EntityConfig;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.restlet.service.CorsService;
import controller.ControllerEncDec;
import controller.ControllerMasterDataCache;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.swing.*;

/**
 * Created by Hasan on 3/1/2018.
 */
public class Main {
    private static EntityConfig conf;

    public static void main(String[] args) {
        try {
            // TODO code application logic here
            //print classpath
            System.out.println("main : " + args);
            ClassLoader cl = ClassLoader.getSystemClassLoader();
            URL[] urls = ((URLClassLoader) cl).getURLs();
            for (URL url : urls) {
                System.out.println("classpath : " + url.getFile());
            }
            //from argument, if devel
            System.out.println("work directory is : " + args[0]);
            String workDir = args[0];

            //get from java, if prod
            //String workDir = System.getProperty("user.dir") + File.separator;
            //System.out.println("work directory is : " + workDir);

            //check if folder cache exist
            File f = new File(workDir + "cache");
            if (!f.exists() && !f.isDirectory()) {
                System.out.println("creating directory: " + f.getName());
                boolean result = false;
                try {
                    f.mkdir();
                    result = true;
                } catch (SecurityException se) {
                    //handle it
                }
                if (result) {
                    System.out.println("directory cache created");
                }
            }
            //check if folder tmp exist
            f = new File(workDir + "tmp");
            if (!f.exists() && !f.isDirectory()) {
                System.out.println("creating directory: " + f.getName());
                boolean result = false;
                try {
                    f.mkdir();
                    result = true;
                } catch (SecurityException se) {
                    //handle it
                }
                if (result) {
                    System.out.println("directory tmp created");
                }
            }
            //check if folder tmp exist
            f = new File(workDir + "uploadFile");
            if (!f.exists() && !f.isDirectory()) {
                System.out.println("creating directory: " + f.getName());
                boolean result = false;
                try {
                    f.mkdir();
                    result = true;
                } catch (SecurityException se) {
                    //handle it
                }
                if (result) {
                    System.out.println("directory uploadFile created");
                }
            }
            //check if folder uploadFile/register_agent exist
            f = new File(workDir + "uploadFile/register_agent");
            if (!f.exists() && !f.isDirectory()) {
                System.out.println("creating directory: " + f.getName());
                boolean result = false;
                try {
                    f.mkdir();
                    result = true;
                } catch (SecurityException se) {
                    //handle it
                }
                if (result) {
                    System.out.println("directory uploadFile/register_agent created");
                }
            }
            //----------------get the config data and pass to config entity--------------------
            Main.conf = new EntityConfig();
            ControllerConfig konf = new ControllerConfig(Main.conf, workDir);
            Main.conf = konf.getData();
            //--------------------------ehcache init--------------------------------------
            DiskStoreConfiguration diskStoreConfiguration = new DiskStoreConfiguration();
            diskStoreConfiguration.setPath(workDir + "cache");
            // Already created a configuration object ...
            Configuration cacheConfiguration = new Configuration();
            cacheConfiguration.addDiskStore(diskStoreConfiguration);
            //Create a CacheManager using custom configuration
            CacheManager cacheManager = CacheManager.create(cacheConfiguration);
            //--------------------------ehcache save the config entity------------------------
            Cache memoryOnlyCache = new Cache("TrueMoneyApp", 1, false, true, 0, 0);
            cacheManager.addCache(memoryOnlyCache);
            //--------------------------get cache and put new element-------------------------------------
            Cache thisCache = cacheManager.getCache("TrueMoneyApp");
            thisCache.put(new Element("config", Main.conf));
            //-------------------------ehcache for cookies--------------------------
            //new Cache(workDir, maxElementsInMemory, overflow to disk, eternal, timeToLiveSeconds, timeToIdleSeconds)
            Cache memoryPlusDiskCache = new Cache("TrueMoneyAppCookies", 100, true, false, 900, 0);
            cacheManager.addCache(memoryPlusDiskCache);

            //-------------------------ehcache for dataMaster--------------------------
            //new Cache(workDir, maxElementsInMemory, overflow to disk, eternal, timeToLiveSeconds, timeToIdleSeconds)
            memoryPlusDiskCache = new Cache("TrueMoneyAppMaster", 1, true, true, 0, 0);
            cacheManager.addCache(memoryPlusDiskCache);
            //get all master data to save to cache
            ControllerMasterDataCache controllerMasterDataCache = new ControllerMasterDataCache(Main.conf);
            ArrayList<HashMap<String, Object>> rowMsWording = controllerMasterDataCache.getMsWording();
            System.out.println("cache ms wording lenght data : " + rowMsWording.size());
            //--------------------------get cache of TrueMoneyAppMaster and put new element-------------------------
            thisCache = cacheManager.getCache("TrueMoneyAppMaster");
            thisCache.put(new Element("msWording", rowMsWording));

            //-----------------------------start web server----------------------------------
            final Component component = new Component();
            // Disable damn log
            component.setLogService(new org.restlet.service.LogService(false));

            //HTTP
            component.getServers().add(Protocol.HTTP, Integer.parseInt(Main.conf.getHttpPort()));
            component.getContext().getParameters().add("maxThreads", Main.conf.getMaxThreads());
            component.getContext().getParameters().add("minThreads", Main.conf.getMinThreads());
            component.getContext().getParameters().add("lowThreads", Main.conf.getLowThreads());
            component.getContext().getParameters().add("maxQueued", Main.conf.getMaxQueued());
            component.getContext().getParameters().add("maxTotalConnections", Main.conf.getMaxTotalConnections());
            component.getContext().getParameters().add("maxIoIdleTimeMs", Main.conf.getMaxIoIdleTimeMs());

//            HTTPS
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            int httpsPort = Integer.parseInt(Main.conf.getHttpPort()) + 1;
            Server server = component.getServers().add(Protocol.HTTPS, httpsPort);
            Series<Parameter> parameters = server.getContext().getParameters();
            parameters.add("sslContextFactory", "org.restlet.engine.ssl.DefaultSslContextFactory");
            parameters.add("sslContextFactory", "org.eclipse.jetty.http.ssl.SslContextFactory");
            parameters.add("keystorePath", workDir + "config/TMMobileAgent.jks");
            parameters.add("keystorePassword", "smartcity123");
            parameters.add("keyPassword", "smartcity123");
            parameters.add("keystoreType", "JKS");
            parameters.add("maxThreads", Main.conf.getMaxThreads());
            parameters.add("minThreads", Main.conf.getMinThreads());
            parameters.add("lowThreads", Main.conf.getLowThreads());
            parameters.add("maxQueued", Main.conf.getMaxQueued()); //never reject
            parameters.add("maxTotalConnections", Main.conf.getMaxTotalConnections());
            parameters.add("maxIoIdleTimeMs", Main.conf.getMaxIoIdleTimeMs());

            //allow cross domain
            CorsService corsService = new CorsService();
            corsService.setAllowedOrigins(new HashSet(Arrays.asList("*")));
            corsService.setAllowedCredentials(true);
            component.getServices().add(corsService);

            // encode the response
//            Encoder encoder = new Encoder(component.getContext().createChildContext(), false, true, new EncoderService(true));
//            encoder.setNext(RESTResource.class);
//            component.getDefaultHost().attach(Main.conf.getPathUri(), encoder);

            component.getDefaultHost().attach(Main.conf.getPathUri(), new ExternalApplication(Main.conf));

            //view all parameter
            System.out.println(component.getContext().getParameters());

            //view all register server
            List<ConnectorHelper<org.restlet.Server>> servers = Engine.getInstance().getRegisteredServers();
            System.out.println("Server connectors - " + servers.size());
            for (ConnectorHelper<org.restlet.Server> connectorHelper : servers) {
                System.out.println("connector = " + connectorHelper.getClass());
            }

            component.start();

            //tester your modul
            Tester tester = new Tester(conf);
//            JSONObject jObjMessage = new JSONObject();
//            jObjMessage.put("mainJudul", "TrueMoney");
//
//            JSONObject jObjExtra = new JSONObject();
//            jObjExtra.put("tipe", "agentToAgent");
//            jObjExtra.put("subJudul", "Transfer agent ke agent sukses sebesar Rp.250.000");
//
//            JSONArray jArrIdAgent = new JSONArray();
//            jArrIdAgent.add("180314100098");
//            jArrIdAgent.add("160115100071");
//            jObjExtra.put("idAgentPenerima", jArrIdAgent);
//
//            jObjMessage.put("extra", jObjExtra);
//
//            JSONArray jArrToken = new JSONArray();
//            jArrToken.add("e0z6SgyAZmA:APA91bEGR5P7cv4WaAOcW8uKmxZ4sJf8GyGVuTbfh98FR2KItKBZVM4cASxRHadYISOmu5eVFm3QbsOboeRMOgImQmY7H_y-Ty5v0u-kjUy2V3_1AI_WJQkvH_0vyLPLt4q93polEjzI7bhBzWGqnJnRBWZaAqVdfQ");
//            jArrToken.add("dbPMB3_djcI:APA91bEU0NyXD_6c2vpQmW3_96w6jqlGAwVIgW0zp-0kfyXyOT1u-aW6ONI5SGgXnl-WxbcAFZDBu8l5llt7d01ho8xO4qAsSl73FrdELK_Xg_kT8zObN7b6HM6dw-z9qW_asC6BxcuefHfNH3KIzYAEwaMdzTW-zQ");
//            tester.tesFcm(jArrToken, jObjMessage);
            //tester.tesEmailKeretaApi();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
