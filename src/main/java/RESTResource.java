/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import com.github.scribejava.core.model.OAuthConstants;
import com.github.scribejava.core.model.Verb;
import controller.*;
import helper.OAuthRequestValidator;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.*;
import entity.EntityConfig;
import helper.CookieHelper;
import org.restlet.data.Status;
import helper.Helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.restlet.data.CookieSetting;
import org.restlet.data.Header;
import org.restlet.representation.ObjectRepresentation;
import org.restlet.util.Series;
import helper.Utilities;

import javax.servlet.ServletContext;

/**
 * @author hasan
 */
public class RESTResource extends ServerResource {

    JSONObject jsonObject;
    JSONParser parser = new JSONParser();
    Cache cacheConfig;
    Helper hlp;
    private EntityConfig entityConfig;
    private ControllerLog controllerLog;
    private String cookieClient;
    private boolean expireCookie;
    private CookieHelper cookieHelper;
    private Utilities utilities;
    private ControllerEncDec encDec;

    public RESTResource() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.controllerLog = new ControllerLog();
        cookieHelper = new CookieHelper();
    }

    public void cookieHandler() {
        //-------------------------------------cookies-------------------------------------
        Series<Header> headers = getRequest().getHeaders();
        this.cookieHelper = new CookieHelper();
        this.cookieClient = headers.getFirstValue("Cookie");
        this.cookieClient = (this.cookieClient != null) ? this.cookieClient : "nullCookieFromClient";
        //System.out.println("cookie from client " + this.cookieClient);
        boolean existCookie = cookieHelper.checkExistCookie(this.cookieClient);
        //System.out.println("cookie exist in cache? " + existCookie);
        this.expireCookie = true;
        if (existCookie) {
            //check expire
            this.expireCookie = cookieHelper.checkExpireCookie(this.cookieClient);
            //System.out.println("cookie expire in cache? " + this.expireCookie);
            if (this.expireCookie) {
                //cookie expire, comment this if request can be paralel from client, if client request just one at the time, uncoment
                cookieHelper.removeCookie(this.cookieClient);
            }
        }
        //----------------------------------end cookies-------------------------------------
    }

    public void recreateCookieCustom(){
        CookieSetting cS = cookieHelper.setCookie(this.cookieClient);//recreate/refresh new cookie
        this.getResponse().getCookieSettings().add(cS);
        this.cookieClient = cS.getValue(); // save new cookie to variable
    }

    public void getCookiesCustom() {
        CookieSetting cS = cookieHelper.setCookie("");
        this.getResponse().getCookieSettings().add(cS);
        this.cookieClient = cS.getValue(); // save new cookie to variable
    }

    public String logRequest(String tipeRequest, JSONObject jsonReq) {
        JSONObject jsonReqForLog = this.utilities.writeLogStreamRequest(tipeRequest, jsonReq);
        String idTmp = this.utilities.insertTmp(tipeRequest, jsonReqForLog);
        return idTmp;
    }

    public void logResponse(String tipeRequest, JSONObject jsonReq, String resp, String idTmp) {
        this.utilities.writeLogStreamResponse(tipeRequest, jsonReq, resp);
        this.utilities.updateTmp(idTmp, resp);
    }

    public JSONObject extractMultipart(Representation entity, String downlineId, String ipAddr, int port, String browserAgent) {
        JSONObject jsonReq = this.hlp.extractMultipart(entity, this.entityConfig);
        //System.out.println("req : " + jsonReq);
        jsonReq.put("downlineId", downlineId);
        jsonReq.put("ipAddr", ipAddr);
        jsonReq.put("port", port);
        jsonReq.put("browserAgent", browserAgent);
        return jsonReq;
    }

    public boolean oauth2LegVerify(JSONObject jsonReq) {
        // Check that all the needed parameters are contained in the request
        // Validate the request
        OAuthRequestValidator oAuthRequestValidator = new OAuthRequestValidator();
        boolean valid = oAuthRequestValidator.validate(
                Verb.valueOf(getMethod().getName()),
                getReference().toString(false, false),
                jsonReq.get(OAuthConstants.TIMESTAMP).toString(),
                jsonReq.get(OAuthConstants.NONCE).toString(),
                jsonReq.get(OAuthConstants.SIGN_METHOD).toString(),
                jsonReq.get(OAuthConstants.VERSION).toString(),
                jsonReq.get(OAuthConstants.CONSUMER_KEY).toString(),
                jsonReq.get("secretKeyHeader").toString(),
                jsonReq.get(OAuthConstants.SIGNATURE).toString());
        return valid;
    }

    @Post
    public Representation postHandler(Representation entity) throws ParseException {
        Representation result = null;
        String resp = "";
        String path = getRequest().getResourceRef().getPath();
        String[] pathSplit = path.split("/");

        String ipAddr = getClientInfo().getAddress();
        int port = getClientInfo().getPort();
        String browserAgent = getRequest().getClientInfo().getAgent();

        this.cookieHandler();

        //the cookie must send from origin agent and ip address
        if (this.cookieHelper.checkExistCookie(this.cookieClient)) {
            if (!cookieHelper.checkExpireCookie(this.cookieClient)) {
                JSONObject jObjCookies = this.cookieHelper.getDataCookie(this.cookieClient);
                if (jObjCookies.containsKey("browserAgent") && jObjCookies.containsKey("ipAddr")) {
                    if (!jObjCookies.get("browserAgent").equals(browserAgent) || !jObjCookies.get("ipAddr").equals(ipAddr)) {
                        resp = this.cookieHelper.respExpire();
                        result = new StringRepresentation(resp, MediaType.TEXT_PLAIN);
                        setStatus(Status.SUCCESS_OK);
                        return result;
                        //throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN, "Forbidden resource");
                    } else {
                        //valid
                    }
                } else {
                    this.cookieHelper.addDataCookie(this.cookieClient, "browserAgent", browserAgent);
                    this.cookieHelper.addDataCookie(this.cookieClient, "ipAddr", ipAddr);
                }
            }
        }

        String downlineId = this.hlp.createID();
        JSONObject jsonReq = new JSONObject();

        if (entity != null) {
            if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true)) {
                String tipeRequest = "";
                String idTmp = "";
                jsonReq = this.extractMultipart(entity, downlineId, ipAddr, port, browserAgent);
                //oauth validation
                boolean valid = this.oauth2LegVerify(jsonReq);
                if (!valid) {
                    //throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN, "Forbidden resource");
                    resp = this.cookieHelper.respExpire();
                    result = new StringRepresentation(resp, MediaType.TEXT_PLAIN);
                    setStatus(Status.SUCCESS_OK);
                    return result;
                }
                if ("auth".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                    if ("verify".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "auth verify delete cookies";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        JSONObject jObjResp = new JSONObject();
                        if (this.cookieHelper.checkExistCookie(this.cookieClient)) {
                            this.cookieHelper.removeCookie(this.cookieClient);
                            jObjResp.put("ACK", "OK");
                            jObjResp.put("pesan", "OK");
                        } else {
                            jObjResp.put("ACK", "NOK");
                            jObjResp.put("pesan", "NOK");
                        }
                        resp = jObjResp.toString();
                    }
                } else if ("login".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                    if ("signintoken".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "signintoken";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerLogin controllerLogin = new ControllerLogin();
                        resp = controllerLogin.signInToken(jsonReq, this.cookieClient);
                        JSONObject jsonResp = (JSONObject) parser.parse(resp);
                        if (jsonResp.get("ACK").toString().equals("OK")
                                && jsonResp.get("device").toString().equals("not change")) {
                            this.getCookiesCustom();
                            this.cookieHelper.addDataCookie(cookieClient, "hasLogin", "true");
                        }
                    } else if ("getotp".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        this.getCookiesCustom();
                        tipeRequest = "getotp";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerLogin controllerLogin = new ControllerLogin();
                        resp = controllerLogin.getOtp(jsonReq, this.cookieClient);
                    } else if ("verifyOtp".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "verifyOtp";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerLogin controllerLogin = new ControllerLogin();
                        resp = controllerLogin.verifyOtp(jsonReq, this.cookieClient);
                    } else if ("blockaccount".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        this.cookieHelper.removeCookie(this.cookieClient);
                        tipeRequest = "blockAccount";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerLogin controllerLogin = new ControllerLogin();
                        resp = controllerLogin.blockAccount(jsonReq, this.cookieClient);
                    } else if ("changedevice".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "changeDevice";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerLogin controllerLogin = new ControllerLogin();
                        resp = controllerLogin.changeDevice(jsonReq, this.cookieClient);
                        JSONObject jsonResp = (JSONObject) parser.parse(resp);
                        if (jsonResp.get("ACK").toString().equals("OK")) {
                            this.cookieHelper.addDataCookie(cookieClient, "hasLogin", "true");
                        }
                    } else if ("setFcmToken".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "setFcmToken";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerLogin controllerLogin = new ControllerLogin();
                        resp = controllerLogin.setFcmToken(jsonReq, this.cookieClient);
                    }
                } else if ("fcm".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                    if ("sentFcm".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "sent message FCM";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerFcm controllerFcm = new ControllerFcm();
                        resp = controllerFcm.sentMessage(jsonReq, this.cookieClient);
                    }
                } else if ("register".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                    if ("getKelurahan".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "getKelurahan";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerRegister controllerRegister = new ControllerRegister();
                        resp = controllerRegister.getKelurahan(jsonReq, this.cookieClient);
                    } else if ("getRegister".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "getRegister";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerRegister controllerRegister = new ControllerRegister();
                        resp = controllerRegister.getRegister(jsonReq, this.cookieClient);
                    } else if ("getOtp".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "getOtp";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerRegister controllerRegister = new ControllerRegister();
                        resp = controllerRegister.getOtp(jsonReq, this.cookieClient);
                    } else if ("getOtpResetPin".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "getOtpResetPin";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerProfile controllerProfile = new ControllerProfile();
                        resp = controllerProfile.getOtpResetPin(jsonReq, this.cookieClient);
                    } else if ("getResetPin".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "getResetPin";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerProfile controllerProfile = new ControllerProfile();
                        resp = controllerProfile.getResetPin(jsonReq, this.cookieClient);
                    } else if ("getReferral".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        tipeRequest = "getReferral";
                        idTmp = this.logRequest(tipeRequest, jsonReq);
                        ControllerRegister controllerRegister = new ControllerRegister();
                        resp = controllerRegister.getReferral(jsonReq, this.cookieClient);
                    }
                } else if ("lakupandai".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                    if ("agen46".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                        if ("getKelurahan".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                            tipeRequest = "getKelurahan";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerAgen46 controllerAgen46 = new ControllerAgen46();
                            resp = controllerAgen46.getKelurahan(jsonReq, this.cookieClient);
                        } else if ("getRegister".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                            tipeRequest = "agen46Reg";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerAgen46 controllerAgen46 = new ControllerAgen46();
                            resp = controllerAgen46.getRegister(jsonReq, this.cookieClient);
                        } else if ("getOtpTarikTunai".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                            tipeRequest = "agen46 getOtpTarikTunai";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerAgen46 controllerAgen46 = new ControllerAgen46();
                            resp = controllerAgen46.getOtpTarikTunai(jsonReq, this.cookieClient);
                        } else if ("getSetorTunai".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                            tipeRequest = "agen46 getSetorTunai";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerAgen46 controllerAgen46 = new ControllerAgen46();
                            resp = controllerAgen46.getSetorTunai(jsonReq, this.cookieClient);
                        } else if ("checkAkun".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                            tipeRequest = "agen46 checkAkun";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerAgen46 controllerAgen46 = new ControllerAgen46();
                            resp = controllerAgen46.checkAkun(jsonReq, this.cookieClient);
                        } else if ("getTarik".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                            tipeRequest = "agen46 getTarik";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerAgen46 controllerAgen46 = new ControllerAgen46();
                            resp = controllerAgen46.getTarik(jsonReq, this.cookieClient);
                        }
                    }
                } else if (this.expireCookie == false) {
                    //check login valid
                    JSONObject jObjDataCookie = this.cookieHelper.getDataCookie(cookieClient);
                    if (jObjDataCookie.containsKey("hasLogin")) {
                        if (!jObjDataCookie.get("hasLogin").toString().equals("true")) {
                            resp = this.cookieHelper.respExpire();
                            result = new StringRepresentation(resp, MediaType.TEXT_PLAIN);
                            setStatus(Status.SUCCESS_OK);
                            return result;
                        } else {
                            //recreate cookies
                            this.recreateCookieCustom();
                        }
                    } else {
                        resp = this.cookieHelper.respExpire();
                        result = new StringRepresentation(resp, MediaType.TEXT_PLAIN);
                        setStatus(Status.SUCCESS_OK);
                        return result;
                    }
                    //if has login and cookie not expire
                    if ("home".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("getlastbalance".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "getlastbalance";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerHome controllerHome = new ControllerHome();
                            resp = controllerHome.getLastBalance(jsonReq, this.cookieClient);
                        } else if ("getPoint".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "getPoint";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerHome controllerHome = new ControllerHome();
                            resp = controllerHome.getPoint(jsonReq, this.cookieClient);
                        } else if ("getIklan".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "getIklan";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerHome controllerHome = new ControllerHome();
                            resp = controllerHome.getIklan(jsonReq, this.cookieClient);
                        }
                    } else if ("profile".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("getProfile".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "getProfile";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerProfile controllerProfile = new ControllerProfile();
                            resp = controllerProfile.getProfile(jsonReq, this.cookieClient);
                        } else if ("getChangePin".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "getChangePin";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerProfile controllerProfile = new ControllerProfile();
                            resp = controllerProfile.getChangePin(jsonReq, this.cookieClient);
                        }
                    } else if ("notification".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("getnotif".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "getnotif";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerNotif controllerNotif = new ControllerNotif();
                            resp = controllerNotif.getNotif(jsonReq, this.cookieClient);
                        }
                    } else if ("mutasi".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("getmutasi".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "getmutasi";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerHistory controllerHistory = new ControllerHistory();
                            resp = controllerHistory.getHistory(jsonReq, this.cookieClient);
                        }
                    } else if ("pembelian".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("pulsapra".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getdenom".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getdenom pulsapra";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianPulsa controllerPembelianPulsa = new ControllerPembelianPulsa();
                                resp = controllerPembelianPulsa.getDenom(jsonReq, this.cookieClient);
                            } else if ("bayarPulsaPrabayar".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "bayarPulsaPrabayar";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianPulsa controllerPembelianPulsa = new ControllerPembelianPulsa();
                                resp = controllerPembelianPulsa.bayarPulsaPrabayar(jsonReq, this.cookieClient);
                            }
                        } else if ("plnPra".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getdenom".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getdenom plnPra";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianPln controllerPembelianPln = new ControllerPembelianPln();
                                resp = controllerPembelianPln.getDenom(jsonReq, this.cookieClient);
                            } else if ("getInqPln".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getInqPln";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianPln controllerPembelianPln = new ControllerPembelianPln();
                                resp = controllerPembelianPln.getInqPln(jsonReq, this.cookieClient);
                            } else if ("bayarPlnPrabayar".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "bayarPlnPrabayar";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianPln controllerPembelianPln = new ControllerPembelianPln();
                                resp = controllerPembelianPln.bayarPlnPrabayar(jsonReq, this.cookieClient);
                            }
                        } else if ("game".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getGame".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "get game";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianGame controllerPembelianGame = new ControllerPembelianGame();
                                resp = controllerPembelianGame.getGame(jsonReq, this.cookieClient);
                            } else if ("getNominal".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "get game nominal";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianGame controllerPembelianGame = new ControllerPembelianGame();
                                resp = controllerPembelianGame.getNominal(jsonReq, this.cookieClient);
                            } else if ("getInquiry".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "get game inquiry";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianGame controllerPembelianGame = new ControllerPembelianGame();
                                resp = controllerPembelianGame.getInquiry(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "get game pay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianGame controllerPembelianGame = new ControllerPembelianGame();
                                resp = controllerPembelianGame.getPay(jsonReq, this.cookieClient);
                            } else if ("getNewPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "get game New pay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianGame controllerPembelianGame = new ControllerPembelianGame();
                                resp = controllerPembelianGame.getNewPay2(jsonReq, this.cookieClient);
                            }
                        } else if ("paketData".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getdenom".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getdenom paketData";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianPaketData controllerPembelianPaketData = new ControllerPembelianPaketData();
                                resp = controllerPembelianPaketData.getDenom(jsonReq, this.cookieClient);
                            } else if ("paketDataPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "paketDataPay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianPaketData controllerPembelianPaketData = new ControllerPembelianPaketData();
                                resp = controllerPembelianPaketData.paketDataPay(jsonReq, this.cookieClient);
                            }
                        } else if ("streaming".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("iflix".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                if ("getdenom".equalsIgnoreCase(pathSplit[5].toLowerCase())) {
                                    tipeRequest = "getdenom iflix";
                                    idTmp = this.logRequest(tipeRequest, jsonReq);
                                    ControllerPembelianIflix controllerPembelianIflix = new ControllerPembelianIflix();
                                    resp = controllerPembelianIflix.getDenom(jsonReq, this.cookieClient);
                                } else if ("getpay".equalsIgnoreCase(pathSplit[5].toLowerCase())) {
                                    tipeRequest = "getpay iflix";
                                    idTmp = this.logRequest(tipeRequest, jsonReq);
                                    ControllerPembelianIflix controllerPembelianIflix = new ControllerPembelianIflix();
                                    resp = controllerPembelianIflix.getPay(jsonReq, this.cookieClient);
                                }
                            }
                        } else if ("keretaApi".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getStasiun".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta getStasiun";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianKeretaApi controllerPembelianKeretaApi = new ControllerPembelianKeretaApi();
                                resp = controllerPembelianKeretaApi.getStasiun(jsonReq, this.cookieClient);
                            } else if ("getSchedule".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta getSchedule";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianKeretaApi controllerPembelianKeretaApi = new ControllerPembelianKeretaApi();
                                resp = controllerPembelianKeretaApi.getSchedule(jsonReq, this.cookieClient);
                            } else if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta getInqAdmin";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianKeretaApi controllerPembelianKeretaApi = new ControllerPembelianKeretaApi();
                                resp = controllerPembelianKeretaApi.getInq(jsonReq, this.cookieClient);
                            } else if ("setManualSeat".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta setManualSeat";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianKeretaApi controllerPembelianKeretaApi = new ControllerPembelianKeretaApi();
                                resp = controllerPembelianKeretaApi.setManualSeat(jsonReq, this.cookieClient);
                            } else if ("getCancelBook".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta getCancelBook";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianKeretaApi controllerPembelianKeretaApi = new ControllerPembelianKeretaApi();
                                resp = controllerPembelianKeretaApi.getCancelBook(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta getPay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianKeretaApi controllerPembelianKeretaApi = new ControllerPembelianKeretaApi();
                                resp = controllerPembelianKeretaApi.getPay(jsonReq, this.cookieClient);
                            } else if ("getSeatMap".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta getSeatMap";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembelianKeretaApi controllerPembelianKeretaApi = new ControllerPembelianKeretaApi();
                                resp = controllerPembelianKeretaApi.getSeatMap(jsonReq, this.cookieClient);
                            }
                        } else if ("qurban".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getKelurahan".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getKelurahan Qurban";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerQurban controllerQurban = new ControllerQurban();
                                resp = controllerQurban.getKelurahan(jsonReq, this.cookieClient);
                            } else if ("getProduk".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getProduk Qurban";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerQurban controllerQurban = new ControllerQurban();
                                resp = controllerQurban.getProduk(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getPay Qurban";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerQurban controllerQurban = new ControllerQurban();
                                resp = controllerQurban.getPay(jsonReq, this.cookieClient);
                            }
                        }
                    } else if ("pembayaran".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("plnPasca".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kereta getInq plnPasca";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranPln controllerPembayaranPln = new ControllerPembayaranPln();
                                resp = controllerPembayaranPln.getInq(jsonReq, this.cookieClient);
                            } else if ("bayarPlnPascabayar".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "bayarPlnPascabayar";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranPln controllerPembayaranPln = new ControllerPembayaranPln();
                                resp = controllerPembayaranPln.bayarPlnPascabayar(jsonReq, this.cookieClient);
                            }
                        } else if ("bpjsPostpaid".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getInq bpjsPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranBpjs controllerPembayaranBpjs = new ControllerPembayaranBpjs();
                                resp = controllerPembayaranBpjs.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getPay bpjsPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranBpjs controllerPembayaranBpjs = new ControllerPembayaranBpjs();
                                resp = controllerPembayaranBpjs.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("jastelPostpaid".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getInq jastelPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranJastel controllerPembayaranJastel = new ControllerPembayaranJastel();
                                resp = controllerPembayaranJastel.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getPay jastelPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranJastel controllerPembayaranJastel = new ControllerPembayaranJastel();
                                resp = controllerPembayaranJastel.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("pdamPostpaid".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getKodeArea".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getKodeArea pdamPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranPdam controllerPembayaranPdam = new ControllerPembayaranPdam();
                                resp = controllerPembayaranPdam.getKodeArea(jsonReq, this.cookieClient);
                            } else if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getInq pdamPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranPdam controllerPembayaranPdam = new ControllerPembayaranPdam();
                                resp = controllerPembayaranPdam.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getPay pdamPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranPdam controllerPembayaranPdam = new ControllerPembayaranPdam();
                                resp = controllerPembayaranPdam.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("internetPostpaid".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getInq internetPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranInternet controllerPembayaranInternet = new ControllerPembayaranInternet();
                                resp = controllerPembayaranInternet.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getPay internetPostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranInternet controllerPembayaranInternet = new ControllerPembayaranInternet();
                                resp = controllerPembayaranInternet.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("multifinancePostpaid".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getMultifinance".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getMultifinance";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranMultifinance controllerPembayaranMf = new ControllerPembayaranMultifinance();
                                resp = controllerPembayaranMf.getMultifinance(jsonReq, this.cookieClient);
                            } else if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getInq multifinancePostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranMultifinance controllerPembayaranMf = new ControllerPembayaranMultifinance();
                                resp = controllerPembayaranMf.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getPay multifinancePostpaid";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranMultifinance controllerPembayaranMf = new ControllerPembayaranMultifinance();
                                resp = controllerPembayaranMf.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("tvPostpaid".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getTv".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getTv";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranTv controllerPembayaranTv = new ControllerPembayaranTv();
                                resp = controllerPembayaranTv.getTv(jsonReq, this.cookieClient);
                            } else if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getInq getTv";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranTv controllerPembayaranTv = new ControllerPembayaranTv();
                                resp = controllerPembayaranTv.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getPay getTv";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranTv controllerPembayaranTv = new ControllerPembayaranTv();
                                resp = controllerPembayaranTv.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("komunitas".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getKomunitas".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "getKomunitas";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranKomunitas controllerPembayaranKomunitas = new ControllerPembayaranKomunitas();
                                resp = controllerPembayaranKomunitas.getKomunitas(jsonReq, this.cookieClient);
                            } else if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "komunitas getInq";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranKomunitas controllerPembayaranKomunitas = new ControllerPembayaranKomunitas();
                                resp = controllerPembayaranKomunitas.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "komunitas getPay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerPembayaranKomunitas controllerPembayaranKomunitas = new ControllerPembayaranKomunitas();
                                resp = controllerPembayaranKomunitas.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("asuransi".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("allianz".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                if ("getKelurahan".equalsIgnoreCase(pathSplit[5].toLowerCase())) {
                                    tipeRequest = "allianz getKelurahan";
                                    idTmp = this.logRequest(tipeRequest, jsonReq);
                                    ControllerAsuransiAllianz controllerAsuransiAllianz = new ControllerAsuransiAllianz();
                                    resp = controllerAsuransiAllianz.getKelurahan(jsonReq, this.cookieClient);
                                } else if ("getProduk".equalsIgnoreCase(pathSplit[5].toLowerCase())) {
                                    tipeRequest = "allianz getProduk";
                                    idTmp = this.logRequest(tipeRequest, jsonReq);
                                    ControllerAsuransiAllianz controllerAsuransiAllianz = new ControllerAsuransiAllianz();
                                    resp = controllerAsuransiAllianz.getProduk(jsonReq, this.cookieClient);
                                } else if ("getPay".equalsIgnoreCase(pathSplit[5].toLowerCase())) {
                                    tipeRequest = "allianz getPay";
                                    idTmp = this.logRequest(tipeRequest, jsonReq);
                                    ControllerAsuransiAllianz controllerAsuransiAllianz = new ControllerAsuransiAllianz();
                                    resp = controllerAsuransiAllianz.getPay(jsonReq, this.cookieClient);
                                }
                            }
                        }
                    } else if ("cairkanSaldo".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("getInq".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "cairkanSaldo getInq";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerCairkanSaldo controllerCairkanSaldo = new ControllerCairkanSaldo();
                            resp = controllerCairkanSaldo.getInq(jsonReq, this.cookieClient);
                        } else if ("getPay".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "cairkanSaldo getPay";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerCairkanSaldo controllerCairkanSaldo = new ControllerCairkanSaldo();
                            resp = controllerCairkanSaldo.getPay(jsonReq, this.cookieClient);
                        }
                    } else if ("multifinance".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("getKelurahan".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "multifinance getKelurahan";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerMultifinance controllerMultifinance = new ControllerMultifinance();
                            resp = controllerMultifinance.getKelurahan(jsonReq, this.cookieClient);
                        } else if ("getProduk".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "multifinance getProduk";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerMultifinance controllerMultifinance = new ControllerMultifinance();
                            resp = controllerMultifinance.getProduk(jsonReq, this.cookieClient);
                        } else if ("getPengajuan".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "multifinance getPengajuan";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerMultifinance controllerMultifinance = new ControllerMultifinance();
                            resp = controllerMultifinance.getPengajuan(jsonReq, this.cookieClient);
                        }
                    } else if ("transfer".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("kirimUang".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kirimUang getInq";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerTransfer controllerTransfer = new ControllerTransfer();
                                resp = controllerTransfer.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "kirimUang getPay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerTransfer controllerTransfer = new ControllerTransfer();
                                resp = controllerTransfer.getPay(jsonReq, this.cookieClient);
                            } else if ("getInqTerimaUang".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "Terima Uang getInq";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerTransfer controllerTransfer = new ControllerTransfer();
                                resp = controllerTransfer.getInqTerimaUang(jsonReq, this.cookieClient);
                            } else if ("getPayTerimaUang".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "Terima Uang getPay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerTransfer controllerTransfer = new ControllerTransfer();
                                resp = controllerTransfer.getPayTerimaUang(jsonReq, this.cookieClient);
                            }
                        } else if ("bank".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getBank".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "Transfer Bank getBank";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerTransferBank controllerTransferBank = new ControllerTransferBank();
                                resp = controllerTransferBank.getBank(jsonReq, this.cookieClient);
                            } else if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "Transfer Bank getInq";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerTransferBank controllerTransferBank = new ControllerTransferBank();
                                resp = controllerTransferBank.getInqBank(jsonReq, this.cookieClient);
                            } else if ("getTransfer".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "Transfer Bank getTransfer";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerTransferBank controllerTransferBank = new ControllerTransferBank();
                                resp = controllerTransferBank.transferBank(jsonReq, this.cookieClient);
                            }
                        }
                    } else if ("p2p".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("agentToAgent".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "TopUp AgentToAgent getInq";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerP2P controllerP2P = new ControllerP2P();
                                resp = controllerP2P.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "TopUp AgentToAgent getPay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerP2P controllerP2P = new ControllerP2P();
                                resp = controllerP2P.getPay(jsonReq, this.cookieClient);
                            }
                        } else if ("agentToMember".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            if ("getInq".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "TopUp AgentToMember getInq";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerMember controllerMember = new ControllerMember();
                                resp = controllerMember.getInq(jsonReq, this.cookieClient);
                            } else if ("getPay".equalsIgnoreCase(pathSplit[4].toLowerCase())) {
                                tipeRequest = "TopUp AgentToMember getPay";
                                idTmp = this.logRequest(tipeRequest, jsonReq);
                                ControllerMember controllerMember = new ControllerMember();
                                resp = controllerMember.getPay(jsonReq, this.cookieClient);
                            }
                        }
                    } else if ("kudoGrab".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
                        if ("getCity".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "KudoGrab Agent To getCity";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.getKotaKudo(jsonReq, this.cookieClient);
                        } else if ("getBank".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "KudoGrab Agent To getBank";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.getBankKudo(jsonReq, this.cookieClient);
                        } else if ("customerProfileList".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "List Profile KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.customerProfileList(jsonReq, this.cookieClient);
                        } else if ("createApplication".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "createApplication KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.createApplication(jsonReq, this.cookieClient);
                        } else if ("phoneVerification".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "phoneVerification KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.phoneNumberVerification(jsonReq, this.cookieClient);
                        } else if ("resendPhoneVerification".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "resend Phone Verification KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.resendPhoneNumberVerification(jsonReq, this.cookieClient);
                        } else if ("isiKtp".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "Isi Ktp KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.isiKtp(jsonReq, this.cookieClient);
                        } else if ("isiSim".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "Isi Sim KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.isiSim(jsonReq, this.cookieClient);
                        } else if ("isiStnk".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "Isi Stnk KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.isiStnk(jsonReq, this.cookieClient);
                        } else if ("isiPernyataan".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "Isi Pernyataan KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.isiDeclaration(jsonReq, this.cookieClient);
                        } else if ("isiBank".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "Isi Bank KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.isiBank(jsonReq, this.cookieClient);
                        } else if ("uploadFile".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "Upload File KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.uploadFileGrab(jsonReq, this.cookieClient);
                        } else if ("completeData".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "complete data KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.completeData(jsonReq, this.cookieClient);
                        } else if ("checkStatusData".equalsIgnoreCase(pathSplit[3].toLowerCase())) {
                            tipeRequest = "check status Data KudoGrab";
                            idTmp = this.logRequest(tipeRequest, jsonReq);
                            ControllerKudoGrab controllerKudoGrab = new ControllerKudoGrab();
                            resp = controllerKudoGrab.checkStatusData(jsonReq, this.cookieClient);
                        }
                    }
                } else {
                    //response to client if cookie is expire
                    resp = this.cookieHelper.respExpire();
                    //System.out.println("response session expire");
                    result = new StringRepresentation(resp, MediaType.TEXT_PLAIN);
                    setStatus(Status.SUCCESS_OK);
                    return result;
                }
                if (!tipeRequest.equals("") && !idTmp.equals("")) {
                    this.logResponse(tipeRequest, jsonReq, resp, idTmp);
                }
            }
        }

        resp = this.utilities.constructResponse(resp, this.entityConfig);
        result = new StringRepresentation(resp, MediaType.TEXT_PLAIN);
        setStatus(Status.SUCCESS_OK);
        return result;
    }


    @Get
    public Representation getHandler(Representation entity) {
        Representation result = null;

        String path = getRequest().getResourceRef().getPath();
        String[] pathSplit = path.split("/");

        if ("getRegFoto".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
            String fileName = pathSplit[3].toLowerCase();
            ObjectRepresentation<byte[]> or = null;
            try {
                InputStream is = new FileInputStream(this.entityConfig.getMasterFolder() + "uploadFile/register_agent" + File.separator + fileName);
                int len;
                int size = 1024;
                byte[] buf;

                if (is instanceof ByteArrayInputStream) {
                    size = is.available();
                    buf = new byte[size];
                    len = is.read(buf, 0, size);
                } else {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    buf = new byte[size];
                    while ((len = is.read(buf, 0, size)) != -1) {
                        bos.write(buf, 0, len);
                    }
                    buf = bos.toByteArray();
                }

                // Close the input stream and return bytes
                is.close();

                // Print the requested URI path
                or = new ObjectRepresentation<byte[]>(buf, MediaType.IMAGE_PNG) {
                    @Override
                    public void write(OutputStream os) throws IOException {
                        super.write(os);
                        os.write(this.getObject());
                    }
                };
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return or;
        } else if ("getBarcodePhoto".equalsIgnoreCase(pathSplit[2].toLowerCase())) {
            String fileName = pathSplit[3].toLowerCase();
            ObjectRepresentation<byte[]> or = null;
            try {
                InputStream is = new FileInputStream(this.entityConfig.getMasterFolder() + "tmp" + File.separator + fileName);
                int len;
                int size = 1024;
                byte[] buf;

                if (is instanceof ByteArrayInputStream) {
                    size = is.available();
                    buf = new byte[size];
                    len = is.read(buf, 0, size);
                } else {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    buf = new byte[size];
                    while ((len = is.read(buf, 0, size)) != -1) {
                        bos.write(buf, 0, len);
                    }
                    buf = bos.toByteArray();
                }

                // Close the input stream and return bytes
                is.close();

                // Print the requested URI path
                or = new ObjectRepresentation<byte[]>(buf, MediaType.IMAGE_PNG) {
                    @Override
                    public void write(OutputStream os) throws IOException {
                        super.write(os);
                        os.write(this.getObject());
                    }
                };
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return or;
        } else {
            result = new StringRepresentation("tes cookie get", MediaType.TEXT_PLAIN);
            setStatus(Status.SUCCESS_OK);
            return result;
        }
    }

    @Options
    public void doOptions(Representation entity) {
        Form responseHeaders = (Form) getResponse().getAttributes().get("org.restlet.http.headers");
        if (responseHeaders == null) {
            responseHeaders = new Form();
            getResponse().getAttributes().put("org.restlet.http.headers", responseHeaders);
        }
        responseHeaders.add("Access-Control-Allow-Origin", "*");
        responseHeaders.add("Access-Control-Allow-Methods", "POST,OPTIONS");
        responseHeaders.add("Access-Control-Allow-Headers", "Content-Type");
        responseHeaders.add("Access-Control-Allow-Credentials", "false");
        responseHeaders.add("Access-Control-Max-Age", "60");
    }
}

