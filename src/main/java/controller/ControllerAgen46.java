package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleAgen46;
import interfc.InterfcAgen46;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;

/**
 * @author hasan
 */
public class ControllerAgen46 {

    private final EntityConfig entityConfig;
    private final InterfcAgen46 service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerAgen46() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleAgen46(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getKelurahan(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            resp = this.service.getKelurahan(jsonReq);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getRegister(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        //prepare file upload
        JSONObject jObjDataFile = new JSONObject();
        String fileKtp = jsonReqRaw.get("fileUploadTmp1").toString();
        this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "register_agen46" + File.separator, fileKtp);
        jObjDataFile.put("fileKtp", fileKtp);

        String fileFotoDiri = jsonReqRaw.get("fileUploadTmp2").toString();
        this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "register_agen46" + File.separator, fileFotoDiri);
        jObjDataFile.put("fileFotoDiri", fileFotoDiri);

        jsonReq.put("fileFoto", jObjDataFile);

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String username = jsonReq.get("username").toString();
                String pin = jsonReq.get("PIN").toString();
                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    resp = this.service.getRegister(jsonReq);
                    JSONObject jObjResp = (JSONObject) parser.parse(resp);
                    if (jObjResp.get("ACK").toString().equals("NOK")) {
                        //delete file ktp
                        File file = new File(entityConfig.getMasterFolder() + "uploadFile" + File.separator + "register_agen46" + File.separator + fileKtp);
                        if (file.delete()) {
                            /*code if sucess delete*/
                            System.out.println("file ktp deleted");
                        }
                        //delete file foto diri
                        file = new File(entityConfig.getMasterFolder() + "uploadFile" + File.separator + "register_agen46" + File.separator + fileFotoDiri);
                        if (file.delete()) {
                    /*code if sucess delete*/
                            System.out.println("file foto diri deleted");
                        }
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Rekening TrueMoney Anda Diblokir. Silakan Hubungi Customer Care Kami.");
                    resp = jsonResp.toString();
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
            return jsonResp.toString();
        }
        return resp;
    }

    public String getOtpTarikTunai(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getOtpTarikTunai(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "gagal request otp");
            return jsonResp.toString();
        }
        return resp;
    }

    public String getSetorTunai(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String username = jsonReq.get("username").toString();
                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    resp = this.service.getSetorTunai(jsonReq);
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "gagal transaksi");
            return jsonResp.toString();
        }
        return resp;
    }

    public String checkAkun(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String username = jsonReq.get("username").toString();
                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    resp = this.service.checkAkun(jsonReq);
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "gagal transaksi");
            return jsonResp.toString();
        }
        return resp;
    }

    public String getTarik(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String username = jsonReq.get("username").toString();
                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    resp = this.service.getTarik(jsonReq);
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "gagal transaksi");
            return jsonResp.toString();
        }
        return resp;
    }
}
