package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleAsuransiAllianz;
import interfc.InterfcAgen46;
import interfc.InterfcAsuransiAllianz;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;

/**
 * @author hasan
 */
public class ControllerAsuransiAllianz {

    private final EntityConfig entityConfig;
    private final InterfcAsuransiAllianz service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerAsuransiAllianz() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleAsuransiAllianz(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getKelurahan(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            resp = this.service.getKelurahan(jsonReq);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }


    public String getProduk(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            resp = this.service.getProduk(jsonReq);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        String username = null;
        String ktp = null;
        String pin = null;
        String idtrx = null;
        String idOperator = "226";
        String idTipeTransaksi = "46";
        String idTipeAplikasi = "3";
        String idDenom = "";

        String noHp;
        String hargaCetak;
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                username = jsonReq.get("username").toString();
                noHp = jsonReq.get("noHp").toString();
                hargaCetak = jsonReq.get("produkCetak").toString();
                pin = jsonReq.get("PIN").toString();
                ktp = jsonReq.get("ktp").toString();
                idDenom = jsonReq.get("idDenom").toString();

                int count = this.service.gettrxid(idtrx);
                if (count == 1) {
                    String status[] = this.service.getStatusTrx(idtrx);
                    String m = "";
                    String ack = "";
                    if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.SUKSES))) {
                        m = String.valueOf(Constant.Pesan.SUKSES.getPesan());
                        ack = String.valueOf(Constant.ACK.OK);
                    } else if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.GAGAL))) {
                        m = String.valueOf(Constant.Pesan.GAGAL.getPesan());
                        ack = String.valueOf(Constant.ACK.NOK);
                    } else {
                        status[0] = String.valueOf(Constant.StatusTrx.PENDING);
                        m = String.valueOf(Constant.Pesan.PENDING.getPesan());
                        ack = String.valueOf(Constant.ACK.OK);
                    }
                    jsonResp.put("statusTRX", status[0]);
                    jsonResp.put("ACK", ack);
                    jsonResp.put("pesan", m);
                    return jsonResp.toString();
                } else if (count > 1) {
                    jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", String.valueOf(Constant.Pesan.GAGAL.getPesan()));
                    return jsonResp.toString();
                }

                if (!this.service.CekIsDuplicate(noHp, idOperator, username)) {
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", String.valueOf(Constant.Pesan.VALIDASITRX.getPesan()));
                    return jsonResp.toString();
                }

                //validasi tenor time, the national_id can transaction again during tenor time
//                if(!this.service.checkTenor(ktp, "2")){
//                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
//                    jsonResp.put("pesan", "Anda tidak dapat melakukan transaksi yang sama dalam masa tenor");
//                    return jsonResp.toString();
//                }

                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                int saldo = Integer.parseInt(rekeningAgentNonEDC[1]);

                int[] com = this.service.getComission(Integer.parseInt(idOperator), Integer.parseInt(idDenom));
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    if (pin.equals(rekeningAgentNonEDC[4])) {

                        String validasiSaldo = this.service.isSaldoCukup(username, Integer.parseInt(hargaCetak));
                        if ("00".equalsIgnoreCase(validasiSaldo)) {
                            jsonResp = this.service.prosesBayar(jsonReq, jsonResp, rekeningAgentNonEDC, com);
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Saldo Anda tidak cukup");
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "PIN Anda salah");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Rekening Anda Diblokir. Silakan Hubungi Customer Care Kami.");
                }
            }
        } catch (Exception ex){
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
            return jsonResp.toString();
        }

        resp = jsonResp.toString();
        return resp;
    }
}
