package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleCairkanSaldo;
import implement.ImplePembayaranJastel;
import interfc.InterfcCairkanSaldo;
import interfc.InterfcPembayaranJastel;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author hasan
 */
public class ControllerCairkanSaldo {

    private final EntityConfig entityConfig;
    private final InterfcCairkanSaldo service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private ControllerLog controllerLog;

    public ControllerCairkanSaldo() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleCairkanSaldo(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getInq(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        String noRekening = null;
        String handphone = null;
        String nama = null;
        String namaBank = null;
        String kodeBank = null;
        String ktp = null;
        String alamat = null;
        String sandiDati = null;

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                //get supplier detil
                JSONObject jObjMsSupplier = this.service.getMsSupplier(jsonReq);
                if (!jObjMsSupplier.get("idSupplier").toString().equals("")) {
                    JSONObject jObjCustDetil = this.service.getCustDetil(jsonReq);
                    if (!jObjCustDetil.get("noRekening").toString().equals("")) {
                        noRekening = jObjCustDetil.get("noRekening").toString();
                        handphone = jObjCustDetil.get("handphone").toString();
                        nama = jObjCustDetil.get("nama").toString();
                        namaBank = jObjCustDetil.get("namaBank").toString();
                        kodeBank = jObjCustDetil.get("kodeBank").toString();
                        ktp = jObjCustDetil.get("ktp").toString();
                        alamat = jObjCustDetil.get("alamat").toString();
                        sandiDati = jObjCustDetil.get("sandiDati").toString();
                        boolean aray[] = this.service.getTbConfig(jsonReq.get("username").toString(), jsonReq.get("nominal").toString());
                        boolean MAX_COUNT_DAY = aray[0];
                        boolean MAX_DAY = aray[1];
                        boolean MAX_TRX = aray[2];
                        boolean MIN_TRX = aray[3];
                        boolean IS_SAMSUNG = aray[4];
                        String config[] = new String[4];
                        int[] fee = new int[4];
                        int BiayaCetak = 0;
                        if (IS_SAMSUNG) {
                            config = this.service.getConfigCairkanSaldo(jsonReq.get("username").toString(), "SAMSUNG");
                            fee = this.service.getFee(2, 30, 0, Integer.parseInt(jsonReq.get("nominal").toString()), "samsung");
                            BiayaCetak = fee[0];
                        } else {
                            config = this.service.getConfigCairkanSaldo(jsonReq.get("username").toString(), "NON_SAMSUNG");
                            fee = this.service.getFee(2, 30, 0, Integer.parseInt(jsonReq.get("nominal").toString()), "biasa");
                            BiayaCetak = fee[0];
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Untuk sementara transaksi tidak dapat dilakukan");
                            return jsonResp.toString();
                        }
                        String max_count_day = config[0];
                        String max_day = config[1];
                        String max_trx = config[2];
                        String min_trx = config[3];

                        Locale locale = new Locale("id", "ID");
                        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
                        String moneyString;
                        if (MIN_TRX) {
                            if (MAX_TRX) {
                                if (MAX_COUNT_DAY) {
                                    if (MAX_DAY) {
                                        if (noRekening != null && !noRekening.isEmpty()) {
                                            if (this.service.cekNorek(noRekening, kodeBank, "agent")) {
                                                if (handphone != null && !handphone.isEmpty()
                                                        && nama != null && !nama.isEmpty()
                                                        && namaBank != null && !namaBank.isEmpty()
                                                        && kodeBank != null && !kodeBank.isEmpty()
                                                        && ktp != null && !ktp.isEmpty()
                                                        && alamat != null && !alamat.isEmpty()
                                                        && sandiDati != null && !sandiDati.isEmpty()) {
                                                    String date = utilities.getDate("yyyy-MM-dd HH:mm:ss");

                                                    String trxid = "TMN" + utilities.createTrxid();
                                                    String Signature = utilities.getSignature(noRekening + "witamidevel" + trxid + kodeBank);

                                                    // DO Inquiry Process
                                                    JSONObject ThirdParty = new JSONObject();
                                                    ThirdParty.put("type", "INQUIRY");
                                                    ThirdParty.put("TrxId", trxid);
                                                    ThirdParty.put("SenderAccount", jsonReq.get("username").toString());
                                                    ThirdParty.put("SenderName", nama.length() < 30 ? nama : nama.substring(0, 30));
                                                    ThirdParty.put("BeneficiaryAccount", noRekening);
                                                    ThirdParty.put("BeneficiaryName", nama.length() < 30 ? nama : nama.substring(0, 30));
                                                    ThirdParty.put("Description", "Cairkan saldo");
                                                    ThirdParty.put("Amount", jsonReq.get("nominal").toString());
                                                    ThirdParty.put("ReceiverBankCode", kodeBank);
                                                    ThirdParty.put("AccountId", "witamidevel");
                                                    ThirdParty.put("datetime", date);
                                                    ThirdParty.put("Signature", Signature);
                                                    ThirdParty.put("SenderSeluler", handphone);

                                                    ThirdParty.put("SenderKtp", ktp);
                                                    ThirdParty.put("SenderAddress", alamat);
                                                    ThirdParty.put("BeneficiaryCity", sandiDati);
                                                    ThirdParty.put("SenderCity", sandiDati);

                                                    //insert log
                                                    int idLog = utilities.insertLog(trxid, ThirdParty);
                                                    String inquiry = this.service.prosesInquiry(ThirdParty.toString(), jObjMsSupplier);
                                                    //updateLog
                                                    utilities.updateLog(idLog, inquiry);
        /*String inquiry = "{\"ResponseCode\":\"200\", \"ACK\":\"OK\",\"MSG\":\"000000~~Approved\", "
        + "\"ResponseSourceName\":\"PT WITAMI TUNAI MANDIRI\", \"ResponseDestinationAcc\":\"0580767842\","
        + "\"ResponseDestinationName\":\"LIBRI MONICA PRICILLA\",\"ResponseDestinationIdBank\":\"014\","
        + "\"ResponseDestinationBankName\":\"BCA\",\"ResponseTerminalId\":\"1R3131382E3926\","
        + "\"ResponseMsgKey\":\"211c50da\"}";*/

                                                    JSONParser jsonParser = new JSONParser();
                                                    JSONObject jsonObject = (JSONObject) jsonParser.parse(inquiry);
                                                    String httpStat = jsonObject.get("ResponseCode").toString();
                                                    if (httpStat.equals("200")) {
                                                        String ACK = (String) jsonObject.get("ACK");
                                                        if (ACK.equals("OK") || ACK.equals("SUSPECT")) {
                                                            String namaTujuan = jsonObject.get("ResponseDestinationName").toString();
                                                            String messageKey = jsonObject.get("ResponseMsgKey").toString();
                                                            String bank = jsonObject.get("ResponseDestinationBankName").toString();
                                                            noRekening = jsonObject.get("ResponseDestinationAcc").toString();
                                                            String status = "Approved";

                                                            int harga_cetak = BiayaCetak + Integer.parseInt(jsonReq.get("nominal").toString());
                                                            double dbl = Double.parseDouble(String.valueOf(harga_cetak));
                                                            String dblStr = String.format("%,.0f", dbl);
                                                            dblStr = dblStr.replaceAll(",", ".");
                                                            jsonResp.put("hargaCetak", dblStr);

                                                            dbl = Double.parseDouble(String.valueOf(BiayaCetak));
                                                            dblStr = String.format("%,.0f", dbl);
                                                            dblStr = dblStr.replaceAll(",", ".");
                                                            jsonResp.put("biayaTransferBank", dblStr);

                                                            dbl = Double.parseDouble(jsonReq.get("nominal").toString());
                                                            dblStr = String.format("%,.0f", dbl);
                                                            dblStr = dblStr.replaceAll(",", ".");
                                                            jsonResp.put("nominal", dblStr);

                                                            jsonResp.put("noRekening", noRekening);
                                                            jsonResp.put("handphone", handphone);
                                                            jsonResp.put("nama", nama.length() < 30 ? namaTujuan : namaTujuan.substring(0, 30));
                                                            jsonResp.put("namaTujuan", namaTujuan);
                                                            jsonResp.put("namaBank", bank);
                                                            jsonResp.put("kodeBank", kodeBank);
                                                            jsonResp.put("messageKey", messageKey);
                                                            jsonResp.put("ktp", ktp);
                                                            jsonResp.put("alamat", alamat);
                                                            jsonResp.put("sandiDati", sandiDati);
                                                            jsonResp.put("ACK", "OK");

                                                        } else {
                                                            jsonResp.put("ACK", "NOK");
                                                            jsonResp.put("pesan", jsonObject.get("MSG"));
                                                        }
                                                    } else {
                                                        jsonResp.put("ACK", "NOK");
                                                        jsonResp.put("pesan", jsonObject.get("MSG"));
                                                    }
                                                } else {
                                                    jsonResp.put("ACK", "NOK");
                                                    jsonResp.put("pesan", "Silakan hubungi Customer Service Truemoney untuk melengkapi informasi Nama lengkap, Nomor KTP, Tipe Bank dan Nomor Rekening Tujuan Anda untuk dapat menggunakan fitur Cairkan Saldo");
                                                }
                                            } else {
                                                jsonResp.put("ACK", "NOK");
                                                jsonResp.put("pesan", utilities.getWording("ERROR_BLACKLIST_CAIRKANSALDO"));
                                            }
                                        } else {
                                            jsonResp.put("ACK", "NOK");
                                            jsonResp.put("pesan", "Silakan hubungi Customer Service Truemoney untuk melengkapi informasi Nama lengkap, Alamat, Nomor KTP, Tipe Bank dan Nomor Rekening Tujuan Anda untuk dapat menggunakan fitur Cairkan Saldo");
                                        }
                                    } else {
                                        moneyString = formatter.format(Integer.parseInt(max_day));
                                        jsonResp.put("ACK", "NOK");
                                        jsonResp.put("pesan", "Anda sudah mencapai limit " + moneyString + " per hari");
                                    }
                                } else {
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan",
                                            "Anda sudah mencapai limit " + max_count_day + "x transaksi cairkan saldo per hari");
                                }
                            } else {
                                moneyString = formatter.format(Integer.parseInt(max_trx));
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Maksimal nominal per transaksi " + moneyString);
                            }
                        } else {
                            moneyString = formatter.format(Integer.parseInt(min_trx));
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Minimal nominal per transaksi " + moneyString);
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Silakan hubungi Customer Service Truemoney untuk melengkapi informasi Nama lengkap, Alamat, Nomor KTP, Tipe Bank dan Nomor Rekening Tujuan Anda untuk dapat menggunakan fitur Cairkan Saldo");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Maaf! Untuk saat ini layanan tidak tersedia");
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Maaf! Untuk saat ini layanan tidak tersedia");
        }

        return jsonResp.toString();
    }

    public String getPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();

        int id_tipetransaksi = 6;
        String NoAgen = null;
        int Nominal = 0;
        String Kode_Bank = null;
        String NORek = null;
        String verifikasi = "PIN";
        String SenderName = null;
        String namaPenerima = null;
        String nohp = null;
        int BiayaTransferBank = 0;
        int Hargacetak = 0;
        String PIN = null;
        String gen_id = null;

        String keteranganPenerima = null;
        String ktp = null;
        String alamat = null;
        String sandiDati = null;

        try {
            NoAgen = jsonReq.get("username").toString();
            Nominal = Integer.parseInt(jsonReq.get("nominal").toString());
            Kode_Bank = jsonReq.get("kodeBank").toString();
            NORek = jsonReq.get("nomorRekening").toString();
            SenderName = jsonReq.get("senderName").toString();
            namaPenerima = jsonReq.get("namaPenerima").toString();
            nohp = jsonReq.get("noHPPengirim").toString();
            BiayaTransferBank = Integer.parseInt(jsonReq.get("biayaTransferBank").toString());
            Hargacetak = Integer.parseInt(jsonReq.get("hargaCetak").toString());
            PIN = jsonReq.get("PIN").toString();
            gen_id = "TMN" + jsonReq.get("idtrx").toString();

            keteranganPenerima = jsonReq.containsKey("keteranganPenerima") ? jsonReq.get("keteranganPenerima").toString() : "Cairkan saldo";
            if (keteranganPenerima.isEmpty()) {
                keteranganPenerima = "Cairkan saldo";
            }

            ktp = jsonReq.containsKey("ktp") ? jsonReq.get("ktp").toString() : "999999999";
            alamat = jsonReq.containsKey("alamat") ? jsonReq.get("alamat").toString() : "JAKARTA";
            sandiDati = jsonReq.containsKey("sandiDati") ? jsonReq.get("sandiDati").toString() : "0300";
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                int count = this.service.getTrxId(gen_id);
                if (count == 1) {
                    String status[] = this.service.getStatusTrx(gen_id);
                    String m = "";
                    String ack = "";
                    if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.SUKSES))) {
                        m = "Transaksi Anda sukses";
                        ack = String.valueOf(Constant.ACK.OK);
                    } else if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.GAGAL))) {
                        m = "Transaksi Anda gagal";
                        ack = String.valueOf(Constant.ACK.NOK);
                    } else {
                        status[0] = String.valueOf(Constant.StatusTrx.PENDING);
                        m = "Transaksi Anda sedang diproses";
                        ack = String.valueOf(Constant.ACK.OK);
                    }
                    jsonResp.put("statusTRX", status[0]);
                    jsonResp.put("ACK", ack);
                    jsonResp.put("pesan", m);
                    return jsonResp.toString();
                } else if (count > 1) {
                    jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", "Transaksi Anda gagal");
                    return jsonResp.toString();
                }

                String id_agent = NoAgen;
                JSONObject jObjMsSupplier = this.service.getMsSupplier(jsonReq);
                int fee[] = this.service.getFee(3, 6, 36, 0, "biasa");
                if (!this.service.cekIsEmptyTransfer(NORek, Nominal, NoAgen)) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Anda tidak dapat melakukan cairkan saldo dengan nominal yang sama, silakan coba 24 jam kemudian");
                    return jsonResp.toString();
                }
                if(this.service.cekNorek(NORek, Kode_Bank, "agent")){
                    resp = this.service.prosesBayar(jsonReq, fee, jObjMsSupplier);
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Missing parameter");
            resp = jsonResp.toString();
        }
        return resp;
    }
}
