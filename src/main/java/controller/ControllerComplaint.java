package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.Helper;
import helper.Utilities;
import implement.ImpleComplaint;
import interfc.InterfcComplaint;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author achmad
 */
public class ControllerComplaint {

    private InterfcComplaint service;
    private final EntityConfig entityConfig;
    private ControllerLog controllerLog;
    private final Cache cacheConfig;

    private final Helper hlp;
    private final Utilities utilities;

    public ControllerComplaint(){
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleComplaint(this.entityConfig);
        this.controllerLog = new ControllerLog();
        this.hlp = new Helper();
        this.utilities = new Utilities();
    }

    public String getTipeTransaksi(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        try {
            //---------------------process message-------------------------------
            resp = this.service.getListTipeTransaksi();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String submitComplaint(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String tipeTransaksi = jsonReq.get("tipeTransaksi").toString();
                String tingkatKeluhan = jsonReq.get("tingkatKeluhan").toString();
                String keluhan = jsonReq.get("keluhan").toString();
                String idAccount = jsonReq.get("idAccount").toString();

                JSONObject request = new JSONObject();
                request.put("tipeTransaksi", tipeTransaksi);
                request.put("tingkatKeluhan", tingkatKeluhan);
                request.put("keluhan", keluhan);
                request.put("idAccount", idAccount);
                return service.saveComplaint(request);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resp;
    }
}
