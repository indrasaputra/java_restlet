package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.CookieHelper;
import helper.FcmHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleLogin;
import interfc.InterfcLogin;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author hasan
 */
public class ControllerFcm {

    private final EntityConfig entityConfig;
    private final InterfcLogin service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;
    private final FcmHelper fcmHelper;

    public ControllerFcm() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleLogin(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
        this.fcmHelper = new FcmHelper();
    }

    public String sentMessage(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            //this is should fcm message code
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString ( ex ) ;
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }
}
