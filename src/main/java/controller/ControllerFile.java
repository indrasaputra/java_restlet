/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import interfc.InterfcFile;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import helper.Helper;
import implement.ImpleFile;
import org.json.simple.JSONArray;

/**
 *
 * @author DPPH
 */
public class ControllerFile {

    private final EntityConfig entityConfig;
    InterfcFile service;
    Cache cacheConfig;
    ControllerEncDec encDec;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    Cache cacheResponse;
//    private final ControllerLog ctrlLog;
    Helper hlp;
    JSONArray jsonArray;

    public ControllerFile() {
        //get config in cacheConfig
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("PERSPEBSI");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleFile(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
//        this.ctrlLog = new ControllerLog();
        this.hlp = new Helper();
    }

    public String saveFile(String jsonMessageEncripted) {
        String encStr = null;
        String createId = this.hlp.createID();
        try {
            //every '+' sign always replace with space, cause of that we must resplace again with '+' 
//            String jsonMessageEnc = jsonMessageEncripted.replace(" ", "+");
//            jsonMessageEnc = jsonMessageEncripted.replace("-", "+");
//            jsonMessageEnc = jsonMessageEncripted.replace("_", "/");
//            jsonMessageEnc = jsonMessageEncripted.replace(",", "=");
//            byte[] decodedBytes = Base64.decodeBase64(jsonMessageEnc);
            //----------------------parsing the json message-------------------------------
//            String queryJson = this.encDec.getDecrypt(decodedBytes);
            //log upline/client request
//            if (this.entityConfig.getLogStream().equals("true")) {
//                this.ctrlLog.logStremWriter(createId + "-> Req : Client Address/Source Port = " + address + '/' + port + "; Agent = " + agent + "; Parameter getAdsFileType = " + queryJson);
//            }
//            Object objJson;
//            objJson = this.parser.parse(queryJson);
//            this.jsonObject = (JSONObject) objJson;
//            String ext = (String) this.jsonObject.get("ext");
            //find in cacheConfig
//            Element cacheQuery = this.cacheResponse.get(id);
            //---------------------process message-------------------------------
            encStr = this.service.saveFile(this.entityConfig, jsonMessageEncripted); // if date null jgn di panggil
//                encStr = this.serviceQuery.getAdsFileType(this.entityConfig, queryStr);
            //------------------------construct json response---------------------
            byte[] byteData = null;
            byte[] encryptByte = null;
//            if (this.entityConfig.getLogStream().equals("true")) {
//                this.ctrlLog.logStremWriter(createId + "-> Resp : " + encStr + ";");
//            }
            encryptByte = this.encDec.getEncrypt(encStr);
            encStr = new String(Base64.encodeBase64(encryptByte));
            encStr = encStr.replace("+", "-");
            encStr = encStr.replace("/", "_");
            encStr = encStr.replace("=", ",");
            //save to cacheConfig
//            this.cacheResponse.put(new Element(id, encStr));
        } catch (Exception ex) {
            Logger.getLogger(ControllerFile.class.getName()).log(Level.SEVERE, null, ex);
//            this.ctrlLog.logErrorWriter(ex.toString());
        }
        return encStr;
    }
}
