package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleKudoGrab;
import interfc.InterfcKudoGrab;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;

/**
 * @author hasan
 */
public class ControllerKudoGrab {

    private final EntityConfig entityConfig;
    private final InterfcKudoGrab service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerKudoGrab() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleKudoGrab(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getKotaKudo(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getKota(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Pilih Kota");
            return jsonResp.toString();
        }
        return resp;
    }

    public String getBankKudo(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getBank(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Pilih Bank");
            return jsonResp.toString();
        }
        return resp;
    }

    public String customerProfileList(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.customerProfileList(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal get profile list");
            return jsonResp.toString();
        }
        return resp;
    }

    public String createApplication(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.createApplication(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal create Application");
            return jsonResp.toString();
        }
        return resp;
    }

    public String phoneNumberVerification(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.phoneNumberVerification(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal phone Number Verification");
            return jsonResp.toString();
        }
        return resp;
    }

    public String resendPhoneNumberVerification(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.resendPhoneNumberVerification(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal phone Number Verification");
            return jsonResp.toString();
        }
        return resp;
    }

    public String isiKtp(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.isiKtp(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Isi KTP");
            return jsonResp.toString();
        }
        return resp;
    }

    public String isiSim(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.isiSim(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Isi SIM");
            return jsonResp.toString();
        }
        return resp;
    }

    public String isiStnk(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.isiStnk(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Isi STNK");
            return jsonResp.toString();
        }
        return resp;
    }

    public String isiDeclaration(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.isiDeclaration(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Isi Pernyataan");
            return jsonResp.toString();
        }
        return resp;
    }

    public String isiBank(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.isiBank(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Isi Buku Tabungan");
            return jsonResp.toString();
        }
        return resp;
    }

    public String uploadFileGrab(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        String tType = jsonReq.get("type_file").toString();
        String files = jsonReqRaw.get("fileUploadTmp1").toString();
        //prepare file upload
        System.out.println("WorkDir : " + entityConfig.getWorkDir());
        System.out.println("MasterFolder : " + entityConfig.getMasterFolder());
        if (tType.equals("FOTO")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-profile");
        } else if (tType.equals("KTP")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-identity-card");
        } else if (tType.equals("SIM")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-driving-license");
        } else if (tType.equals("STNK")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-vehicle-registration");
        } else if (tType.equals("SKCK")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-police-clearance");
        } else if (tType.equals("SPK")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-rental-agreement");
        } else if (tType.equals("BANK")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-bank-account");
        } else if (tType.equals("FOTONKTP")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-photo-with-identity");
        } else if (tType.equals("FOTONKENDARAAN")) {
            this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator, files);
            jsonReq.put("file", files);
            jsonReq.put("endpoint", "upload-photo-with-vehicle");
        }

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.uploadFileKudoGrab(jsonReq);
                JSONObject jObjResp = (JSONObject) parser.parse(resp);
                if (jObjResp.get("ACK").toString().equals("NOK")) {
                    //delete file ktp
                    File file = new File(entityConfig.getMasterFolder() + "uploadFile" + File.separator + "kudograb" + File.separator + files);
                    if (file.delete()) {
                        /*code if sucess delete*/
                        controllerLog.logStreamWriter("file " + tType + "deleted");
                    }
                }

            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Upload File " + tType);
            return jsonResp.toString();
        }
        return resp;
    }

    public String completeData(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.completeData(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Complete Data");
            return jsonResp.toString();
        }
        return resp;
    }

    public String checkStatusData(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.checkStatusData(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Gagal Check Status Data");
            return jsonResp.toString();
        }
        return resp;
    }
//    public String getRegister(JSONObject jsonReqRaw, String cookieClient) {
//        String resp = null;
//        JSONParser parser = new JSONParser();
//        JSONObject jsonResp = new JSONObject();
//        jsonResp.put("ACK", "NOK");
//
//        String createId = this.hlp.createID();
//        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
//        //prepare file upload
//        JSONObject jObjDataFile = new JSONObject();
//        String fileKtp = jsonReqRaw.get("fileUploadTmp1").toString();
//        this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "register_agen46" + File.separator, fileKtp);
//        jObjDataFile.put("fileKtp", fileKtp);
//
//        String fileFotoDiri = jsonReqRaw.get("fileUploadTmp2").toString();
//        this.utilities.moveFile(entityConfig.getWorkDir() + "uploadFile" + File.separator + "register_agen46" + File.separator, fileFotoDiri);
//        jObjDataFile.put("fileFotoDiri", fileFotoDiri);
//
//        jsonReq.put("fileFoto", jObjDataFile);
//
//        try {
//            //---------------------process message-------------------------------
//            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
//            if (jsonResp.get("ACK").equals("OK")) {
//                String username = jsonReq.get("username").toString();
//                String pin = jsonReq.get("PIN").toString();
//                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
//                String statusRekening = rekeningAgentNonEDC[0];
//                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
//                    resp = this.service.getRegister(jsonReq);
//                    JSONObject jObjResp = (JSONObject) parser.parse(resp);
//                    if (jObjResp.get("ACK").toString().equals("NOK")) {
//                        //delete file ktp
//                        File file = new File(entityConfig.getMasterFolder() + "uploadFile" + File.separator + "register_agen46" + File.separator + fileKtp);
//                        if (file.delete()) {
//                            /*code if sucess delete*/
//                            System.out.println("file ktp deleted");
//                        }
//                        //delete file foto diri
//                        file = new File(entityConfig.getMasterFolder() + "uploadFile" + File.separator + "register_agen46" + File.separator + fileFotoDiri);
//                        if (file.delete()) {
//                    /*code if sucess delete*/
//                            System.out.println("file foto diri deleted");
//                        }
//                    }
//                } else {
//                    jsonResp.put("ACK", "NOK");
//                    jsonResp.put("pesan", "Rekening TrueMoney Anda Diblokir. Silakan Hubungi Customer Care Kami.");
//                    resp = jsonResp.toString();
//                }
//            }
//        } catch (Exception ex) {
//            String s = Throwables.getStackTraceAsString(ex);
//            controllerLog.logErrorWriter(s);
//            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
//            jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
//            return jsonResp.toString();
//        }
//        return resp;
//    }
//
//    public String getOtpTarikTunai(JSONObject jsonReqRaw, String cookieClient) {
//        String resp = null;
//        JSONParser parser = new JSONParser();
//        JSONObject jsonResp = new JSONObject();
//        jsonResp.put("ACK", "NOK");
//
//        String createId = this.hlp.createID();
//        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
//        try {
//            //---------------------process message-------------------------------
//            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
//            if (jsonResp.get("ACK").equals("OK")) {
//                resp = this.service.getKota(jsonReq);
//            } else {
//                resp = jsonResp.toString();
//            }
//        } catch (Exception ex) {
//            String s = Throwables.getStackTraceAsString(ex);
//            controllerLog.logErrorWriter(s);
//            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
//            jsonResp.put("pesan", "gagal request otp");
//            return jsonResp.toString();
//        }
//        return resp;
//    }
//
//    public String getSetorTunai(JSONObject jsonReqRaw, String cookieClient) {
//        String resp = null;
//        JSONParser parser = new JSONParser();
//        JSONObject jsonResp = new JSONObject();
//        jsonResp.put("ACK", "NOK");
//
//        String createId = this.hlp.createID();
//        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
//        try {
//            //---------------------process message-------------------------------
//            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
//            if (jsonResp.get("ACK").equals("OK")) {
//                String username = jsonReq.get("username").toString();
//                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
//                String statusRekening = rekeningAgentNonEDC[0];
//                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
//                    resp = this.service.getSetorTunai(jsonReq);
//                }
//            } else {
//                resp = jsonResp.toString();
//            }
//        } catch (Exception ex) {
//            String s = Throwables.getStackTraceAsString(ex);
//            controllerLog.logErrorWriter(s);
//            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
//            jsonResp.put("pesan", "gagal transaksi");
//            return jsonResp.toString();
//        }
//        return resp;
//    }
//
//    public String checkAkun(JSONObject jsonReqRaw, String cookieClient) {
//        String resp = null;
//        JSONParser parser = new JSONParser();
//        JSONObject jsonResp = new JSONObject();
//        jsonResp.put("ACK", "NOK");
//
//        String createId = this.hlp.createID();
//        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
//        try {
//            //---------------------process message-------------------------------
//            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
//            if (jsonResp.get("ACK").equals("OK")) {
//                String username = jsonReq.get("username").toString();
//                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
//                String statusRekening = rekeningAgentNonEDC[0];
//                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
//                    resp = this.service.checkAkun(jsonReq);
//                }
//            } else {
//                resp = jsonResp.toString();
//            }
//        } catch (Exception ex) {
//            String s = Throwables.getStackTraceAsString(ex);
//            controllerLog.logErrorWriter(s);
//            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
//            jsonResp.put("pesan", "gagal transaksi");
//            return jsonResp.toString();
//        }
//        return resp;
//    }
//
//    public String getTarik(JSONObject jsonReqRaw, String cookieClient) {
//        String resp = null;
//        JSONParser parser = new JSONParser();
//        JSONObject jsonResp = new JSONObject();
//        jsonResp.put("ACK", "NOK");
//
//        String createId = this.hlp.createID();
//        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
//        try {
//            //---------------------process message-------------------------------
//            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
//            if (jsonResp.get("ACK").equals("OK")) {
//                String username = jsonReq.get("username").toString();
//                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
//                String statusRekening = rekeningAgentNonEDC[0];
//                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
//                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
//                    resp = this.service.getTarik(jsonReq);
//                }
//            } else {
//                resp = jsonResp.toString();
//            }
//        } catch (Exception ex) {
//            String s = Throwables.getStackTraceAsString(ex);
//            controllerLog.logErrorWriter(s);
//            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
//            jsonResp.put("pesan", "gagal transaksi");
//            return jsonResp.toString();
//        }
//        return resp;
//    }
}
