package controller;

import com.google.common.base.Throwables;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import entity.EntityConfig;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleLogin;
import interfc.InterfcLogin;

/**
 *
 * @author hasan
 */
public class ControllerLogin {

    private final EntityConfig entityConfig;
    private final InterfcLogin service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerLogin() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleLogin(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String signInToken(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            resp = this.service.getSignInToken(jsonReq);
        } catch (Exception ex) {
            //Logger.getLogger(ControllerLogin.class.getName()).log(Level.SEVERE, null, ex);
            String s = Throwables.getStackTraceAsString ( ex ) ;
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getOtp(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            //check android version
            jsonResp = this.utilities.checkVersion(jsonReq);
            //bypass check version
            jsonResp.put("ACK", "OK");
            jsonResp.put("pesan", "");
            if(jsonResp.get("ACK").toString().equals("OK")){
                //change device info
                jsonResp = this.service.requestToken(jsonReq);
                if(jsonResp.get("ACK").toString().equalsIgnoreCase("OK")){
                    jsonResp.put("pesan", this.utilities.getWording("NOTIF_SEND_TOKEN"));
                    //save otp to session
                    this.cookieHelper.addDataCookie(cookieClient, "otp", jsonResp.get("otp").toString());
                    jsonResp.put("otp", "");
                }
                resp = jsonResp.toString();
            } else{
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString ( ex ) ;
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String blockAccount(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            jsonReq.put("remark", "Blocked by android tmw [SALAH OTP : " + this.utilities.getDate() + "]");
            //---------------------process message-------------------------------
            this.service.blockAccount(jsonReq);
            jsonResp.put("pesan", this.utilities.getWording("OTP_NOK_3_TIMES"));
            jsonResp.put("ACK", "OK");
            resp = jsonResp.toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString ( ex ) ;
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String changeDevice(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            String jsonRespStr = this.verifyOtp(jsonReqRaw, cookieClient);
            jsonResp = (JSONObject) parser.parse(jsonRespStr);
            if(jsonResp.get("ACK").toString().equals("OK")){
                jsonResp = this.service.changeDevice(jsonReq);
            }
            resp = jsonResp.toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString ( ex ) ;
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String setFcmToken(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "OK");
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            this.service.setFcmToken(jsonReq);
            resp = jsonResp.toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString ( ex ) ;
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String verifyOtp(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "OK");
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            JSONObject jObjDataCookie = this.cookieHelper.getDataCookie(cookieClient);
            if(jObjDataCookie.containsKey("otp")) {
                String otp = jObjDataCookie.get("otp").toString();
                if (otp.equals(jsonReq.get("otp").toString())) {
                    //valid
                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "valid otp");
                } else {
                    //not valid
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "not valid otp");
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "internal error");
            }
            resp = jsonResp.toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString ( ex ) ;
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }
}
