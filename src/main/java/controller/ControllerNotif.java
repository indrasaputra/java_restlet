package controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleNotif;
import interfc.InterfcNotif;

/**
 * @author hasan
 */
public class ControllerNotif {

    private final EntityConfig entityConfig;
    private final InterfcNotif service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;

    public ControllerNotif() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleNotif(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
    }

    public String getNotif(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            JSONObject jObjDataCookie = this.cookieHelper.getDataCookie(cookieClient);
            if (jObjDataCookie.containsKey("hasLogin")) {
                if (jObjDataCookie.get("hasLogin").toString().equals("true")) {
                    jsonResp = this.utilities.checkDeviceAgent(jsonReq);
                    if (jsonResp.get("ACK").equals("OK")) {
                        resp = this.service.getNotif(jsonReq);
                    } else {
                        resp = jsonResp.toString();
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "session expire or not login");
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "session expire or not login");
            }
        } catch (Exception ex) {
            Logger.getLogger(ControllerNotif.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resp;
    }
}
