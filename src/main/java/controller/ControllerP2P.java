package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.CookieHelper;
import helper.FcmHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleP2P;
import interfc.InterfcP2P;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author hasan
 */
public class ControllerP2P {

    private final EntityConfig entityConfig;
    private final InterfcP2P service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private ControllerLog controllerLog;

    public ControllerP2P() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleP2P(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getInq(JSONObject jsonReqRaw, String cookieClient) {
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                if (!jsonReq.get("agenAccountPenerima").toString().equals(jsonReq.get("username").toString())) {
                    JSONObject jObjComnision = this.service.getCommision();
                    JSONArray jArrInfoFee = (JSONArray) jObjComnision.get("infoMsFee");
                    JSONObject jObjInfoFee = (JSONObject) jArrInfoFee.get(0);
                    boolean cukup = this.service.checkBalance(jsonReq, jObjInfoFee);
                    if (cukup) {
                        //validate nominal
                        String[] a = {"250.000", "350.000", "500.000", "1.000.000", "1.500.000"};
                        boolean whiteListNominal = false;
                        for (String s : a) {
                            if (s.replace(".", "").equals(jsonReq.get("nominal").toString())) {
                                whiteListNominal = true;
                            }
                        }
                        if ((Integer.signum(Integer.parseInt(jsonReq.get("nominal").toString().replace(".", ""))) > 0) && whiteListNominal) {
                            jsonResp = this.service.getInfoAgent(jsonReq.get("agenAccountPenerima").toString());
                            if (jsonResp.get("ACK").equals("OK")) {
                                JSONObject jsonRespTemp = new JSONObject();
                                JSONArray jArrInfoAgent = (JSONArray) jsonResp.get("infoAgent");
                                JSONObject jObjInfoAgent = (JSONObject) jArrInfoAgent.get(0);

                                jsonRespTemp.put("idAgentAccount", jObjInfoAgent.get("id_agentaccount").toString());
                                jsonRespTemp.put("nama", jObjInfoAgent.get("Nama").toString());
                                jsonRespTemp.put("alamat", jObjInfoAgent.get("Alamat").toString());
                                jsonRespTemp.put("nominal", jsonReq.get("nominal").toString());
                                jsonRespTemp.put("fee", jObjInfoFee.get("BiayaAdmin").toString());
                                long totalBayar = Long.parseLong(jsonReq.get("nominal").toString().replace(".", "")) + Long.parseLong(jObjInfoFee.get("BiayaAdmin").toString().replace(".", ""));
                                jsonRespTemp.put("totalBayar", String.valueOf(totalBayar));
                                jsonRespTemp.put("ACK", "OK");

                                //save amount to cookies
                                JSONObject jsonObjectCookies = new JSONObject();
                                jsonObjectCookies.put("nominalTransfer", jsonReq.get("nominal").toString());
                                jsonObjectCookies.put("feeTransfer", jObjInfoFee.get("BiayaAdmin").toString());
                                jsonObjectCookies.put("totalTransfer", String.valueOf(totalBayar));
                                jsonObjectCookies.put("idAgentAccount", jsonReq.get("username").toString());
                                jsonObjectCookies.put("idAgentAccountPenerima", jObjInfoAgent.get("id_agentaccount").toString());
                                jsonObjectCookies.put("namaAgentPenerima", jObjInfoAgent.get("Nama").toString());
                                jsonObjectCookies.put("fcmTokenAgenPenerima", jObjInfoAgent.get("gcm_token").toString());
                                jsonObjectCookies.put("idFeeSupplier", jObjInfoFee.get("id_feesupplier").toString());
                                jsonObjectCookies.put("idFee", jObjInfoFee.get("id_fee").toString());
                                jsonObjectCookies.put("idOperator", jObjInfoFee.get("id_operator").toString());
                                jsonObjectCookies.put("idSupplier", jObjInfoFee.get("id_supplier").toString());
                                jsonObjectCookies.put("idDenom", jObjInfoFee.get("id_denom").toString());
                                jsonObjectCookies.put("idTipeTransaksi", jObjInfoFee.get("id_tipetransaksi").toString());
                                jsonObjectCookies.put("feeAgent", jObjInfoFee.get("AgentCommNom").toString());
                                jsonObjectCookies.put("feeTrue", jObjInfoFee.get("TrueFeeNom").toString());
                                //get info agent pengirim
                                jsonResp = this.service.getInfoAgent(jsonReq.get("username").toString());
                                jArrInfoAgent = (JSONArray) jsonResp.get("infoAgent");
                                jObjInfoAgent = (JSONObject) jArrInfoAgent.get(0);
                                jsonObjectCookies.put("fcmTokenAgenPengirim", jObjInfoAgent.get("gcm_token").toString());

                                this.cookieHelper.addDataCookie(cookieClient, "agentToAgentData", jsonObjectCookies.toString());
                                jsonResp = jsonRespTemp;
                            } else {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Id Agen Account tidak ditemukan!");
                            }
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Maaf, transaksi tidak dapat dilakukan!");
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Saldo anda tidak cukup!");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Pengirim dan penerima tidak boleh sama!");
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Maaf! Untuk saat ini layanan tidak tersedia");
        }

        return jsonResp.toString();
    }

    public String getPay(JSONObject jsonReqRaw, String cookieClient) {
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                JSONObject jObjCookies = this.cookieHelper.getDataCookie(cookieClient);
                String stringAgentToAgentData = jObjCookies.get("agentToAgentData").toString();
                final JSONObject jObjAgentToAgentData = (JSONObject) jSONParser.parse(stringAgentToAgentData);

                jsonResp = this.service.getInfoAgent(jObjAgentToAgentData.get("idAgentAccount").toString());
                JSONArray jArrInfoAgent = (JSONArray) jsonResp.get("infoAgent");
                JSONObject jObjInfoAgent = (JSONObject) jArrInfoAgent.get(0);
                String pin = this.encDec.decryptDbVal(jObjInfoAgent.get("PIN").toString(), this.entityConfig.getEncKeyDb());
                if (pin.equals(jsonReq.get("PIN").toString())) {
                    //deduct balance
                    long lastBalanceAfterDeductNominal = this.service.deductBalance(jObjAgentToAgentData.get("idAgentAccount").toString(), jObjAgentToAgentData.get("nominalTransfer").toString().replace(".", ""));
                    //insert tr_transaksi
                    String idTransaksi = this.service.insertTrTransaksi(jsonReq, jObjAgentToAgentData);
                    //insert tr_stock
                    this.service.insertTrStockOpen(idTransaksi, lastBalanceAfterDeductNominal, jObjAgentToAgentData.get("nominalTransfer").toString(), jsonReq, jObjAgentToAgentData);
                    //deduct agent Fee
                    long lastBalanceAfterDeductFee = this.service.deductBalance(jObjAgentToAgentData.get("idAgentAccount").toString(), jObjAgentToAgentData.get("feeTransfer").toString().replace(".", ""));
                    //insert tr_stock
                    this.service.insertTrStockOpen(idTransaksi, lastBalanceAfterDeductFee, jObjAgentToAgentData.get("feeTransfer").toString(), jsonReq, jObjAgentToAgentData);

                    //transfer transaction
                    long lastBalanceAfterTransaction = this.service.transfer(jsonReq, jObjAgentToAgentData);

                    //update tr_transaksi
                    this.service.updateTransaksi(idTransaksi, jsonReq, jObjAgentToAgentData);
                    //insert tr_stock
                    this.service.insertTrStockClose(idTransaksi, lastBalanceAfterTransaction, jsonReq, jObjAgentToAgentData);

                    jsonResp = new JSONObject();
                    String pesan = "Transaksi agent ke agent sukses dengan nomor transaksi " + jsonReq.get("idTrx").toString();
                    jsonResp.put("idTrx", jsonReq.get("idTrx").toString());
                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", pesan);
                    jsonResp.put("timestamp", utilities.getDate().toString());
                    jsonResp.put("status", "SUKSES");
                    jsonResp.put("receiverName", jObjAgentToAgentData.get("namaAgentPenerima").toString());
                    jsonResp.put("receiverIdAgentAccount", jObjAgentToAgentData.get("idAgentAccountPenerima").toString());
                    jsonResp.put("nominal", jObjAgentToAgentData.get("nominalTransfer").toString().replace(".", ""));
                    jsonResp.put("fee", jObjAgentToAgentData.get("feeTransfer").toString().replace(".", ""));
                    jsonResp.put("totalBayar", jObjAgentToAgentData.get("totalTransfer").toString().replace(".", ""));

                    //sent fcm message
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String fcmTokenAgentPenerima = jObjAgentToAgentData.get("fcmTokenAgenPenerima").toString();
                            String fcmTokenAgentPengirim = jObjAgentToAgentData.get("fcmTokenAgenPengirim").toString();
                            JSONArray jArrToken = new JSONArray();
                            jArrToken.add(fcmTokenAgentPenerima);
                            jArrToken.add(fcmTokenAgentPengirim);
                            if (!fcmTokenAgentPenerima.equals("") && !fcmTokenAgentPengirim.equals("")
                                    && !jObjAgentToAgentData.get("idAgentAccountPenerima").toString().equals("")
                                    && !jObjAgentToAgentData.get("idAgentAccount").toString().equals("")) {
                                JSONObject jObjMessage = new JSONObject();
                                jObjMessage.put("mainJudul", "TrueMoney");

                                JSONObject jObjExtra = new JSONObject();
                                jObjExtra.put("tipe", "agentToAgent");
                                double dbl = Double.parseDouble(jObjAgentToAgentData.get("nominalTransfer").toString());
                                String dblStr = String.format("%,.0f", dbl);
                                dblStr = dblStr.replaceAll(",", ".");
                                jObjExtra.put("subJudul", "Transfer agent ke agent sukses sebesar Rp." + dblStr);

                                JSONArray jArrIdAgent = new JSONArray();
                                jArrIdAgent.add(jObjAgentToAgentData.get("idAgentAccountPenerima").toString());
                                jArrIdAgent.add(jObjAgentToAgentData.get("idAgentAccount").toString());
                                jObjExtra.put("idAgentPenerima", jArrIdAgent);

                                jObjMessage.put("extra", jObjExtra);
                                FcmHelper fcmHelper = new FcmHelper();
                                fcmHelper.sendFcm(jArrToken, jObjMessage);
                            }
                        }
                    }).start();
                } else {
                    jsonResp = new JSONObject();
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Password anda salah!");
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp = new JSONObject();
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Maaf! Untuk saat ini layanan tidak tersedia");
        }

        return jsonResp.toString();
    }
}
