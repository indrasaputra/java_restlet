package controller;

import com.google.common.base.Throwables;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImplePembayaranInternet;
import implement.ImplePembayaranJastel;
import interfc.InterfcPembayaranInternet;
import interfc.InterfcPembayaranJastel;

/**
 *
 * @author hasan
 */
public class ControllerPembayaranInternet {

    private final EntityConfig entityConfig;
    private final InterfcPembayaranInternet service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerPembayaranInternet() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImplePembayaranInternet(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getInq(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ID", "inquiry");
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        String idpelanggan = jsonReq.get("id_Pelanggan").toString();
        String idoperator = jsonReq.get("id_Operator").toString();
        String idaccount = jsonReq.get("username").toString();
        String idtrx = jsonReq.get("idtrx").toString();
        int idSupplier = 0;
        String tipeOperator = "";

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                idSupplier = this.service.getIdSupplier(jsonReq);
                if (idSupplier == 0) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Mohon maaf, supplier tidak tersedia");
                    return jsonResp.toString();
                }
                String productcode = this.service.getProductCode(idoperator, idSupplier);
                if (productcode.equals("")) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                    return jsonResp.toString();
                }
                JSONObject jObjCurl = new JSONObject();
                jObjCurl.put("trxid", idtrx);
                jObjCurl.put("type", "INQUIRY");
                jObjCurl.put("product", productcode);
                jObjCurl.put("idpelanggan", idpelanggan);
                jObjCurl.put("idpartner", "0001111");
                jObjCurl.put("interface", "MOBILE");
                String url = this.service.getUrl("postpaid_telkom_inq_" + idSupplier);
                //insertLog
                int idLog = utilities.insertLog(idtrx, jObjCurl);
                String response = this.service.curlPost(url, jObjCurl.toString(), false);
                //updateLog
                utilities.updateLog(idLog, response);
                Object object2 = jSONParser.parse(response);
                JSONObject fromGateway = (JSONObject) object2;
                if (fromGateway.get("ResponseCode").toString().equals("200")) {
                    if (fromGateway.get("ACK").toString().equals("OK")) {
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("nama", fromGateway.get("namapelanggan").toString());

                        double tagihan = Double.parseDouble(fromGateway.get("jumlahtagihan").toString());
                        String tagihanStr = String.format("%,.0f", tagihan);
                        tagihanStr = tagihanStr.replaceAll(",", ".");
                        jsonResp.put("tagihan", tagihanStr);

                        double hargaCetak = Double.parseDouble(fromGateway.get("jumlahbayar").toString());
                        String hargaCetakStr = String.format("%,.0f", hargaCetak);
                        hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                        jsonResp.put("hargaCetak", hargaCetakStr);

                        double biayaBayar = Double.parseDouble(fromGateway.get("jumlahadm").toString());
                        String biayaBayarStr = String.format("%,.0f", biayaBayar);
                        biayaBayarStr = biayaBayarStr.replaceAll(",", ".");
                        jsonResp.put("biayaBayar", biayaBayarStr);

                        jsonResp.put("periode", fromGateway.get("bulan_thn").toString());
                        jsonResp.put("idtrx", idtrx);
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", fromGateway.get("pesan").toString());
                    }
                } else if (fromGateway.get("ResponseCode").toString().equals("500")) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", fromGateway.get("pesan").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Maaf! Untuk saat ini layanan tidak tersedia");
        }

        return jsonResp.toString();
    }

    public String getPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ID", "inquiry");
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        int idSupplier = Constant.idSupplierVsi;
        String username = null;
        String idPelanggan = null;
        String nominal = null;
        String biayaAdmin = null;
        String hargaCetak = null;
        String idtrx = null;
        String idOperator = null;
        String pin = null;
        String Handphone = null;

        String idTipeTransaksi = null;
        String idTipeAplikasi = null;

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                username = jsonReq.get("username").toString();
                idPelanggan = jsonReq.get("id_Pelanggan").toString();
                nominal = jsonReq.get("tagihan").toString();
                biayaAdmin = jsonReq.get("biayaBayar").toString();
                hargaCetak = jsonReq.get("hargaCetak").toString();
                idtrx = jsonReq.get("idtrx").toString();
                idOperator = jsonReq.get("id_Operator").toString();
                pin = jsonReq.get("PIN").toString();
                idTipeTransaksi = jsonReq.get("ID_TipeTransaksi").toString();
                idTipeAplikasi = jsonReq.get("id_TipeAplikasi").toString();

                int count = this.service.gettrxid(idtrx);
                if (count == 1) {
                    String status[] = this.service.getStatusTrx(idtrx);
                    String m = "";
                    String ack = "";
                    if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.SUKSES))) {
                        m = String.valueOf(Constant.Pesan.SUKSES.getPesan());
                        ack = String.valueOf(Constant.ACK.OK);
                    } else if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.GAGAL))) {
                        m = String.valueOf(Constant.Pesan.GAGAL.getPesan());
                        ack = String.valueOf(Constant.ACK.NOK);
                    } else {
                        status[0] = String.valueOf(Constant.StatusTrx.PENDING);
                        m = String.valueOf(Constant.Pesan.PENDING.getPesan());
                        ack = String.valueOf(Constant.ACK.OK);
                    }
                    jsonResp.put("statusTRX", status[0]);
                    jsonResp.put("ACK", ack);
                    jsonResp.put("pesan", m);
                    return jsonResp.toString();
                } else if (count > 1) {
                    jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", String.valueOf(Constant.Pesan.GAGAL.getPesan()));
                    return jsonResp.toString();
                }

                if (!this.service.CekIsEmptyPostpaid(idPelanggan, idOperator, username)) {
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", String.valueOf(Constant.Pesan.VALIDASITRX.getPesan()));
                    return jsonResp.toString();
                }

                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                int saldo = Integer.parseInt(rekeningAgentNonEDC[1]);

                String[] com = this.service.getComission(Integer.parseInt(idTipeTransaksi), Integer.parseInt(idTipeAplikasi), Integer.parseInt(idOperator));
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    if (pin.equals(rekeningAgentNonEDC[4])) {

                        String validasiSaldo = this.service.isSaldoCukup(username, Integer.parseInt(hargaCetak));
                        if ("00".equalsIgnoreCase(validasiSaldo)) {
                            jsonResp = this.service.prosesBayar(jsonReq, jsonResp, rekeningAgentNonEDC, com);

                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Saldo Anda tidak cukup");
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "PIN Anda salah");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Rekening Anda Diblokir. Silakan Hubungi Customer Care Kami.");
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
            return jsonResp.toString();
        }
        resp = jsonResp.toString();
        return resp;
    }

}
