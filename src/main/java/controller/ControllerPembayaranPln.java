package controller;

import com.google.common.base.Throwables;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImplePembayaranPln;
import interfc.InterfcPembayaranPln;

/**
 * @author hasan
 */
public class ControllerPembayaranPln {

    private final EntityConfig entityConfig;
    private final InterfcPembayaranPln service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cache;
    private Element elemConfig;
    private Element elem;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerPembayaranPln() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        elemConfig = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) elemConfig.getObjectValue();

        this.cache = cacheManager.getCache("TrueMoneyAppCookies");

        this.service = new ImplePembayaranPln(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getInq(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ID", "inquiry");
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        String jsonReqTmp = jsonReq.toString();
        jsonReqTmp = jsonReqTmp.replace(".", "");
        JSONParser jSONParser = new JSONParser();
        try {
            jsonReq = (JSONObject) jSONParser.parse(jsonReqTmp);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        String idpelanggan = jsonReq.get("id_Pelanggan").toString();
        String idoperator = jsonReq.get("id_Operator").toString();
        String idaccount = jsonReq.get("username").toString();
        String idtrx = jsonReq.get("idtrx").toString();
        String tipeOperator = "";
        int idSupplier = 0;

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                idSupplier = this.service.getIdSupplier(jsonReq);
                if (idSupplier == 0) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Mohon maaf, supplier tidak tersedia");
                    return jsonResp.toString();
                }
                tipeOperator = this.service.getTipeOperator(idoperator);
                if (tipeOperator.equals("PLN-PASCA")) {
                    String productcode = "";
                    productcode = this.service.getProductCode(idoperator, idSupplier);
                    if (productcode == null && productcode.isEmpty()) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                        return jsonResp.toString();
                    }
                    JSONObject jCurlObj = new JSONObject();
                    jCurlObj.put("trxid", idtrx);
                    jCurlObj.put("type", "INQUIRY");
                    jCurlObj.put("product", productcode);
                    jCurlObj.put("idpelanggan", idpelanggan);
                    jCurlObj.put("idpartner", "0001111");
                    jCurlObj.put("interface", "MOBILE");
                    String url = this.service.getUrl("postpaid_pln_inq_" + idSupplier);
                    //insert log
                    int idLog = utilities.insertLog(idtrx, jCurlObj);
                    String response = this.service.curlPost(url, jCurlObj.toString(), false);
                    //updateLog
                    utilities.updateLog(idLog, response);
                    JSONObject fromGateway = (JSONObject) jSONParser.parse(response);
                    if (fromGateway.get("ResponseCode").toString().equals("200")) {
                        if (fromGateway.get("ACK").toString().equals("OK")) {
                            jsonResp.put("biayaBayar", fromGateway.get("jumlahadm").toString());
                            jsonResp.put("ACK", "OK");
                            jsonResp.put("nama", fromGateway.get("namapelanggan").toString());
                            jsonResp.put("tagihan", fromGateway.get("jumlahtagihan").toString());

                            double hargaCetak = Double.parseDouble(fromGateway.get("jumlahbayar").toString());
                            String hargaCetakStr = String.format("%,.0f", hargaCetak);
                            hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                            jsonResp.put("hargaCetak", hargaCetakStr);

                            double biayaAdminCetak = Double.parseDouble(fromGateway.get("jumlahadm").toString());
                            String biayaAdminCetakStr = String.format("%,.0f", biayaAdminCetak);
                            biayaAdminCetakStr = biayaAdminCetakStr.replaceAll(",", ".");
                            jsonResp.put("biayaAdmin", biayaAdminCetakStr);

                            double tagihanCetak = Double.parseDouble(fromGateway.get("jumlahtagihan").toString());
                            String tagihanStr = String.format("%,.0f", tagihanCetak);
                            tagihanStr = tagihanStr.replaceAll(",", ".");
                            jsonResp.put("tagihan", tagihanStr);

                            jsonResp.put("daya", fromGateway.get("daya").toString());
                            jsonResp.put("lwbp", fromGateway.get("lwbp").toString());
                            jsonResp.put("periode", fromGateway.get("bulan_thn").toString());
                            jsonResp.put("idtrx", idtrx);
                            jsonResp.put("pesan", fromGateway.get("pesan").toString());
                            jsonResp.put("idpelanggan", fromGateway.get("idpelanggan").toString());
                            jsonResp.put("swResp", fromGateway.toString());
                            //this.cache.put(new Element("inqPlnPasca" + jsonReq.get("idtrx").toString(), jsonResp));
                            this.cookieHelper.addDataCookie(cookieClient, "inqPlnPasca" + jsonReq.get("idtrx").toString(), jsonResp.toString());
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", fromGateway.get("pesan").toString());
                        }
                    } else if (fromGateway.get("ResponseCode").toString().equals("500")) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", fromGateway.get("pesan").toString());
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        resp = jsonResp.toString();
        return resp;
    }

    public String bayarPlnPascabayar(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ID", "payment");
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        String username = null;
        String idpelanggan = null;
        String nominal = null;
        String biayaadmin = null;
        String hargacetak = null;
        String idtrx = null;
        String verifikasi = "PIN";
        String idoperator = null;
        String idtipetransaksi = null;
        String idtipeaplikasi = null;
        String pin = null;

        try {
            username = jsonReq.get("username").toString();
            idpelanggan = jsonReq.get("id_Pelanggan").toString();
            nominal = jsonReq.get("tagihan").toString();
            biayaadmin = jsonReq.get("biayaBayar").toString();
            hargacetak = jsonReq.get("hargaCetak").toString();
            idtrx = jsonReq.get("idtrx").toString();
            idoperator = jsonReq.get("id_Operator").toString();
            pin = jsonReq.get("PIN").toString();
            idtipetransaksi = jsonReq.get("ID_TipeTransaksi").toString();
            idtipeaplikasi = jsonReq.get("id_TipeAplikasi").toString();
            //check harga from client is equal in cache
            boolean valid = false;
            JSONObject jObjDataCookie = this.cookieHelper.getDataCookie(cookieClient);
            if (jObjDataCookie.containsKey("inqPlnPasca" + idtrx)) {
                String inqPln = jObjDataCookie.get("inqPlnPasca" + idtrx).toString();
                JSONObject jObjInqPln = (JSONObject) parser.parse(inqPln);
                if (nominal.equals(jObjInqPln.get("tagihan").toString().replace(".",""))
                        && biayaadmin.equals(jObjInqPln.get("biayaAdmin").toString().replace(".", ""))
                        && hargacetak.equals(jObjInqPln.get("hargaCetak").toString().replace(".", ""))) {
                    valid = true;
                }
            }

            if(valid == false){
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Internal Error Parameter. Info Cs: 08041000100");
                return jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Missing parameter");
            return jsonResp.toString();
        }

        //check if pricing inquiry is equal with request data
//        boolean valid = false;
//        String keyCachePlnPostInq = "inqPlnPasca" + idtrx;
//        if (this.cache.isKeyInCache(keyCachePlnPostInq)) {
//            this.elem = this.cache.get(keyCachePlnPostInq);
//            if (this.elem != null) {
//                if (this.cache.isExpired(this.elem) == false) {
//                    //get data from cache
//                    JSONObject jObjDataInq = (JSONObject) this.elem.getObjectValue();
//                    if (nominal.equals(jObjDataInq.get("tagihan").toString().replace(".",""))
//                            && biayaadmin.equals(jObjDataInq.get("biayaAdmin").toString().replace(".",""))
//                            && hargacetak.equals(jObjDataInq.get("hargaCetak").toString().replace(".",""))) {
//                        valid = true;
//                    }
//                }
//            }
//        }
//
//        if (valid == false) {
//            jsonResp.put("ACK", "NOK");
//            jsonResp.put("pesan", "Internal Error Parameter. Info Cs: 08041000100");
//            return jsonResp.toString();
//        }

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                int count = this.service.getTrxId(idtrx);
                if (count == 1) {
                    String status[] = this.service.getStatusTrx(idtrx);

                    String m = "";
                    String ack = "";
                    if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.SUKSES))) {
                        m = "Transaksi Anda sukses";
                        ack = String.valueOf(Constant.ACK.OK);
                    } else if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.GAGAL))) {
                        m = "Transaksi Anda gagal";
                        ack = String.valueOf(Constant.ACK.NOK);
                    } else {
                        status[0] = String.valueOf(Constant.StatusTrx.PENDING);
                        m = "Transaksi Anda sedang diproses";
                        ack = String.valueOf(Constant.ACK.OK);
                    }
                    jsonResp.put("statusTRX", status[0]);
                    jsonResp.put("ACK", ack);
                    jsonResp.put("pesan", m);
                    return jsonResp.toString();
                } else if (count > 1) {
                    jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", "Transaksi Anda gagal");
                    return jsonResp.toString();
                }
            }

            String[] statRekAgent = new String[5];
            statRekAgent = this.service.StatusRekeningAgentNonEDC(username);
            String statusRekening = statRekAgent[0];
            int saldo = Integer.parseInt(statRekAgent[1]);
            String id_agentaccount = statRekAgent[3];
            int[] com = this.service.getComission(Integer.parseInt(idoperator));
            String stattrx = "";
            if (statusRekening.equals("ACTIVE") || statusRekening.equals("INACTIVE") || statusRekening.equals("DORMANT")) {
                if (pin.equals(statRekAgent[4])) {
                    String validasiSaldo = this.service.isSaldoCukup(id_agentaccount, Integer.parseInt(hargacetak));
                    if (validasiSaldo.equalsIgnoreCase("00")) {
                        jsonResp = this.service.prosesBayar(jsonReq, jsonResp, statRekAgent, com);
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Saldo Anda tidak cukup");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "PIN Anda salah");
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Rekening Anda Diblokir. Silakan Hubungi Customer Care Kami.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        resp = jsonResp.toString();
        return resp;
    }
}
