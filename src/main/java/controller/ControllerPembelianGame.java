package controller;

import com.google.common.base.Throwables;
import helper.Constant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImplePembelianGame;
import interfc.InterfcPembelianGame;

/**
 *
 * @author hasan
 */
public class ControllerPembelianGame {

    private final EntityConfig entityConfig;
    private final InterfcPembelianGame service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerPembelianGame() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImplePembelianGame(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getGame(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getGame(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getNominal(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getNominal(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getInquiry(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getInquiry(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getNewPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getNewPay(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getNewPay2(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                try {
                    String no_HP = jsonReq.get("noHp").toString();
                    String nominal = jsonReq.get("nominal").toString();
                    String id_operator = jsonReq.get("id_Operator").toString();
                    String username = jsonReq.get("username").toString();
                    String pin = jsonReq.get("PIN").toString();
                    String hargaCetak = jsonReq.get("hargaCetak").toString();
                    String idTrx = jsonReq.get("idtrx").toString();

                    int count = this.service.getCountTrxByIdtrx(idTrx);
                    if (count == 1) {
                        String statusTrx = this.service.getStatusTrxByIdtrx(jsonReq);
                        if (statusTrx.equalsIgnoreCase("SUKSES")) {
                            jsonResp.put("ACK", "OK");
                            jsonResp.put("pesan", "Transaksi Anda sukes");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        } else if (statusTrx.equalsIgnoreCase("GAGAL")) {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Transaksi Anda gagal");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        } else if (statusTrx.equalsIgnoreCase("PENDING")) {
                            jsonResp.put("ACK", "OK");
                            jsonResp.put("pesan", "Transaksi Anda sedang diproses");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        }
                    } else if (count > 1) {
                        jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                        jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                        jsonResp.put("pesan", String.valueOf(Constant.Pesan.GAGAL.getPesan()));
                        return jsonResp.toString();
                    }

                    int checkDuplicateTransaction = this.service.checkDuplicateTransaction(jsonReq);
                    if (checkDuplicateTransaction > 0) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Anda tidak dapat melakukan transaksi, silakan coba 24 jam kemudian");
                        return jsonResp.toString();
                    }

                    ArrayList<HashMap<String, Object>> row = new ArrayList<>();
                    row = this.service.getMsAgentAccountDetil(jsonReq);
                    long accountStatus = 0;
                    String pinFromDb = "";
                    long lastBalance = 0;
                    for (HashMap<String, Object> map : row) {
                        Iterator it = map.entrySet().iterator();
                        while (it.hasNext()) {
                            int j = 0;
                            Map.Entry pair = (Map.Entry) it.next();
                            if (pair.getKey().equals("id_accountstatus")) {
                                accountStatus = Long.parseLong(pair.getValue().toString());
                            } else if (pair.getKey().equals("PIN")) {
                                pinFromDb = this.encDec.decryptDbVal(pair.getValue().toString(), this.entityConfig.getEncKeyDb());
                            } else if (pair.getKey().equals("LastBalance")) {
                                lastBalance = Long.parseLong(pair.getValue().toString());
                            }
                            it.remove(); // avoids a ConcurrentModificationException
                        }
                    }

                    if (accountStatus == 1 || accountStatus == 2 || accountStatus == 3) {
                        if (jsonReq.get("PIN").toString().equals(pinFromDb)) {
                            resp = this.service.getNewPay(jsonReq);
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "PIN Anda salah");
                            jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                            return jsonResp.toString();
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Rekening Anda diblokir. Silakan hubungi customer care kami");
                        jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                        return jsonResp.toString();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
                    return jsonResp.toString();
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resp;
    }

    public String getPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                try {
                    String no_HP = jsonReq.get("noHp").toString();
                    String nominal = jsonReq.get("nominal").toString();
                    String id_operator = jsonReq.get("id_Operator").toString();
                    String username = jsonReq.get("username").toString();
                    String pin = jsonReq.get("PIN").toString();
                    String hargaCetak = jsonReq.get("hargaCetak").toString();
                    String idTrx = jsonReq.get("idtrx").toString();

                    int count = this.service.getCountTrxByIdtrx(idTrx);
                    if (count == 1) {
                        String statusTrx = this.service.getStatusTrxByIdtrx(jsonReq);
                        if (statusTrx.equalsIgnoreCase("SUKSES")) {
                            jsonResp.put("ACK", "OK");
                            jsonResp.put("pesan", "Transaksi Anda sukes");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        } else if (statusTrx.equalsIgnoreCase("GAGAL")) {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Transaksi Anda gagal");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        } else if (statusTrx.equalsIgnoreCase("PENDING")) {
                            jsonResp.put("ACK", "OK");
                            jsonResp.put("pesan", "Transaksi Anda sedang diproses");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        }
                    } else if (count > 1) {
                        jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                        jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                        jsonResp.put("pesan", String.valueOf(Constant.Pesan.GAGAL.getPesan()));
                        return jsonResp.toString();
                    }

                    int checkDuplicateTransaction = this.service.checkDuplicateTransaction(jsonReq);
                    if (checkDuplicateTransaction > 0) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Anda tidak dapat melakukan transaksi, silakan coba 24 jam kemudian");
                        return jsonResp.toString();
                    }

                    ArrayList<HashMap<String, Object>> row = new ArrayList<>();
                    row = this.service.getMsAgentAccountDetil(jsonReq);
                    long accountStatus = 0;
                    String pinFromDb = "";
                    long lastBalance = 0;
                    for (HashMap<String, Object> map : row) {
                        Iterator it = map.entrySet().iterator();
                        while (it.hasNext()) {
                            int j = 0;
                            Map.Entry pair = (Map.Entry) it.next();
                            if (pair.getKey().equals("id_accountstatus")) {
                                accountStatus = Long.parseLong(pair.getValue().toString());
                            } else if (pair.getKey().equals("PIN")) {
                                pinFromDb = this.encDec.decryptDbVal(pair.getValue().toString(), this.entityConfig.getEncKeyDb());
                            } else if (pair.getKey().equals("LastBalance")) {
                                lastBalance = Long.parseLong(pair.getValue().toString());
                            }
                            it.remove(); // avoids a ConcurrentModificationException
                        }
                    }

                    if (accountStatus == 1 || accountStatus == 2 || accountStatus == 3) {
                        if (jsonReq.get("PIN").toString().equals(pinFromDb)) {
                            if (lastBalance >= Integer.parseInt(hargaCetak)) {
                                row = this.service.getDetilHarga(jsonReq);
                                jsonResp = this.service.deductBalance(jsonReq);
                                if (jsonResp.get("ACK").equals("OK")) {
                                    this.service.insertTrTransTrStock(jsonReq, row);
                                    // Update Status Account
                                    if (accountStatus == Constant.idStatusAccount.INACTIVE
                                            || accountStatus == Constant.idStatusAccount.DORMANT) {
                                        this.service.updateStatusAccount(jsonReq);
                                    }
                                    this.service.hitApiSwitcing(jsonReq);
                                    jsonResp.put("ACK", "OK");
                                    jsonResp.put("pesan", "Transaksi Anda sedang diproses");
                                    jsonResp.put("status", "PENDING");
                                    jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                                    return jsonResp.toString();
                                } else {
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan", "Transaksi Anda gagal");
                                    jsonResp.put("status", "GAGAL");
                                    return jsonResp.toString();
                                }
                            } else {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Saldo Anda tidak cukup");
                                jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                                return jsonResp.toString();
                            }
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "PIN Anda salah");
                            jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                            return jsonResp.toString();
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Rekening Anda diblokir. Silakan hubungi customer care kami");
                        jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                        return jsonResp.toString();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
                    return jsonResp.toString();
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resp;
    }

}
