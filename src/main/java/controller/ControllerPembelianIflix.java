package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImplePembelianIflix;
import interfc.InterfcPembelianIflix;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author hasan
 */
public class ControllerPembelianIflix {

    private final EntityConfig entityConfig;
    private final InterfcPembelianIflix service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerPembelianIflix() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImplePembelianIflix(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getDenom(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getDenom(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                try {
                    String no_HP = jsonReq.get("noHp").toString();
                    String nominal = jsonReq.get("nominal").toString();
                    String id_operator = jsonReq.get("id_Operator").toString();
                    String username = jsonReq.get("username").toString();
                    String pin = jsonReq.get("PIN").toString();
                    String hargaCetak = jsonReq.get("hargaCetak").toString();
                    String idTrx = jsonReq.get("idtrx").toString();
                    String productCode = jsonReq.get("productCode").toString();
                    String idTipeTransaksi = jsonReq.get("ID_TipeTransaksi").toString();
                    String idDenom = jsonReq.get("idDenom").toString();

                    int count = this.service.getCountTrxByIdtrx(idTrx);
                    if (count == 1) {
                        String statusTrx = this.service.getStatusTrxByIdtrx(jsonReq);
                        if (statusTrx.equalsIgnoreCase("SUKSES")) {
                            jsonResp.put("ACK", "OK");
                            jsonResp.put("pesan", "Transaksi Anda sukes");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        } else if (statusTrx.equalsIgnoreCase("GAGAL")) {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Transaksi Anda gagal");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        } else if (statusTrx.equalsIgnoreCase("PENDING")) {
                            jsonResp.put("ACK", "OK");
                            jsonResp.put("pesan", "Transaksi Anda sedang diproses");
                            jsonResp.put("status", statusTrx);
                            return jsonResp.toString();
                        }
                    } else if (count > 1) {
                        jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                        jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                        jsonResp.put("pesan", String.valueOf(Constant.Pesan.GAGAL.getPesan()));
                        return jsonResp.toString();
                    }

                    int checkDuplicateTransaction = this.service.checkDuplicateTransaction(jsonReq);
                    if (checkDuplicateTransaction > 0) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Anda tidak dapat melakukan transaksi, silakan coba 24 jam kemudian");
                        return jsonResp.toString();
                    }

                    ArrayList<HashMap<String, Object>> row = new ArrayList<>();
                    row = this.service.getMsAgentAccountDetil(jsonReq);
                    long accountStatus = 0;
                    String pinFromDb = "";
                    long lastBalance = 0;
                    for (HashMap<String, Object> map : row) {
                        Iterator it = map.entrySet().iterator();
                        while (it.hasNext()) {
                            int j = 0;
                            Map.Entry pair = (Map.Entry) it.next();
                            if (pair.getKey().equals("id_accountstatus")) {
                                accountStatus = Long.parseLong(pair.getValue().toString());
                            } else if (pair.getKey().equals("PIN")) {
                                pinFromDb = this.encDec.decryptDbVal(pair.getValue().toString(), this.entityConfig.getEncKeyDb());
                            } else if (pair.getKey().equals("LastBalance")) {
                                lastBalance = Long.parseLong(pair.getValue().toString());
                            }
                            it.remove(); // avoids a ConcurrentModificationException
                        }
                    }

                    if (accountStatus == 1 || accountStatus == 2 || accountStatus == 3) {
                        if (jsonReq.get("PIN").toString().equals(pinFromDb)) {
                            if (lastBalance >= Integer.parseInt(hargaCetak)) {
                                row = this.service.getDetilHarga(jsonReq);
                                jsonResp = this.service.getPay(jsonReq);
                                return jsonResp.toString();
                            } else {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Saldo Anda tidak cukup");
                                jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                                return jsonResp.toString();
                            }
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "PIN Anda salah");
                            jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                            return jsonResp.toString();
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Rekening Anda diblokir. Silakan hubungi customer care kami");
                        jsonResp.put("idtrx", jsonReq.get("idtrx").toString());
                        return jsonResp.toString();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
                    return jsonResp.toString();
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resp;
    }
}
