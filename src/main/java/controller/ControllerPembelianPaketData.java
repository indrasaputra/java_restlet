package controller;

import com.google.common.base.Throwables;
import helper.Constant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImplePembelianPaketData;
import interfc.InterfcPembelianPaketData;

/**
 *
 * @author hasan
 */
public class ControllerPembelianPaketData {

    private final EntityConfig entityConfig;
    private final InterfcPembelianPaketData service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerPembelianPaketData() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImplePembelianPaketData(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getDenom(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getDenom(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String paketDataPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                //resp = this.service.bayarPulsaPrabayar(jsonReq);

                String nohp = jsonReq.get("handphone").toString();
                String nominal = jsonReq.get("denom").toString();
                String idOperator = jsonReq.get("idOperator").toString();
                String username = jsonReq.get("username").toString();
                String pin = jsonReq.get("pin").toString();
                String hargaCetak = jsonReq.get("hargaCetak").toString();
                hargaCetak = hargaCetak.replace(".", "");
                String idtrx = jsonReq.get("idTrx").toString();

                int countTrxByIdTrx = this.service.getCountIdTrx(jsonReq);
                if (countTrxByIdTrx == 1) {
                    String statusTrx = this.service.getStatusTrxByIdtrx(jsonReq);
                    if (statusTrx.equalsIgnoreCase("SUKSES")) {
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("pesan", "Transaksi Anda sukes");
                        jsonResp.put("status", statusTrx);
                        return jsonResp.toString();
                    } else if (statusTrx.equalsIgnoreCase("GAGAL")) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Transaksi Anda gagal");
                        jsonResp.put("status", statusTrx);
                        return jsonResp.toString();
                    } else if (statusTrx.equalsIgnoreCase("PENDING")) {
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("pesan", "Transaksi Anda sedang diproses");
                        jsonResp.put("status", statusTrx);
                        return jsonResp.toString();
                    }
                } else if (countTrxByIdTrx > 1) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Transaksi Anda gagal");
                    jsonResp.put("status", "GAGAL");
                    return jsonResp.toString();
                }

                int checkDuplicateTransaction = this.service.checkDuplicateTransaction(jsonReq);
                if (checkDuplicateTransaction > 0) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Anda tidak dapat melakukan transaksi, silakan coba 24 jam kemudian");
                    return jsonResp.toString();
                }

                long accountStatus = 0;
                String pinFromDb = "";
                long lastBalance = 0;
                ArrayList<HashMap<String, Object>> row = new ArrayList<>();
                row = this.service.getMsAgentAccountDetil(jsonReq);
                for (HashMap<String, Object> map : row) {
                    Iterator it = map.entrySet().iterator();
                    while (it.hasNext()) {
                        int j = 0;
                        Map.Entry pair = (Map.Entry) it.next();
                        if (pair.getKey().equals("id_accountstatus")) {
                            accountStatus = Long.parseLong(pair.getValue().toString());
                        } else if (pair.getKey().equals("PIN")) {
                            pinFromDb = this.encDec.decryptDbVal(pair.getValue().toString(), this.entityConfig.getEncKeyDb());
                        } else if (pair.getKey().equals("LastBalance")) {
                            lastBalance = Long.parseLong(pair.getValue().toString());
                        }
                        it.remove(); // avoids a ConcurrentModificationException
                    }
                }

                if (accountStatus == 1 || accountStatus == 2 || accountStatus == 3) {
                    if (jsonReq.get("pin").toString().equals(pinFromDb)) {
                        if (lastBalance >= Integer.parseInt(hargaCetak)) {
                            row = this.service.getDetilHarga(jsonReq);
                            jsonResp = this.service.deductBalance(jsonReq);
                            if (jsonResp.get("ACK").equals("OK")) {
                                this.service.insertTrTransTrStock(jsonReq, row);
                                // Update Status Account
                                if (accountStatus == Constant.idStatusAccount.INACTIVE
                                        || accountStatus == Constant.idStatusAccount.DORMANT) {
                                    this.service.updateStatusAccount(jsonReq);
                                }
                                this.service.hitApiSwitcing(jsonReq);
                                jsonResp.put("ACK", "OK");
                                jsonResp.put("pesan", "Transaksi Anda sedang diproses");
                                jsonResp.put("status", "PENDING");
                                jsonResp.put("idtrx", jsonReq.get("idTrx").toString());
                                return jsonResp.toString();
                            } else {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Transaksi Anda gagal");
                                jsonResp.put("status", "GAGAL");
                                return jsonResp.toString();
                            }
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Saldo Anda tidak cukup");
                            jsonResp.put("idtrx", jsonReq.get("idTrx").toString());
                            return jsonResp.toString();
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "PIN Anda salah");
                        jsonResp.put("idtrx", jsonReq.get("idTrx").toString());
                        return jsonResp.toString();
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Rekening Anda diblokir. Silakan hubungi customer care kami");
                    jsonResp.put("idtrx", jsonReq.get("idTrx").toString());
                    return jsonResp.toString();
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resp;
    }
}
