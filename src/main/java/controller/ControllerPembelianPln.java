package controller;

import com.google.common.base.Throwables;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImplePembelianPln;
import interfc.InterfcPembelianPln;

/**
 *
 * @author hasan
 */
public class ControllerPembelianPln {

    private final EntityConfig entityConfig;
    private final InterfcPembelianPln service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerPembelianPln() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImplePembelianPln(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getDenom(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getDenom(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getInqPln(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            int length = 0;
            String idtrx;
            String denom;
            if (jsonResp.get("ACK").equals("OK")) {
                try {
                    length = jsonReq.get("idPelanggan").toString().length();
                    idtrx = jsonReq.get("idTrx").toString();
                    denom = jsonReq.get("denom").toString();
                } catch (Exception e) {
                    //e.printStackTrace();
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Missing parameter");
                    return jsonResp.toString();
                }
                ArrayList<HashMap<String, Object>> row = new ArrayList<>();
                if (length <= 12 && length >= 11) {
                    row = this.service.getIDAgenKomisi(jsonReq);
                    int biayaAdmin = 0;
                    int trueSpareFee = 0;
                    int supplierPrice = 0;
                    int agentCommNom = 0;
                    int dealerCommNom = 0;
                    int trueFeeNom = 0;
                    int memberDiscNom = 0;
                    int idSupplier = 0;
                    for (HashMap<String, Object> map : row) {
                        Iterator it = map.entrySet().iterator();
                        while (it.hasNext()) {
                            int j = 0;
                            Map.Entry pair = (Map.Entry) it.next();
                            if (pair.getKey().equals("BiayaAdmin")) {
                                biayaAdmin = Integer.parseInt(pair.getValue().toString());
                            } else if (pair.getKey().equals("TrueSpareFee")) {
                                trueSpareFee = Integer.parseInt(pair.getValue().toString());
                            } else if (pair.getKey().equals("SupplierPrice")) {
                                supplierPrice = Integer.parseInt(pair.getValue().toString());
                            } else if (pair.getKey().equals("AgentCommNom")) {
                                agentCommNom = Integer.parseInt(pair.getValue().toString());
                            } else if (pair.getKey().equals("DealerCommNom")) {
                                dealerCommNom = Integer.parseInt(pair.getValue().toString());
                            } else if (pair.getKey().equals("TrueFeeNom")) {
                                trueFeeNom = Integer.parseInt(pair.getValue().toString());
                            } else if (pair.getKey().equals("MemberDiscNom")) {
                                memberDiscNom = Integer.parseInt(pair.getValue().toString());
                            } else if (pair.getKey().equals("id_supplier")) {
                                idSupplier = Integer.parseInt(pair.getValue().toString());
                            }
                            it.remove(); // avoids a ConcurrentModificationException
                        }
                    }

                    String url = this.service.getURL("prepaid_pln_inq_" + idSupplier);
                    int idLog = this.service.insertLog(jsonReq);
                    resp = this.service.curlPost(url, jsonReq, false);
                    this.service.updateLog(resp, idLog);
                } else {
                    resp = "{\"pesan\":\"EXT: NOMOR METER/IDPEL YANG ANDA MASUKKAN SALAH, MOHON TELITI KEMBALI\",\"ACK\":\"NOK\"}";
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String bayarPlnPrabayar(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                //check if hargaCetak is equals from db
                String hargaCetak = jsonReq.get("hargaCetak").toString();
                hargaCetak = hargaCetak.replace(".", "");
                String nominal = jsonReq.get("denom").toString();
                nominal = nominal.replace(".", "");
                String idOperator = jsonReq.get("id_Operator").toString();
                JSONArray jArrDetilHarga = this.service.getDetilharga(idOperator, nominal);
                if (jArrDetilHarga.size() == 0) {
                    jsonResp = new JSONObject();
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Produk Tidak Tersedia. Info Cs: 08041000100");
                    return jsonResp.toString();
                } else {
                    JSONObject jObjDetilHarga = (JSONObject) jArrDetilHarga.get(0);
                    if (!jObjDetilHarga.get("HargaCetak").toString().equals(hargaCetak)) {
                        jsonResp = new JSONObject();
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Internal Error Parameter. Info Cs: 08041000100");
                        return jsonResp.toString();
                    }
                }
                resp = this.service.bayarPlnPrabayar(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }
}
