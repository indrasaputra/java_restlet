package controller;

import com.google.common.base.Throwables;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.GcmNew;
import helper.Helper;
import helper.Utilities;
import implement.ImpleProfile;
import interfc.InterfcProfile;

/**
 * @author hasan
 */
public class ControllerProfile {

    private final EntityConfig entityConfig;
    private final InterfcProfile service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerProfile() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleProfile(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getProfile(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getProfile(jsonReq);
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getChangePin(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();

        String username = null;
        String pinLama = null;
        String pinBaru = null;

        try {
            username = jsonReq.get("username").toString();
            pinLama = jsonReq.get("pin").toString();
            pinBaru = jsonReq.get("pinBaru").toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Missing Paramater");
            return jsonResp.toString();
        }

        try {
            //---------------------process message-------------------------------
            //check user has login or not
            JSONObject jObjDataCookie = this.cookieHelper.getDataCookie(cookieClient);
            if (jObjDataCookie.containsKey("hasLogin")) {
                if (jObjDataCookie.get("hasLogin").toString().equals("true")) {
                    jsonResp = this.utilities.checkDeviceAgent(jsonReq);
                    if (jsonResp.get("ACK").equals("OK")) {
                        //resp = this.service.getChangePin(jsonReq);
                        String agent[] = this.service.StatusRekeningAgent(username);
                        if (pinLama.equals(agent[3])) {
                            if (!agent[3].equalsIgnoreCase(pinBaru)) {
                                resp = this.service.changePin(username, pinBaru);
                            } else {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "PIN baru tidak boleh sama dengan PIN lama");
                                resp = jsonResp.toString();
                            }
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "PIN lama Anda salah");
                            resp = jsonResp.toString();
                        }
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "session expire or not login");
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "session expire or not login");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getOtpResetPin(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();

        String username = null;

        try {
            username = jsonReq.get("idAgentAcc").toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Missing Paramater");
            return jsonResp.toString();
        }

        if (username.startsWith("0")) {
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "ID Account tidak ditemukan");
            resp = jsonResp.toString();
        } else {
            String requestBody = "{\"id_account\":\"" + username + "\",\"type\":\"token_reguler\"}";
            String responseBody = this.service.requestToken(requestBody);
            JSONObject curlResp = null;
            try {
                curlResp = (JSONObject) parser.parse(responseBody);

            } catch (ParseException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
                jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                jsonResp.put("pesan", "Terjadi kesalahan, silakan request token kembali");
                resp = jsonResp.toString();
            }
            if (curlResp.get("ACK").toString().equalsIgnoreCase(String.valueOf(Constant.ACK.OK))) {
                jsonResp.put("ACK", curlResp.get("ACK").toString());
                jsonResp.put("pesan", utilities.getWording("NOTIF_SEND_TOKEN"));
                resp = jsonResp.toString();
            } else {
                jsonResp.put("ACK", curlResp.get("ACK").toString());
                jsonResp.put("pesan", curlResp.get("pesan").toString());
                resp = jsonResp.toString();
            }
        }

        return resp;
    }

    public String getResetPin(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();

        String username = null;
        String otp = null;
        String idtrx = null;

        try {
            username = jsonReq.get("idAgentAcc").toString();
            otp = jsonReq.get("otp").toString();
            idtrx = jsonReq.get("idtrx").toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Missing Paramater");
            return jsonResp.toString();
        }

        String curlResponse = this.service.validasiTokenReguler(username, otp);
        try {
            JSONObject jObjCurlResp = (JSONObject) parser.parse(curlResponse);
            if (jObjCurlResp.get("ACK").toString().equalsIgnoreCase(String.valueOf(Constant.ACK.OK))) {
                String pin = utilities.GeneratePIN();
                String pinEncrypt = this.encDec.encryptDbVal(pin, this.entityConfig.getEncKeyDb());
                this.service.updatePin(username, pinEncrypt);
                jsonResp.put("ACK", "OK");
                jsonResp.put("pesan", "Terimakasih, permintaan Anda sedang diproses");
                resp = jsonResp.toString();
                String pesan = String.format(utilities.getWording("NOTIF_FORGOT_PIN_AGENT"), username, pin);
                new GcmNew().sent(idtrx, username, pesan, 1, true, this.entityConfig);
            } else {
                jsonResp.put("ACK", jObjCurlResp.get("ACK").toString());
                jsonResp.put("pesan", jObjCurlResp.get("pesan").toString());
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", "Terjadi kesalahan, silakan coba kembali");
            resp = jsonResp.toString();
        }

        return resp;
    }
}
