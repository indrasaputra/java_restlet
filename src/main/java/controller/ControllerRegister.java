package controller;

import com.google.common.base.Throwables;
import java.io.File;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.restlet.data.MediaType;
import org.restlet.representation.StringRepresentation;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleRegister;
import interfc.InterfcRegister;

/**
 *
 * @author hasan
 */
public class ControllerRegister {

    private final EntityConfig entityConfig;
    private final InterfcRegister service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerRegister() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleRegister(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getKelurahan(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            resp = this.service.getKelurahan(jsonReq);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getRegister(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        //prepare file upload
        JSONObject jObjDataFile = new JSONObject();
        String fileKtp = jsonReqRaw.get("fileUploadTmp1").toString();
        this.utilities.moveFile(entityConfig.getMasterFolder() + "uploadFile/register_agent/", fileKtp);
        jObjDataFile.put("fileKtp", fileKtp);
        
        String fileFotoDiri = jsonReqRaw.get("fileUploadTmp2").toString();
        this.utilities.moveFile(entityConfig.getMasterFolder() + "uploadFile/register_agent/", fileFotoDiri);
        jObjDataFile.put("fileFotoDiri", fileFotoDiri);
        jsonReq.put("fileFoto", jObjDataFile);

        JSONObject jObjDataDiri = (JSONObject) jsonReq.get("dataPribadi");
        JSONObject jObjDataToko = (JSONObject) jsonReq.get("dataToko");

        try {
            //---------------------process message-------------------------------
            resp = this.service.getRegister(jsonReq);
            JSONObject jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.get("ACK").toString().equals("NOK")) {
                //delete file ktp
                File file = new File(entityConfig.getMasterFolder() + "uploadFile/register_agent/" + fileKtp);
                if (file.delete()) {
                    /*code if sucess delete*/
                    System.out.println("file ktp deleted");
                }
                //delete file foto diri
                file = new File(entityConfig.getMasterFolder() + "uploadFile/register_agent/" + fileFotoDiri);
                if (file.delete()) {
                    /*code if sucess delete*/
                    System.out.println("file foto diri deleted");
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getOtp(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            resp = this.service.getOtp(jsonReq);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getReferral (JSONObject jsonReqRaw, String cookieClient) {
        String resp = null; //"{\"ACK\":\"OK\",\"pesan\":\"valid\"}";
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            resp = this.service.getReferral(jsonReq);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }
}
