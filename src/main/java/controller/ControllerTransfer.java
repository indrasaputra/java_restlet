package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImplePembayaranKomunitas;
import implement.ImpleTransfer;
import interfc.InterfcPembayaranKomunitas;
import interfc.InterfcTransfer;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author hasan
 */
public class ControllerTransfer {

    private final EntityConfig entityConfig;
    private final InterfcTransfer service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerTransfer() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleTransfer(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getInq(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getInq(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getPay(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String username = jsonReq.get("username").toString();
                String pin = jsonReq.get("PIN").toString();
                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    if (pin.equals(rekeningAgentNonEDC[4])) {
                        resp = this.service.getPay(jsonReq);
                        JSONObject respObj = (JSONObject) parser.parse(resp);
                        if(respObj.get("ACK").equals("OK")){
                            //send sms
                            //this.service.sendSms(jsonReq, respObj);
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "PIN Anda salah");
                        resp = jsonResp.toString();
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Rekening Anda Diblokir. Silakan Hubungi Customer Care Kami.");
                    resp = jsonResp.toString();
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getInqTerimaUang(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getInqTerimaUang(jsonReq);
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getPayTerimaUang(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String username = jsonReq.get("username").toString();
                String pin = jsonReq.get("PIN").toString();
                String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(username);
                String statusRekening = rekeningAgentNonEDC[0];
                if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                        || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {
                    if (pin.equals(rekeningAgentNonEDC[4])) {
                        resp = this.service.getPayTerimaUang(jsonReq);
                        JSONObject respObj = (JSONObject) parser.parse(resp);
                        if(respObj.get("ACK").equals("OK")){
                            //send sms
                            //this.service.sendSms(jsonReq, respObj);
                        }
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "PIN Anda salah");
                        resp = jsonResp.toString();
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Rekening Anda Diblokir. Silakan Hubungi Customer Care Kami.");
                    resp = jsonResp.toString();
                }
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }
}
