package controller;

import com.google.common.base.Throwables;
import entity.EntityConfig;
import helper.Constant;
import helper.CookieHelper;
import helper.Helper;
import helper.Utilities;
import implement.ImpleTransferBank;
import interfc.InterfcTransferBank;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Created by Fino Indrasaputra on 20/07/2018.
 */
public class ControllerTransferBank {
    private final EntityConfig entityConfig;
    private final InterfcTransferBank service;
    private final Cache cacheConfig;
    private final ControllerEncDec encDec;
    private Cache cacheResponse;
    private final Helper hlp;
    private final Utilities utilities;
    private final CookieHelper cookieHelper;
    private final ControllerLog controllerLog;

    public ControllerTransferBank() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.service = new ImpleTransferBank(this.entityConfig);
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.cookieHelper = new CookieHelper();
        this.controllerLog = new ControllerLog();
    }

    public String getBank(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                resp = this.service.getBank();
            } else {
                resp = jsonResp.toString();
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return resp;
    }

    public String getInqBank(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        int Hargacetak = 0;
        int nominal = 0;
        String PIN = null;
        String id_tipetransaksi = "6";
        String createId = this.hlp.createID();
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();

        jsonResp.put("ID", "inquiry");
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                String username = jsonReq.get("username").toString();
                nominal = Integer.parseInt(jsonReq.get("nominal").toString());
                int idSupplier = this.service.getIdSupplier(jsonReq);
                if (nominal >= 50000) {
                    //String[] rekeningMemberNonEDC = this.service.StatusRekeningMemberNonEDC(username);
                    //String statusRekening = rekeningMemberNonEDC[0];
                    //String ID_Member = rekeningMemberNonEDC[4];
                    String minPayment = this.service.getMinPayment(id_tipetransaksi);
                    String maxPayment = this.service.getMaxPayment(id_tipetransaksi);
                    String notifMinPay = this.service.getNotifMinPay();
                    String notifMaxPay = this.service.getNotifMaxPay();

                    if (idSupplier != 0) {
                        //if (username.startsWith("0")) {
                            if (nominal >= Integer.parseInt(minPayment)) {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", notifMinPay);
                            } else if (nominal >= Integer.parseInt(maxPayment)) {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", notifMaxPay);
                                return jsonResp.toString();
                            }
                            if (!this.service.cekStatus(username)) {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Transaksi tidak dapat diproses. Silahkan hubungi Customer Service TrueMoney untuk melengkapi data Anda");
                                return jsonResp.toString();
                            } else {
                                String BeneficiaryAccount = jsonReq.get("BeneficiaryAccount").toString();
                                String Kode_Bank = jsonReq.get("kodeBank").toString();

                                if (this.service.cekNorek(BeneficiaryAccount, Kode_Bank, "member")) {
                                    jsonResp = this.service.prosesInq(jsonReq, jsonResp, idSupplier);
                                } else {
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan", this.utilities.getWording("ERROR_BLACKLIST_T2B"));
                                    return jsonResp.toString();
                                }
                            }
                        /*} else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Untuk sementara transaksi tidak dapat dilakukan");
                            jsonResp.put("suppid", idSupplier);
                            return jsonResp.toString();
                        }*/
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Agen tidak dapat melakukan transfer");
                        return jsonResp.toString();
                    }

                } else {
                    String minPayment = this.service.getMinPayment(id_tipetransaksi);
                    String notifMinPay = this.service.getNotifMinPay();
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", notifMinPay);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
            return jsonResp.toString();
        }
        resp = jsonResp.toString();
        return resp;
    }

    public String transferBank(JSONObject jsonReqRaw, String cookieClient) {
        String resp = null;
        String createId = this.hlp.createID();
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonReq = (JSONObject) jsonReqRaw.get("action");
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ID", "inquiry");
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");

        String id_tipetransaksi = "6";
        String NoKartu = null;
        int Nominal = 0;
        String Kode_Bank = null;
        String NORek = null;
        String verifikasi = "PIN";
        String SenderName = null;
        String namaPenerima = null;
        String keteranganPenerima = null;
        String messageKey = null;
        int BiayaTransferBank = 0;
        int Hargacetak = 0;
        String PIN = null;
        String gen_id = null;
        String ktp = null;

        try {
            //---------------------process message-------------------------------
            jsonResp = this.utilities.checkDeviceAgent(jsonReq);
            if (jsonResp.get("ACK").equals("OK")) {
                NoKartu = jsonReq.get("username").toString();
                Nominal = Integer.parseInt(jsonReq.get("nominal").toString());
                Kode_Bank = jsonReq.get("kodeBank").toString();
                NORek = jsonReq.get("BeneficiaryAccount").toString();
//                SenderName = jsonReq.get("senderName").toString();
//                namaPenerima = jsonReq.get("BeneficiaryName").toString();
//                keteranganPenerima = jsonReq.get("transferIssue").toString();
//                messageKey = jsonReq.get("messageKey").toString();
//                BiayaTransferBank = Integer.parseInt(jsonReq.get("biayaTransferBank").toString());
                Hargacetak = Integer.parseInt(jsonReq.get("hargaCetak").toString());
                PIN = jsonReq.get("PIN").toString();
                gen_id = "TMN" + jsonReq.get("idtrx").toString();
//                ktp = jsonReq.get("ktp").toString();

                if (Nominal >= 50000) {
                    int count = this.service.gettrxid(gen_id);
                    if (count == 1) {
                        String status[] = this.service.getStatusTrx(gen_id);
                        String m = "";
                        String ack = "";
                        if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.SUKSES))) {
                            m = String.valueOf(Constant.Pesan.SUKSES.getPesan());
                            ack = String.valueOf(Constant.ACK.OK);
                        } else if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.GAGAL))) {
                            m = String.valueOf(Constant.Pesan.GAGAL.getPesan());
                            ack = String.valueOf(Constant.ACK.NOK);
                        } else if (status[0].equalsIgnoreCase(String.valueOf(Constant.StatusTrx.PENDING))) {
                            m = String.valueOf(Constant.Pesan.PENDING.getPesan());
                            ack = String.valueOf(Constant.ACK.SUSPECT);
                        }
                        jsonResp.put("statusTRX", status[0]);
                        jsonResp.put("ACK", ack);
                        jsonResp.put("pesan", m);
                        return jsonResp.toString();
                    } else if (count > 1) {
                        jsonResp.put("statusTRX", String.valueOf(Constant.StatusTrx.GAGAL));
                        jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                        jsonResp.put("pesan", String.valueOf(Constant.Pesan.GAGAL.getPesan()));
                        return jsonResp.toString();
                    }

                    String[] rekeningAgentNonEDC = this.service.StatusRekeningAgentNonEDC(NoKartu);
                    String statusRekening = rekeningAgentNonEDC[2];
                    String ID_Agent = rekeningAgentNonEDC[3];
                    //String BeneficiaryCity = this.service.getCity(NoKartu);
                    String minPayment = this.service.getMinPayment(id_tipetransaksi);
                    String maxPayment = this.service.getMaxPayment(id_tipetransaksi);
                    String notifMinPay = this.service.getNotifMinPay();
                    String notifMaxPay = this.service.getNotifMaxPay();

                    if (!this.service.CekIsEmptyTransfer(NORek, Nominal, ID_Agent)) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Anda tidak dapat melakukan transfer, silakan coba 24 jam kemudian");
                        return jsonResp.toString();
                    }

                    if (this.service.cekNorek(NORek, Kode_Bank, "agent")) {
                        /*if (statusRekening.equals(String.valueOf(Constant.StatusAccount.ACTIVE))
                                || statusRekening.equals(String.valueOf(Constant.StatusAccount.INACTIVE))
                                || statusRekening.equals(String.valueOf(Constant.StatusAccount.DORMANT))) {*/
                            if (PIN.equals(rekeningAgentNonEDC[4])) {
                                String validasiSaldo = this.service.isSaldoCukup(ID_Agent, Hargacetak);
                                //System.out.println("validasisaldo ===== " + validasiSaldo);
                                if ("00".equalsIgnoreCase(validasiSaldo)) {
                                    if (Nominal >= Integer.parseInt(minPayment)) {
                                        if (Nominal <= Integer.parseInt(maxPayment)) {
                                            String[] com = this.service.getComission(6, 3, 36);
                                            jsonResp = this.service.prosesTransfer(jsonReq, jsonResp, ID_Agent, com);
                                        } else {
                                            jsonResp.put("ACK", "NOK");
                                            jsonResp.put("pesan", notifMaxPay);
                                        }
                                    } else {
                                        jsonResp.put("ACK", "NOK");
                                        jsonResp.put("pesan", notifMinPay);
                                    }
                                } else if (validasiSaldo.equalsIgnoreCase("01")) {
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan", "Saldo Anda tidak cukup");
                                } else {
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan", "Saldo melebihi limit member");
                                }
                            } else {
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "PIN Anda salah");

                                return jsonResp.toString();
                            }
                        /*} else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Rekening Anda Diblokir. Silakan Hubungi Customer Care Kami");
                        }*/
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Maaf, Transfer ke Bank ke Nomor Rekening Tujuan terdaftar diblock");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Minimal Transfer adalah Rp 50.000");
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            //ex.printStackTrace();
            jsonResp.put("ACK", String.valueOf(Constant.ACK.NOK));
            jsonResp.put("pesan", String.valueOf(Constant.Pesan.MISSINGPARAMETER.getPesan()));
            return jsonResp.toString();
        }
        resp = jsonResp.toString();
        return resp;
    }
}
