package helper;

/**
 * Created by Iman Khaeruddin on 23/01/2017.
 */
public class Constant {

    public static final String VERSION = "v1.4.5";

    public static String KUNCI = "4ss4l4mu4l41kumw4r0hm4tull4h!.wb";

    public static String key = "7d9b24c3-ca9f-43a9-9aa4-be82b9c496d7";

    public static int idSupplierBimaSakti = 15;
    public static int idSupplierVsi = 14;

    public static String KARTU_MEMBER = "KARTU_MEMBER";
    public static String AGENT = "AGENT";
    public static String MEMBER = "MEMBER";

    public static int idPromo = 2;
    public static String userId = "truemoneydev";
    public static String pin = "IIV9HHSQEOOQ106A";

    public enum StatusTrx {
        SUKSES("SUKSES"),
        GAGAL("GAGAL"),
        PENDING("PENDING"),
        OPEN("OPEN");

        private String status;

        StatusTrx(String vStatus) {
            this.status = vStatus;
        }

    }

    public enum ACK {
        OK("OK"),NOK("NOK"),SUSPECT("SUSPECT");

        private String ack;

        ACK(String ack) {
            this.ack = ack;
        }

    }

    public enum Pesan {
        SUKSES("Transaksi Anda sukes"),
        PENDING("Transaksi Anda sedang diproses"),
        GAGAL("Transaksi Anda gagal"),
        SALAHPIN("PIN Anda salah"),
        BALANCEINSUFICIENT("Saldo Anda tidak cukup"),
        BLOCKED("Rekening Anda diblokir. Silakan hubungi customer care kami"),
        MISSINGPARAMETER("Missing Parameter"),
        LIMIT20JT("Limit ID Account melampaui batas nominal transaksi"),
        OVERLIMITSALDOMEMBER("Saldo melebihi limit member"),
        VALIDASITRX("Anda tidak dapat melakukan transaksi, silakan coba 24 jam kemudian"),
        DUPLICATEKEYVALUE("Transaksi Anda sudah pernah dilakukan, silahkan cek mutasi"),
        AGENTKOMISIKOSONG("Agent komisi kosong, tidak dapat melakukan transaksi"),
        PRODUCTUNAVAILABLE("Mohon maaf, produk tidak tersedia");

        private String pesan;

        Pesan(String pesan) {
            this.pesan = pesan;
        }

        public String getPesan() {
            return pesan;
        }

    }

    public enum StatusAccount {
        ACTIVE("ACTIVE"),INACTIVE("INACTIVE"),DORMANT("DORMANT");

        private String statusAccount;

        StatusAccount(String statusAccount) {
            this.statusAccount = statusAccount;
        }

    }

    public class idStatusAccount {
        public static final int ACTIVE = 1;
        public static final int INACTIVE = 2;
        public static final int DORMANT = 3;
    }

    public enum StockType{
        MEMBER("Member"),
        AGENT("Agent"),
        TRUE("True"),
        DEALER("Dealer"),
        SUPPLIER("Supplier");

        private String stockType;

        StockType(String stockType) {
            this.stockType = stockType;
        }

        public String getStockType() {
            return stockType;
        }
    }

    public enum TypeTrx {
        WITHDRAW("WITHDRAW"),DEPOSIT("DEPOSIT");

        private String typeTrx;

        TypeTrx(String typeTrx) {
            this.typeTrx = typeTrx;
        }

    }

    public enum StatusDana {
        DEDUCT("DEDUCT"),REFUND("REFUND");

        private String statusDana;

        StatusDana(String statusDana) {
            this.statusDana = statusDana;
        }

    }

    public class rc{
        public static final String exception = "01";
    }
}
