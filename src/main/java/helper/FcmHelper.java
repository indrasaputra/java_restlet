package helper;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Hasan on 7/5/2018.
 */
public class FcmHelper {
    private final Cache cacheConfig;
    private final EntityConfig entityConfig;
    private final ControllerEncDec encDec;
    private Utilities utilities;
    private Helper helper;
    private ControllerLog controllerLog;

    public FcmHelper() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.utilities = new Utilities();
        this.helper = new Helper();
        this.controllerLog = new ControllerLog();
    }

    public void sendFcm(JSONArray jArrToken, JSONObject jObjMessage) {
        Sender sender = new Sender("AIzaSyDPjI3K2bB9G0Rcqhk9YqNLCSuNq5pKMcw"); //FCM API Key
        ArrayList<String> devicesList = new ArrayList();
        //deviceList should add from token id db
        for (int n = 0; n < jArrToken.size(); n++) {
            String token = (String) jArrToken.get(n);
            devicesList.add(token); //device token regId
            // do some stuff....
        }
        String reqId = helper.createID();
        controllerLog.logStreamWriter("Request to FCM; id: " + reqId + " ;body:" + jObjMessage.toString());
        String data = "TrueMoney";
        Message message = new Message.Builder()
                .collapseKey("1") // The key for all updates.
                .timeToLive(900) // Time in seconds to keep message queued if device offline.
                .delayWhileIdle(true) // Wait for device to become active before sending.
                .addData("message", data)
                .addData("comment", jObjMessage.toString())
                .build();
        MulticastResult result = null;
        try {
            final int retries = 3;
            result = sender.send(message, devicesList, retries);
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        controllerLog.logStreamWriter("Response from FCM; id: " + reqId + " ;body:" + result.toString());

        if (result.getResults() != null) {
            int canonicalRegId = result.getCanonicalIds();
            if (canonicalRegId != 0) {
                //jalankan update canonicalId ke database
                String newRegId = result.getResults().get(0).toString();
                newRegId = newRegId.replace("[ ", "");
                newRegId = newRegId.replace(" ]", "");
                String[] newArrayRegId = newRegId.split(" ");
                //update data ini NewStringRegId ke database
                String NewStringRegId = newArrayRegId[1].replace("canonicalRegistrationId=", "");
                //System.out.println("New regId : " + NewStringRegId);
            }
            //remove token if get result errorCode=NotRegistered
            if (result.getResults().get(0).toString().contains("NotRegistered")) {
                //remove token from gcm table
            }
        } else {
            int error = result.getFailure();
            controllerLog.logErrorWriter("Fcm server error " + String.valueOf(error));
        }
    }
}
