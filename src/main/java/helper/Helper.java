/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import com.google.common.base.Throwables;
import controller.ControllerLog;
import koneksi.DatabaseUtilitiesPgsql;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
//import bright.express.controller.ControllerLog;
import entity.EntityConfig;
import koneksi.DatabaseUtilitiesMysql;

import javax.xml.parsers.ParserConfigurationException;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FilenameUtils;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import sun.misc.BASE64Encoder;
import controller.ControllerEncDec;

/**
 * @author Hasan
 */
public class Helper {

    //    ControllerLog ctrlLog;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final Cache cacheConfig;
    private final EntityConfig entityConfig;
    private ControllerEncDec encDec;
    private Utilities utilities;
    private ControllerLog controllerLog;

    public Helper() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    public synchronized String createID() {
        String Id = null;
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.reset();
            messageDigest.update(Long.toString(System.nanoTime()).getBytes(Charset.forName("UTF8")));
            final byte[] resultByte = messageDigest.digest();
            Id = new String(Hex.encodeHex(resultByte));
        } catch (NoSuchAlgorithmException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return Id;
    }

    public BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();

        return resizedImage;
    }

    public BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {

        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        g.setComposite(AlphaComposite.Src);

        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        return resizedImage;
    }

    public String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();

            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);

            bos.close();
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return imageString;
    }

    public String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64String(imageByteArray);
    }

    public ArrayList<HashMap<String, Object>> executeQuery(EntityConfig conf, String query, JSONObject jObj) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        try {
            DatabaseUtilitiesMysql databaseUtilitiesMysql = new DatabaseUtilitiesMysql();
            try {
                conn = databaseUtilitiesMysql.getConnection(conf);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
            }
            st = conn.prepareStatement(query);

            Object json = this.parser.parse(jObj.toString());
            this.jsonObject = (JSONObject) json;
            for (int i = 1; i <= this.jsonObject.size(); i++) {
                JSONObject jsonobj = (JSONObject) this.jsonObject.get(String.valueOf(i));
                String getType = (String) jsonobj.get("type");
                String getVal = (String) jsonobj.get("val");
                if (getType.equals("string")) {
                    st.setString(i, getVal);
                } else if (getType.equals("timestamp")) {
                    st.setTimestamp(i, Timestamp.valueOf(getVal));
                } else if (getType.equals("int")) {
                    st.setInt(i, Integer.parseInt(getVal));
                }
            }
            rs = st.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            int colCount = metaData.getColumnCount();
            row = new ArrayList<>();
            //convert resultset to arraylist
            while (rs.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rs.getObject(i));
                }
                row.add(columns);
            }
        } catch (SQLException | ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                rs.close();
            } catch (Exception e) { /* ignored */ }
            try {
                st.close();
            } catch (Exception e) { /* ignored */ }
            try {
                conn.close();
            } catch (Exception e) { /* ignored */ }
        }
        return row;
    }

    public String executeUpdate(EntityConfig conf, String queryUpd, JSONObject jObjUpd) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            DatabaseUtilitiesMysql databaseUtilitiesMysql = new DatabaseUtilitiesMysql();
            try {
                conn = databaseUtilitiesMysql.getConnection(conf);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
            }
            st = conn.prepareStatement(queryUpd);

            Object json = this.parser.parse(jObjUpd.toString());
            this.jsonObject = (JSONObject) json;
            for (int i = 1; i <= this.jsonObject.size(); i++) {
                JSONObject jsonobj = (JSONObject) this.jsonObject.get(String.valueOf(i));
                String getType = (String) jsonobj.get("type");
                String getVal = (String) jsonobj.get("val");
                if (getType.equals("string")) {
                    st.setString(i, getVal);
                } else if (getType.equals("timestamp")) {
                    st.setTimestamp(i, Timestamp.valueOf(getVal));
                } else if (getType.equals("int")) {
                    st.setInt(i, Integer.parseInt(getVal));
                }
            }
            st.executeUpdate();
        } catch (SQLException | ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                rs.close();
            } catch (Exception e) { /* ignored */ }
            try {
                st.close();
            } catch (Exception e) { /* ignored */ }
            try {
                conn.close();
            } catch (Exception e) { /* ignored */ }
        }
        return null;
    }

    public JSONObject extractMultipart(Representation entity, EntityConfig entityConfig) {
        JSONObject json = new JSONObject();
        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1000240);
            RestletFileUpload upload = new RestletFileUpload(factory);
            FileItemIterator fileIterator = upload.getItemIterator(entity);
            String extFile = null;
            String nameFile = null;
            StringBuilder jsonMessageEncripted = null;
            while (fileIterator.hasNext()) {
                FileItemStream fi = fileIterator.next();
                if (!fi.getFieldName().equalsIgnoreCase("action")
                        && !fi.getFieldName().equalsIgnoreCase("fileUpload1")
                        && !fi.getFieldName().equalsIgnoreCase("fileUpload2")
                        && !fi.getFieldName().equalsIgnoreCase("fileUpload3")
                        && !fi.getFieldName().equalsIgnoreCase("fileUpload4")
                        && !fi.getFieldName().equalsIgnoreCase("fileUpload5")) {
                    StringBuilder openParam = new StringBuilder();
                    BufferedReader br = new BufferedReader(new InputStreamReader(fi.openStream()));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        openParam.append(line);
                    }
                    json.put(fi.getFieldName(), openParam.toString());
                } else if (fi.getFieldName().equals("action")) {
                    jsonMessageEncripted = new StringBuilder();
                    BufferedReader br = new BufferedReader(new InputStreamReader(fi.openStream()));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        jsonMessageEncripted.append(line);
                    }
                    //System.out.println(jsonMessageEncripted.toString());
                    byte[] decodedBytes = Base64.decodeBase64(jsonMessageEncripted.toString());
                    //----------------------parsing the json message-------------------------------
                    String queryJson = this.encDec.getDecrypt(decodedBytes);
                    JSONObject jsonReq = (JSONObject) parser.parse(queryJson);
                    json.put("action", jsonReq);
                } else if (fi.getFieldName().equals("fileUpload1")) {
                    //System.out.println("origin file name : " + fi.getName());
                    extFile = FilenameUtils.getExtension(fi.getName());
                    String fileBefore = this.createID();
                    nameFile = fileBefore + "." + extFile;

                    InputStream attachmentStream = fi.openStream();
                    byte[] attachmentBytes;

                    //cara 1
//                    try {
//                        attachmentBytes = ByteStreams.toByteArray(attachmentStream);
//                    } finally {
//                        attachmentStream.close();
//                    }
                    //cara 2
                    ByteArrayOutputStream out = null;
                    try {
                        out = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024]; // you can configure the buffer size
                        int length;
                        while ((length = attachmentStream.read(buffer)) != -1) {
                            out.write(buffer, 0, length); //copy streams
                        }
                        attachmentBytes = out.toByteArray();
                    } finally {
                        attachmentStream.close();
                        out.close();
                    }

                    FileOutputStream fos = new FileOutputStream(entityConfig.getWorkDir() + "tmp" + File.separator + nameFile);
                    try {
                        fos.write(attachmentBytes);
                    } finally {
                        fos.close();
                    }
                    json.put("fileUploadTmp1", nameFile);
                } else if (fi.getFieldName().equals("fileUpload2")) {
                    //System.out.println("origin file name : " + fi.getName());
                    extFile = FilenameUtils.getExtension(fi.getName());
                    String fileBefore = this.createID();
                    nameFile = fileBefore + "." + extFile;

                    InputStream attachmentStream = fi.openStream();
                    byte[] attachmentBytes;

                    ByteArrayOutputStream out = null;
                    try {
                        out = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024]; // you can configure the buffer size
                        int length;
                        while ((length = attachmentStream.read(buffer)) != -1) {
                            out.write(buffer, 0, length); //copy streams
                        }
                        attachmentBytes = out.toByteArray();
                    } finally {
                        attachmentStream.close();
                        out.close();
                    }

                    FileOutputStream fos = new FileOutputStream(entityConfig.getWorkDir() + "tmp" + File.separator + nameFile);
                    try {
                        fos.write(attachmentBytes);
                    } finally {
                        fos.close();
                    }
                    json.put("fileUploadTmp2", nameFile);
                } else if (fi.getFieldName().equals("fileUpload3")) {
                    //System.out.println("origin file name : " + fi.getName());
                    extFile = FilenameUtils.getExtension(fi.getName());
                    String fileBefore = this.createID();
                    nameFile = fileBefore + "." + extFile;

                    InputStream attachmentStream = fi.openStream();
                    byte[] attachmentBytes;

                    ByteArrayOutputStream out = null;
                    try {
                        out = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024]; // you can configure the buffer size
                        int length;
                        while ((length = attachmentStream.read(buffer)) != -1) {
                            out.write(buffer, 0, length); //copy streams
                        }
                        attachmentBytes = out.toByteArray();
                    } finally {
                        attachmentStream.close();
                        out.close();
                    }

                    FileOutputStream fos = new FileOutputStream(entityConfig.getWorkDir() + "tmp" + File.separator + nameFile);
                    try {
                        fos.write(attachmentBytes);
                    } finally {
                        fos.close();
                    }
                    json.put("fileUploadTmp3", nameFile);
                } else if (fi.getFieldName().equals("fileUpload4")) {
                    //System.out.println("origin file name : " + fi.getName());
                    extFile = FilenameUtils.getExtension(fi.getName());
                    String fileBefore = this.createID();
                    nameFile = fileBefore + "." + extFile;

                    InputStream attachmentStream = fi.openStream();
                    byte[] attachmentBytes;

                    ByteArrayOutputStream out = null;
                    try {
                        out = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024]; // you can configure the buffer size
                        int length;
                        while ((length = attachmentStream.read(buffer)) != -1) {
                            out.write(buffer, 0, length); //copy streams
                        }
                        attachmentBytes = out.toByteArray();
                    } finally {
                        attachmentStream.close();
                        out.close();
                    }

                    FileOutputStream fos = new FileOutputStream(entityConfig.getWorkDir() + "tmp" + File.separator + nameFile);
                    try {
                        fos.write(attachmentBytes);
                    } finally {
                        fos.close();
                    }
                    json.put("fileUploadTmp4", nameFile);
                } else if (fi.getFieldName().equals("fileUpload5")) {
                    //System.out.println("origin file name : " + fi.getName());
                    extFile = FilenameUtils.getExtension(fi.getName());
                    String fileBefore = this.createID();
                    nameFile = fileBefore + "." + extFile;

                    InputStream attachmentStream = fi.openStream();
                    byte[] attachmentBytes;

                    ByteArrayOutputStream out = null;
                    try {
                        out = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024]; // you can configure the buffer size
                        int length;
                        while ((length = attachmentStream.read(buffer)) != -1) {
                            out.write(buffer, 0, length); //copy streams
                        }
                        attachmentBytes = out.toByteArray();
                    } finally {
                        attachmentStream.close();
                        out.close();
                    }

                    FileOutputStream fos = new FileOutputStream(entityConfig.getWorkDir() + "tmp" + File.separator + nameFile);
                    try {
                        fos.write(attachmentBytes);
                    } finally {
                        fos.close();
                    }
                    json.put("fileUploadTmp5", nameFile);
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return json;
    }

    public String getParamMsParameter(String paramName) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        String url = "";
        try {
            //get msparam
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, paramName);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return url;
    }

    public String requestToGwSocket(JSONObject payloadRequest, String paramName) {
        String paramValue = this.getParamMsParameter(paramName);
        String[] param = paramValue.split(":");
        InetSocketAddress socketAddress = new InetSocketAddress(param[0], Integer.parseInt(param[1]));
        final Socket socket = new Socket();
        final int tTimeout = 1000 * 120;
        final byte cEndMessageByte = -0x01;

        ControllerLog ctrlLog = new ControllerLog();
        ctrlLog.logStreamWriter("Upline -> Req : " + socketAddress.getAddress() + ":" + socketAddress.getPort()
                + " " + payloadRequest.toString());

        String tResponse = "";
        try {
            socket.setSoTimeout(tTimeout);
            socket.connect(socketAddress, tTimeout);

            ByteArrayOutputStream tRequestByteStream = new ByteArrayOutputStream();
            tRequestByteStream.write(payloadRequest.toString().getBytes());
            tRequestByteStream.write(cEndMessageByte);
            socket.getOutputStream().write(tRequestByteStream.toByteArray());

            byte tMessageByte = cEndMessageByte;
            StringBuffer sb = new StringBuffer();
            while ((tMessageByte = (byte) socket.getInputStream().read()) != cEndMessageByte) {
                sb.append((char) tMessageByte);
            }
            tResponse = sb.toString();
            ctrlLog.logStreamWriter("Upline -> Resp : " + tResponse);
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return tResponse;
    }
}
