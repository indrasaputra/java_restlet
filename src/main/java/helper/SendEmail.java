package helper;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import java.io.File;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import koneksi.DatabaseUtilitiesPgsql;

public class SendEmail {

    public void send(String email, String otp, String data) {
        try {
            SendEmailThread gcm = new SendEmailThread(email, otp, data);
            Thread t = new Thread(gcm);
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class SendEmailThread implements Runnable {

    private String data;
    private String email;
    private String otp;
    private Cache cacheConfig;
    private EntityConfig entityConfig;

    public SendEmailThread(String email, String otp, String data) throws ParseException {
        this.data = data;
        this.email = email;
        this.otp = otp;
    }

    @Override
    public void run() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            String senderuser = null;
            String senderpassword = null;
            String port = null;
            String host = null;

            String from = "Info TrueMoney";
            String subject = "";
            String header = "";
            String emailBody = "";

            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String sql = "SELECT * FROM tb_mailserver WHERE keyword = 'NOREPLY'"
                    + " AND flagactive = TRUE ORDER BY \"timestamp\" DESC LIMIT 1";

            PreparedStatement ps = connPgsql.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                senderuser = rs.getString("username");
                senderpassword = rs.getString("password");
                port = rs.getString("port");
                host = rs.getString("host");
            }

            final String username = senderuser;
            final String password = senderpassword;

            Properties props = new Properties();
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.starttls.required", "true");
            // props.put("mail.debug", "true");
            props.put("mail.smtp.host", host);

            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            try {
                message.setFrom(new InternetAddress(username, from));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Set To: header field of the header.
            InternetAddress[] toArray = InternetAddress.parse(email);
            message.setRecipients(Message.RecipientType.TO, toArray);

            if (otp.equalsIgnoreCase("")) {
                Gson gson = new Gson();
                JSONParser jSONParser = new JSONParser();
                JSONObject jObjReq = (JSONObject) jSONParser.parse(data);
                JSONObject jObjDataDiri = (JSONObject) jObjReq.get("dataPribadi");

                subject = "[TrueMoney Notification] Pendaftaran Agent Sukses";

                header = "[TrueMoney Notification] Pendaftaran Agent";

                emailBody = "Yth. Sdr/i.  <b>" + jObjDataDiri.get("namaAgen").toString().toUpperCase() + "</b>"
                        + "<br><br>Terima kasih sudah mendaftar menjadi agent TrueMoney. <br>Data berhasil diterima, silakan menunggu (Max 2 hari) untuk aktivasi Account. "
                        + "<br><br>" + "Terima kasih. <br> Salam hangat." + "<br><br><b>"
                        + "Tim Support Truemoney.</b> <br> Info:(0804)1000100.";
            } else {
                subject = "OTP - TrueMoney Indonesia";

                header = subject;

                emailBody = "Berikut adalah OTP Anda, " + otp + ". <br><br>";
            }

            // Set Subject: header field
            message.setSubject(subject);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Initialize velocity
            VelocityEngine ve = new VelocityEngine();
            ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
            ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
            ve.init();

            /* next, get the Template */
            Template t = ve.getTemplate("mail-template.vm");

            /* create a context and add data */
            VelocityContext context1 = new VelocityContext();
            context1.put("headerTemplate", header);
            context1.put("bodyTemplate", emailBody);

            /* now render the template into a StringWriter */
            StringWriter out = new StringWriter();
            t.merge(context1, out);

            messageBodyPart.setContent(out.toString(), "text/html");

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

}
