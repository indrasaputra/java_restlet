package helper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.sql.DataSource;

import com.google.common.base.Throwables;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import controller.ControllerLog;
import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfObject;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import entity.EntityConfig;
import koneksi.DatabaseUtilitiesPgsql;

public class SendEmailKereta {

    public void send(String email, String data) {
        try {
            SendEmailThreadKereta gcm = new SendEmailThreadKereta(email, data);
            Thread t = new Thread(gcm);
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class SendEmailThreadKereta implements Runnable {

    private String jsonDataString;
    private String email;
    private Cache cacheConfig;
    private EntityConfig entityConfig;
    private Helper helper;
    private Utilities utilities;
    private ControllerLog controllerLog;

    public SendEmailThreadKereta(String email, String data) throws ParseException {
        this.jsonDataString = data;
        this.email = email;
        this.helper = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public void run() {
        CacheManager cacheManager = CacheManager.getInstance();
        this.cacheConfig = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cacheConfig.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        String path = entityConfig.getMasterFolder() + File.separator + "tmp" + File.separator;
        String filename1 = "";
        String filename2 = "";
        try {
            String senderuser = null;
            String senderpassword = null;
            String port = null;
            String host = null;

            String from = "Info TrueMoney";
            String subject = "";
            String header = "";
            String emailBody = "";

            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String sql = "SELECT * FROM tb_mailserver WHERE keyword = 'NOREPLY'"
                    + " AND flagactive = TRUE ORDER BY \"timestamp\" DESC LIMIT 1";

            PreparedStatement ps = connPgsql.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                senderuser = rs.getString("username");
                senderpassword = rs.getString("password");
                port = rs.getString("port");
                host = rs.getString("host");
            }

            final String username = senderuser;
            final String password = senderpassword;

            Properties props = new Properties();
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.starttls.required", "true");
            // props.put("mail.debug", "true");
            props.put("mail.smtp.host", host);

            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(username, from));

            // Set To: header field of the header.
            InternetAddress[] toArray = InternetAddress.parse(email);
            message.setRecipients(Message.RecipientType.TO, toArray);

            // Set Subject: header field
            subject = "[TrueMoney Notification] Pembelian Tiket Kereta Api";
            message.setSubject(subject);

            // Initialize velocity
            VelocityEngine ve = new VelocityEngine();
            ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
            ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
            ve.init();

            /* next, get the Template */
            Template template = ve.getTemplate("mail-template-kereta.vm");

            emailBody = this.genEmailContent(jsonDataString);

            JSONParser jSONParser = new JSONParser();
            JSONObject jObjReq = (JSONObject) jSONParser.parse(jsonDataString);
            JSONObject jObjReqOrig = (JSONObject) jObjReq.get("jObjReq");
            JSONObject jObjBundle = (JSONObject) jObjReqOrig.get("jObjBundle");
            JSONArray jArrDataDewasa = (JSONArray) jObjBundle.get("dataDewasa");
            JSONObject jObjDataDewasa1 = (JSONObject) jArrDataDewasa.get(0);
            JSONArray jArrDataAnak = (jObjBundle.containsKey("dataAnak")) ? (JSONArray) jObjBundle.get("dataAnak") : new JSONArray();
            JSONObject jObjDataPemesan = (JSONObject) jObjBundle.get("dataPemesan");

            /* create a context and add data */
            VelocityContext context = new VelocityContext();
            context.put("bodyTemplate", emailBody);
            context.put("greetings", "<b>Yth sdr/i. " + jObjDataDewasa1.get("nama").toString() + " </b><br>Terima kasih sudah melakukan pembelian tiket kereta api.");

            /* now render the template into a StringWriter */
            StringWriter out = new StringWriter();
            template.merge(context, out);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(out.toString(), "text/html");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            //------------------------create file attachment pergi-----------------------------------------
            template = ve.getTemplate("mail-template-kereta-attachment.vm");

            /* create a context and add data */
            context = new VelocityContext();
            context.put("kodeBooking", jObjReq.get("bookPergi").toString());
            context.put("kodeTransaksi", jObjReqOrig.get("trxId").toString());
            context.put("tglPemesanan", this.utilities.getDate("dd-MM-yyyy"));
            context.put("namaPelanggan", jObjDataPemesan.get("nama").toString());
            context.put("tlp", jObjDataPemesan.get("noHp").toString());
            context.put("email", jObjDataPemesan.get("email").toString());

            emailBody = this.genEmailAttachmentPenumpangPergi(jsonDataString);
            context.put("rincianPenumpang", emailBody);

            emailBody = this.genEmailAttachmentJadwalPergi(jsonDataString);
            context.put("rincianJadwal", emailBody);

            context.put("jmlDewasa", jArrDataDewasa.size() + " Dewasa");
            context.put("jmlAnak", jArrDataAnak.size() + " Bayi");

            double dbl = Double.parseDouble(jObjReqOrig.get("totalBiaya").toString());
            String dblStr = String.format("%,.0f", dbl);
            dblStr = dblStr.replaceAll(",", ".");
            context.put("totalBiaya", "Rp." + dblStr);

            dbl = Double.parseDouble(jObjReqOrig.get("totalAdmin").toString());
            dblStr = String.format("%,.0f", dbl);
            dblStr = dblStr.replaceAll(",", ".");
            context.put("totalAdmin", "Rp." + dblStr);

            dbl = Double.parseDouble(jObjReqOrig.get("totalDiskon").toString());
            dblStr = String.format("%,.0f", dbl);
            dblStr = dblStr.replaceAll(",", ".");
            context.put("totalDiskon", "Rp." + dblStr);

            dbl = Double.parseDouble(jObjReqOrig.get("totalBayar").toString());
            dblStr = String.format("%,.0f", dbl);
            dblStr = dblStr.replaceAll(",", ".");
            context.put("hargaCetak", "Rp." + dblStr);

            context.put("tglBayar", utilities.getDate("dd-MM-yyyy"));

            dbl = Double.parseDouble(jObjReqOrig.get("totalBayar").toString());
            dblStr = String.format("%,.0f", dbl);
            dblStr = dblStr.replaceAll(",", ".");
            context.put("hargaBooking", "Rp." + dblStr);

            Helper helper = new Helper();
            filename1 = helper.createID();

            //create barcode
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(jObjReq.get("bookPergi").toString(), BarcodeFormat.QR_CODE, 350, 350);

            Path pathObj = FileSystems.getDefault().getPath(path + filename1 + ".png");
            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", pathObj);

            //make sure file barcode is already
            Thread.sleep(2500);

            context.put("qrLink", "<img src=\"http://127.0.0.1:" + (Integer.parseInt(entityConfig.getHttpPort())) + "/TrueMoneyApp/getBarcodePhoto/" + filename1 + ".png\"" + " width=\"600\" height=\"\" alt=\"\" border=\"0\" style=\"width: 60%; max-width: 800px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555; margin: auto;\" class=\"g-img\" />");

            out = new StringWriter();
            template.merge(context, out);

            String attachment = out.toString();

            //create pdf
            boolean success = HtmlToPdf.create()
                    .object(HtmlToPdfObject.forHtml(attachment))
                    .convert(path + filename1 + ".pdf");

            //make sure file barcode is already
            Thread.sleep(2500);

            //add atachment
            messageBodyPart = new MimeBodyPart();
            FileDataSource source = new FileDataSource(path + filename1 + ".pdf");
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename1 + ".pdf");
            multipart.addBodyPart(messageBodyPart);
            //---------------------------end create attachment pergi--------------------------------------------


            //------------------------create file attachment pulang-----------------------------------------
            if (jObjReq.containsKey("bookPulang")) {
                template = ve.getTemplate("mail-template-kereta-attachment.vm");

                /* create a context and add data */
                context = new VelocityContext();
                context.put("kodeBooking", jObjReq.get("bookPulang").toString());
                context.put("kodeTransaksi", jObjReqOrig.get("trxId").toString());
                context.put("tglPemesanan", this.utilities.getDate("dd-MM-yyyy"));
                context.put("namaPelanggan", jObjDataPemesan.get("nama").toString());
                context.put("tlp", jObjDataPemesan.get("noHp").toString());
                context.put("email", jObjDataPemesan.get("email").toString());

                emailBody = this.genEmailAttachmentPenumpangPulang(jsonDataString);
                context.put("rincianPenumpang", emailBody);

                emailBody = this.genEmailAttachmentJadwalPulang(jsonDataString);
                context.put("rincianJadwal", emailBody);

                context.put("jmlDewasa", jArrDataDewasa.size() + " Dewasa");
                context.put("jmlAnak", jArrDataAnak.size() + " Bayi");

                dbl = Double.parseDouble(jObjReqOrig.get("totalBiaya").toString());
                dblStr = String.format("%,.0f", dbl);
                dblStr = dblStr.replaceAll(",", ".");
                context.put("totalBiaya", "Rp." + dblStr);

                dbl = Double.parseDouble(jObjReqOrig.get("totalAdmin").toString());
                dblStr = String.format("%,.0f", dbl);
                dblStr = dblStr.replaceAll(",", ".");
                context.put("totalAdmin", "Rp." + dblStr);

                dbl = Double.parseDouble(jObjReqOrig.get("totalDiskon").toString());
                dblStr = String.format("%,.0f", dbl);
                dblStr = dblStr.replaceAll(",", ".");
                context.put("totalDiskon", "Rp." + dblStr);

                dbl = Double.parseDouble(jObjReqOrig.get("totalBayar").toString());
                dblStr = String.format("%,.0f", dbl);
                dblStr = dblStr.replaceAll(",", ".");
                context.put("hargaCetak", "Rp." + dblStr);

                context.put("tglBayar", utilities.getDate("dd-MM-yyyy"));

                dbl = Double.parseDouble(jObjReqOrig.get("totalBayar").toString());
                dblStr = String.format("%,.0f", dbl);
                dblStr = dblStr.replaceAll(",", ".");
                context.put("hargaBooking", "Rp." + dblStr);

                path = entityConfig.getMasterFolder() + File.separator + "tmp" + File.separator;
                filename2 = helper.createID();

                //create barcode
                qrCodeWriter = new QRCodeWriter();
                bitMatrix = qrCodeWriter.encode(jObjReq.get("bookPulang").toString(), BarcodeFormat.QR_CODE, 350, 350);

                pathObj = FileSystems.getDefault().getPath(path + filename2 + ".png");
                MatrixToImageWriter.writeToPath(bitMatrix, "PNG", pathObj);

                //make sure file barcode is already
                Thread.sleep(2500);

                context.put("qrLink", "<img src=\"http://127.0.0.1:" + (Integer.parseInt(entityConfig.getHttpPort())) + "/TrueMoneyApp/getBarcodePhoto/" + filename2 + ".png\"" + " width=\"600\" height=\"\" alt=\"\" border=\"0\" style=\"width: 60%; max-width: 800px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555; margin: auto;\" class=\"g-img\" />");

                out = new StringWriter();
                template.merge(context, out);

                attachment = out.toString();

                //create pdf
                success = HtmlToPdf.create()
                        .object(HtmlToPdfObject.forHtml(attachment))
                        .convert(path + filename2 + ".pdf");

                //make sure file barcode is already
                Thread.sleep(2500);

                //add atachment
                messageBodyPart = new MimeBodyPart();
                source = new FileDataSource(path + filename2 + ".pdf");
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename2 + ".pdf");
                multipart.addBodyPart(messageBodyPart);
                //---------------------------end create attachment pulang--------------------------------------------
            }

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }

    }

    public String genEmailContent(String jStringReq) {
        String emailBody = "";
        try {
            JSONParser jSONParser = new JSONParser();
            JSONObject jObjReq = (JSONObject) jSONParser.parse(jStringReq);

            emailBody = "<tr>" +
                    "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                    jObjReq.get("bookPergi").toString().toUpperCase() +
                    "</td>" +
                    "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                    jObjReq.get("noKeretaPergi").toString() +
                    "</td>" +
                    "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                    jObjReq.get("namaKeretaPergi").toString() +
                    "</td>" +
                    "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                    jObjReq.get("stasiunPergi").toString() +
                    "</td>" +
                    "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                    jObjReq.get("stasiunPergiTujuan").toString() +
                    "</td>" +
                    "</tr>";

            if (jObjReq.containsKey("bookPulang")) {
                emailBody = emailBody +
                        "<tr>" +
                        "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                        jObjReq.get("bookPulang").toString().toUpperCase() +
                        "</td>" +
                        "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                        jObjReq.get("noKeretaPulang").toString() +
                        "</td>" +
                        "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                        jObjReq.get("namaKeretaPulang").toString() +
                        "</td>" +
                        "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                        jObjReq.get("stasiunPulang").toString() +
                        "</td>" +
                        "<td  align=\"justify\" style=\"padding: 0px 0 0px 0; color: #444; font-family: Arial, sans-serif; font-size: 11px; line-height: 20px;\">" +
                        jObjReq.get("stasiunPulangTujuan").toString() +
                        "</td>" +
                        "</tr>";
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return emailBody;
    }

    public String genEmailAttachmentPenumpangPergi(String jStringReq) {
        String emailBody = "";
        try {
            JSONParser jSONParser = new JSONParser();
            JSONObject jObjReq = (JSONObject) jSONParser.parse(jsonDataString);
            JSONObject jObjReqOrig = (JSONObject) jObjReq.get("jObjReq");
            JSONObject jObjBundle = (JSONObject) jObjReqOrig.get("jObjBundle");
            JSONArray jArrDataDewasa = (JSONArray) jObjBundle.get("dataDewasa");
            JSONArray jArrDataAnak = new JSONArray();
            if(jObjBundle.containsKey("dataAnak")){
                jArrDataAnak = (JSONArray) jObjBundle.get("dataAnak");
            }

            String seatPergi = (String) jObjReqOrig.get("seatPergi");
            String[] seatPergiArr = seatPergi.split(",");

            for (int i = 0; i < jArrDataDewasa.size(); i++) {
                JSONObject jObjDewasa = (JSONObject) jArrDataDewasa.get(i);
                emailBody = emailBody +
                        "<tr style=\"background-color: white\">" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjDewasa.get("nama").toString() + "</p>" +
                        "</td>" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjDewasa.get("noTandaPengenal").toString() + "</p>" +
                        "</td>" +
                        "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + seatPergiArr[i] + "</p>" +
                        "</td>" +
                        "</tr>";
            }
            for (int i = 0; i < jArrDataAnak.size(); i++) {
                JSONObject jObjAnak = (JSONObject) jArrDataAnak.get(i);
                emailBody = emailBody +
                        "<tr style=\"background-color: white\">" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjAnak.get("nama").toString() + "</p>" +
                        "</td>" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjAnak.get("dob").toString() + "</p>" +
                        "</td>" +
                        "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">-</p>" +
                        "</td>" +
                        "</tr>";
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return emailBody;
    }

    public String genEmailAttachmentJadwalPergi(String jStringReq) {
        String emailBody = "";
        try {
            JSONParser jSONParser = new JSONParser();
            JSONObject jObjReq = (JSONObject) jSONParser.parse(jsonDataString);
            JSONObject jObjReqOrig = (JSONObject) jObjReq.get("jObjReq");
            JSONObject jObjBundle = (JSONObject) jObjReqOrig.get("jObjBundle");
            JSONArray jArrDataDewasa = (JSONArray) jObjBundle.get("dataDewasa");
            JSONObject jObjTrainPergi = (JSONObject) jObjBundle.get("trainPergi");

            emailBody = emailBody +
                    "<tr style=\"background-color: white\">" +
                    "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjTrainPergi.get("trainNo").toString() + "</p>" +
                    "</td>" +
                    "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjTrainPergi.get("trainNm").toString() + "</p>" +
                    "</td>" +
                    "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjBundle.get("orgName").toString() + ", " + utilities.convertFormatDate("yyyyMMdd", "dd-MM-yyyy", jObjTrainPergi.get("depDate").toString()) + " " + jObjTrainPergi.get("depTime").toString() + "</p>" +
                    "</td>" +
                    "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjBundle.get("desName").toString() + ", " + utilities.convertFormatDate("yyyyMMdd", "dd-MM-yyyy", jObjTrainPergi.get("arrvDate").toString()) + " " + jObjTrainPergi.get("arrvTime").toString() + "</p>" +
                    "</td>" +
                    "</tr>";
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return emailBody;
    }

    public String genEmailAttachmentPenumpangPulang(String jStringReq) {
        String emailBody = "";
        try {
            JSONParser jSONParser = new JSONParser();
            JSONObject jObjReq = (JSONObject) jSONParser.parse(jsonDataString);
            JSONObject jObjReqOrig = (JSONObject) jObjReq.get("jObjReq");
            JSONObject jObjBundle = (JSONObject) jObjReqOrig.get("jObjBundle");
            JSONArray jArrDataDewasa = (JSONArray) jObjBundle.get("dataDewasa");
            JSONArray jArrDataAnak = new JSONArray();
            if(jObjBundle.containsKey("dataAnak")){
                jArrDataAnak = (JSONArray) jObjBundle.get("dataAnak");
            }

            String seatPulang = (String) jObjReqOrig.get("seatPulang");
            String[] seatPulangArr = seatPulang.split(",");

            for (int i = 0; i < jArrDataDewasa.size(); i++) {
                JSONObject jObjDewasa = (JSONObject) jArrDataDewasa.get(i);
                emailBody = emailBody +
                        "<tr style=\"background-color: white\">" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjDewasa.get("nama").toString() + "</p>" +
                        "</td>" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjDewasa.get("noTandaPengenal").toString() + "</p>" +
                        "</td>" +
                        "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + seatPulangArr[i] + "</p>" +
                        "</td>" +
                        "</tr>";
            }
            for (int i = 0; i < jArrDataAnak.size(); i++) {
                JSONObject jObjAnak = (JSONObject) jArrDataAnak.get(i);
                emailBody = emailBody +
                        "<tr style=\"background-color: white\">" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjAnak.get("nama").toString() + "</p>" +
                        "</td>" +
                        "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">" + jObjAnak.get("dob").toString() + "</p>" +
                        "</td>" +
                        "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                        "<p style=\"margin: 0;\">-</p>" +
                        "</td>" +
                        "</tr>";
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return emailBody;
    }

    public String genEmailAttachmentJadwalPulang(String jStringReq) {
        String emailBody = "";
        try {
            JSONParser jSONParser = new JSONParser();
            JSONObject jObjReq = (JSONObject) jSONParser.parse(jsonDataString);
            JSONObject jObjReqOrig = (JSONObject) jObjReq.get("jObjReq");
            JSONObject jObjBundle = (JSONObject) jObjReqOrig.get("jObjBundle");
            JSONArray jArrDataDewasa = (JSONArray) jObjBundle.get("dataDewasa");
            JSONObject jObjTrainPulang = (JSONObject) jObjBundle.get("trainPulang");

            emailBody = emailBody +
                    "<tr style=\"background-color: white\">" +
                    "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjTrainPulang.get("trainNo").toString() + "</p>" +
                    "</td>" +
                    "<td style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjTrainPulang.get("trainNm").toString() + "</p>" +
                    "</td>" +
                    "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjBundle.get("desName").toString() + ", " + utilities.convertFormatDate("yyyyMMdd", "dd-MM-yyyy", jObjTrainPulang.get("depDate").toString()) + " " + jObjTrainPulang.get("depTime").toString() + "</p>" +
                    "</td>" +
                    "<td valign=\"top\" width=\"20%\" style=\"text-align: left; font-family: sans-serif; font-size: 11px; line-height: 20px; color: #555555; padding: 10px 10px 0;\">" +
                    "<p style=\"margin: 0;\">" + jObjBundle.get("orgName").toString() + ", " + utilities.convertFormatDate("yyyyMMdd", "dd-MM-yyyy", jObjTrainPulang.get("arrvDate").toString()) + " " + jObjTrainPulang.get("arrvTime").toString() + "</p>" +
                    "</td>" +
                    "</tr>";
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return emailBody;
    }

}
