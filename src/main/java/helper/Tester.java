package helper;

import controller.ControllerEncDec;
import entity.EntityConfig;
import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfObject;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Hasan on 6/22/2018.
 */
public class Tester {
    EntityConfig conf;

    public Tester(EntityConfig conf){
        this.conf = conf;
    }

    public void tesFcm(JSONArray fcmToken, JSONObject jObjMessage){
        FcmHelper fcmHelper = new FcmHelper();
        fcmHelper.sendFcm(fcmToken, jObjMessage);
    }

    public void tesEmailAms(){
        //tes email
            Map<String, String> map = new HashMap<>();
            map.put("Tipe", "sendRegisterAgentAndroid");
            map.put("idAgent", "13123");
            map.put("idAgentAccount", "94375983934");
            map.put("VarNama", "tes");
            map.put("VarHP", "0823423423");
            map.put("VarEmail", "acang.torvalds@gmail.com");
            map.put("VarKTP", "werewrww");

            new SendEmailAMSAgentReg().sent(map);
    }

    public void tesEmailKeretaApi(){
        try {
            //email to specific email for kereta api
            String jsonStr = "{\"totalBiaya\":\"335000\",\"stasiunPergi\":\"BD\",\"seatPulang\":\"EKON 1 \\/ 2A,EKON 1 \\/ 2B\",\"totalDiskon\":\"15000\",\"keretaPulang\":\"GAMBIR\",\"jamPergi\":\"22 Juni 2018 12:00\",\"noHp\":\"082190764546\",\"packageName\":\"com.truemoney.witami.agent\",\"trnType\":\"round-trip\",\"jObjBundle\":{\"orgName\":\"BANDUNG\",\"org\":\"BD\",\"jmlDewasa\":\"2\",\"jmlAnak\":\"2\",\"trainPulang\":{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,40 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nEkonomi (K)\",\"adultFare\":\"78.500\",\"arrvTime\":\"11:40\",\"destStationCode\":\"BD\",\"depTime\":\"08:00\",\"trainClass\":\"K\",\"trainNo\":\"11G\",\"avb\":\"105\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-11G-K-C\",\"jenisLayout\":\"common\"},\"switchPrintStruk\":false,\"depDate\":\"20180622\",\"dataDewasa\":[{\"titel\":\"Tuan\",\"nama\":\"hasan\",\"tandaPengenal\":\"KTP\",\"noTandaPengenal\":\"hdhbhx\",\"noHp\":\"082190764546\",\"email\":\"acang.torvalds@gmail.com\"},{\"titel\":\"Nyonya\",\"nama\":\"reny\",\"tandaPengenal\":\"SIM\",\"noTandaPengenal\":\"jdjhdc\"}],\"returnDate\":\"20180623\",\"des\":\"GMR\",\"respInqSchedule\":{\"pesan\":\"Sukses\",\"ResponseCode\":\"200\",\"ACK\":\"OK\",\"enRespMsg\":\"Success\",\"trnType\":\"round-trip\",\"respCode\":\"0\",\"returnList\":{\"destStationCode\":\"BD\",\"trainSchedule\":[{\"infantFare\":\"0\",\"trainSubClass\":\"A\",\"travelTime\":\"3 jam,40 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nEksekutif (E)\",\"adultFare\":\"109.000\",\"arrvTime\":\"11:40\",\"destStationCode\":\"BD\",\"depTime\":\"08:00\",\"trainClass\":\"E\",\"trainNo\":\"11G\",\"avb\":\"50\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-11G-E-A\"},{\"infantFare\":\"0\",\"trainSubClass\":\"B\",\"travelTime\":\"3 jam,40 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nBisnis (B)\",\"adultFare\":\"99.000\",\"arrvTime\":\"11:40\",\"destStationCode\":\"BD\",\"depTime\":\"08:00\",\"trainClass\":\"B\",\"trainNo\":\"11G\",\"avb\":\"80\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-11G-B-B\"},{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,40 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nEkonomi (K)\",\"adultFare\":\"78.500\",\"arrvTime\":\"11:40\",\"destStationCode\":\"BD\",\"depTime\":\"08:00\",\"trainClass\":\"K\",\"trainNo\":\"11G\",\"avb\":\"105\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-11G-K-C\"},{\"infantFare\":\"0\",\"trainSubClass\":\"A\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO DHOHO \\nEksekutif (E)\",\"adultFare\":\"360.000\",\"arrvTime\":\"12:00\",\"destStationCode\":\"BD\",\"depTime\":\"09:00\",\"trainClass\":\"E\",\"trainNo\":\"55G\",\"avb\":\"150\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-55G-E-A\"},{\"infantFare\":\"0\",\"trainSubClass\":\"B\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO DHOHO \\nBisnis (B)\",\"adultFare\":\"280.000\",\"arrvTime\":\"12:00\",\"destStationCode\":\"BD\",\"depTime\":\"09:00\",\"trainClass\":\"B\",\"trainNo\":\"55G\",\"avb\":\"239\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-55G-B-B\"},{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO DHOHO \\nEkonomi (K)\",\"adultFare\":\"200.000\",\"arrvTime\":\"12:00\",\"destStationCode\":\"BD\",\"depTime\":\"09:00\",\"trainClass\":\"K\",\"trainNo\":\"55G\",\"avb\":\"240\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-55G-K-C\"},{\"infantFare\":\"0\",\"trainSubClass\":\"A\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nEksekutif (E)\",\"adultFare\":\"110.000\",\"arrvTime\":\"15:00\",\"destStationCode\":\"BD\",\"depTime\":\"12:00\",\"trainClass\":\"E\",\"trainNo\":\"22G\",\"avb\":\"50\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-22G-E-A\"},{\"infantFare\":\"0\",\"trainSubClass\":\"B\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nBisnis (B)\",\"adultFare\":\"100.000\",\"arrvTime\":\"15:00\",\"destStationCode\":\"BD\",\"depTime\":\"12:00\",\"trainClass\":\"B\",\"trainNo\":\"22G\",\"avb\":\"80\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-22G-B-B\"},{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nEkonomi (K)\",\"adultFare\":\"89.000\",\"arrvTime\":\"15:00\",\"destStationCode\":\"BD\",\"depTime\":\"12:00\",\"trainClass\":\"K\",\"trainNo\":\"22G\",\"avb\":\"106\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-22G-K-C\"},{\"infantFare\":\"0\",\"trainSubClass\":\"A\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nEksekutif (E)\",\"adultFare\":\"109.000\",\"arrvTime\":\"19:00\",\"destStationCode\":\"BD\",\"depTime\":\"16:00\",\"trainClass\":\"E\",\"trainNo\":\"33G\",\"avb\":\"50\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-33G-E-A\"},{\"infantFare\":\"0\",\"trainSubClass\":\"B\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nBisnis (B)\",\"adultFare\":\"99.000\",\"arrvTime\":\"19:00\",\"destStationCode\":\"BD\",\"depTime\":\"16:00\",\"trainClass\":\"B\",\"trainNo\":\"33G\",\"avb\":\"80\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-33G-B-B\"},{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180623\",\"trainNm\":\"ARGO GOPAR \\nEkonomi (K)\",\"adultFare\":\"78.500\",\"arrvTime\":\"19:00\",\"destStationCode\":\"BD\",\"depTime\":\"16:00\",\"trainClass\":\"K\",\"trainNo\":\"33G\",\"avb\":\"106\",\"depDate\":\"20180623\",\"orgStationCode\":\"GMR\",\"trainClassId\":\"GMR-BD-20180623-33G-K-C\"}],\"orgStationCode\":\"GMR\",\"depDate\":\"20180623\"},\"inRespMsg\":\"Sukses\",\"departureList\":{\"destStationCode\":\"GMR\",\"trainSchedule\":[{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,10 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO PARAHYANGAN PREMIUM \\nEkonomi (K)\",\"adultFare\":\"99.000\",\"arrvTime\":\"07:25\",\"destStationCode\":\"GMR\",\"depTime\":\"04:15\",\"trainClass\":\"K\",\"trainNo\":\"10501\",\"avb\":\"790\",\"hourDiffTimeNow\":\"14\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-10501-K-C\",\"jenisLayout\":\"common\"},{\"infantFare\":\"0\",\"trainSubClass\":\"A\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO GOPAR \\nEksekutif (E)\",\"adultFare\":\"109.000\",\"arrvTime\":\"15:00\",\"destStationCode\":\"GMR\",\"depTime\":\"12:00\",\"trainClass\":\"E\",\"trainNo\":\"77A\",\"avb\":\"50\",\"hourDiffTimeNow\":\"21\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-77A-E-A\",\"jenisLayout\":\"common\"},{\"infantFare\":\"0\",\"trainSubClass\":\"B\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO GOPAR \\nBisnis (B)\",\"adultFare\":\"89.000\",\"arrvTime\":\"15:00\",\"destStationCode\":\"GMR\",\"depTime\":\"12:00\",\"trainClass\":\"B\",\"trainNo\":\"77A\",\"avb\":\"48\",\"hourDiffTimeNow\":\"21\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-77A-B-B\",\"jenisLayout\":\"common\"},{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO GOPAR \\nEkonomi (K)\",\"adultFare\":\"59.000\",\"arrvTime\":\"15:00\",\"destStationCode\":\"GMR\",\"depTime\":\"12:00\",\"trainClass\":\"K\",\"trainNo\":\"77A\",\"avb\":\"48\",\"hourDiffTimeNow\":\"21\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-77A-K-C\",\"jenisLayout\":\"common\"},{\"infantFare\":\"0\",\"trainSubClass\":\"A\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO PARAHYANGAN \\nEksekutif (E)\",\"adultFare\":\"1.008.000\",\"arrvTime\":\"23:00\",\"destStationCode\":\"GMR\",\"depTime\":\"20:00\",\"trainClass\":\"E\",\"trainNo\":\"P05\",\"avb\":\"0\",\"hourDiffTimeNow\":\"29\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-P05-E-A\",\"jenisLayout\":\"common\"},{\"infantFare\":\"0\",\"trainSubClass\":\"B\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO PARAHYANGAN \\nBisnis (B)\",\"adultFare\":\"858.000\",\"arrvTime\":\"23:00\",\"destStationCode\":\"GMR\",\"depTime\":\"20:00\",\"trainClass\":\"B\",\"trainNo\":\"P05\",\"avb\":\"0\",\"hourDiffTimeNow\":\"29\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-P05-B-B\",\"jenisLayout\":\"common\"},{\"infantFare\":\"0\",\"trainSubClass\":\"C\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO PARAHYANGAN \\nEkonomi (K)\",\"adultFare\":\"59.000\",\"arrvTime\":\"23:00\",\"destStationCode\":\"GMR\",\"depTime\":\"20:00\",\"trainClass\":\"K\",\"trainNo\":\"P05\",\"avb\":\"239\",\"hourDiffTimeNow\":\"29\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-P05-K-C\",\"jenisLayout\":\"common\"}],\"orgStationCode\":\"BD\",\"depDate\":\"20180622\"}},\"trainPergi\":{\"infantFare\":\"0\",\"trainSubClass\":\"B\",\"travelTime\":\"3 jam,0 mnt\",\"arrvDate\":\"20180622\",\"trainNm\":\"ARGO GOPAR \\nBisnis (B)\",\"adultFare\":\"89.000\",\"arrvTime\":\"15:00\",\"destStationCode\":\"GMR\",\"depTime\":\"12:00\",\"trainClass\":\"B\",\"trainNo\":\"77A\",\"avb\":\"48\",\"hourDiffTimeNow\":\"21\",\"depDate\":\"20180622\",\"orgStationCode\":\"BD\",\"trainClassId\":\"BD-GMR-20180622-77A-B-B\",\"jenisLayout\":\"common\"},\"trnType\":\"round-trip\",\"dataAnak\":[{\"titel\":\"Tuan\",\"nama\":\"zumar\"},{\"titel\":\"Nona\",\"nama\":\"syifa\"}],\"desName\":\"GAMBIR\"},\"email\":\"acang.torvalds@gmail.com\",\"seatPergi\":\"BIS 1 \\/ 2A,BIS 1 \\/ 2B\",\"returnBookCode\":\"HHNCJY\",\"jmlDewasa\":\"2\",\"jmlAnak\":\"2\",\"depBookCode\":\"UUXWC8\",\"trxId\":\"1806216420006\",\"totalAdmin\":\"15000\",\"stasiunPulang\":\"GMR\",\"deviceInfo\":\"5cf63e834db32da2\",\"versionCode\":\"1.7.8\",\"tmnIdTrx\":\"35CF63E834DB32DA2180621140319\",\"totalBayar\":\"320000\",\"keretaPergi\":\"BANDUNG\",\"jamPulang\":\"23 Juni 2018 08:00\",\"username\":\"160115100071\"}";
            JSONParser parser = new JSONParser();
            JSONObject jObjReq = (JSONObject) parser.parse(jsonStr);
            JSONObject jsonReqEmail = new JSONObject();
            jsonReqEmail.put("jObjReq", jObjReq);
            jsonReqEmail.put("nama", "Hasan");
            jsonReqEmail.put("bookPergi", "8HJ68");
            jsonReqEmail.put("noKeretaPergi", "405");
            jsonReqEmail.put("namaKeretaPergi", "Argo Bromo");
            jsonReqEmail.put("stasiunPergi", "Bandung");
            jsonReqEmail.put("stasiunPergiTujuan", "Gambir");

            jsonReqEmail.put("bookPulang", "95TG8");
            jsonReqEmail.put("noKeretaPulang", "150");
            jsonReqEmail.put("namaKeretaPulang", "Argo Dhoho");
            jsonReqEmail.put("stasiunPulang", "Gambir");
            jsonReqEmail.put("stasiunPulangTujuan", "Bandung");

            new SendEmailKereta().send("acang.torvalds@gmail.com", jsonReqEmail.toString());
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void tesConvertHtmlToPdf(){
        try {
            //create attachment
            // Initialize velocity
            VelocityEngine ve = new VelocityEngine();
            ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
            ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
            ve.init();
            //------------------------create file attachment-----------------------------------------
            Template template = ve.getTemplate("mail-template-kereta.vm");

            /* create a context and add data */
            VelocityContext context = new VelocityContext();
            //context.put("headerTemplate", header);
            //context.put("bodyTemplate", emailBody);

            StringWriter out = new StringWriter();
            template.merge(context, out);

            System.out.println(out.toString());

            //create pdf
            Helper helper = new Helper();
            String filename = helper.createID();
            String path = conf.getMasterFolder() + File.separator + "tmp" + File.separator;

            //create html file
            BufferedWriter writers = new BufferedWriter(new FileWriter(new File(path + filename + ".html")));
            writers.write(out.toString());
            writers.close();

            boolean success = HtmlToPdf.create()
                    .object(HtmlToPdfObject.forHtml(out.toString()))
                    .convert(path + filename + ".pdf");
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void tesEncryptDecrypt(){
        //encrypt dec
        try {
            ControllerEncDec controllerEncDec = new ControllerEncDec(conf.getEncIv(), conf.getEncKey());
            System.out.println(controllerEncDec.decryptDbVal("I2v3S5H91p1gn9M7OeoIWw==", Constant.KUNCI));
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void tesSms(){
        //test sms
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        new SmsHelper().sent(uuid, "082190764546", "tes sms tm", "INFO_REGISTRASI", conf);
    }
}
