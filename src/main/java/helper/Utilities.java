/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import com.google.common.base.Throwables;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import implement.ImpleHome;
import implement.ImpleNotif;
import koneksi.DatabaseUtilitiesPgsql;

/**
 * @author Hasan
 */
public class Utilities {

    private Cache cache;
    private EntityConfig entityConfig;
    private final ControllerLog controllerLog;

    public Utilities() {
        CacheManager cacheManager = CacheManager.getInstance();
        //get config cache
        this.cache = cacheManager.getCache("TrueMoneyApp");
        Element config = this.cache.get("config");
        this.entityConfig = (EntityConfig) config.getObjectValue();
        //getMasterCache to process in module
        this.cache = cacheManager.getCache("TrueMoneyAppMaster");
        this.controllerLog = new ControllerLog();
    }

    public String GeneratePassword() {
        final String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final int N = alphabet.length();
        Random r = new Random();
        String password = "";
        for (int i = 0; i < 6; i++) {
            password = password + alphabet.charAt(r.nextInt(N));
        }
        return password;
    }

    public String GeneratePIN() {
        final String alphabet = "0123456789";
        final int N = alphabet.length();
        Random r = new Random();
        String password = "";
        for (int i = 0; i < 6; i++) {
            password = password + alphabet.charAt(r.nextInt(N));
        }
        return password;
    }

    public synchronized String createStan() {
        SecureRandom random = new SecureRandom();
        int num = random.nextInt(999999999);
        String formatted = String.format("%012d", num);
        return formatted;
    }

    public String createTrxid() {
        String hasil = "";
        try {
            NumberFormat formatter = new DecimalFormat("000");
            Random random = new Random();
            int temp = random.nextInt(999);
            hasil = this.getDate("yyyyMMddHHmmss") + formatter.format(temp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasil;
    }

    public String getSignature(String string) {
        String hasil = "";
        try {

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(string.getBytes());

            byte byteData[] = md.digest();

            // convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            hasil = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public Timestamp getDate() {
        Timestamp timestamp = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = format.format(new Date()).toString();
            Date date1 = format.parse(date);
            timestamp = new Timestamp(date1.getTime());
        } catch (ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return timestamp;
    }

    public String getDate(String format) {
        // "yyyy-MM-dd HH:mm:ss"
        Date now = new Date();
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat(format);
        String strDate = sdf.format(now);

        sdf = null;
        return strDate;
    }

    public String getRcMessage(String rc) {
        //cari status message
        Element elem = this.cache.get("msResponseCode");
        ArrayList<HashMap<String, Object>> msResponseCode = (ArrayList<HashMap<String, Object>>) elem.getObjectValue();
        int found = 0;
        String message = "RESPONSE TIDAK TERDAFTAR";
        //find the data
        for (HashMap<String, Object> map : msResponseCode) {
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = pair.getKey().toString();
                String value = pair.getValue().toString();
                //System.out.println("key : " + pair.getKey() + ", value : " + pair.getValue().toString());
                if (key.equals("response_code") && value.equals(rc)) {
                    found = 1;
                }
                if (key.equals("response_detail")) {
                    message = value;
                }
            }
            if (found == 1) {
                break;
            }
        }
        return message;
    }

    public String getWording(String wordContent) {
        //cari status message
        Element elem = this.cache.get("msWording");
        ArrayList<HashMap<String, Object>> msResponseCode = (ArrayList<HashMap<String, Object>>) elem.getObjectValue();
        int found = 0;
        String message = "WORDING TIDAK TERDAFTAR";
        //find the data
        for (HashMap<String, Object> map : msResponseCode) {
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = pair.getKey().toString();
                String value = pair.getValue().toString();
                //System.out.println("key : " + pair.getKey() + ", value : " + pair.getValue().toString());
                if (key.equals("wording_content_name") && value.equals(wordContent)) {
                    found = 1;
                }
                if (key.equals("wording_content")) {
                    message = value;
                }
            }
            if (found == 1) {
                break;
            }
        }
        return message;
    }

    public JSONObject curlPost(String url, String data, boolean encode) {
        JSONObject responseObj = null;
        try {
            BufferedReader rd = null;
            String line = null;
            if (encode) {
                data = URLEncoder.encode(data.toString(), "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

            this.controllerLog.logStreamWriter("SEND REQUEST TO : " + url + " with data : " + data);

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");
            con.setRequestProperty("Content-Type", "text/plain");

            con.setConnectTimeout(120000);
            con.setReadTimeout(120000);

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String code = String.valueOf(con.getResponseCode());

            //System.err.println(code);
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));

            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            if (response.toString().trim().length() > 10) {
                //System.out.println("RESPONSE OK..!!");
                JSONParser parser = new JSONParser();
                this.controllerLog.logStreamWriter("RESPONSE FROM : " + response.toString());
                Object obj = parser.parse(response.toString());
                responseObj = (JSONObject) obj;
                responseObj.put("ResponseCode", code);
            } else {
                //System.out.println("RESPONSE KOSONG..!!");
                //System.out.println(response);
                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "500");
                responseObj.put("ACK", "NOK");
                responseObj.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                responseObj.put("MSG", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                responseObj.put("result_code", "");
            }

        } catch (java.net.SocketTimeoutException e) {
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "504");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "TIME OUT!!");
            responseObj.put("result_code", "");
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);

            return responseObj;
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);

            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "500");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "EXT : INTERNAL SERVER ERROR");
            responseObj.put("result_code", "");
            return responseObj;

        }
        return responseObj;
    }

     public JSONObject checkDeviceAgent(JSONObject jsonReq) {
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "device changed");

        //<editor-fold defaultstate="collapsed" desc="query checkDevice">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        int count = 0;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select count(*) "
                    + "from \"MsAgentAccount\" "
                    + "where device_info = ?"
                    + "and id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("deviceInfo").toString());
            stPgsql.setString(2, jsonReq.get("username").toString());
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //</editor-fold>

        if (count > 0) {
            jobjResp.put("ACK", "OK");
            jobjResp.put("pesan", "device not changed");
        }

        return jobjResp;
    }

    public JSONObject checkVersion(JSONObject objJson) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Terdapat Update applikasi");
        //<editor-fold defaultstate="collapsed" desc="query checkVersion">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            String packageName = objJson.get("packageName").toString();
            String versionCode = objJson.get("versionCode").toString();
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select \"AppsName\",\"Version\",\"TimeStamp\",\"IsActive\" from "
                    + "\"MsAndroidVersion\" where \"TimeStamp\" = (select max(\"TimeStamp\") from "
                    + "\"MsAndroidVersion\" where \"AppsName\" = ?) and "
                    + "\"Version\" = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, packageName);
            stPgsql.setString(2, versionCode);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                String version = rsPgsql.getString("Version");

                if (!versionCode.equalsIgnoreCase(version)) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Terdapat Update applikasi");
                } else {
                    if (rsPgsql.getBoolean("IsActive") == true) {
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("pesan", "");
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Under Construction");
                    }
                }
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //</editor-fold>
        return jsonResp;
    }

    public JSONObject removePasswordObj(JSONObject jsonReq) {
        JSONObject objJsonRecreate = new JSONObject();
        for (Iterator iterator = jsonReq.keySet().iterator(); iterator.hasNext(); ) {
            String key = (String) iterator.next();
            //System.out.println(this.jSONObject.get(key));
            if (!key.equalsIgnoreCase("password")
                    && !key.equalsIgnoreCase("pin")
                    && !key.equalsIgnoreCase("pinBaru")
                    && !key.equalsIgnoreCase("pinLama")) {
                objJsonRecreate.put(key, jsonReq.get(key));
            }
        }
        return objJsonRecreate;
    }

    public String insertTmp(String typeReq, JSONObject objJson) {
        //<editor-fold defaultstate="collapsed" desc="query insertTmp">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        int idTemp = 0;
        String idResultTmp = "0";
        //remove password object before insert to log
        JSONObject objJsonRecreate = this.removePasswordObj(objJson);
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "insert \n"
                    + "    into\n"
                    + "        \"Temp\"\n"
                    + "        (\"Request\", \"RequestTime\") \n"
                    + "    values\n"
                    + "        (?, ?);";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, typeReq + "=" + objJsonRecreate.toString());
            stPgsql.setTimestamp(2, this.getDate());
            idTemp = stPgsql.executeUpdate();
            rsPgsql = stPgsql.getGeneratedKeys();
            while (rsPgsql.next()) {
                idResultTmp = rsPgsql.getString(1);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        return idResultTmp;
    }

    public void updateTmp(String idTmp, String resp) {
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = null;
        try {
            jsonResp = (JSONObject) parser.parse(resp);
        } catch (org.json.simple.parser.ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        //<editor-fold defaultstate="collapsed" desc="query updateTmp">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "update \"Temp\" set \"Response\" = ?, \"ResponseTime\" = ? where id_temp = ?;";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, jsonResp.toString());
            stPgsql.setTimestamp(2, this.getDate());
            stPgsql.setInt(3, Integer.parseInt(idTmp));
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }

    public int insertLog(String idTrx, JSONObject objJson) {
        //<editor-fold defaultstate="collapsed" desc="query insertLog">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        int idTemp = 0;
        int idResultTmp = 0;
        //remove password object before insert to log
        JSONObject objJsonRecreate = new JSONObject();
        for (Iterator iterator = objJson.keySet().iterator(); iterator.hasNext(); ) {
            String key = (String) iterator.next();
            //System.out.println(this.jSONObject.get(key));
            if (!key.equals("password")) {
                objJsonRecreate.put(key, objJson.get(key));
            }
        }
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "insert "
                    + "    into"
                    + "        \"tr_trx_token_log\""
                    + "        (\"id_trx\", \"request\", \"request_time\") "
                    + "    values"
                    + "        (?, ?, ?);";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, idTrx);
            stPgsql.setString(2, objJsonRecreate.toString());
            stPgsql.setTimestamp(3, this.getDate());
            idTemp = stPgsql.executeUpdate();
            rsPgsql = stPgsql.getGeneratedKeys();
            while (rsPgsql.next()) {
                idResultTmp = rsPgsql.getInt(1);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        return idResultTmp;
    }

    public void updateLog(int idTmp, String resp) {
        JSONParser parser = new JSONParser();
        JSONObject jsonResp = null;
        try {
            jsonResp = (JSONObject) parser.parse(resp);
        } catch (org.json.simple.parser.ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        //<editor-fold defaultstate="collapsed" desc="query updateTmp">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "update \"tr_trx_token_log\" set \"response\" = ?, \"response_time\" = ? where id_trx_token_log = ?;";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, jsonResp.toString());
            stPgsql.setTimestamp(2, this.getDate());
            stPgsql.setLong(3, idTmp);
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }

    public String constructResponse(String resp, EntityConfig entityConfig) {
        ControllerEncDec encDec = new ControllerEncDec(entityConfig.getEncIv(), entityConfig.getEncKey());
        byte[] encryptByte = null;
        try {
            encryptByte = encDec.getEncrypt(resp);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        resp = new String(Base64.encodeBase64(encryptByte));
        return resp;
    }

    public String convertFormatDate(String formatOld, String formatNew, String dateString) {
        String bulanTahun = "";
        try {
            DateFormat df = new SimpleDateFormat(formatOld);
            Date bulanTahunDate = df.parse(dateString);
            df = new SimpleDateFormat(formatNew);
            bulanTahun = df.format(bulanTahunDate);
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return bulanTahun;
    }

    public String calculateDiffDate(String startDate, String endDate, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        Date d1 = null;
        Date d2 = null;
        String diffTime = "";
        try {
            d1 = format.parse(startDate);
            d2 = format.parse(endDate);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if (diffDays != 0) {
                diffTime = diffDays + " hari," + diffHours + " jam," + diffMinutes + " mnt";
            } else {
                diffTime = diffHours + " jam," + diffMinutes + " mnt";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return diffTime;
    }

    public long calculateDiffDateOnMiliSecond(String dateOne, String dateTwo, String formatDate) {
        long difference = -1;
        try {
            DateFormat format = new SimpleDateFormat(formatDate, Locale.ENGLISH);
            Date one = format.parse(dateOne);
            Date two = format.parse(dateTwo);
            difference = two.getTime() - one.getTime();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return difference;
    }

    public String formatTanggal(String strtime) {
        String strbulan = "";
        String strtanggal = "";
        String strtahun = "";
        String strjam = "";
        if (strtime != null && !strtime.isEmpty()) {
            strtahun = strtime.substring(0, 4);
            strbulan = strtime.substring(5, 7);
            if (strbulan.equals("01")) {
                strbulan = "Januari";
            } else if (strbulan.equals("02")) {
                strbulan = "Februari";
            } else if (strbulan.equals("03")) {
                strbulan = "Maret";
            } else if (strbulan.equals("04")) {
                strbulan = "April";
            } else if (strbulan.equals("05")) {
                strbulan = "Mei";
            } else if (strbulan.equals("06")) {
                strbulan = "Juni";
            } else if (strbulan.equals("07")) {
                strbulan = "Juli";
            } else if (strbulan.equals("08")) {
                strbulan = "Agustus";
            } else if (strbulan.equals("09")) {
                strbulan = "September";
            } else if (strbulan.equals("10")) {
                strbulan = "Oktober";
            } else if (strbulan.equals("11")) {
                strbulan = "November";
            } else if (strbulan.equals("12")) {
                strbulan = "Desember";
            }
            strtanggal = strtime.substring(8, 10);
            if (strtime.length() > 10) {
                strjam = " " + strtime.substring(11, 16);
            }
            return strtanggal + " " + strbulan + " " + strtahun + strjam;
        } else {
            return strtime;
        }
    }

    public boolean moveFile(String to, String fileName) {
        boolean sukses = false;
        String pathTarget = to + fileName;
        try {
            File afile = new File(entityConfig.getWorkDir() + "tmp/" + fileName);
            if (afile.renameTo(new File(pathTarget))) {
                sukses = true;
            } else {
                sukses = false;
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return sukses;
    }

    public String postMultipart(String url, HttpEntity entity) {
        this.controllerLog.logStreamWriter("REQUEST TO : " + url);
        HttpPost post = new HttpPost(url);
        //begin send
        post.setEntity(entity);
        String responseString = "";
        InputStream responseStream = null;
        HttpClient client = new DefaultHttpClient();
        try {
            HttpResponse response = client.execute(post);
            if (response != null) {
                HttpEntity responseEntity = response.getEntity();

                if (responseEntity != null) {
                    responseStream = responseEntity.getContent();
                    if (responseStream != null) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(responseStream));
                        String responseLine = br.readLine();
                        String tempResponseString = "";
                        while (responseLine != null) {
                            tempResponseString = tempResponseString + responseLine + System.getProperty("line.separator");
                            responseLine = br.readLine();
                        }
                        br.close();
                        if (tempResponseString.length() > 0) {
                            responseString = tempResponseString;
                        }
                    }
                }
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        } finally {
            if (responseStream != null) {
                try {
                    responseStream.close();
                } catch (IOException e) {
                    String s = Throwables.getStackTraceAsString(e);
                    controllerLog.logErrorWriter(s);
                }
            }
        }
        client.getConnectionManager().shutdown();
        this.controllerLog.logStreamWriter("RESPONSE FROM : " + url + " : " + responseString);
        return responseString;
    }

    public JSONObject writeLogStreamRequest(String tipe, JSONObject jsonRequest) {
        JSONObject jsonReqAction = (JSONObject) jsonRequest.get("action");
        JSONObject jsonReqForLog = this.removePasswordObj(jsonReqAction);
        //System.out.println("json req for log : " + jsonReqForLog.toString());
        this.controllerLog.logStreamWriter("Req " + tipe + ": id = " + jsonRequest.get("downlineId").toString() + " ; sourceIp/sport = " + jsonRequest.get("ipAddr").toString() + '/' + jsonRequest.get("port").toString() + "; Agent = " + jsonRequest.get("browserAgent").toString() + "; Body = " + jsonReqForLog.toString());
        return jsonReqForLog;
    }

    public void writeLogStreamResponse(String tipe, JSONObject jsonRequest, String resp) {
        this.controllerLog.logStreamWriter("Resp " + tipe + ": id = " + jsonRequest.get("downlineId").toString() + " ; Response = " + resp);
    }


    public String getIklan(String idTipeTransaksi, String productCode, String idtrx) {
        String url = "";
        String response = "";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            //<editor-fold defaultstate="collapsed" desc="query getmsparam and hit apiSwitching">
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String paramName = "iklan_truemoney";
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, paramName);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("productCode", productCode);
            jSONObject.put("idTipeAplikasi", "3");
            jSONObject.put("idTipeTrkansaksi", idTipeTransaksi);
            jSONObject.put("accountType", "AGENT");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            jSONObject.put("waktu", sdf.format(new Date()).toString());
            String data = jSONObject.toString();

            //insert log
            int idLog = this.insertLog(idtrx, jSONObject);
            JSONObject curlResp = this.curlPost(url, data, false);
            response = curlResp.toString();
            //updateLog
            this.updateLog(idLog, curlResp.toString());
            //</editor-fold>
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return response;
    }
}
