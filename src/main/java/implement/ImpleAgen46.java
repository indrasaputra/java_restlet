/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.*;
import interfc.InterfcAgen46;
import koneksi.DatabaseUtilitiesPgsql;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.sql.*;
import java.util.UUID;

/**
 * @author Hasan
 */
public class ImpleAgen46 implements InterfcAgen46 {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleAgen46(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getKelurahan(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        try {
            //System.out.println("pass default 123456 : " + this.encDec.encryptDbVal(reqPass, this.entityConfig.getEncKeyDb()));

            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //1, check usernya
            String query = "select "
                    + " a.id_kelurahan, a.\"NamaKelurahan\", "
                    + " b.id_kecamatan, b.\"NamaKecamatan\", "
                    + " c.id_kota, c.\"NamaKota\", "
                    + " d.id_provinsi, d.\"NamaProvinsi\" "
                    + " from \"MsKelurahan\" a, \"MsKecamatan\" b, \"MsKota\" c, \"MsProvinsi\" d "
                    + " where a.id_kecamatan = b.id_kecamatan "
                    + " and b.id_kota = c.id_kota "
                    + " and c.id_provinsi = d.id_provinsi "
                    + " and a.\"NamaKelurahan\" ILIKE ?"
                    + " ORDER BY a.\"NamaKelurahan\"  ASC;";
            st = conn.prepareStatement(query);
            st.setString(1, objJson.get("search").toString() + "%");
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idKelurahan", rs.getString("id_kelurahan"));
                jObj.put("namaKelurahan", rs.getString("NamaKelurahan"));
                jObj.put("idKecamatan", rs.getString("id_kecamatan"));
                jObj.put("namaKecamatan", rs.getString("NamaKecamatan"));
                jObj.put("idKota", rs.getString("id_kota"));
                jObj.put("namaKota", rs.getString("NamaKota"));
                jObj.put("idProvinsi", rs.getString("id_provinsi"));
                jObj.put("namaProvinsi", rs.getString("NamaProvinsi"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("kelurahan", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String username) {
        String[] status = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"MsAccountStatus\".\"NamaAccountStatus\""
                    + " ,\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\""
                    + " ,\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\""
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\""
                    + " WHERE \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus"
                    + " AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("NamaAccountStatus");
                status[1] = rsPgsql.getString("LastBalance");
                status[2] = rsPgsql.getString("StatusKartu");
                status[3] = rsPgsql.getString("id_agentaccount");
                status[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String getOtpTarikTunai(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal request otp");
        jsonResp.put("status", "GAGAL");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "BNI-04");
        jsonObject.put("MC", "BNI46Tarik");
        jsonObject.put("MT", "2100");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObjectMp.put("idTipeTransaksi", "66");
        jsonObjectMp.put("TipeUser", "agent");
        jsonObjectMp.put("Tipe", "BNITarikTunai");
        jsonObjectMp.put("trxid", jsonReq.get("idTrx").toString());
        jsonObjectMp.put("id_Account", jsonReq.get("username").toString());
        jsonObjectMp.put("customerAccountNum", jsonReq.get("nomorRekening").toString());
        String nominal = jsonReq.get("nominal").toString().replace(".","");
        jsonObjectMp.put("amount", nominal);
        jsonObject.put("MP", jsonObjectMp);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "bni_agen46_switcher");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("ACK", "OK");
                    jsonResp.put("status", "SUKSES");
                    jsonResp.put("pesan", "sukses request otp");
                    jsonResp.put("nama", jObjRespMpo.get("name").toString());

                    double dbl = Double.parseDouble(jObjRespMpo.get("biaya_admin").toString());
                    String dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jsonResp.put("fee", dblStr);

                    dbl = Double.parseDouble(jObjRespMpo.get("total_tarik").toString());
                    dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jsonResp.put("total", dblStr);
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "gagal request otp");
                    JSONObject jObjMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("pesan", jObjMpo.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal request otp");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String getSetorTunai(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal transaksi");
        jsonResp.put("status", "GAGAL");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "BNI-02");
        jsonObject.put("MC", "BNISETOR");
        jsonObject.put("MT", "2200");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObjectMp.put("idTipeTransaksi", "65");
        jsonObjectMp.put("amount", jsonReq.get("nominal").toString());
        jsonObjectMp.put("customerAccountNum", jsonReq.get("nomorRekening").toString());
        jsonObjectMp.put("trxid", jsonReq.get("idTrx").toString());
        jsonObjectMp.put("Tipe", "BNISetorTunaiAgent");
        jsonObjectMp.put("TipeUser", "agent");
        jsonObjectMp.put("pin_transaksi", jsonReq.get("PIN").toString());
        jsonObjectMp.put("id_Account", jsonReq.get("username").toString());
        jsonObject.put("MP", jsonObjectMp);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "bni_agen46_switcher");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        try {
            JSONObject jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("ACK", "OK");

                    double dbl = Double.parseDouble(jObjRespMpo.get("jumlahsetoran").toString());
                    String dblStr = String.format("%,.0f", dbl);
                    String setor = dblStr.replaceAll(",", ".");

                    jsonResp.put("pesan", "Setor tunai berhasil"+ "\n"
                            + "No jurnal: " + jObjRespMpo.get("TrJournal").toString() + "\n"
                            + "No rekening: " + jObjRespMpo.get("NoRek").toString() + "\n"
                            + "Nama nasabah: " + jObjRespMpo.get("NamaPendaftar").toString() + "\n"
                            + "Nominal: Rp." + setor + "\n");
                    jsonResp.put("status", "SUKSES");

                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "gagal transaksi");
                    JSONObject jObjMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("pesan", jObjMpo.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal transaksi");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String checkAkun(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal transaksi");
        jsonResp.put("status", "GAGAL");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "BNI-00");
        jsonObject.put("MC", "BNISETOR");
        jsonObject.put("MT", "2100");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObjectMp.put("customerAccountNum", jsonReq.get("nomorRekening").toString());
        jsonObjectMp.put("trxid", jsonReq.get("idTrx").toString());
        jsonObjectMp.put("Tipe", "BNISetorTunaiAgent");
        jsonObjectMp.put("amount", jsonReq.get("nominal").toString());
        jsonObject.put("MP", jsonObjectMp);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "bni_agen46_switcher");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        try {
            JSONObject jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "Sukses transaksi");
                    jsonResp.put("status", "SUKSES");
                    jsonResp.put("nama", jObjRespMpo.get("d_name").toString());

                    double dbl = Double.parseDouble(jObjRespMpo.get("fee").toString());
                    String dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jsonResp.put("fee", dblStr);
                } else {
                    jsonResp.put("ACK", "NOK");
                    JSONObject jObjMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("pesan", jObjMpo.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal transaksi");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String getTarik(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal transaksi");
        jsonResp.put("status", "GAGAL");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "BNI-05");
        jsonObject.put("MC", "BNI46Tarik");
        jsonObject.put("MT", "2200");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObjectMp.put("idTipeTransaksi", "66");
        jsonObjectMp.put("TipeUser", "agent");
        jsonObjectMp.put("Tipe", "BNITarikTunaiAgent");
        jsonObjectMp.put("trxid", jsonReq.get("idTrx").toString());
        jsonObjectMp.put("id_Account", jsonReq.get("username").toString());
        jsonObjectMp.put("customerAccountNum", jsonReq.get("nomorRekening").toString());
        jsonObjectMp.put("amount", jsonReq.get("nominal").toString());
        jsonObjectMp.put("customerOtp", jsonReq.get("otp").toString());
        jsonObjectMp.put("pin_transaksi", jsonReq.get("PIN").toString());
        jsonObjectMp.put("d_name", jsonReq.get("nama").toString());
        jsonObject.put("MP", jsonObjectMp);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "bni_agen46_switcher");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        try {
            JSONObject jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("ACK", "OK");

                    double dbl = Double.parseDouble(jObjRespMpo.get("amount").toString());
                    String dblStr = String.format("%,.0f", dbl);
                    String tarik = dblStr.replaceAll(",", ".");

                    jsonResp.put("pesan", "Tarik tunai berhasil"+ "\n"
                            + "No jurnal: " + jObjRespMpo.get("bni_reffnum").toString() + "\n"
                            + "No rekening: " + jObjRespMpo.get("NoRek").toString() + "\n"
                            + "Nominal: Rp." + tarik + "\n");

                    jsonResp.put("status", "SUKSES");
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "gagal transaksi");
                    JSONObject jObjMpo = (JSONObject) jObjResp.get("MPO");
                    jsonResp.put("pesan", jObjMpo.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal transaksi");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String getRegister(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "registrasi gagal");
        jsonResp.put("status", "GAGAL");

        //collect data diri
        String pin = jsonReq.get("PIN").toString();
        String provinsi = jsonReq.get("provinsi").toString();
        String income = jsonReq.get("income").toString();
        income = income.replace(".", "");
        String rt = jsonReq.get("rt").toString();
        String rw = jsonReq.get("rw").toString();
        String jkel = jsonReq.get("kelamin").toString();
        if (jkel.equals("Pria")) {
            jkel = "M";
        } else {
            jkel = "F";
        }
        String tlpKantor = jsonReq.get("tlpKantor").toString();
        String noHp = jsonReq.get("noHp").toString();
        String tlpRumah = jsonReq.get("tlpRumah").toString();
        String kota = jsonReq.get("kota").toString();
        String masaBerlakuKtp = jsonReq.get("masaBerlakuKtp").toString();
        masaBerlakuKtp = masaBerlakuKtp.replace("-", "");
        String noKtp = jsonReq.get("noKtp").toString();
        String kelurahan = jsonReq.get("kelurahan").toString();
        String alamat = jsonReq.get("alamat").toString();
        String tglLahir = jsonReq.get("tglLahir").toString();
        tglLahir = tglLahir.replace("-", "");
        String setorAwal = jsonReq.get("setorAwal").toString();
        setorAwal = setorAwal.replace(".", "");
        String namaDepan = jsonReq.get("namaDepan").toString();
        String namaTengah = jsonReq.get("namaTengah").toString();
        String namaBelakang = jsonReq.get("namaBelakang").toString();
        String pekerjaan = jsonReq.get("pekerjaan").toString();
        String kecamatan = jsonReq.get("kecamatan").toString();
        String tempatLahir = jsonReq.get("tempatLahir").toString();
        String kodePos = jsonReq.get("kodePos").toString();
        String username = jsonReq.get("username").toString();
        String status = jsonReq.get("status").toString();
        if (status.equals("Menikah")) {
            status = "S";
        } else {
            status = "M";
        }

        //collect data foto
        JSONObject jObjDataFile = (JSONObject) jsonReq.get("fileFoto");
        String fotoDiri = jObjDataFile.get("fileFotoDiri").toString();
        String fotoKTP = jObjDataFile.get("fileKtp").toString();

        //send foto ktp to gw
        File file = new File(entityConfig.getWorkDir() + "uploadFile" + File.separator + "register_agen46" + File.separator + fotoKTP);
        FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);
        StringBody stringBodyNoktp = new StringBody(noKtp, ContentType.MULTIPART_FORM_DATA);
        StringBody stringBodyType = new StringBody("ktp", ContentType.MULTIPART_FORM_DATA);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("noktp", stringBodyNoktp);
        builder.addPart("type", stringBodyType);
        builder.addPart("File", fileBody);
        HttpEntity entity = builder.build();
        String url = hlp.getParamMsParameter("bni_agen46_gateway");
        url = url + "uploadphoto";
        String responseString = utilities.postMultipart(url, entity);
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) parser.parse(responseString);
            fotoKTP = jsonObject.get("fileName").toString();
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        if (jsonObject.get("ACK").equals("OK")) {
            //send foto diri to gw
            file = new File(entityConfig.getWorkDir() + "uploadFile" + File.separator + "register_agen46" + File.separator + fotoDiri);
            fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);
            stringBodyNoktp = new StringBody(noKtp, ContentType.MULTIPART_FORM_DATA);
            stringBodyType = new StringBody("pasfoto", ContentType.MULTIPART_FORM_DATA);

            builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("noktp", stringBodyNoktp);
            builder.addPart("type", stringBodyType);
            builder.addPart("File", fileBody);
            entity = builder.build();
            url = hlp.getParamMsParameter("bni_agen46_gateway");
            url = url + "uploadphoto";
            responseString = utilities.postMultipart(url, entity);
            try {
                jsonObject = (JSONObject) parser.parse(responseString);
                fotoDiri = jsonObject.get("fileName").toString();
            } catch (ParseException e) {
                String s = Throwables.getStackTraceAsString(e);
                controllerLog.logErrorWriter(s);
            }
            if (jsonObject.get("ACK").equals("OK")) {
                //send data string to sw
                String idAgentAccount = jsonReq.get("username").toString();
                String idAgent = idAgentAccount.substring(0, 6) + '2' + idAgentAccount.substring(7);
                jsonObject = new JSONObject();
                jsonObject.put("CC", "3");
                jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
                jsonObject.put("ST", utilities.createStan());
                jsonObject.put("PC", "BNI-01");
                jsonObject.put("MC", "BNI46");
                jsonObject.put("MT", "2100");
                JSONObject jsonObjectMp = new JSONObject();
                jsonObjectMp.put("idTipeTransaksi", "64");
                jsonObjectMp.put("TipeUser", "agent");
                jsonObjectMp.put("Tipe", "BNIBukaRekening");
                jsonObjectMp.put("trxid", jsonReq.get("idtrx").toString());
                jsonObjectMp.put("id_Account", idAgentAccount);
                jsonObjectMp.put("firstName", namaDepan);
                jsonObjectMp.put("middleName", namaTengah);
                jsonObjectMp.put("lastName", namaBelakang);
                jsonObjectMp.put("adrress1", kecamatan);
                jsonObjectMp.put("address2", rt + "/" + rw + ", " + kelurahan);
                jsonObjectMp.put("address3", kota);
                jsonObjectMp.put("address4", provinsi);
                jsonObjectMp.put("zipCode", kodePos);
                jsonObjectMp.put("homePhone1", tlpRumah);
                jsonObjectMp.put("homePhone2", "");
                jsonObjectMp.put("noFax1", "");
                jsonObjectMp.put("noFax2", "");
                jsonObjectMp.put("officePhone1", tlpKantor);
                jsonObjectMp.put("officePhone2", "");
                jsonObjectMp.put("handPhone1", noHp);
                jsonObjectMp.put("handPhone2", "");
                jsonObjectMp.put("idNumber", noKtp);
                jsonObjectMp.put("jobCode", pekerjaan);
                jsonObjectMp.put("idDueDate", masaBerlakuKtp);
                jsonObjectMp.put("placeOfBirth", tempatLahir);
                jsonObjectMp.put("dateOfBirth", tglLahir);
                jsonObjectMp.put("gender", jkel);
                jsonObjectMp.put("isMaried", status);
                jsonObjectMp.put("income", income);
                jsonObjectMp.put("nominal", "1");
                jsonObjectMp.put("setoran_awal", setorAwal);
                jsonObjectMp.put("lat", setorAwal);
                jsonObjectMp.put("lng", setorAwal);
                jsonObjectMp.put("accountNum", "4447");
                jsonObjectMp.put("pin_transaksi", pin);
                jsonObjectMp.put("image_foto", "pendaftaran/" + fotoDiri);
                jsonObjectMp.put("image_ktp", "pendaftaran/" + fotoKTP);
                jsonObject.put("MP", jsonObjectMp);
                //insert log
                int idLog = utilities.insertLog(jsonReq.get("idtrx").toString(), jsonObject);
                String resp = this.hlp.requestToGwSocket(jsonObject, "bni_agen46_switcher");
                //updateLog
                utilities.updateLog(idLog, resp);
                parser = new JSONParser();
                JSONObject jObjResp = null;
                try {
                    jObjResp = (JSONObject) parser.parse(resp);
                    if (jObjResp.get("RC").toString().equals("0000")) {
                        jsonResp.put("ACK", "OK");

                        JSONObject jObjMpo = (JSONObject) jObjResp.get("MPO");

                        double dbl = Double.parseDouble(jObjMpo.get("setoran_awal").toString());
                        String dblStr = String.format("%,.0f", dbl);
                        setorAwal = dblStr.replaceAll(",", ".");

                        dbl = Double.parseDouble(jObjMpo.get("jumlahadmin").toString());
                        dblStr = String.format("%,.0f", dbl);
                        String jmlAdmin = dblStr.replaceAll(",", ".");

                        dbl = Double.parseDouble(jObjMpo.get("jumlahbayar").toString());
                        dblStr = String.format("%,.0f", dbl);
                        String jmlBayar = dblStr.replaceAll(",", ".");

                        jsonResp.put("pesan",
                                "Pembukaan rekening berhasil\n"
                                        + "No rekening: " + jObjMpo.get("NoRekBaru").toString() + "\n"
                                        + "No jurnal: " + jObjMpo.get("bni_reffnum").toString() + "\n"
                                        + "Nama Nasabah: " + jObjMpo.get("NamaPendaftar").toString() + "\n"
                                        + "No Hp: " + noHp + "\n"
                                        + "Setor Awal : Rp." + setorAwal + "\n"
                                        + "Biaya Admin  : Rp." + jmlAdmin + "\n"
                                        + "Total Bayar: Rp." + jmlBayar);
                        jsonResp.put("status", "SUKSES");
                        //send sms to customer
                        String smsScript = "Terima Kasih, Registrasi BNI AGEN46 Anda telah berhasil dengan No Rekening %s. Info hub: 08041000100";
                        String pesan = String.format(smsScript,
                                jObjMpo.get("NoRekBaru").toString());

                        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                        new SmsHelper().sent(uuid, noHp, pesan, "INFO_REGISTRASI_AGEN46", entityConfig);
                    } else {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", jObjResp.get("RCM").toString());
                        jsonResp.put("status", "GAGAL");
                    }
                } catch (ParseException e) {
                    String s = Throwables.getStackTraceAsString(e);
                    controllerLog.logErrorWriter(s);
                }
            }
        }
        return jsonResp.toString();
    }

    public void uploadFtp(String filename, File image) {
        //upload to inventory
        String server = entityConfig.getFtpIp();
        int port = Integer.parseInt(entityConfig.getFtpPort());
        String user = entityConfig.getFtpUser();
        String pass = entityConfig.getFtpPass();
        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            // ftpClient.enterLocalActiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // File firstLocalFile = new File("D://BackGround/Ao8tG.jpg");
            //  devel "/home/development/devel/android/";
            //  prod "/home/development/prod/picture/";
            String path = entityConfig.getFtpPath();
            String firstRemoteFile = path + filename;
            InputStream inputStream = new FileInputStream(image);
            controllerLog.logStreamWriter("Start uploading file " + firstRemoteFile);
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            controllerLog.logStreamWriter(ftpClient.getReplyString());
            inputStream.close();
            if (done) {
                controllerLog.logStreamWriter("The file is uploaded successfully.");
            } else {

            }
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    public void uploadFileViaCommandLine(String file) {
        String server = entityConfig.getFtpIp();
        int port = Integer.parseInt(entityConfig.getFtpPort());
        String user = entityConfig.getFtpUser();
        String pass = "helloworld..2018\\!\\#";
        String path = entityConfig.getFtpPath();
        try {
            final Process p = Runtime.getRuntime().exec("curl -T " + file + " " + "ftp://" + server + path + " --user " + user + ":" + pass);
            System.out.println("command curl -T " + file + " " + "ftp://" + server + path + " --user " + user + ":" + pass);
            new Thread(new Runnable() {
                public void run() {
                    BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = null;
                    try {
                        while ((line = input.readLine()) != null) {
                            controllerLog.logStreamWriter("Start uploading file " + line);
                        }
                    } catch (IOException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    }
                }
            }).start();

            p.waitFor();
        } catch (InterruptedException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
    }
}
