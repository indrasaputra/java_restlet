/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Constant;
import helper.Helper;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcCairkanSaldo;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.*;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Hasan
 */
public class ImpleCairkanSaldo implements InterfcCairkanSaldo {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleCairkanSaldo(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }


    @Override
    public JSONObject getMsSupplier(JSONObject jsonReq) {
        JSONObject jObjMsSupplier = new JSONObject();
        jObjMsSupplier.put("idSupplier", "");
        jObjMsSupplier.put("urlGateway", "");
        //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "WITH tabela AS ( " +
                    " SELECT " +
                    "  * " +
                    " FROM " +
                    "  \"MsSupplier\" " +
                    " WHERE " +
                    "  id_supplier IN (5, 19) " +
                    " AND \"FlagActive\" = 't' " +
                    "), " +
                    " tabelchecknull AS ( " +
                    " SELECT " +
                    "  \"count\" (*) AS jml " +
                    " FROM " +
                    "  tabela " +
                    " WHERE " +
                    "  tabela.\"Default\" IS NULL " +
                    "), " +
                    " tabelcheckok AS ( " +
                    " SELECT " +
                    "  \"count\" (*) AS jml " +
                    " FROM " +
                    "  tabela " +
                    " WHERE " +
                    "  tabela.\"Default\" = 'OK' " +
                    "), tabelok AS (SELECT " +
//                "  CASE WHEN tabelchecknull.jml = 2 THEN 19 ELSE tabela.id_supplier END AS idnull,\n" +
//                "  CASE WHEN tabelcheckok.jml = 2 THEN 19 ELSE tabela.id_supplier END AS idok,\n" +
                    " CASE WHEN ((CASE WHEN tabelchecknull.jml = 2 THEN 19 ELSE tabela.id_supplier END) = (CASE WHEN tabelcheckok.jml = 2 THEN 19 ELSE tabela.id_supplier END)) THEN (CASE WHEN tabelcheckok.jml = 2 THEN 19 ELSE tabela.id_supplier END) END AS id_supplier  " +
                    "FROM " +
                    " tabela, " +
                    " tabelchecknull, " +
                    " tabelcheckok " +
                    "LIMIT 1) " +
                    "SELECT tabela.id_supplier,tabela.\"URLGateway\" FROM tabela, tabelok WHERE tabela.id_supplier = tabelok.id_supplier AND tabela.\"FlagActive\" = 't'";

            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                jObjMsSupplier.put("idSupplier", rsPgsql.getString("id_supplier"));
                jObjMsSupplier.put("urlGateway", rsPgsql.getString("URLGateway"));
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return jObjMsSupplier;
    }

    @Override
    public JSONObject getCustDetil(JSONObject jsonReq) {
        JSONObject jObjCustDetil = new JSONObject();
        jObjCustDetil.put("noRekening", "");
        jObjCustDetil.put("handphone", "");
        jObjCustDetil.put("nama", "");
        jObjCustDetil.put("namaBank", "");
        jObjCustDetil.put("kodeBank", "");
        jObjCustDetil.put("ktp", "");
        jObjCustDetil.put("alamat", "");
        jObjCustDetil.put("sandiDati", "");
        //<editor-fold defaultstate="collapsed" desc="query getCustDetil">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "WITH field_handphone AS (\n" +
                    "	SELECT\n" +
                    "		b.\"Handphone\",\n" +
                    "		b.\"NomorKTP\"\n" +
                    "	FROM\n" +
                    "		\"MsInformasiAgent\" b\n" +
                    "	INNER JOIN \"MsAgentAccount\" d ON b.id_agent = d.id_agent\n" +
                    "	WHERE\n" +
                    "		d.id_agentaccount = '" + jsonReq.get("username").toString() + "'\n" +
                    "	AND b.id_tipeinformasiagent = '1'\n" +
                    ") SELECT\n" +
                    "	b.\"Alamat\",\n" +
                    "	b.\"NoRekening\",\n" +
                    "	b.\"Nama\",\n" +
                    "	e.kodebank,\n" +
                    "	e.\"NamaBank\",\n" +
                    "	x.\"Handphone\",\n" +
                    "	x.\"NomorKTP\",\n" +
                    "	\"MsKota\".\"SandiDATI\"\n" +
                    "FROM\n" +
                    "	field_handphone AS x,\n" +
                    "	\"MsInformasiAgent\" b	\n" +
                    "INNER JOIN \"MsAgentAccount\" d ON b.id_agent = d.id_agent\n" +
                    "INNER JOIN \"MsBank\" e ON b.id_bank = e.id_bank\n" +
                    "INNER JOIN \"MsKelurahan\" ON b.id_kelurahan = \"MsKelurahan\".id_kelurahan\n" +
                    "INNER JOIN \"MsKecamatan\" ON \"MsKelurahan\".id_kecamatan = \"MsKecamatan\".id_kecamatan\n" +
                    "INNER JOIN \"MsKota\" ON \"MsKecamatan\".id_kota = \"MsKota\".id_kota\n" +
                    "WHERE\n" +
                    "	d.id_agentaccount = '" + jsonReq.get("username").toString() + "'\n" +
                    "AND b.id_tipeinformasiagent = '1'";

            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                jObjCustDetil.put("noRekening", rsPgsql.getString("NoRekening"));
                jObjCustDetil.put("handphone", rsPgsql.getString("Handphone"));
                jObjCustDetil.put("nama", rsPgsql.getString("Nama"));
                jObjCustDetil.put("namaBank", rsPgsql.getString("NamaBank"));
                jObjCustDetil.put("kodeBank", rsPgsql.getString("kodebank"));
                jObjCustDetil.put("ktp", rsPgsql.getString("NomorKTP"));
                jObjCustDetil.put("alamat", rsPgsql.getString("Alamat"));
                jObjCustDetil.put("sandiDati", rsPgsql.getString("SandiDATI"));
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return jObjCustDetil;
    }

    @Override
    public boolean[] getTbConfig(String idaccount, String nominal) {
        boolean hasil[] = new boolean[5];
        //<editor-fold defaultstate="collapsed" desc="query getTbConfig">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "WITH agentacc AS (\n" + "	SELECT\n" + "		*, " + nominal + " AS nom\n" + "	FROM\n"
                    + "		\"MsAgentAccount\"\n" + "	WHERE\n" + "		id_agentaccount = '" + idaccount + "'\n"
                    + "),\n" + " detail AS (\n" + "	SELECT\n" + "		\"count\" (id_transaksi) AS hitung,\n"
                    + "		COALESCE (\n" + "			\"sum\" (\"Nominal\" :: INT) + " + nominal + ",\n"
                    + "			'0'\n" + "		) AS total\n" + "	FROM\n" + "		\"TrTransaksi\"\n" + "	WHERE\n"
                    + "		id_tipetransaksi = 30\n" + "	AND id_agentaccount = '" + idaccount + "'\n"
                    + "	AND \"TimeStamp\" :: DATE = now() :: DATE\n"
                    + "AND \"StatusTRX\" in ('SUKSES', 'PENDING', 'OPEN')),\n" + " max_count_day AS (\n"
                    + "	SELECT\n" + "		value_config AS max_count_day\n" + "	FROM\n" + "		tb_config\n"
                    + "	WHERE\n" + "		id_tipetransaksi = 30\n" + "	AND key_config = 'MAX_COUNT_DAY'\n"
                    + "),\n" + " max_day AS (\n" + "	SELECT\n" + "		value_config AS max_day\n" + "	FROM\n"
                    + "		tb_config\n" + "	WHERE\n" + "		id_tipetransaksi = 30\n"
                    + "	AND key_config = 'MAX_DAY'\n" + "),\n" + " max_trx AS (\n" + "	SELECT\n"
                    + "		value_config AS max_trx\n" + "	FROM\n" + "		tb_config\n" + "	WHERE\n"
                    + "		id_tipetransaksi = 30\n" + "	AND key_config = 'MAX_TRX'\n" + "),\n"
                    + " max_trx_samsung AS (\n" + "	SELECT\n" + "		value_config AS max_trx\n" + "	FROM\n"
                    + "		tb_config\n" + "	WHERE\n" + "		id_tipetransaksi = 30\n"
                    + "	AND key_config = 'MAX_TRX_SAMSUNG'\n" + "),\n" + " min_trx_samsung AS (\n" + "	SELECT\n"
                    + "		value_config AS max_trx\n" + "	FROM\n" + "		tb_config\n" + "	WHERE\n"
                    + "		id_tipetransaksi = 30\n" + "	AND key_config = 'MIN_TRX_SAMSUNG'\n" + "),\n"
                    + "max_day_samsung AS (\n" + "	SELECT\n" + "		value_config AS max_day_samsung\n"
                    + "	FROM\n" + "		tb_config\n" + "	WHERE\n" + "		id_tipetransaksi = 30\n"
                    + "	AND key_config = 'MAX_DAY_SAMSUNG'\n" + "), " + " min_trx AS (\n" + "	SELECT\n"
                    + "		value_config AS max_trx\n" + "	FROM\n" + "		tb_config\n" + "	WHERE\n"
                    + "		id_tipetransaksi = 30\n" + "	AND key_config = 'MIN_TRX'\n" + ") SELECT\n"
                    + "	CASE WHEN agentacc.id_dealer = 9 THEN 't' ELSE 'f' END AS \"isSamsung\",\n"
                    + "	detail.hitung < max_count_day.max_count_day :: INTEGER AS maxcountday,\n"
                    + "   CASE WHEN agentacc.id_dealer = 9 THEN detail.total <= max_day_samsung.max_day_samsung :: INTEGER ELSE detail.total <= max_day.max_day :: INTEGER END AS maxday, "
                    + "	CASE WHEN agentacc.id_dealer = 9 THEN agentacc.nom <= max_trx_samsung.max_trx :: INTEGER ELSE agentacc.nom <= max_trx.max_trx :: INTEGER END AS amountmaxday,\n"
                    + "	CASE WHEN agentacc.id_dealer = 9 THEN agentacc.nom >= min_trx_samsung.max_trx :: INTEGER ELSE agentacc.nom >= min_trx.max_trx :: INTEGER END AS amountminday\n"
                    + "FROM\n" + "	detail,\n" + "	max_count_day,\n" + "	max_trx,\n" + "	max_trx_samsung,\n"
                    + "	min_trx,\n" + "	min_trx_samsung,\n" + "   max_day_samsung, " + "	max_day,\n"
                    + "	agentacc";

            //System.out.println(sql);
            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                hasil[0] = rsPgsql.getBoolean("maxcountday"); // MAX_COUNT_DAY
                hasil[1] = rsPgsql.getBoolean("maxday"); // MAX_DAY
                hasil[2] = rsPgsql.getBoolean("amountmaxday"); // MAX_TRX
                hasil[3] = rsPgsql.getBoolean("amountminday"); // MIN_TRX
                hasil[4] = rsPgsql.getBoolean("isSamsung"); // IS_SAMSUNG
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return hasil;
    }

    @Override
    public String[] getConfigCairkanSaldo(String idaccount, String key_config) {
        String value_config[] = new String[4];
        String max, min, max_day = null;
        if (key_config.equalsIgnoreCase("SAMSUNG")) {
            max = "MAX_TRX_SAMSUNG";
            min = "MIN_TRX_SAMSUNG";
            max_day = "MAX_DAY_SAMSUNG";
        } else {
            max = "MAX_TRX";
            min = "MIN_TRX";
            max_day = "MAX_DAY";
        }

        //<editor-fold defaultstate="collapsed" desc="query getConfigCairkanSaldo">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "WITH detail1 AS (\n" + "	SELECT\n" + "		value_config AS \"MAX_TRX\"\n" + "	FROM\n"
                    + "		tb_config\n" + "	WHERE\n" + "		key_config = '" + max + "'\n"
                    + "AND id_tipetransaksi = 30\n" + "),\n" + " detail2 AS (\n" + "	SELECT\n"
                    + "		value_config AS \"MAX_DAY\"\n" + "	FROM\n" + "		tb_config\n" + "	WHERE\n"
                    + "		key_config = '" + max_day + "'\n" + "AND id_tipetransaksi = 30\n" + "),\n"
                    + " detail3 AS (\n" + "	SELECT\n" + "		value_config AS \"MAX_COUNT_DAY\"\n" + "	FROM\n"
                    + "		tb_config\n" + "	WHERE\n" + "		key_config = 'MAX_COUNT_DAY'\n"
                    + "AND id_tipetransaksi = 30\n" + "),\n" + " detail4 AS (\n" + "	SELECT\n"
                    + "		value_config AS \"MIN_TRX\"\n" + "	FROM\n" + "		tb_config\n" + "	WHERE\n"
                    + "		key_config = '" + min + "'\n" + "AND id_tipetransaksi = 30\n" + ") SELECT\n"
                    + "	detail1.\"MAX_TRX\",\n" + "	detail2.\"MAX_DAY\",\n" + "	detail3.\"MAX_COUNT_DAY\",\n"
                    + "	detail4.\"MIN_TRX\"\n" + "FROM\n" + "	detail1,\n" + "	detail2,\n" + "	detail3,\n"
                    + "	detail4";

            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                value_config[0] = rsPgsql.getString("MAX_COUNT_DAY"); // MAX_COUNT_DAY
                value_config[1] = rsPgsql.getString("MAX_DAY"); // MAX_DAY
                value_config[2] = rsPgsql.getString("MAX_TRX"); // MAX_TRX
                value_config[3] = rsPgsql.getString("MIN_TRX"); // MIN_TRX
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return value_config;
    }

    @Override
    public int[] getFee(int idtipefee, int idtipetransaksi, int idoperator, int nominal, String tipe) {
        int[] fee = new int[4];
        //<editor-fold defaultstate="collapsed" desc="query getFee">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPosgre = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connPosgre = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            if (idtipefee == 3) {
                String query = "SELECT\n" + "b.\"HargaCetak\",\n" + "b.\"HargaCetakMember\",\n" + "b.\"HargaBeli\",\n"
                        + "b.id_supplier\n" + "FROM\n" + "\t\"MsFeeSupplier\" AS b\n"
                        + "INNER JOIN \"MsFee\" d ON b.id_fee = d.id_fee\n" + "WHERE\n" + "\td.id_tipeaplikasi = 3\n"
                        + "AND d.id_tipetransaksi = " + idtipetransaksi + "\n" + "AND b.id_operator = " + idoperator
                        + " AND b.\"FlagActive\" = TRUE";
                ps = connPosgre.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    fee[0] = rs.getInt("id_supplier");
                    fee[1] = rs.getInt("HargaBeli");
                    fee[2] = rs.getInt("HargaCetak");
                    fee[3] = rs.getInt("HargaCetakMember");
                }
                if ((fee[1] + "" != null && !String.valueOf(fee[1]).isEmpty())
                        || (fee[2] + "" != null && !String.valueOf(fee[2]).isEmpty())
                        || fee[3] + "" != null && !String.valueOf(fee[3]).isEmpty()) {
                } else {
                    fee[1] = 0;
                    fee[2] = 0;
                    fee[3] = 0;
                }
            } else if (idtipefee == 1) {
                String getFee = "SELECT \"Nominal\" FROM \"MsFee\" WHERE id_tipetransaksi = " + idtipetransaksi
                        + " AND id_tipeaplikasi=3";
                ps = connPosgre.prepareStatement(getFee);
                rs = ps.executeQuery();

                while (rs.next()) {
                    fee[0] = rs.getInt("Nominal");
                }
                if (fee[0] + "" != null && !String.valueOf(fee[0]).isEmpty()) {
                } else {
                    fee[0] = 0;
                }
            } else if (idtipefee == 2) {
                String sqlGetFee = null;

                if (tipe.equalsIgnoreCase("samsung")) {
                    sqlGetFee = "SELECT\n" + "	\"MsFeeRange\".*\n" + "FROM\n" + "	\"MsFeeRange\"\n"
                            + "JOIN \"MsFee\" ON \"MsFeeRange\".id_fee = \"MsFee\".id_fee\n"
                            + "JOIN \"MsTipeTransaksi\" ON \"MsFee\".id_tipetransaksi = \"MsTipeTransaksi\".id_tipetransaksi\n"
                            + "WHERE\n" + "	\"MsFee\".id_tipetransaksi = " + idtipetransaksi
                            + " AND \"MsFee\".id_tipeaplikasi = 3\n" + "AND " + nominal
                            + " >= \"MsFeeRange\".\"RangeMin\"\n" + "AND " + nominal
                            + " <= \"MsFeeRange\".\"RangeMax\"\n" + "AND \"MsFeeRange\".\"FlagActive\" = 't'\n"
                            + "AND \"MsFee\".\"FlagActive\" = 't'\n" + "ORDER BY\n" + "	\"Nominal\" DESC";
                    //System.out.println(sqlGetFee);
                } else {
                    sqlGetFee = "SELECT\n" + "	\"MsFeeRange\".*\n" + "FROM\n" + "	\"MsFeeRange\"\n"
                            + "JOIN \"MsFee\" ON \"MsFeeRange\".id_fee = \"MsFee\".id_fee\n"
                            + "JOIN \"MsTipeTransaksi\" ON \"MsFee\".id_tipetransaksi = \"MsTipeTransaksi\".id_tipetransaksi\n"
                            + "WHERE\n" + "	\"MsFee\".id_tipetransaksi = " + idtipetransaksi
                            + " AND \"MsFee\".id_tipeaplikasi = 3\n" + "AND " + nominal
                            + " >= \"MsFeeRange\".\"RangeMin\"\n" + "AND " + nominal
                            + " <= \"MsFeeRange\".\"RangeMax\"\n" + "AND \"MsFeeRange\".\"FlagActive\" = 't'\n"
                            + "AND \"MsFee\".\"FlagActive\" = 't'\n" + "ORDER BY\n" + "	\"Nominal\" ASC";

                }

                ps = connPosgre.prepareStatement(sqlGetFee);
                rs = ps.executeQuery();

                while (rs.next()) {
                    fee[0] = rs.getInt("Nominal");
                }
                if (fee[0] + "" != null && !String.valueOf(fee[0]).isEmpty()) {
                } else {
                    fee[0] = 0;
                }
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connPosgre != null) {
                    connPosgre.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return fee;
    }

    @Override
    public boolean cekNorek(String noRekening, String kodeBank, String agent) {
        boolean status = false;
        //<editor-fold defaultstate="collapsed" desc="query cekNorek">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "WITH kode_bank AS (\n" + " SELECT\n" + "  id_bank\n" + " FROM\n" + "  \"MsBank\"\n" + " WHERE\n"
                    + "  kodebank = '" + kodeBank + "'\n" + ") SELECT\n" + " COUNT (d.id_blacklist) AS jumlah\n"
                    + "FROM\n" + " kode_bank b,\n" + " \"MsBlackList\" d\n" + "WHERE\n" + " b.id_bank = d.id_bank\n"
                    + "AND \"lower\"(d.account_type) = 'receiver' " + "AND d.account_number = '" + noRekening + "'\n"
                    + "AND d.id_tipetransaksi = 30 " + "AND d.flagactive = 't';";

            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            int count = 0;
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("jumlah");
            }
            if (count == 0) {
                status = true;
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String prosesInquiry(String data, JSONObject jObjMsSupplier) {
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = utilities.curlPost(jObjMsSupplier.get("urlGateway").toString(), data, false);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        return jObjResp.toString();
    }

    @Override
    public int getTrxId(String trxid) {
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query getTrxId">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "select count(*) from \"TrTransaksi\" where id_trx='" + trxid + "'";

            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return count;
    }

    @Override
    public String[] getStatusTrx(String trxid) {
        String status[] = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "SELECT \n"
                    + "  \"TrTransaksi\".\"CustomerReference\", \"TrTransaksi\".\"NoHPTujuan\", \"TrTransaksi\".\"NoRekTujuan\","
                    + "  \"TrTransaksi\".\"StatusTRX\", \"TrTransaksi\".\"id_pelanggan\"\n" + "FROM \n"
                    + "  public.\"TrTransaksi\"\n" + "WHERE \n" + "  \"TrTransaksi\".id_trx = '" + trxid + "';";

            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("StatusTRX");
                status[1] = rsPgsql.getString("CustomerReference");
                status[2] = rsPgsql.getString("NoHPTujuan");
                status[3] = rsPgsql.getString("NoRekTujuan");
                status[4] = rsPgsql.getString("id_pelanggan");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public boolean cekIsEmptyTransfer(String nORek, int nominal, String idaccount) {
        boolean cek = false;
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query cekIsEmptyTransfer">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "SELECT \"count\"(*) from \"TrTransaksi\" WHERE \"NoRekTujuan\" = '" + nORek
                    + "' and \"Nominal\" = '" + nominal + "' " + " AND \"TimeStamp\"::DATE = now()::DATE "
                    + " AND (id_memberaccount = '" + idaccount + "' OR id_agentaccount = '" + idaccount + "')"
                    + " AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";

            stPgsql = connPgsql.prepareStatement(sql);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
            if (count == 0) {
                cek = true;
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return cek;
    }

    @Override
    public String prosesBayar(JSONObject jsonReq, int[] fee, JSONObject jObjMsSupplier) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Transaksi Anda gagal");
        int id_tipetransaksi = 6;
        String NoAgen = null;
        int Nominal = 0;
        String Kode_Bank = null;
        String NORek = null;
        String verifikasi = "PIN";
        String SenderName = null;
        String namaPenerima = null;
        String nohp = null;
        int BiayaTransferBank = 0;
        int Hargacetak = 0;
        String PIN = null;
        String gen_id = null;

        String keteranganPenerima = null;
        String ktp = null;
        String alamat = null;
        String sandiDati = null;

        NoAgen = jsonReq.get("username").toString();
        Nominal = Integer.parseInt(jsonReq.get("nominal").toString());
        Kode_Bank = jsonReq.get("kodeBank").toString();
        NORek = jsonReq.get("nomorRekening").toString();
        SenderName = jsonReq.get("senderName").toString();
        namaPenerima = jsonReq.get("namaPenerima").toString();
        nohp = jsonReq.get("noHPPengirim").toString();
        BiayaTransferBank = Integer.parseInt(jsonReq.get("biayaTransferBank").toString());
        Hargacetak = Integer.parseInt(jsonReq.get("hargaCetak").toString());
        PIN = jsonReq.get("PIN").toString();
        gen_id = "TMN" + jsonReq.get("idtrx").toString();

        keteranganPenerima = jsonReq.containsKey("keteranganPenerima") ? jsonReq.get("keteranganPenerima").toString() : "Cairkan saldo";
        if (keteranganPenerima.isEmpty()) {
            keteranganPenerima = "Cairkan saldo";
        }

        ktp = jsonReq.containsKey("ktp") ? jsonReq.get("ktp").toString() : "999999999";
        alamat = jsonReq.containsKey("alamat") ? jsonReq.get("alamat").toString() : "JAKARTA";
        sandiDati = jsonReq.containsKey("sandiDati") ? jsonReq.get("sandiDati").toString() : "0300";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql2 = new DatabaseUtilitiesPgsql();
        Connection connPgsql2 = null;
        PreparedStatement stPgsql2 = null;
        ResultSet rsPgsql2 = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);
            connPgsql2 = databaseUtilitiesPgsql2.getConnection(this.entityConfig);
            connPgsql2.setAutoCommit(false);

            String[] StatusRekeningAgen = new String[5];
            //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgen">
            String cekStatusRekening = "SELECT \n" + "\"MsAccountStatus\".\"NamaAccountStatus\"\n"
                    + ",\"MsAgentAccount\".\"LastBalance\"\n" + ",\"MsAgentAccount\".\"StatusKartu\"\n"
                    + ",\"MsAgentAccount\".id_agentaccount\n" + ",\"MsAgentAccount\".\"PIN\"\n"
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\"\n" + " WHERE \n"
                    + "\"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus \n"
                    + "AND \"MsAgentAccount\".\"id_agentaccount\" = '" + NoAgen + "';";
            stPgsql = connPgsql.prepareStatement(cekStatusRekening);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                StatusRekeningAgen[0] = rsPgsql.getString("NamaAccountStatus");
                StatusRekeningAgen[1] = rsPgsql.getString("LastBalance");
                StatusRekeningAgen[2] = rsPgsql.getString("StatusKartu");
                StatusRekeningAgen[3] = rsPgsql.getString("id_agentaccount");
                StatusRekeningAgen[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
            //</editor-fold>
            String StatusRekening = "";
            try {
                StatusRekening = StatusRekeningAgen[0];
            } catch (Exception e) {
                StatusRekening = "";
            }
            int saldo = 0;
            try {
                saldo = Integer.parseInt(StatusRekeningAgen[1]);
            } catch (Exception e) {
                saldo = 0;
            }
            String date = utilities.getDate("yyyy-MM-dd HH:mm:ss");
            String tipe_transaksi = String.valueOf(id_tipetransaksi);
            if (StatusRekening.equals("ACTIVE") || StatusRekening.equals("INACTIVE")
                    || StatusRekening.equals("DORMANT")) {
                if (!PIN.equals(StatusRekeningAgen[4])) {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "PIN Anda salah");
                    return jsonResp.toString();
                }

                int total = Hargacetak;
                String validasiSaldo = this.isSaldoCukup(NoAgen, total);
                if (validasiSaldo.equalsIgnoreCase("00")) {
                    //deduct balance agent
                    String statusDeduct = "NOK";
                    //<editor-fold defaultstate="collapsed" desc="query deduct balance agent">
                    String sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE id_agentaccount = '" + NoAgen + "' FOR UPDATE";

                    String deductBalance = "UPDATE \"MsAgentAccount\" " + " SET \"LastBalance\"= \"LastBalance\" - " + total
                            + " " + " WHERE \"id_agentaccount\"='" + NoAgen + "';";

                    stPgsql = connPgsql.prepareStatement(sql);
                    rsPgsql = stPgsql.executeQuery();

                    long lastBalance = 0;
                    while (rsPgsql.next()) {
                        lastBalance = rsPgsql.getLong("LastBalance");
                    }
                    if (lastBalance < Long.parseLong(String.valueOf(total))) {
                        statusDeduct = "NOK";
                    }
                    stPgsql = connPgsql.prepareStatement(deductBalance);
                    int status = stPgsql.executeUpdate();
                    if (status == 1) {
                        statusDeduct = "OK";
                    } else {
                        statusDeduct = "NOK";
                    }
                    //</editor-fold>
                    if (statusDeduct.equalsIgnoreCase("OK")) {
                        int komTrue = BiayaTransferBank - fee[1];
                        String idtrxsupp = null;
                        if (jObjMsSupplier.get("idSupplier").toString().equalsIgnoreCase("19")) {
                            idtrxsupp = gen_id;
                            String info = "PENDING";
                            String Keterangan = "Cairkan saldo ke " + this.getNamaBank(Kode_Bank) + " " + NORek
                                    + " atas nama " + namaPenerima.toUpperCase() + " sebesar " + Nominal;
                            Keterangan = Keterangan.replace("'", "''");
                            String cetakTransaksi = "INSERT INTO \"TrTransaksi\"\n"
                                    + "(id_tipetransaksi, id_agentaccount,\n" + "  id_supplier,\n"
                                    + "\"Nominal\", \"StatusTRX\", \"StatusDeduct\",\n"
                                    + "\"StatusRefund\", \"TypeTRX\", "
                                    + "\"OpsVerifikasi\", \"Total\", id_trx,\n"
                                    + "\"StatusKomisi\", \"TimeStamp\", id_tipeaplikasi, id_operator, \"Keterangan\", id_bank, \"Biaya\""
                                    + ", \"Nomorktp\"" + ", \"NoRekTujuan\", \"StatusDana\", id_trxsupplier, hargabeli, hargajual)\n" + "VALUES\n" + "(" + 30
                                    + ", '" + NoAgen + "', " + jObjMsSupplier.get("idSupplier").toString() + ",\n" + "" + Nominal + ", " + "'OPEN', '" + statusDeduct
                                    + "','OPEN','WITHDRAW', " + "'" + verifikasi + "', " + total + ", '" + gen_id
                                    + "', 'OPEN', now(), " + 3 + ", " + 36 + ", '" + Keterangan + "',"
                                    + this.getidbank(Kode_Bank) + "," + "'" + BiayaTransferBank + "'," + "'" + ktp + "',"
                                    + "'" + NORek + "', 'DEDUCT', '" + idtrxsupp + "', '" + (fee[1] + Nominal) + "','" + total + "')";
                            stPgsql = connPgsql.prepareStatement(cetakTransaksi, Statement.RETURN_GENERATED_KEYS);
                            int hasil = stPgsql.executeUpdate();

                            String id_transaksi = null;
                            if (hasil > 0) {
                                rsPgsql = stPgsql.getGeneratedKeys();
                                while (rsPgsql.next()) {
                                    id_transaksi = rsPgsql.getString(1);
                                }
                            }
                            // agent
                            //get lastbalance agent
                            String lastbalance = "";
                            //<editor-fold defaultstate="collapsed" desc="query getLastBalance">
                            String getAmount = "SELECT  " + "  \"MsAgentAccount\".\"LastBalance\" " + "FROM  "
                                    + "  public.\"MsAgentAccount\" " + "WHERE  " + "  \"MsAgentAccount\".\"id_agentaccount\" = '"
                                    + NoAgen + "';";

                            stPgsql = connPgsql.prepareStatement(getAmount);
                            rsPgsql = stPgsql.executeQuery();
                            while (rsPgsql.next()) {
                                lastbalance = rsPgsql.getString("LastBalance");
                            }
                            if (lastbalance != null && !lastbalance.isEmpty()) {

                            } else {
                                System.err.println("masuk");
                                lastbalance = "0";
                            }
                            //</editor-fold>
                            //insert tmnStock
                            //<editor-fold defaultstate="collapsed" desc="query tmnStock">
                            String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                    + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                    + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\") " + "    VALUES (" + lastbalance + ", now(), "
                                    + 0 + ", '" + "Agent" + "', " + Hargacetak + ",  " + 0 + ", '" + NoAgen
                                    + "', '" + "WITHDRAW" + "', '" + id_transaksi + "', '" + 0 + "');";

                            stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                            status = stPgsql.executeUpdate();
                            //</editor-fold>

                            connPgsql.commit();

                            JSONObject ThirdParty = new JSONObject();
                            String Signature = utilities.getSignature(NORek + "" + NoAgen + "" + gen_id + "" + Kode_Bank);
                            ThirdParty.put("type", "INQUIRY");
                            ThirdParty.put("TrxId", gen_id);
                            ThirdParty.put("SenderAccount", nohp);
                            ThirdParty.put("SenderName", SenderName.length() < 30 ? SenderName : SenderName.substring(0, 30));
                            ThirdParty.put("BeneficiaryAccount", NORek);
                            ThirdParty.put("BeneficiaryName", namaPenerima.length() < 30 ? namaPenerima : namaPenerima.substring(0, 30));
                            ThirdParty.put("Description", keteranganPenerima);
                            ThirdParty.put("Amount", Nominal + "");
                            ThirdParty.put("ReceiverBankCode", Kode_Bank);
                            ThirdParty.put("AccountId", NoAgen);
                            ThirdParty.put("dateTime", date);
                            ThirdParty.put("Signature", Signature);
                            ThirdParty.put("SenderSeluler", nohp);

                            ThirdParty.put("SenderKtp", ktp);
                            ThirdParty.put("SenderAddress", alamat);
                            ThirdParty.put("BeneficiaryCity", sandiDati);
                            ThirdParty.put("SenderCity", sandiDati);

                            //insert log
                            int idLog = utilities.insertLog(gen_id, ThirdParty);
                            String inquiry = this.prosesInquiry(ThirdParty.toString(), jObjMsSupplier);
                            //updateLog
                            utilities.updateLog(idLog, inquiry);

                            JSONParser jsonParser = new JSONParser();
                            JSONObject jsonObject = (JSONObject) jsonParser.parse(inquiry);
                            String httpStat = jsonObject.get("ResponseCode").toString();
                            if (httpStat.equals("200")) {
                                String ACK = (String) jsonObject.get("ACK");
                                if (ACK.equals("OK") || ACK.equals("SUSPECT")) {
                                    date = utilities.getDate("yyyy-MM-dd HH:mm:ss");
                                    String message = jsonObject.get("ResponseMsgKey").toString();
                                    Signature = utilities.getSignature(NORek + "" + gen_id + "" + Kode_Bank
                                            + jsonObject.get("ResponseMsgKey").toString());
                                    ThirdParty = new JSONObject();
                                    ThirdParty.put("type", "TRANSFERONLINE");
                                    ThirdParty.put("TrxId", gen_id);
                                    ThirdParty.put("AccountId", NoAgen);
                                    ThirdParty.put("datetime", date);
                                    ThirdParty.put("MsgKey", message);
                                    ThirdParty.put("Signature", Signature);
                                    ThirdParty.put("note", keteranganPenerima);

                                    //insert log
                                    idLog = utilities.insertLog(gen_id, ThirdParty);
                                    inquiry = this.prosesTransfer(ThirdParty.toString(), jObjMsSupplier);
                                    //updateLog
                                    utilities.updateLog(idLog, inquiry);

                                    jsonParser = new JSONParser();
                                    jsonObject = (JSONObject) jsonParser.parse(inquiry);
                                    httpStat = jsonObject.get("ResponseCode").toString();
                                    if (httpStat.equals("200")) {
                                        ACK = (String) jsonObject.get("ACK");
                                        if (ACK.equals("OK")) {
                                            int nominalTrStock = Nominal + fee[1];
                                            //<editor-fold defaultstate="collapsed" desc="query update TrTransaksi">
                                            String updateTransaksi = "UPDATE \"TrTransaksi\" "
                                                    + "SET \"StatusTRX\" = 'SUKSES', \"StatusDana\"='DEDUCT' "
                                                    + ", \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK', "
                                                    + "timestampupdatetrx = now() " + "WHERE id_trx = '" + gen_id + "'";
                                            stPgsql2 = connPgsql2.prepareStatement(updateTransaksi);
                                            hasil = stPgsql2.executeUpdate();
                                            //</editor-fold>
                                            //<editor-fold defaultstate="collapsed" desc="query insert TrStock">
                                            sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                                    + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                                    + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\") " + "    VALUES (" + "123456" + ", now(), "
                                                    + 0 + ", '" + "True" + "', " + nominalTrStock + ", 0 , '" + 0
                                                    + "', '" + "DEPOSIT" + "', '" + id_transaksi + "', '" + 0 + "');";

                                            stPgsql2 = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                                            status = stPgsql2.executeUpdate();
                                            //</editor-fold>
                                            //<editor-fold defaultstate="collapsed" desc="query komisi true">
                                            sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                                    + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                                    + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\") " + "    VALUES (" + "123456" + ", now(), "
                                                    + 100 + ", '" + "True" + "', " + komTrue + ",  " + 0 + ", '" + 1
                                                    + "', '" + "DEPOSIT" + "', '" + id_transaksi + "', '" + 0 + "');";

                                            stPgsql2 = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                                            status = stPgsql2.executeUpdate();
                                            //</editor-fold>
                                            //<editor-fold defaultstate="collapsed" desc="query TrStock supplier">
                                            sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                                    + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                                    + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\") " + "    VALUES (" + "123456" + ", now(), "
                                                    + 0 + ", '" + "Supplier" + "', " + nominalTrStock + ", 0, '" + jObjMsSupplier.get("idSupplier").toString()
                                                    + "', '" + "WITHDRAW" + "', '" + id_transaksi + "', '" + 0 + "');";

                                            stPgsql2 = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                                            status = stPgsql2.executeUpdate();
                                            //</editor-fold>

                                            jsonResp.put("id_Trx", gen_id);
                                            jsonResp.put("pesan", Keterangan);
                                            jsonResp.put("ID", "transfer");
                                            jsonResp.put("timestamp", utilities.getDate().toString());
                                            jsonResp.put("statusTRX", "SUKSES");
                                            jsonResp.put("ACK", "OK");

                                            String nominal = NumberFormat.getInstance().format(Nominal);

                                            String pesan = "Cairkan saldo ke " + this.getNamaBank(Kode_Bank) + " " + NORek
                                                    + " atas nama " + namaPenerima.toUpperCase() + " sebesar Rp" + nominal + " pada "
                                                    + new SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date()) + " SUKSES";

                                            //<editor-fold defaultstate="collapsed" desc="query insert notif">
                                            sql = "INSERT INTO \"Notifikasi\" "
                                                    + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                                    + " VALUES (?, ?, ?,'FALSE',now())";
                                            stPgsql = connPgsql2.prepareStatement(sql);
                                            stPgsql.setString(1, pesan);
                                            stPgsql.setString(2, NoAgen);
                                            stPgsql.setString(3, gen_id);
                                            stPgsql.executeUpdate();
                                            //</editor-fold>

                                            String smsScript = "";
                                            //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                                            sql = "select"
                                                    + "        this_.id,"
                                                    + "        this_.account_type,"
                                                    + "        this_.created_by,"
                                                    + "        this_.flag_active,"
                                                    + "        this_.id_tipeaplikasi,"
                                                    + "        this_.id_tipetransaksi,"
                                                    + "        this_.sms_script,"
                                                    + "        this_.timestamp,"
                                                    + "        this_.trx_status "
                                                    + "    from"
                                                    + "        public.\"MsSMSNotification\" this_ "
                                                    + "    where"
                                                    + "        this_.id_tipetransaksi=? "
                                                    + "        and this_.id_tipeaplikasi=? "
                                                    + "        and this_.account_type=? "
                                                    + "        and this_.trx_status=?";
                                            stPgsql = connPgsql.prepareStatement(sql);
                                            stPgsql.setLong(1, 30);
                                            stPgsql.setLong(2, 3);
                                            stPgsql.setString(3, "AGENT");
                                            stPgsql.setString(4, "SUKSES");
                                            rsPgsql = stPgsql.executeQuery();
                                            while (rsPgsql.next()) {
                                                smsScript = rsPgsql.getString("sms_script");
                                            }
                                            if (smsScript != null) {
                                                pesan = String.format(smsScript, this.getNamaBank(Kode_Bank), NORek, namaPenerima.toUpperCase(), nominal, new SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date()));
                                                new SmsHelper().sent(gen_id, nohp, pesan, "CAIRKAN_SALDO", entityConfig);
                                            }
                                            //</editor-fold>
                                        } else if (ACK.equals("SUSPECT")) {
                                            // update tr transaksi
                                            String updateTransaksi = "UPDATE \"TrTransaksi\" "
                                                    + "SET \"StatusTRX\" = 'PENDING', \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'PENDING' "
                                                    + " WHERE id_trx = '" + gen_id + "'";
                                            stPgsql2 = connPgsql2.prepareStatement(updateTransaksi);
                                            hasil = stPgsql2.executeUpdate();

                                            String updateTemp = "UPDATE \"TempKomisi\" "
                                                    + "SET \"Status\" = 'PENDING', \"StatusTransaksi\" = 'PENDING' "
                                                    + "WHERE \"id_trx\" = '" + gen_id + "'";
                                            stPgsql2 = connPgsql2.prepareStatement(updateTemp);
                                            stPgsql2.executeUpdate();

                                            String pesan = "Transaksi Anda sedang diproses";

                                            jsonResp.put("id_Trx", gen_id);
                                            jsonResp.put("pesan", pesan);
                                            jsonResp.put("timestamp", utilities.getDate().toString());
                                            jsonResp.put("statusTRX", "PENDING");
                                            jsonResp.put("ACK", "OK");
                                        } else {
                                            //<editor-fold defaultstate="collapsed" desc="query refund agent">
                                            String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = '" + NoAgen + "' FOR UPDATE";
                                            sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + String.valueOf(total) + " "
                                                    + "WHERE id_agentaccount = '" + NoAgen + "'";

                                            stPgsql2 = connPgsql2.prepareStatement(sql1);
                                            rsPgsql2 = stPgsql2.executeQuery();

                                            stPgsql2 = connPgsql2.prepareStatement(sql);
                                            hasil = stPgsql2.executeUpdate();
                                            //</editor-fold>

                                            //<editor-fold defaultstate="collapsed" desc="query update TrTransaksi>
                                            sql = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK',"
                                                    + "\"StatusDana\" = 'REFUND', \"StatusDeduct\" = 'NOK', \"StatusKomisi\" = 'NOK',"
                                                    + "timestamprefund = now() " + "WHERE \"id_trx\" = '" + gen_id
                                                    + "'";
                                            stPgsql2 = connPgsql2.prepareStatement(sql);
                                            hasil = stPgsql2.executeUpdate();
                                            //</editor-fold>

                                            // UpdateTMNStock agent refund
                                            //<editor-fold defaultstate="collapsed" desc="query insert TrStock">
                                            sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                                    + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                                    + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\") " + "    VALUES (" + saldo + ", now(), "
                                                    + 0 + ", '" + "Agent" + "', " + total + ",  " + 0 + ", '" + NoAgen
                                                    + "', '" + "REFUND" + "', '" + id_transaksi + "', '" + 0 + "');";

                                            stPgsql2 = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                                            status = stPgsql2.executeUpdate();
                                            //</editor-fold>

                                            String pesan = "Transaksi Anda gagal dengan nomor transaksi: " + gen_id;
                                            //<editor-fold defaultstate="collapsed" desc="query insert notif">
                                            sql = "INSERT INTO \"Notifikasi\" "
                                                    + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                                    + " VALUES (?, ?, ?,'FALSE',now())";
                                            stPgsql = connPgsql2.prepareStatement(sql);
                                            stPgsql.setString(1, pesan);
                                            stPgsql.setString(2, NoAgen);
                                            stPgsql.setString(3, gen_id);
                                            stPgsql.executeUpdate();
                                            //</editor-fold>

                                            jsonResp.put("id_Trx", gen_id);
                                            jsonResp.put("ACK", "NOK");
                                            jsonResp.put("pesan", "Transaksi Anda gagal");
                                            jsonResp.put("timestamp", utilities.getDate().toString());
                                            jsonResp.put("statusTRX", "GAGAL");

                                            //sent sms
                                            String smsScript = "";
                                            //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                                            sql = "select"
                                                    + "        this_.id,"
                                                    + "        this_.account_type,"
                                                    + "        this_.created_by,"
                                                    + "        this_.flag_active,"
                                                    + "        this_.id_tipeaplikasi,"
                                                    + "        this_.id_tipetransaksi,"
                                                    + "        this_.sms_script,"
                                                    + "        this_.timestamp,"
                                                    + "        this_.trx_status "
                                                    + "    from"
                                                    + "        public.\"MsSMSNotification\" this_ "
                                                    + "    where"
                                                    + "        this_.id_tipetransaksi=? "
                                                    + "        and this_.id_tipeaplikasi=? "
                                                    + "        and this_.account_type=? "
                                                    + "        and this_.trx_status=?";
                                            stPgsql = connPgsql.prepareStatement(sql);
                                            stPgsql.setLong(1, 30);
                                            stPgsql.setLong(2, 3);
                                            stPgsql.setString(3, "AGENT");
                                            stPgsql.setString(4, "GAGAL");
                                            rsPgsql = stPgsql.executeQuery();
                                            while (rsPgsql.next()) {
                                                smsScript = rsPgsql.getString("sms_script");
                                            }
                                            if (smsScript != null) {
                                                pesan = String.format(smsScript, this.getNamaBank(Kode_Bank), NORek, namaPenerima.toUpperCase(), String.valueOf(Nominal), new SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date()));
                                                new SmsHelper().sent(gen_id, nohp, pesan, "CAIRKAN_SALDO", entityConfig);
                                            }
                                            //</editor-fold>
                                        }
                                    } else {
                                        // update tr transaksi
                                        String updateTransaksi = "UPDATE \"TrTransaksi\" "
                                                + "SET \"StatusTRX\" = 'PENDING', \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'PENDING' "
                                                + " WHERE id_trx = '" + gen_id + "'";
                                        stPgsql2 = connPgsql2.prepareStatement(updateTransaksi);
                                        hasil = stPgsql2.executeUpdate();

                                        String updateTemp = "UPDATE \"TempKomisi\" "
                                                + "SET \"Status\" = 'PENDING', \"StatusTransaksi\" = 'PENDING' "
                                                + "WHERE \"id_trx\" = '" + gen_id + "'";
                                        stPgsql2 = connPgsql2.prepareStatement(updateTemp);
                                        stPgsql2.executeUpdate();

                                        String pesan = "Transaksi Anda sedang diproses";

                                        jsonResp.put("id_Trx", gen_id);
                                        jsonResp.put("pesan", pesan);
                                        jsonResp.put("timestamp", utilities.getDate().toString());
                                        jsonResp.put("statusTRX", "PENDING");
                                        jsonResp.put("ACK", "OK");
                                    }
                                } else {
                                    //<editor-fold defaultstate="collapsed" desc="query refund agent">
                                    String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = '" + NoAgen + "' FOR UPDATE";
                                    sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + String.valueOf(total) + " "
                                            + "WHERE id_agentaccount = '" + NoAgen + "'";

                                    stPgsql2 = connPgsql2.prepareStatement(sql1);
                                    rsPgsql2 = stPgsql2.executeQuery();

                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    hasil = stPgsql2.executeUpdate();
                                    //</editor-fold>

                                    //<editor-fold defaultstate="collapsed" desc="query update TrTransaksi>
                                    sql = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK',"
                                            + "\"StatusDana\" = 'REFUND', \"StatusDeduct\" = 'NOK', \"StatusKomisi\" = 'NOK',"
                                            + "timestamprefund = now() " + "WHERE \"id_trx\" = '" + gen_id
                                            + "'";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    hasil = stPgsql2.executeUpdate();
                                    //</editor-fold>

                                    // UpdateTMNStock agent refund
                                    //<editor-fold defaultstate="collapsed" desc="query insert TrStock">
                                    sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                            + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                            + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\") " + "    VALUES (" + saldo + ", now(), "
                                            + 0 + ", '" + "Agent" + "', " + total + ",  " + 0 + ", '" + NoAgen
                                            + "', '" + "REFUND" + "', '" + id_transaksi + "', '" + 0 + "');";

                                    stPgsql2 = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                                    status = stPgsql2.executeUpdate();
                                    //</editor-fold>

                                    String pesan = "Transaksi Anda gagal dengan nomor transaksi: " + gen_id;
                                    //<editor-fold defaultstate="collapsed" desc="query insert notif">
                                    sql = "INSERT INTO \"Notifikasi\" "
                                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                            + " VALUES (?, ?, ?,'FALSE',now())";
                                    stPgsql = connPgsql2.prepareStatement(sql);
                                    stPgsql.setString(1, pesan);
                                    stPgsql.setString(2, NoAgen);
                                    stPgsql.setString(3, gen_id);
                                    stPgsql.executeUpdate();
                                    //</editor-fold>

                                    jsonResp.put("id_Trx", gen_id);
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan", "Transaksi Anda gagal");
                                    jsonResp.put("timestamp", utilities.getDate().toString());
                                    jsonResp.put("statusTRX", "GAGAL");

                                    //sent sms
                                    String smsScript = "";
                                    //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                                    sql = "select"
                                            + "        this_.id,"
                                            + "        this_.account_type,"
                                            + "        this_.created_by,"
                                            + "        this_.flag_active,"
                                            + "        this_.id_tipeaplikasi,"
                                            + "        this_.id_tipetransaksi,"
                                            + "        this_.sms_script,"
                                            + "        this_.timestamp,"
                                            + "        this_.trx_status "
                                            + "    from"
                                            + "        public.\"MsSMSNotification\" this_ "
                                            + "    where"
                                            + "        this_.id_tipetransaksi=? "
                                            + "        and this_.id_tipeaplikasi=? "
                                            + "        and this_.account_type=? "
                                            + "        and this_.trx_status=?";
                                    stPgsql = connPgsql.prepareStatement(sql);
                                    stPgsql.setLong(1, 30);
                                    stPgsql.setLong(2, 3);
                                    stPgsql.setString(3, "AGENT");
                                    stPgsql.setString(4, "GAGAL");
                                    rsPgsql = stPgsql.executeQuery();
                                    while (rsPgsql.next()) {
                                        smsScript = rsPgsql.getString("sms_script");
                                    }
                                    if (smsScript != null) {
                                        pesan = String.format(smsScript, this.getNamaBank(Kode_Bank), NORek, namaPenerima.toUpperCase(), String.valueOf(Nominal), new SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date()));
                                        new SmsHelper().sent(gen_id, nohp, pesan, "CAIRKAN_SALDO", entityConfig);
                                    }
                                    //</editor-fold>
                                }
                            } else {
                                //<editor-fold defaultstate="collapsed" desc="query refund agent">
                                String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = '" + NoAgen + "' FOR UPDATE";
                                sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + String.valueOf(total) + " "
                                        + "WHERE id_agentaccount = '" + NoAgen + "'";

                                stPgsql2 = connPgsql2.prepareStatement(sql1);
                                rsPgsql2 = stPgsql2.executeQuery();

                                stPgsql2 = connPgsql2.prepareStatement(sql);
                                hasil = stPgsql2.executeUpdate();
                                //</editor-fold>

                                //<editor-fold defaultstate="collapsed" desc="query update TrTransaksi>
                                sql = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK',"
                                        + "\"StatusDana\" = 'REFUND', \"StatusDeduct\" = 'NOK', \"StatusKomisi\" = 'NOK',"
                                        + "timestamprefund = now() " + "WHERE \"id_trx\" = '" + gen_id
                                        + "'";
                                stPgsql2 = connPgsql2.prepareStatement(sql);
                                hasil = stPgsql2.executeUpdate();
                                //</editor-fold>

                                // UpdateTMNStock agent refund
                                //<editor-fold defaultstate="collapsed" desc="query insert TrStock">
                                sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                        + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                        + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\") " + "    VALUES (" + saldo + ", now(), "
                                        + 0 + ", '" + "Agent" + "', " + total + ",  " + 0 + ", '" + NoAgen
                                        + "', '" + "REFUND" + "', '" + id_transaksi + "', '" + 0 + "');";

                                stPgsql2 = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                                status = stPgsql2.executeUpdate();
                                //</editor-fold>

                                String pesan = "Transaksi Anda gagal dengan nomor transaksi: " + gen_id;
                                //<editor-fold defaultstate="collapsed" desc="query insert notif">
                                sql = "INSERT INTO \"Notifikasi\" "
                                        + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                        + " VALUES (?, ?, ?,'FALSE',now())";
                                stPgsql = connPgsql2.prepareStatement(sql);
                                stPgsql.setString(1, pesan);
                                stPgsql.setString(2, NoAgen);
                                stPgsql.setString(3, gen_id);
                                stPgsql.executeUpdate();
                                //</editor-fold>

                                jsonResp.put("id_Trx", gen_id);
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Transaksi Anda gagal");
                                jsonResp.put("timestamp", utilities.getDate().toString());
                                jsonResp.put("statusTRX", "GAGAL");

                                //sent sms
                                String smsScript = "";
                                //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                                sql = "select"
                                        + "        this_.id,"
                                        + "        this_.account_type,"
                                        + "        this_.created_by,"
                                        + "        this_.flag_active,"
                                        + "        this_.id_tipeaplikasi,"
                                        + "        this_.id_tipetransaksi,"
                                        + "        this_.sms_script,"
                                        + "        this_.timestamp,"
                                        + "        this_.trx_status "
                                        + "    from"
                                        + "        public.\"MsSMSNotification\" this_ "
                                        + "    where"
                                        + "        this_.id_tipetransaksi=? "
                                        + "        and this_.id_tipeaplikasi=? "
                                        + "        and this_.account_type=? "
                                        + "        and this_.trx_status=?";
                                stPgsql = connPgsql.prepareStatement(sql);
                                stPgsql.setLong(1, 30);
                                stPgsql.setLong(2, 3);
                                stPgsql.setString(3, "AGENT");
                                stPgsql.setString(4, "GAGAL");
                                rsPgsql = stPgsql.executeQuery();
                                while (rsPgsql.next()) {
                                    smsScript = rsPgsql.getString("sms_script");
                                }
                                if (smsScript != null) {
                                    pesan = String.format(smsScript, this.getNamaBank(Kode_Bank), NORek, namaPenerima.toUpperCase(), String.valueOf(Nominal), new SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date()));
                                    new SmsHelper().sent(gen_id, nohp, pesan, "CAIRKAN_SALDO", entityConfig);
                                }
                                //</editor-fold>
                            }
                        }
                    } else {
                        jsonResp.put("id_Trx", gen_id);
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Transaksi Anda gagal");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Saldo Tidak Cukup");
                }
            }
            connPgsql2.commit();
        } catch (Exception ex) {
            try {
                connPgsql.rollback();
                connPgsql2.rollback();
            } catch (SQLException e) {
                String s = Throwables.getStackTraceAsString(e);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
                if (rsPgsql2 != null) {
                    rsPgsql2.close();
                }
                if (stPgsql2 != null) {
                    stPgsql2.close();
                }
                if (connPgsql2 != null) {
                    connPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>


        return jsonResp.toString();
    }

    private String prosesTransfer(String data, JSONObject jObjMsSupplier) {
        String response = "";
        try {

            JSONObject jObjResp = utilities.curlPost(jObjMsSupplier.get("urlGateway").toString(), data, false);
            response = jObjResp.toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        return response;
    }

    private String getidbank(String kode_bank) {
        String idbank = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String SQL = "SELECT \"MsBank\".id_bank FROM \"MsBank\" WHERE kodebank = '" + kode_bank + "';";
            PreparedStatement ps = connPgsql.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idbank = rs.getString("id_bank");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return idbank;
    }

    private String getNamaBank(String kode_bank) {
        String idbank = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String SQL = "SELECT \"NamaBank\" FROM \"MsBank\" WHERE kodebank = '" + kode_bank + "'";
            PreparedStatement ps = connPgsql.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idbank = rs.getString("NamaBank");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return idbank;
    }

    private String isSaldoCukup(String idAccount, int total) {
        String hasil = "--";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            String bit7 = idAccount.substring(6, 7);
            String q = "";
            String tipeAkun = "AGENT";
            q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo FROM \"MsAgentAccount\" WHERE id_agentaccount = '"
                    + idAccount + "' FOR UPDATE";
            if (q.trim().length() != 0) {

                long saldo = 0;

                stPgsql = connPgsql.prepareStatement(q);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    saldo = rsPgsql.getLong("saldo");
                }

                boolean lanjut = false;

                if (tipeAkun.equals("MEMBER")) {

                    q = "SELECT\n" + " d.\"LimitBalance\" :: INTEGER >= " + saldo + " AS status\n" + "FROM\n"
                            + " \"MsMemberAccount\" b\n"
                            + "INNER JOIN \"MsStatusVerifikasi\" d ON b.id_statusverifikasi = d.id_statusverifikasi\n"
                            + "WHERE\n" + " id_memberaccount = '" + idAccount + "'";
                    stPgsql = connPgsql.prepareStatement(q);
                    rsPgsql = stPgsql.executeQuery();
                    while (rsPgsql.next()) {
                        lanjut = rsPgsql.getBoolean("status");
                    }

                    if (!lanjut) {
                        hasil = "02";        //BALANCEINSUFICIENT LEBIH DARI LIMIT, DIBLOKIR

                        q = "UPDATE \"MsMemberAccount\" SET id_accountstatus = 5, \"Remark\" = 'Block by Android - Saldo melebihi limit member - " + utilities.getDate("yyyy-MM-dd HH:mm:ss") + "' WHERE id_memberaccount = '"
                                + idAccount + "'";
                        stPgsql = connPgsql.prepareStatement(q);
                        int hasilUpdate = stPgsql.executeUpdate();
                    }

                } else {
                    lanjut = true;
                }
                if (lanjut) {
                    if (saldo >= total) {
                        hasil = "00";        //BALANCEINSUFICIENT CUKUP
                    } else {
                        hasil = "01";        //BALANCEINSUFICIENT TIDAK CUKUP
                    }
                }
            }
            connPgsql.commit();
        } catch (Exception ex) {
            try {
                connPgsql.rollback();
            } catch (SQLException e) {
                String s = Throwables.getStackTraceAsString(e);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return hasil;
    }
}
