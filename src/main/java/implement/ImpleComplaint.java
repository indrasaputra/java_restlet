package implement;

import com.google.common.base.Throwables;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcComplaint;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImpleComplaint implements InterfcComplaint {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerLog controllerLog;
    private final Utilities utilities;

    public ImpleComplaint(EntityConfig entityConfig){
        this.entityConfig = entityConfig;
        this.hlp = new Helper();
        this.controllerLog = new ControllerLog();
        this.utilities = new Utilities();
    }

    @Override
    public String getListTipeTransaksi() {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT\n" +
                    "	* \n" +
                    "FROM\n" +
                    "	\"MsTipeTransaksi\" \n" +
                    "WHERE\n" +
                    "	\"FlagActive\" = 't' \n" +
                    "ORDER BY\n" +
                    "	\"NamaTipeTransaksi\"";
            st = conn.prepareStatement(query);
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idTipeTransaksi", rs.getString("id_tipetransaksi"));
                jObj.put("namaTipeTransaksi", rs.getString("NamaTipeTransaksi"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("tipeTransaksi", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String saveComplaint(final JSONObject jsonReq) {
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "Submit Complaint gagal, silakan coba lagi.");

        String tipeTransaksi = jsonReq.get("tipeTransaksi").toString();
        String tingkatKeluhan = jsonReq.get("tingkatKeluhan").toString();
        String keluhan = jsonReq.get("keluhan").toString();
        String idAccount = jsonReq.get("idAccount").toString();

        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();

        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);
            String query = "INSERT INTO tr_complaint ( id_account, title_complaint, detail_complaint, priority_complaint, created_timestamp, created_by, flag_active, trx_type_id, id_tipeaccount )\n" +
                    "VALUES\n" +
                    "	(?,?,?,?,?,?,?,?,?)";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, idAccount);

            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy-HHmmss");
            String title = "CU-"+format.format(date);

            stPgsql.setString(2, title);
            stPgsql.setString(3, keluhan);
            stPgsql.setString(4, tingkatKeluhan);
            stPgsql.setTimestamp(5, this.utilities.getDate());
            stPgsql.setString(6, "ANDROID AGENT");
            stPgsql.setString(7, "OPEN");
            stPgsql.setInt(8, Integer.valueOf(tipeTransaksi));
            stPgsql.setInt(9, 2);
            stPgsql.executeUpdate();
            rsPgsql = stPgsql.getGeneratedKeys();
            connPgsql.commit();
        } catch (SQLException ex) {
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                String s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (ParserConfigurationException ex) {
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                String s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "Terimakasih telah menggunakan layanan bantuan kami. Customer kami akan segera menghubungi Anda.");
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }

        return jobjResp.toString();
    }
}
