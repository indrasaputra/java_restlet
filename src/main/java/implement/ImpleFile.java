/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import interfc.InterfcFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.simple.JSONObject;
import entity.EntityConfig;
import helper.Helper;

/**
 *
 * @author DPPH
 */
public class ImpleFile implements InterfcFile {

    EntityConfig entityConfig;
    Helper hlp;
//    private final ControllerLog ctrlLog;
    String nameFile;

    public ImpleFile(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.hlp = new Helper();
//        this.ctrlLog = new ControllerLog();
    }

    @Override
    public String saveFile(EntityConfig conf, String nameFile) {
        this.nameFile = nameFile;
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        String query;
        JSONObject jObj;
        JSONObject obj;
        String queryUpd;
        JSONObject jObjUpd;
        JSONObject objUpd;
        queryUpd = "INSERT INTO file (f_id, f_name) "
                + "VALUES (?,?)";

        jObjUpd = new JSONObject();
        objUpd = new JSONObject();
        objUpd.put("type", "string");
        objUpd.put("val", this.hlp.createID());
        jObjUpd.put("1", objUpd);

        objUpd = new JSONObject();
        objUpd.put("type", "string");
        objUpd.put("val", nameFile);
        jObjUpd.put("2", objUpd);

        this.hlp.executeUpdate(conf, queryUpd, jObjUpd);

        query = "select count(*) as count from file where f_name = ?";
        jObj = new JSONObject();
        obj = new JSONObject();
        obj.put("type", "string");
        obj.put("val", nameFile);
        jObj.put("1", obj);

        row = new ArrayList<>();
        row = this.hlp.executeQuery(conf, query, jObj);
        //convert ArrayList<HashMap<String, Object>> to cakephp equivalent query result at model in json format
        JSONObject jsonParrent = new JSONObject();
        int i = 0;
        for (HashMap<String, Object> map : row) {
            JSONObject jsonChild1st = new JSONObject();
            JSONObject jsonChild2nd = new JSONObject();
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                int j = 0;
                Map.Entry pair = (Map.Entry) it.next();
                if (pair.getValue() == null) {
                    jsonChild2nd.put(pair.getKey(), "");
                } else {
                    jsonChild2nd.put(pair.getKey(), pair.getValue().toString());
                }
                jsonChild1st.put(j, jsonChild2nd);
                it.remove(); // avoids a ConcurrentModificationException
            }
            jsonParrent.put(i, jsonChild1st);
            i++;
        }
        String encStrJson;
        if (jsonParrent.size() > 0) {
            encStrJson = "{"
                    + "\"status\":\"Success Input\","
                    + "\"rc\":\"00\""
                    + "}";
        } else {
            encStrJson = encStrJson = "{"
                    + "\"status\":\"Not Input\","
                    + "\"rc\":\"88\""
                    + "}";
        }
        return encStrJson;
    }
}
