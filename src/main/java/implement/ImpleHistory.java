/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import org.json.simple.JSONObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcHistory;
import interfc.InterfcNotif;
import koneksi.DatabaseUtilitiesPgsql;

/**
 *
 * @author Hasan
 */
public class ImpleHistory implements InterfcHistory {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;

    public ImpleHistory(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
    }

    @Override
    public String getHistory(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //get last balance
            if (!objJson.get("tgl").equals("")) {
                String query = "select "
                        + "a.id_transaksi, a.id_agentaccount, a.\"TimeStamp\","
                        + "b.\"LastBalance\", b.id_trstock, b.\"Nominal\" as \"Total\", a.id_memberaccount, d.\"Nama\", "
                        + "case "
                        + "	when b.\"TypeTrx\" = 'REFUND' then "
                        + "		'DEPOSIT' "
                        + "	else "
                        + "		b.\"TypeTrx\" "
                        + "end as \"TypeTRX\", "
                        + "case "
                        + "	when b.\"TypeTrx\" = 'REFUND' then "
                        + "		'Refund ' || c.\"NamaTipeTransaksi\" "
                        + " when A .id_tipetransaksi = '3' THEN "
                        + "     c.\"NamaTipeTransaksi\" || ' Member'"
                        + "	else "
                        + "		c.\"NamaTipeTransaksi\" "
                        + "end as \"NamaTipeTransaksi\"  "
                        + "from \"TrTransaksi\" a LEFT JOIN \"MsMemberAccount\" d  ON a.id_memberaccount = D.id_memberaccount, \"TrStock\" b, \"MsTipeTransaksi\" c "
                        + "where a.id_transaksi = b. id_transaksi "
                        + "and a.id_tipetransaksi = c.id_tipetransaksi "
                        + "and (b.\"StokType\" ILIKE 'agent' or b.\"StokType\" ILIKE 'agen' or b.\"StokType\" ILIKE 'agentSchool') "
                        + "and (b.\"TypeTrx\" = 'DEPOSIT' or b.\"TypeTrx\" = 'WITHDRAW' or b.\"TypeTrx\" = 'REFUND') "
                        + "and ((a.id_agentaccount = ? or a.\"NoRekTujuan\" = ?) and b.id_stock = ? and b.\"Nominal\" != '0')  "
                        + "and a.\"TimeStamp\"::date = ? "
                        + "ORDER BY b.\"TimeStamp\" DESC "
                        + "LIMIT ? OFFSET ?";
                st = conn.prepareStatement(query);
                st.setString(1, objJson.get("username").toString());
                st.setString(2, objJson.get("username").toString());
                st.setString(3, objJson.get("username").toString());
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                java.util.Date date = formatter.parse(objJson.get("tgl").toString());
                Date sqlDate = new Date(date.getTime());
                st.setDate(4, sqlDate);
                st.setInt(5, Integer.parseInt(objJson.get("limit").toString()));
                st.setInt(6, Integer.parseInt(objJson.get("offset").toString()));
            } else {
                String query = "select "
                        + "a.id_transaksi, a.id_agentaccount, a.\"TimeStamp\", "
                        + "b.\"LastBalance\", b.id_trstock, b.\"Nominal\" as \"Total\", a.id_memberaccount, d.\"Nama\", "
                        + "case "
                        + "	when b.\"TypeTrx\" = 'REFUND' then "
                        + "		'DEPOSIT' "
                        + "	else "
                        + "		b.\"TypeTrx\" "
                        + "end as \"TypeTRX\", "
                        + "case "
                        + "	when b.\"TypeTrx\" = 'REFUND' then "
                        + "		'Refund ' || c.\"NamaTipeTransaksi\" "
                        + " when A .id_tipetransaksi = '3' THEN "
                        + "     c.\"NamaTipeTransaksi\" || ' Member'"
                        + "	else "
                        + "		c.\"NamaTipeTransaksi\" "
                        + "end as \"NamaTipeTransaksi\" "
                        + "from \"TrTransaksi\" a LEFT JOIN \"MsMemberAccount\" d  ON a.id_memberaccount = d.id_memberaccount, \"TrStock\" b, \"MsTipeTransaksi\" c "
                        + "where a.id_transaksi = b. id_transaksi "
                        + "and a.id_tipetransaksi = c.id_tipetransaksi "
                        + "and (b.\"StokType\" ILIKE 'agent' or b.\"StokType\" ILIKE 'agentSchool') "
                        + "and (b.\"TypeTrx\" = 'DEPOSIT' or b.\"TypeTrx\" = 'WITHDRAW' or b.\"TypeTrx\" = 'REFUND') "
                        + "and ((a.id_agentaccount = ? or a.\"NoRekTujuan\" = ?) and b.id_stock = ? and b.\"Nominal\" != '0') "
                        + "ORDER BY b.\"TimeStamp\" DESC "
                        + "LIMIT ? OFFSET ?";
                st = conn.prepareStatement(query);
                st.setString(1, objJson.get("username").toString());
                st.setString(2, objJson.get("username").toString());
                st.setString(3, objJson.get("username").toString());
                st.setInt(4, Integer.parseInt(objJson.get("limit").toString()));
                st.setInt(5, Integer.parseInt(objJson.get("offset").toString()));
            }
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idTransaksi", rs.getString("id_transaksi"));

                double total = Double.parseDouble(rs.getString("Total"));
                String totalStr = String.format("%,.0f", total);
                totalStr = totalStr.replaceAll(",", ".");
                jObj.put("total", totalStr);

                jObj.put("typeTrx", rs.getString("TypeTRX"));

                double lastBalance = Double.parseDouble(rs.getString("LastBalance"));
                String lastBalanceStr = String.format("%,.0f", lastBalance);
                lastBalanceStr = lastBalanceStr.replaceAll(",", ".");
                jObj.put("lastBalance", lastBalanceStr);

                if(rs.getString("NamaTipeTransaksi").equals("Pembayaran Merchant")){
                    jObj.put("tipeTransaksi", rs.getString("NamaTipeTransaksi") + " dari " + rs.getString("Nama"));
                } else {
                    jObj.put("tipeTransaksi", rs.getString("NamaTipeTransaksi"));
                }
                String timeStamp = rs.getString("TimeStamp");
                if (!timeStamp.contains(".")) {
                    timeStamp = timeStamp + ".000000";
                }
                jObj.put("timeStamp", timeStamp);
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("mutasi", jArr);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ImpleHistory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpleHistory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ImpleHistory.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ImpleHistory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return jobjResp.toString();
    }
}
