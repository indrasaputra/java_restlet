/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.simple.JSONObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcHome;
import interfc.InterfcLogin;
import koneksi.DatabaseUtilitiesPgsql;

/**
 *
 * @author Hasan
 */
public class ImpleHome implements InterfcHome {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final Helper helper;
    private final ControllerLog controllerLog;

    public ImpleHome(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.helper = new Helper();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getLastBalance(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        double lastBalance = 0;
        String lastBalanceStr = "";
        //from request
        String reqUserName = (String) objJson.get("username");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //get last balance
            String query = "select \"LastBalance\" FROM \"MsAgentAccount\" "
                    + "where \"id_agentaccount\" = ?";
            st = conn.prepareStatement(query);
            st.setString(1, reqUserName);
            rs = st.executeQuery();
            while (rs.next()) {
                lastBalance = Double.parseDouble(rs.getString("LastBalance"));
                lastBalanceStr = String.format("%,.0f", lastBalance);
                lastBalanceStr = lastBalanceStr.replaceAll(",", ".");
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("lastBalance", lastBalanceStr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String getPoint(JSONObject jsonReq) {
        JSONObject jObjCurl = new JSONObject();
        jObjCurl.put("user_id",jsonReq.get("username").toString());
        jObjCurl.put("transaction_id",this.helper.createID());

        String url = "";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            st = conn.prepareStatement(query);
            st.setString(1, "excite_getbalance");
            rs = st.executeQuery();
            while (rs.next()) {
                url = rs.getString("parameter_value");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        JSONObject jobjResp = this.utilities.curlPost(url, jObjCurl.toString(), false);
        if(jobjResp.get("result_code").toString().equals("0")){
            jobjResp.put("ACK", "OK");
        } else{
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "point tidak tersedia");
        }
        return jobjResp.toString();
    }

    @Override
    public String getIklan(JSONObject jsonReq) {
        JSONObject jObjCurl = new JSONObject();
        jObjCurl.put("productCode", "BNRAGENT");
        jObjCurl.put("idTipeAplikasi","3");
        //jObjCurl.put("idTipeTransaksi","17");
        jObjCurl.put("accountType","AGENT");

        String url = "";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            st = conn.prepareStatement(query);
            st.setString(1, "iklan_truemoney");
            rs = st.executeQuery();
            while (rs.next()) {
                url = rs.getString("parameter_value");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        JSONObject jobjResp = this.utilities.curlPost(url, jObjCurl.toString(), false);
        if(jobjResp.get("ack").toString().equals("OK")){
            jobjResp.put("ACK", "OK");
        } else{
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "iklan tidak tersedia");
        }
        return jobjResp.toString();
    }
}
