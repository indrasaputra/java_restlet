/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.*;
import interfc.InterfcKudoGrab;
import koneksi.DatabaseUtilitiesPgsql;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author Hasan
 */
public class ImpleKudoGrab implements InterfcKudoGrab {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleKudoGrab(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getKota(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Gagal Pilih Kota");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("page", 1);

        //insert log
//        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/city-list";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
//        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses pilih kota");
                    jsonResp.put("data", jObjResp.get("data"));
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "gagal pilih kota");
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal pilih kota");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    public String getUrl() {
        String response = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            //<editor-fold defaultstate="collapsed" desc="query getmsparam and hit apiSwitching">
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String paramName = "url_kudo_registration_driver";
            String query = "SELECT parameter_value FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, paramName);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                response = rsPgsql.getString("parameter_value");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return response;
    }

    @Override
    public String getBank(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "Gagal Pilih Bank");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("page", 1);

        //insert log
//        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/bank-list";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
//        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses pilih Bank");
                    jsonResp.put("data", jObjResp.get("data"));
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal pilih Bank");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String createApplication(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal create applicant");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("first_name", jsonReq.get("first_name").toString());
        jsonObject.put("last_name", jsonReq.get("last_name").toString());
        jsonObject.put("phone_number", jsonReq.get("phone_number").toString());
        jsonObject.put("email", jsonReq.get("email").toString());
        jsonObject.put("city_name", jsonReq.get("city_name").toString());
        jsonObject.put("grab_type", jsonReq.get("grab_type").toString());
        if (jsonReq.get("grab_type").toString().equals("car")) {
            jsonObject.put("vehicle_ownership", jsonReq.get("vehicle_ownership").toString());
        }
        jsonObject.put("partner_unique_id", jsonReq.get("partner_unique_id").toString());
        jsonObject.put("id_account", jsonReq.get("id_account").toString());

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/applications";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan"));
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal create applicant");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String phoneNumberVerification(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal phone number verification");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("verification_code", jsonReq.get("verification_code").toString());
        jsonObject.put("tr_kudo_id", Long.parseLong(jsonReq.get("tr_kudo_id").toString()));

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/verification";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan"));
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal phone number verificationt");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String resendPhoneNumberVerification(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal resend Phone Number Verification");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_account", jsonReq.get("id_account").toString());
        jsonObject.put("tr_kudo_id", jsonReq.get("tr_kudo_id").toString());

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/resend-verification";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Gagal resend phone number verificationt");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String customerProfileList(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal get profile list");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_account", jsonReq.get("id_account").toString());
        if (jsonReq.containsKey("tr_kudo_id")) {
            jsonObject.put("tr_kudo_id", Integer.valueOf(jsonReq.get("tr_kudo_id").toString()));
        }

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/profile-list";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal get profile list");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String uploadFileKudoGrab(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal upload file " + jsonReq.get("type_file").toString());
        String fileKudo = jsonReq.get("file").toString();
        String trKudoId = jsonReq.get("tr_kudo_id").toString();
        String tType = jsonReq.get("type").toString();

        File file = new File(entityConfig.getWorkDir() + "uploadFile" + File.separator + "kudograb" + File.separator + fileKudo);
        FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);
        StringBody stringBodyTrKudoId = new StringBody(trKudoId, ContentType.MULTIPART_FORM_DATA);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("tr_kudo_id", stringBodyTrKudoId);
        builder.addPart("File", fileBody);
        HttpEntity entity = builder.build();
        String IpPort = getUrl();
        String endPointCity = "/" + jsonReq.get("endpoint").toString();
        String url = "http://" + IpPort + endPointCity;
        String responseString = utilities.postMultipart(url, entity);
        JSONParser parser = new JSONParser();

        // JSONParser parser = new JSONParser();
        JSONObject jObjResp; // = (JSONObject) parser.parse(responseString);
        try {
            jObjResp = (JSONObject) parser.parse(responseString);
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan")); //"gagal upload Ktp");
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal upload file " + tType);
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String isiKtp(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal upload Ktp");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("date_of_birth", jsonReq.get("date_of_birth").toString());
        jsonObject.put("number", jsonReq.get("number").toString());
        jsonObject.put("full_name", jsonReq.get("full_name").toString());
        jsonObject.put("issue_date", jsonReq.get("issue_date").toString());
        jsonObject.put("tr_kudo_id", jsonReq.get("tr_kudo_id").toString());

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/identity";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal upload Ktp");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String isiSim(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal upload Sim");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("number", jsonReq.get("number").toString());
        jsonObject.put("holder_name", jsonReq.get("holder_name").toString());
        jsonObject.put("type", jsonReq.get("type").toString());
        jsonObject.put("issue_date", jsonReq.get("issue_date").toString());
        jsonObject.put("tr_kudo_id", jsonReq.get("tr_kudo_id").toString());

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/driving-license";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal upload Sim");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String isiStnk(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal upload Stnk");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("plate_number", jsonReq.get("plate_number").toString());
        jsonObject.put("make", jsonReq.get("make").toString());
        jsonObject.put("model", jsonReq.get("model").toString());
        jsonObject.put("tr_kudo_id", jsonReq.get("tr_kudo_id").toString());

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/vehicle";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal upload Stnk");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String isiDeclaration(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal declaration");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("convicted", jsonReq.get("convicted").toString());
        jsonObject.put("proceeding_against", jsonReq.get("proceeding_against").toString());
        jsonObject.put("medical_condition", jsonReq.get("medical_condition").toString());
        jsonObject.put("agree_term", jsonReq.get("agree_term").toString());
        jsonObject.put("tr_kudo_id", jsonReq.get("tr_kudo_id").toString());

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/declaration";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal declaration");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String isiBank(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal declaration");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("bank_name", jsonReq.get("bank_name").toString());
        jsonObject.put("name", jsonReq.get("name").toString());
        jsonObject.put("number", jsonReq.get("number").toString());
        jsonObject.put("tr_kudo_id", jsonReq.get("tr_kudo_id").toString());

        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String IpPort = getUrl();
        String endPointCity = "/bank";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, resp.toString());
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id").toString());
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Maaf, permintaan gagal");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String completeData(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal complete data");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tr_kudo_id", jsonReq.get("tr_kudo_id").toString());

        //insert log
        String IpPort = getUrl();
        String endPointCity = "/complete";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        JSONObject jObjResp = resp;
        try {
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                    jsonResp.put("tr_kudo_id", jObjResp.get("tr_kudo_id"));
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal complete data");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

    @Override
    public String checkStatusData(JSONObject jsonReq) {
//        jsonReq.put("idTrx", utilities.createTrxid());
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "gagal check Status Data");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id_account", (String) jsonReq.get("id_account"));
        if (jsonReq.containsKey("tr_kudo_id")) {
            jsonObject.put("tr_kudo_id", (Integer) jsonReq.get("tr_kudo_id"));
            System.out.println("tr_kudo_id : "+(Integer) jsonReq.get("tr_kudo_id"));
        }

        System.out.println("idAccount : "+(String) jsonReq.get("id_account"));

        //insert log
        String IpPort = getUrl();
        String endPointCity = "/status";
        String url = "http://" + IpPort + endPointCity;
        JSONObject resp = utilities.curlPost(url, String.valueOf(jsonObject), false);
        //updateLog
        // JSONParser parser = new JSONParser();
        JSONObject jObjResp = resp;
        try {
            // jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("ack")) {
                if (jObjResp.get("ack").toString().equals("OK")) {

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("pesan", "sukses");
                    jsonResp.put("data", jObjResp.get("data"));
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", jObjResp.get("pesan").toString());
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "gagal check Status Data");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jsonResp.toString();
    }

}
