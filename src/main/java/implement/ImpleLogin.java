/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.simple.JSONObject;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcLogin;
import koneksi.DatabaseUtilitiesPgsql;

/**
 * @author Hasan
 */
public class ImpleLogin implements InterfcLogin {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleLogin(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getSignInToken(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        //for db
        String pass = "";
        String idAgentAccount = "";
        String device_info = "";
        String hp = "";
        boolean flag_change_pin = false;
        int countBlacklist = 0;
        //from request
        String reqUserName = (String) objJson.get("username");
        String reqPass = (String) objJson.get("password");
        String deviceInfo = (String) objJson.get("deviceInfo");

        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //1, check usernya 
            String query = "Select a.\"PIN\", a.id_agentaccount, a.\"device_info\", a.\"flag_change_pin\", a.\"NomorKartu\", b.\"Handphone\""
                    + " FROM \"MsAgentAccount\" a, \"MsInformasiAgent\" b"
                    + " WHERE (a.id_agentaccount = ? or b.\"Handphone\" = ?)"
                    + " and a.id_agent = b.id_agent"
                    + " and b.id_tipeinformasiagent = '1'"
                    + " AND substring(A .id_agentaccount,7,1) != '4'";
            st = conn.prepareStatement(query);
            st.setString(1, reqUserName);
            st.setString(2, reqUserName);
            rs = st.executeQuery();
            while (rs.next()) {
                pass = rs.getString("PIN");
                pass = this.encDec.decryptDbVal(pass, this.entityConfig.getEncKeyDb());
                idAgentAccount = rs.getString("id_agentaccount");
                device_info = rs.getString("device_info");
                flag_change_pin = rs.getBoolean("flag_change_pin");
                hp = rs.getString("Handphone");
                //System.out.println("pass db : " + pass);
            }
            //2, check device blacklist
            query = "SELECT count (id_device_blacklist) as juml "
                    + "FROM "
                    + "ms_device_blacklist as trx "
                    + "where "
                    + "device_info = ? "
                    + "and flag_active = true ";
            st = conn.prepareStatement(query);
            st.setString(1, deviceInfo);
            rs = st.executeQuery();
            while (rs.next()) {
                countBlacklist = rs.getInt("juml");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }

        if ((reqPass.equals(pass) && reqUserName.equals(idAgentAccount))
                || (reqPass.equals(pass) && reqUserName.equals(hp))) {

            //reset counterPin to 0
            //<editor-fold defaultstate="collapsed" desc="query reset counterPin">
            try {
                databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                String query = "update \"MsAgentAccount\" set \"counterPin\" = 0 where id_agentaccount = ?";
                st = conn.prepareStatement(query);
                st.setString(1, idAgentAccount);
                st.executeUpdate();
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (st != null) {
                        st.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>

            //save tr_android_update to tracking agent uses new apps
            //1. check if idAgentAcc has inserted
            int countExist = 1;
            //<editor-fold defaultstate="collapsed" desc="query checkExistIdAccount">
            try {
                databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                String query = "select count(*)"
                        + " from tr_android_update"
                        + " where id_account = ?";
                st = conn.prepareStatement(query);
                st.setString(1, idAgentAccount);
                rs = st.executeQuery();
                while (rs.next()) {
                    countExist = rs.getInt("count");
                }
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (st != null) {
                        st.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            //2. if not exits, insert it
            if (countExist == 0) {
                //<editor-fold defaultstate="collapsed" desc="query insert to tr_android_update">
                try {
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                    String query = "INSERT INTO tr_android_update (id_account, account_type, update_timestamp)"
                            + " VALUES (?, ?, now())";
                    st = conn.prepareStatement(query);
                    st.setLong(1, Long.parseLong(idAgentAccount));
                    st.setString(2, "AGENT");
                    st.executeUpdate();
                } catch (Exception ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                } finally {
                    try {
                        if (st != null) {
                            st.close();
                        }
                        if (rs != null) {
                            rs.close();
                        }
                        if (conn != null) {
                            conn.close();
                        }
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    }
                }
                //</editor-fold>
            }

            //check agent is samsung or not
            countExist = 0;
            //<editor-fold defaultstate="collapsed" desc="query checkAgentSamsung">
            try {
                databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                conn = databaseUtilitiesPgsql.getConnection(entityConfig);
//                String query = "select count(*)"
//                        + " from \"MsWhiteList\""
//                        + " where id_agentaccount = ?"
//                        + " and onbehalf ILIKE 'samsung'"
//                        + " and flagcairkansaldo = true";
                String query = "select count(*) from \"MsAgentAccount\" " +
                        " where id_agentaccount = ? " +
                        " and (group_id IS NOT NULL and group_id = 5)";
                st = conn.prepareStatement(query);
                st.setString(1, idAgentAccount);
                rs = st.executeQuery();
                while (rs.next()) {
                    countExist = rs.getInt("count");
                }
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (st != null) {
                        st.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            if (countExist > 0) {
                jobjResp.put("fiturCairkanSaldo", "true");
            } else {
                jobjResp.put("fiturCairkanSaldo", "false");
            }

            //check agent is bni46 (group_id 2) or not
            countExist = 0;
            //<editor-fold defaultstate="collapsed" desc="query checkAgent46">
            try {
                databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                String query = "select count(*) from \"MsAgentAccount\" " +
                        " where id_agentaccount = ? " +
                        " and (group_id IS NOT NULL and group_id = 2)";
//                        " and (id_agent_bni IS NOT NULL and id_agent_bni != '')";
                st = conn.prepareStatement(query);
                st.setString(1, idAgentAccount);
                rs = st.executeQuery();
                while (rs.next()) {
                    countExist = rs.getInt("count");
                }
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (st != null) {
                        st.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            if (countExist > 0) {
                jobjResp.put("fiturLakuPandai", "true");
            } else {
                jobjResp.put("fiturLakuPandai", "false");
            }

            //check agent cash2cash (group_id 3)
            countExist = 0;
            //<editor-fold defaultstate="collapsed" desc="query check groupId 3 (agent spot)">
            try {
                databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                String query = "select count(*) from \"MsAgentAccount\" " +
                        " where id_agentaccount = ? " +
                        " and (group_id IS NOT NULL and group_id = 3)";
                st = conn.prepareStatement(query);
                st.setString(1, idAgentAccount);
                rs = st.executeQuery();
                while (rs.next()) {
                    countExist = rs.getInt("count");
                }
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (st != null) {
                        st.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            if (countExist > 0) {
                jobjResp.put("fiturCashToCash", "true");
            } else {
                jobjResp.put("fiturCashToCash", "false");
            }

            if (device_info != null && !device_info.isEmpty()) {
                if (countBlacklist == 0) {
                    if (device_info.equals(deviceInfo)) {
                        jobjResp.put("ACK", "OK");
                        jobjResp.put("device", "not change");
                        jobjResp.put("idAgentAccount", idAgentAccount);
                    } else {
                        jobjResp.put("ACK", "OK");
                        jobjResp.put("idAgentAccount", idAgentAccount);
                        jobjResp.put("device", "change");
                        jobjResp.put("pesan", this.utilities.getWording("PAIR_DEVICE_NOTIF"));
                    }
                } else {
                    //device blacklist
                    jobjResp.put("ACK", "OK");
                    jobjResp.put("idAgentAccount", idAgentAccount);
                    jobjResp.put("device", "blacklist");
                    jobjResp.put("pesan", this.utilities.getWording("NOTIF_BLOCK_DEVICE"));
                }
            } else {
                jobjResp.put("ACK", "OK");
                jobjResp.put("idAgentAccount", idAgentAccount);
                jobjResp.put("device", "change");
                jobjResp.put("pesan", this.utilities.getWording("PAIR_DEVICE_NOTIF"));
            }
        } else {
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "pin atau user salah");
            //counter salah password
            if (!idAgentAccount.equals("")) {
                int counterPin = 0;
                //<editor-fold defaultstate="collapsed" desc="query counterPin">
                try {
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                    //check if counterPin is null
                    String query = "select count(*) from \"MsAgentAccount\" where id_agentaccount = ? and \"counterPin\" is null";
                    st = conn.prepareStatement(query);
                    st.setString(1, idAgentAccount);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        counterPin = rs.getInt("count");
                    }
                    if (counterPin > 0) {
                        //if counterPin null set to 1
                        query = "update \"MsAgentAccount\" set \"counterPin\" = 1 where id_agentaccount = ?";
                        st = conn.prepareStatement(query);
                        st.setString(1, idAgentAccount);
                        st.executeUpdate();
                    } else {
                        //if counterPin not null set counterPin + 1
                        query = "update \"MsAgentAccount\" set \"counterPin\" = \"counterPin\" + 1 where id_agentaccount = ?";
                        st = conn.prepareStatement(query);
                        st.setString(1, idAgentAccount);
                        st.executeUpdate();
                    }

                    //block if counterPin >= 3
                    int idAccountStatus = 0;
                    query = "select \"counterPin\", id_accountstatus from \"MsAgentAccount\" where id_agentaccount = ?";
                    st = conn.prepareStatement(query);
                    st.setString(1, idAgentAccount);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        counterPin = rs.getInt("counterPin");
                        idAccountStatus = rs.getInt("id_accountstatus");
                    }
                    if (counterPin >= 3 && idAccountStatus != 14) {
                        query = "update \"MsAgentAccount\" set id_accountstatus = 5 where id_agentaccount = ?";
                        st = conn.prepareStatement(query);
                        st.setString(1, idAgentAccount);
                        st.executeUpdate();
                    }
                } catch (Exception ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                } finally {
                    try {
                        if (st != null) {
                            st.close();
                        }
                        if (rs != null) {
                            rs.close();
                        }
                        if (conn != null) {
                            conn.close();
                        }
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    }
                }
                //</editor-fold>
            }
        }
        return jobjResp.toString();
    }

    @Override
    public JSONObject requestToken(JSONObject jsonReq) {
        JSONObject response = new JSONObject();
        response.put("ACK", "NOK");
        response.put("pesan", "ID Account tidak ditemukan");
        //send otp
        String url = null;
        //<editor-fold defaultstate="collapsed" desc="query getUrlSms">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'tokenotp_url';";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        String requestBody = "{\"id_account\":\"" + jsonReq.get("username").toString() + "\",\"type\":\"token_reguler\"}";
        //request token to server
        response = this.utilities.curlPost(url, requestBody, false);
        //{"id_account":"180222100020","ACK":"OK","otp":"350588","pesan":"Token TrueMoney Anda 350588, berlaku hingga 04/07/2018 15:22, berlaku 1x transaksi. Ayo Download Aplikasi Android TrueMoney Indonesia. Info hub: 08041000100","type":"token_reguler"}
        //own otp
//        Random rand = new Random();
//        int value = rand.nextInt(999999);
//        response.put("ACK", "OK");
//        response.put("otp", value);
        if (response.get("ACK").toString().equalsIgnoreCase("OK")) {
            response.put("ACK", "OK");
            response.put("pesan", this.utilities.getWording("NOTIF_SEND_TOKEN"));
        } else {
            response.put("ACK", "NOK");
        }

        return response;
    }

    @Override
    public void blockAccount(JSONObject jsonReq) {
        //<editor-fold defaultstate="collapsed" desc="query blockAccount">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        int hasil = 0;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            //check is already block by ams
            int idAccountStatus = 0;
            String query = "select id_accountstatus from \"MsAgentAccount\" where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("username").toString());
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idAccountStatus = rsPgsql.getInt("id_accountstatus");
            }

            //if account status not already block by ams then block by android
            if (idAccountStatus != 14) {
                query = "update\n"
                        + "        \"MsAgentAccount\" "
                        + "    set"
                        + "        id_accountstatus = 5,"
                        + "        \"Remark\"=? "
                        + "    where"
                        + "        id_agentaccount = ?; ";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setString(1, jsonReq.get("remark").toString());
                stPgsql.setString(2, jsonReq.get("username").toString());
                hasil = stPgsql.executeUpdate();
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }

    @Override
    public JSONObject changeDevice(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        String urlToken = "";
        //<editor-fold defaultstate="collapsed" desc="query validasi token reguler">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'tokenotp_url';;";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                urlToken = rsPgsql.getString("parameter_value");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        //get json object from array
        String requestBody = "{\"id_account\":\"" + jsonReq.get("username").toString()
                + "\",\"otp\":\"" + jsonReq.get("otp").toString() + "\",\"type\":\"token_otentifikasi_reguler\"}";
        jsonResp = this.utilities.curlPost(urlToken, requestBody, false);
        //<editor-fold defaultstate="collapsed" desc="query changeDevice">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "update \"MsAgentAccount\" set device_info = ? where \"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("deviceInfo").toString());
            stPgsql.setString(2, jsonReq.get("username").toString());
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        jsonResp.put("ACK", "OK");
        jsonResp.put("pesan", this.utilities.getWording("PAIR_DEVICE_OK"));
        return jsonResp;
    }

    @Override
    public void setFcmToken(JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "update \"MsAgentAccount\" set gcm_token = ? where \"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("token").toString());
            stPgsql.setString(2, jsonReq.get("username").toString());
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

}
