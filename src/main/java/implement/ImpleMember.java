package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcMember;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.*;

/**
 * Created by Fino Indrasaputra on 03/07/2018.
 */
public class ImpleMember implements InterfcMember {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleMember(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getInqIsiSaldo(JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "Gagal mendapatkan data Member");
        boolean validMember = false;
        boolean memberExist = false;
        String namaMember = "";
        String idMemberAccount = "";
        int idStatusVerifikasi = 0;
        int lastBalance = 0;
        int maxBalance = 1000000;
        String biayaAdmin = "";
        String id_operator = "";
        String id_feesupplier = "";
        int nominal = Integer.parseInt(jsonReq.get("nominal").toString().replace(".",""));
        //cek if member valid
        //<editor-fold defaultstate="collapsed" desc="query getMsMemberAccount">
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT id_memberaccount, id_statusverifikasi, \"Nama\", \"LastBalance\" FROM \"MsMemberAccount\" " +
                    "WHERE \"Handphone\" = ?; ";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("noHpMember").toString());
            //stPgsql.setLong(2, 2);
            rsPgsql = stPgsql.executeQuery();

            if (rsPgsql.next()) {
                idMemberAccount = rsPgsql.getString("id_memberaccount");
                idStatusVerifikasi = rsPgsql.getInt("id_statusverifikasi");
                namaMember = rsPgsql.getString("Nama");
                lastBalance = rsPgsql.getInt("LastBalance");
                memberExist = true;
            } else {
                jobjResp.put("pesan", "Maaf, Member tidak ditemukan. Mohon cek kembali input Anda.");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        if (memberExist) {
            //<editor-fold defaultstate="collapsed" desc="query checkMaxBalance">
            databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
            connPgsql = null;
            stPgsql = null;
            rsPgsql = null;
            try {
                connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                String query = "SELECT \"LimitBalance\" FROM \"MsStatusVerifikasi\" " +
                        "WHERE id_statusverifikasi = ?; ";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setInt(1, idStatusVerifikasi);
                rsPgsql = stPgsql.executeQuery();

                while (rsPgsql.next()) {
                    maxBalance = rsPgsql.getInt("LimitBalance");
                }

                int endBalance = lastBalance + nominal;
                if (endBalance <= maxBalance) {
                    validMember = true;
                } else {
                    double dMaxBal = maxBalance;
                    jobjResp.put("pesan", "Tidak bisa menambah saldo. Saldo Member melebihi limit"); // Maksimal saldo Member " + namaMember + " = " + String.format("%,.0f", dMaxBal));
                    jobjResp.put("namaMember", namaMember);
                }
            } catch (ParserConfigurationException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (rsPgsql != null) {
                        rsPgsql.close();
                    }
                    if (stPgsql != null) {
                        stPgsql.close();
                    }
                    if (connPgsql != null) {
                        connPgsql.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
        }

        if (validMember) {
            //<editor-fold defaultstate="collapsed" desc="query getMsMemberAccount">
            databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
            connPgsql = null;
            stPgsql = null;
            rsPgsql = null;
            try {
                connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                String query = "SELECT id_feesupplier, id_fee, id_operator, \"BiayaAdmin\" FROM \"MsFeeSupplier\" " +
                        " WHERE \"ProductCode\" = 'TMSTOR';";
                //stPgsql.setString(1, "TMSTOR");
                stPgsql = connPgsql.prepareStatement(query);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    biayaAdmin = rsPgsql.getLong("BiayaAdmin") + "";
                    id_operator = rsPgsql.getLong("id_operator") + "";
                    id_feesupplier = rsPgsql.getLong("id_feesupplier") + "";
                }
                jobjResp = new JSONObject();
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "SUKSES");
                jobjResp.put("namaMember", namaMember);
                jobjResp.put("idMemberAccount", idMemberAccount);
                jobjResp.put("biayaAdmin", biayaAdmin);
                jobjResp.put("id_operator", id_operator);
                jobjResp.put("id_feesupplier", id_feesupplier);
            } catch (ParserConfigurationException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (rsPgsql != null) {
                        rsPgsql.close();
                    }
                    if (stPgsql != null) {
                        stPgsql.close();
                    }
                    if (connPgsql != null) {
                        connPgsql.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
        }

        return jobjResp.toString();
    }

    @Override
    public String prosesIsiSaldo (JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "ISI SALDO GAGAL");

        String idAgentAccount = jsonReq.get("idAgentAccount").toString();
        String noHpMember = jsonReq.get("noHpMember").toString();
        String namaMember = jsonReq.get("namaMember").toString();
        String idMemberAccount = jsonReq.get("idMemberAccount").toString();
        String nominal = jsonReq.get("nominal").toString();
        nominal = nominal.replace(".", "");
        String idtrx = jsonReq.get("idTrx").toString();
        String idOperator = jsonReq.get("id_operator").toString();
        //String idFeeSupplier = jsonReq.get("id_feesupplier").toString();
        String biayaAdmin = jsonReq.get("biayaAdmin").toString();
        String total = nominal; //jsonReq.get("total").toString().replace(".", "");
        String pin = jsonReq.get("PIN").toString();

        int countTrxByIdTrx = 0;
        //<editor-fold defaultstate="collapsed" desc="query getCountTrxByIdtrx">
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT count(*) as juml FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idtrx);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                countTrxByIdTrx = rsPgsql.getInt("juml");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        if (countTrxByIdTrx == 1) {
            String statusTrx = "";
            //<editor-fold defaultstate="collapsed" desc="query getStatusTrxByIdtrx">
            databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
            connPgsql = null;
            stPgsql = null;
            rsPgsql = null;
            try {
                connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                String query = "SELECT \"StatusTRX\" FROM \"TrTransaksi\" where id_trx = ?;";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setString(1, idtrx);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    statusTrx = rsPgsql.getString("StatusTRX");
                }
            } catch (ParserConfigurationException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (rsPgsql != null) {
                        rsPgsql.close();
                    }
                    if (stPgsql != null) {
                        stPgsql.close();
                    }
                    if (connPgsql != null) {
                        connPgsql.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            if (statusTrx.equalsIgnoreCase("SUKSES")) {
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "Transaksi Anda sukes");
                jobjResp.put("statusTRX", statusTrx);
            } else if (statusTrx.equalsIgnoreCase("GAGAL")) {
                jobjResp.put("ACK", "NOK");
                jobjResp.put("pesan", "Transaksi Anda gagal");
                jobjResp.put("statusTRX", statusTrx);
            } else if (statusTrx.equalsIgnoreCase("PENDING")) {
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "Transaksi Anda sedang diproses");
                jobjResp.put("statusTRX", statusTrx);
            }
            return jobjResp.toString();
        } else if (countTrxByIdTrx > 1) {
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "Transaksi Anda gagal");
            jobjResp.put("status", "GAGAL");
            return jobjResp.toString();
        }

        int checkDuplicateTransaction = 0;
        //<editor-fold defaultstate="collapsed" desc="query checkDuplicateTransaction">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\" "
                    + "WHERE \"NoHPTujuan\" = ? "
                    + "AND \"Nominal\" = ? "
                    + "and id_operator = ?  "
                    + "AND \"TimeStamp\"::DATE = now()::DATE "
                    + "AND (id_memberaccount = ? OR id_agentaccount = ?)  "
                    + "AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, noHpMember);
            stPgsql.setString(2, nominal);
            stPgsql.setInt(3, Integer.parseInt(idOperator));
            stPgsql.setString(4, idAgentAccount);
            stPgsql.setString(5, idAgentAccount);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                checkDuplicateTransaction = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        if (checkDuplicateTransaction > 0) {
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "Anda tidak dapat melakukan transaksi, silakan coba 24 jam kemudian");
            return jobjResp.toString();
        }

        long accountStatus = 0;
        String pinFromDb = "";
        long lastBalance = 0;
        //<editor-fold defaultstate="collapsed" desc="query getMsAgentAccountDetil">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsAgentAccount\" "
                    + "where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idAgentAccount);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                accountStatus = rsPgsql.getInt("id_accountstatus");
                pinFromDb = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
                lastBalance = rsPgsql.getLong("LastBalance");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        if (accountStatus == 1 || accountStatus == 2 || accountStatus == 3) {
            if (pin.equals(pinFromDb)) {
                if (lastBalance >= Integer.parseInt(nominal)) {
                    //<editor-fold defaultstate="collapsed" desc="query deduct balance">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "update \"MsAgentAccount\" set \"LastBalance\" = \"LastBalance\" - ? "
                                + "where "
                                + "id_agentaccount = ?";
                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setInt(1, Integer.parseInt(nominal));
                        stPgsql.setString(2, idAgentAccount);
                        stPgsql.executeUpdate();
                    } catch (ParserConfigurationException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query insertTrTransaksi">
                    String keterangan = "Pengisian Saldo Member " + namaMember + " " + noHpMember + " sebesar Rp " + String.format("%,.0f", Double.parseDouble(nominal));
                    int idTipeTransaksi = 2;

                    String idTransaksi = "0";
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        connPgsql.setAutoCommit(false);
                        String query = "INSERT INTO \"TrTransaksi\" ("
                                + "	\"id_tipetransaksi\","
                                + "	\"id_agentaccount\","
                                + "	\"NoRekTujuan\","
                                + "	\"Nominal\","
                                + "	\"StatusTRX\","
                                + "	\"StatusDeduct\","
                                + "	\"TypeTRX\","
                                + "	\"TimeStamp\","
                                + "	\"OpsVerifikasi\","
                                + "	\"Total\","
                                + "	\"id_trx\","
                                + "	\"StatusKomisi\","
                                + "	\"id_tipeaplikasi\","
                                + "	\"id_operator\","
                                + "	\"Keterangan\","
                                + "	\"StatusRefund\","
                                + "	\"StatusDana\","
                                + "	\"id_supplier\","
                                + "	\"Biaya\""
                                + ")"
                                + "VALUES"
                                + "	(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                        stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                        stPgsql.setLong(1, idTipeTransaksi);
                        stPgsql.setString(2, idAgentAccount);
                        stPgsql.setString(3, idMemberAccount);
                        stPgsql.setString(4, nominal);
                        stPgsql.setString(5, "OPEN");
                        stPgsql.setString(6, "OK");
                        stPgsql.setString(7, "WITHDRAW");
                        stPgsql.setTimestamp(8, this.utilities.getDate());
                        stPgsql.setString(9, "PIN");
                        stPgsql.setInt(10, Integer.parseInt(total));
                        stPgsql.setString(11, idtrx);
                        stPgsql.setString(12, "PENDING");
                        stPgsql.setLong(13, 3);
                        stPgsql.setLong(14, Long.parseLong(idOperator));
                        stPgsql.setString(15, keterangan);
                        stPgsql.setString(16, "NOK");
                        stPgsql.setString(17, "DEDUCT");
                        stPgsql.setLong(18, 6);
                        stPgsql.setString(19, biayaAdmin);
                        stPgsql.executeUpdate();
                        rsPgsql = stPgsql.getGeneratedKeys();
                        while (rsPgsql.next()) {
                            idTransaksi = rsPgsql.getString(1);
                        }
                        connPgsql.commit();
                    } catch (ParserConfigurationException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query getLastBalanceAgent">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "select \"LastBalance\" from \"MsAgentAccount\" "
                                + "where id_agentaccount = ?";
                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setString(1, idAgentAccount);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            lastBalance = rsPgsql.getLong("LastBalance");
                        }
                    } catch (ParserConfigurationException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="query insertTrStockAgentNominal">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "INSERT INTO \"TrStock\" (" +
                                " \"LastBalance\"," +
                                " \"TimeStamp\"," +
                                " \"True\"," +
                                " \"StokType\"," +
                                " \"Nominal\"," +
                                " \"Agent\"," +
                                " id_stock," +
                                " \"TypeTrx\"," +
                                " id_transaksi," +
                                " \"Dealer\")"
                                + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";

                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setLong(1, lastBalance);
                        stPgsql.setLong(2, Long.parseLong(nominal));
                        stPgsql.setString(3, idAgentAccount);
                        stPgsql.setLong(4, Long.parseLong(idTransaksi));
                        stPgsql.executeUpdate();
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="query insertTrStockAgentFee">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "INSERT INTO \"TrStock\" (" +
                                " \"LastBalance\"," +
                                " \"TimeStamp\"," +
                                " \"True\"," +
                                " \"StokType\"," +
                                " \"Nominal\"," +
                                " \"Agent\"," +
                                " id_stock," +
                                " \"TypeTrx\"," +
                                " id_transaksi," +
                                " \"Dealer\")"
                                + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";

                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setLong(1, lastBalance);
                        stPgsql.setLong(2, 0);
                        stPgsql.setString(3, idAgentAccount);
                        stPgsql.setLong(4, Long.parseLong(idTransaksi));
                        stPgsql.executeUpdate();
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query deposit member">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "UPDATE \"MsMemberAccount\" set \"LastBalance\" = \"LastBalance\" + ? "
                                + " WHERE "
                                + "id_memberaccount = ?;";
                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setInt(1, Integer.parseInt(nominal));
                        stPgsql.setString(2, idMemberAccount);
                        stPgsql.executeUpdate();
                    } catch (ParserConfigurationException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    long lastBalanceMember = 0;
                    //<editor-fold defaultstate="collapsed" desc="query getLastBalanceMember">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "select \"LastBalance\" from \"MsMemberAccount\" "
                                + "where id_memberaccount = ?";
                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setString(1, idMemberAccount);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            lastBalanceMember = rsPgsql.getLong("LastBalance");
                        }
                    } catch (ParserConfigurationException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query insertTrStockMemberNominal">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "INSERT INTO \"TrStock\" (" +
                                " \"LastBalance\"," +
                                " \"TimeStamp\"," +
                                " \"True\"," +
                                " \"StokType\"," +
                                " \"Nominal\"," +
                                " \"Agent\"," +
                                " id_stock," +
                                " \"TypeTrx\"," +
                                " id_transaksi," +
                                " \"Dealer\")"
                                + " VALUES (?,now(),'0','Member',?,'0',?,'DEPOSIT',?,'0');";

                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setLong(1, lastBalanceMember);
                        stPgsql.setLong(2, Long.parseLong(nominal));
                        stPgsql.setString(3, idMemberAccount);
                        stPgsql.setLong(4, Long.parseLong(idTransaksi));
                        stPgsql.executeUpdate();
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query insertTrStockTrue">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "INSERT INTO \"TrStock\" (" +
                                " \"LastBalance\"," +
                                " \"TimeStamp\"," +
                                " \"True\"," +
                                " \"StokType\"," +
                                " \"Nominal\"," +
                                " \"Agent\"," +
                                " id_stock," +
                                " \"TypeTrx\"," +
                                " id_transaksi," +
                                " \"Dealer\")"
                                + " VALUES (?,now(),'0','True',?,'0',?,'DEPOSIT',?,'0');";

                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setLong(1, 123456);
                        stPgsql.setLong(2, 0);
                        stPgsql.setString(3, "1");
                        stPgsql.setLong(4, Long.parseLong(idTransaksi));
                        stPgsql.executeUpdate();
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>


                    //<editor-fold defaultstate="collapsed" desc="query update TrTransaksi">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String updateTransaksi = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = ?, \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK' , \"StatusDana\" = 'DEDUCT', \"hargabeli\" = ?, \"hargajual\" = ?, \"Biaya\" = ?, timestampupdatetrx = now(), id_trxsupplier = ?"
                                + " WHERE id_transaksi = ?";
                        stPgsql = connPgsql.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, "SUKSES");
                        stPgsql.setLong(2, Long.parseLong(nominal));
                        stPgsql.setLong(3, Long.parseLong(nominal));
                        stPgsql.setString(4, "0");
                        stPgsql.setString(5, "");
                        stPgsql.setLong(6, Long.parseLong(idTransaksi));
                        stPgsql.executeUpdate();
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    jobjResp = new JSONObject();
                    jobjResp.put("idTrx", jsonReq.get("idTrx").toString());
                    jobjResp.put("ACK", "OK");
                    jobjResp.put("pesan", "Transaksi isi Saldo Member " + namaMember + " " + noHpMember + " sebesar Rp " + String.format("%,.0f", Double.parseDouble(nominal)) + " SUKSES dengan nomor transaksi " + idTransaksi);
                    jobjResp.put("timestamp", utilities.getDate().toString());
                    jobjResp.put("status", "SUKSES");
                    jobjResp.put("receiverName", namaMember);
                    jobjResp.put("receiverIdMemberAccount", idMemberAccount);
                    jobjResp.put("nominal", nominal);
                    jobjResp.put("fee", "0");
                    jobjResp.put("totalBayar", nominal);
                    jobjResp.put("statusTRX", "SUKSES");
                } else {
                    jobjResp.put("ACK", "NOK");
                    jobjResp.put("pesan", "Saldo Anda tidak cukup");
                    jobjResp.put("idtrx", idtrx);
                }
            } else {
                jobjResp.put("ACK", "NOK");
                jobjResp.put("pesan", "PIN Anda salah");
                jobjResp.put("idtrx", idtrx);
            }
        } else {
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "Rekening Anda diblokir. Silakan hubungi customer care kami");
            jobjResp.put("idtrx", idtrx);
        }


        return jobjResp.toString();
    }
}
