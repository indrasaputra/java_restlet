/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import org.json.simple.JSONObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcNotif;
import koneksi.DatabaseUtilitiesPgsql;

/**
 *
 * @author Hasan
 */
public class ImpleNotif implements InterfcNotif {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleNotif(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getNotif(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //get last balance
            String query = "SELECT a.id_transaksi, a.\"Keterangan\", a.\"TimeStamp\", a.\"Total\", a.\"StatusTRX\", a.\"CustomerReference\", b.\"NamaTipeTransaksi\" "
                    + " FROM \"TrTransaksi\" as a, \"MsTipeTransaksi\" as b "
                    + " WHERE a.id_agentaccount = ? "
                    + " AND a.id_tipetransaksi = b.id_tipetransaksi "
                    + " ORDER BY a.\"TimeStamp\" DESC LIMIT ? OFFSET ?";
            st = conn.prepareStatement(query);
            st.setString(1, objJson.get("username").toString());
            st.setInt(2, Integer.parseInt(objJson.get("limit").toString()));
            st.setInt(3, Integer.parseInt(objJson.get("offset").toString()));
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                if(rs.getString("CustomerReference") == null) {
                    jObj.put("keterangan", rs.getString("Keterangan"));
                } else {
                    jObj.put("keterangan", rs.getString("Keterangan") + " dengan referensi " + rs.getString("CustomerReference"));
                }

                double hargaCetak = Double.parseDouble(rs.getString("Total"));
                String hargaCetakStr = String.format("%,.0f", hargaCetak);
                hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                jObj.put("total", hargaCetakStr);

                jObj.put("statusTrx", rs.getString("StatusTRX").equals("OPEN") ? "PENDING" : rs.getString("StatusTRX").toUpperCase());
                jObj.put("timeStamp", rs.getString("TimeStamp"));
                jObj.put("tipeTransaksi", rs.getString("NamaTipeTransaksi"));
                jObj.put("id_print", rs.getInt("id_transaksi") + "");
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("notifikasi", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }
}
