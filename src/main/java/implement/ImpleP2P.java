/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcCairkanSaldo;
import interfc.InterfcP2P;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

/**
 * @author Hasan
 */
public class ImpleP2P implements InterfcP2P {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleP2P(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public JSONObject getCommision() {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ACK", "NOK");
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            //get informasi fee
            //<editor-fold defaultstate="collapsed" desc="query getComission">
            String query = "SELECT" +
                    " *" +
                    " FROM" +
                    " \"MsFeeSupplier\" A," +
                    " \"MsFee\" b" +
                    " WHERE" +
                    " A .id_fee = b.id_fee" +
                    " AND A .\"ProductCode\" = 'T2TAG'" +
                    " AND b.id_tipeaplikasi = '3'" +
                    " AND A .\"FlagActive\" = TRUE" +
                    " AND b.\"FlagActive\" = TRUE;";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            //convert resultset to jsonArray
            JSONArray jArrRow = new JSONArray();
            int j = 0;
            while (rsPgsql.next()) {
                JSONObject jObjColumn = new JSONObject();
                for (int i = 1; i <= colCount; i++) {
                    if (rsPgsql.getObject(i) == null) {
                        jObjColumn.put(metaData.getColumnLabel(i), "");
                    } else {
                        jObjColumn.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                    }
                }
                jArrRow.add(j, jObjColumn);
                j = j++;
            }
            //</editor-fold>
            if(jArrRow.size() > 0) {
                jsonObject.put("infoMsFee", jArrRow);
                jsonObject.put("ACK", "OK");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jsonObject;
    }

    @Override
    public boolean checkBalance(JSONObject jsonReq, JSONObject jObjInfoFee) {
        boolean cukup = false;
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        String idAccount = jsonReq.get("username").toString();
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            if (q.trim().length() != 0) {
                long saldo = 0;
                stPgsql = connPgsql.prepareStatement(q);
                stPgsql.setString(1, idAccount);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    saldo = rsPgsql.getLong("saldo");
                }
                if (saldo >= (Long.parseLong(jsonReq.get("nominal").toString().replace(".","")) +
                        Long.parseLong(jObjInfoFee.get("BiayaAdmin").toString().replace(".","")))) {
                    cukup = true;
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return cukup;
    }

    @Override
    public JSONObject getInfoAgent(String idAgentAccount) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ACK", "NOK");
        JSONArray jArrRow = new JSONArray();
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            //get informasi agent
            //<editor-fold defaultstate="collapsed" desc="query getComission">
            String query = "select * from \"MsAgentAccount\" a, \"MsInformasiAgent\" b" +
                    " where a.id_agentaccount = ?" +
                    " and b.id_tipeinformasiagent = '1'" +
                    " and a.id_agent = b.id_agent";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idAgentAccount);
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            //convert resultset to jsonArray
            jArrRow = new JSONArray();
            int j = 0;
            while (rsPgsql.next()) {
                JSONObject jObjColumn = new JSONObject();
                for (int i = 1; i <= colCount; i++) {
                    if (rsPgsql.getObject(i) == null) {
                        jObjColumn.put(metaData.getColumnLabel(i), "");
                    } else {
                        jObjColumn.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                    }
                }
                jArrRow.add(j, jObjColumn);
                j = j++;
            }
            //</editor-fold>
            if(jArrRow.size() > 0) {
                jsonObject.put("infoAgent", jArrRow);
                jsonObject.put("ACK", "OK");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jsonObject;
    }

    @Override
    public long deductBalance(String idAgentAccount, String nominal) {
        long lastBalance = 0;
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String query = "UPDATE \"MsAgentAccount\""
                    + " SET \"LastBalance\"= \"LastBalance\" - ? WHERE \"id_agentaccount\" = ?;";
            long saldo = 0;
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(nominal.replace(".","")));
            stPgsql.setString(2, idAgentAccount);
            stPgsql.executeUpdate();

            query = "select * from \"MsAgentAccount\" where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idAgentAccount);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                lastBalance = rsPgsql.getLong("LastBalance");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return lastBalance;
    }

    @Override
    public String insertTrTransaksi(JSONObject jsonReq, JSONObject jObjAgentToAgentData) {
        String idTrTransaksi = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String keterangan = "Transfer ke " + jObjAgentToAgentData.get("idAgentAccountPenerima").toString() + " " +
                    jObjAgentToAgentData.get("namaAgentPenerima").toString() + " sebesar Rp." +
                    jObjAgentToAgentData.get("totalTransfer").toString();
            String query = "INSERT INTO \"TrTransaksi\" (" +
                    " id_tipetransaksi," +
                    " id_agentaccount," +
                    " \"NoRekTujuan\"," +
                    " id_supplier," +
                    " \"Nominal\"," +
                    " \"StatusTRX\"," +
                    " \"StatusDeduct\"," +
                    " \"StatusRefund\"," +
                    " \"TypeTRX\"," +
                    " \"OpsVerifikasi\"," +
                    " \"Total\"," +
                    " id_trx," +
                    " \"StatusKomisi\"," +
                    " \"TimeStamp\"," +
                    " \"Keterangan\"," +
                    " id_tipeaplikasi," +
                    " id_operator," +
                    " \"Biaya\")"
                    + " VALUES (?,?,?,?,?,'OPEN',?,'OPEN','WITHDRAW','PIN',?,?,'OPEN',now(),?,3,?,?)";

            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setLong(1, Long.parseLong(jObjAgentToAgentData.get("idTipeTransaksi").toString()));
            stPgsql.setString(2, jObjAgentToAgentData.get("idAgentAccount").toString());
            stPgsql.setString(3, jObjAgentToAgentData.get("idAgentAccountPenerima").toString());
            stPgsql.setLong(4, Long.parseLong(jObjAgentToAgentData.get("idSupplier").toString()));
            stPgsql.setString(5, jObjAgentToAgentData.get("nominalTransfer").toString());
            stPgsql.setString(6, "OK");
            stPgsql.setLong(7, Long.parseLong(jObjAgentToAgentData.get("totalTransfer").toString()));
            stPgsql.setString(8, jsonReq.get("idTrx").toString());
            stPgsql.setString(9, keterangan);
            stPgsql.setLong(10, Long.parseLong(jObjAgentToAgentData.get("idOperator").toString()));
            stPgsql.setString(11, jObjAgentToAgentData.get("feeTransfer").toString());
            int hasil = stPgsql.executeUpdate();
            if (hasil > 0) {
                rsPgsql = stPgsql.getGeneratedKeys();
                while (rsPgsql.next()) {
                    idTrTransaksi = rsPgsql.getString(1);
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return idTrTransaksi;
    }

    @Override
    public void insertTrStockOpen(String idTransaksi, long lastBalanceAfterDeductNominal, String nominal, JSONObject jsonReq, JSONObject jObjAgentToAgentData) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "INSERT INTO \"TrStock\" (" +
                    " \"LastBalance\"," +
                    " \"TimeStamp\"," +
                    " \"True\"," +
                    " \"StokType\"," +
                    " \"Nominal\"," +
                    " \"Agent\"," +
                    " id_stock," +
                    " \"TypeTrx\"," +
                    " id_transaksi," +
                    " \"Dealer\")"
                    + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";

            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, lastBalanceAfterDeductNominal);
            stPgsql.setLong(2, Long.parseLong(nominal.replace(".","")));
            stPgsql.setString(3, jObjAgentToAgentData.get("idAgentAccount").toString());
            stPgsql.setLong(4, Long.parseLong(idTransaksi));
            stPgsql.executeUpdate();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    @Override
    public long transfer(JSONObject jsonReq, JSONObject jObjAgentToAgentData) {
        long lastBalance = 0;
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String query = "UPDATE \"MsAgentAccount\""
                    + " SET \"LastBalance\"= \"LastBalance\" + ? WHERE \"id_agentaccount\" = ?;";
            long saldo = 0;
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(jObjAgentToAgentData.get("nominalTransfer").toString().replace(".","")));
            stPgsql.setString(2, jObjAgentToAgentData.get("idAgentAccountPenerima").toString());
            stPgsql.executeUpdate();

            query = "select * from \"MsAgentAccount\" where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jObjAgentToAgentData.get("idAgentAccountPenerima").toString());
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                lastBalance = rsPgsql.getLong("LastBalance");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return lastBalance;
    }

    @Override
    public void updateTransaksi(String idTransaksi, JSONObject jsonReq, JSONObject jObjAgentToAgentData) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String updateTransaksi = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = ?, \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK' , \"StatusDana\" = 'DEDUCT', \"hargabeli\" = ?, \"hargajual\" = ?, \"Biaya\" = ?, timestampupdatetrx = now(), id_trxsupplier = ?"
                    + " WHERE id_transaksi = ?";
            stPgsql = connPgsql.prepareStatement(updateTransaksi);
            stPgsql.setString(1, "SUKSES");
            stPgsql.setLong(2, Long.parseLong(jObjAgentToAgentData.get("nominalTransfer").toString().replace(".","")));
            stPgsql.setLong(3, Long.parseLong(jObjAgentToAgentData.get("totalTransfer").toString().replace(".","")));
            stPgsql.setString(4, jObjAgentToAgentData.get("feeTransfer").toString().replace(".",""));
            stPgsql.setString(5, "");
            stPgsql.setLong(6, Long.parseLong(idTransaksi));
            stPgsql.executeUpdate();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    @Override
    public void insertTrStockClose(String idTransaksi, long lastBalanceAfterTransaction, JSONObject jsonReq, JSONObject jObjAgentToAgentData) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "INSERT INTO \"TrStock\"( "
                    + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                    + " VALUES (?, now(), 0, 'Agent', ?, 0, ?, 'DEPOSIT', ?, 0);";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, lastBalanceAfterTransaction);
            stPgsql.setLong(2, Long.parseLong(jObjAgentToAgentData.get("nominalTransfer").toString()));
            stPgsql.setString(3, jObjAgentToAgentData.get("idAgentAccountPenerima").toString());
            stPgsql.setLong(4, Long.parseLong(idTransaksi));
            stPgsql.executeUpdate();

            query = "INSERT INTO \"TrStock\"( "
                    + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                    + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                    + " VALUES ('123456', now(), ?, 'True', ?,  0, 1, 'DEPOSIT', ?, 0);";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, 0);
            stPgsql.setLong(2, Long.parseLong(jObjAgentToAgentData.get("feeTransfer").toString()));
            stPgsql.setLong(3, Long.parseLong(idTransaksi));
            stPgsql.executeUpdate();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

}
