/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcPembayaranKomunitas;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.*;
import java.util.*;

/**
 * @author Hasan
 */
public class ImplePembayaranKomunitas implements InterfcPembayaranKomunitas {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembayaranKomunitas(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getKomunitas(JSONObject objJson) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "CM-00");
        jsonObject.put("MC", "CM0102");
        jsonObject.put("MT", "2100");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObjectMp.put("idTipeTransaksi", "37");
        jsonObjectMp.put("TipeUser", "list");
        jsonObjectMp.put("Tipe", "CommunityList");
        jsonObjectMp.put("trxid", objJson.get("idTrx").toString());
        jsonObject.put("MP", jsonObjectMp);
        //insert log
        int idLog = utilities.insertLog(objJson.get("idTrx").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "komunitas_gw");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    jObjResp.put("ACK", "OK");
                } else {
                    jObjResp.put("ACK", "NOK");
                    jObjResp.put("pesan", "gagal mengambil list komunitas");
                }
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal mengambil list komunitas");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jObjResp.toString();
    }

    @Override
    public String getInq(JSONObject jsonReq) {
        String idAgentAccount = jsonReq.get("username").toString();
        String idAgent = idAgentAccount.substring(0, 6) + '2' + idAgentAccount.substring(7);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "CM-01");
        jsonObject.put("MC", "CM0102");
        jsonObject.put("MT", "2100");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObjectMp.put("idTipeTransaksi", jsonReq.get("idTipeTransaksi").toString());
        jsonObjectMp.put("TipeUser", jsonReq.get("tipeUser").toString());
        jsonObjectMp.put("id_Denom", jsonReq.get("idDenom").toString());
        jsonObjectMp.put("Tipe", jsonReq.get("tipe").toString());
        jsonObjectMp.put("trxid", jsonReq.get("idTrx").toString());
        jsonObjectMp.put("communityAgentAccount", jsonReq.get("username").toString());
        jsonObjectMp.put("no_KartuAgent", "");
        jsonObjectMp.put("id_Operator", jsonReq.get("idOperator").toString());
        jsonObjectMp.put("communityCode", jsonReq.get("communityCode").toString());
        jsonObjectMp.put("id_Account", idAgent);
        jsonObjectMp.put("unique_id", jsonReq.get("idPelanggan").toString());
        jsonObject.put("MP", jsonObjectMp);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "komunitas_gw");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    jObjResp.put("ACK", "OK");

                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    String jmlTagihan = jObjRespMpo.get("jumlahtagihan").toString();
                    double dbl = Double.parseDouble(jmlTagihan);
                    String dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jObjRespMpo.put("jumlahtagihan", dblStr);

                    String jumlahadm = jObjRespMpo.get("jumlahadm").toString();
                    dbl = Double.parseDouble(jumlahadm);
                    dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jObjRespMpo.put("jumlahadm", dblStr);

                    String jumlahbayar = jObjRespMpo.get("jumlahbayar").toString();
                    dbl = Double.parseDouble(jumlahbayar);
                    dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jObjRespMpo.put("jumlahbayar", dblStr);
                } else {
                    jObjResp.put("ACK", "NOK");
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                }
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal inq komunitas");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jObjResp.toString();
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String NoKartu) {
        String[] status = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"MsAccountStatus\".\"NamaAccountStatus\""
                    + " ,\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\""
                    + " ,\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\""
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\""
                    + " WHERE \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus"
                    + " AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, NoKartu);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("NamaAccountStatus");
                status[1] = rsPgsql.getString("LastBalance");
                status[2] = rsPgsql.getString("StatusKartu");
                status[3] = rsPgsql.getString("id_agentaccount");
                status[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String getPay(JSONObject jsonReq) {
        String idAgentAccount = jsonReq.get("username").toString();
        String idAgent = idAgentAccount.substring(0, 6) + '2' + idAgentAccount.substring(7);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "CM-02");
        jsonObject.put("MC", "CM0102");
        jsonObject.put("MT", "2200");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObjectMp.put("idTipeTransaksi", jsonReq.get("idTipeTransaksi").toString());
        jsonObjectMp.put("TipeUser", jsonReq.get("tipeUser").toString());
        jsonObjectMp.put("id_Denom", jsonReq.get("idDenom").toString());
        jsonObjectMp.put("unique_id", jsonReq.get("idPelanggan").toString());
        jsonObjectMp.put("Tipe", jsonReq.get("tipe").toString());
        jsonObjectMp.put("trxid", jsonReq.get("idTrx").toString());
        jsonObjectMp.put("no_KartuAgent", "");
        jsonObjectMp.put("id_Operator", jsonReq.get("idOperator").toString());
        jsonObjectMp.put("communityCode", jsonReq.get("communityCode").toString());
        jsonObjectMp.put("id_Account", idAgentAccount);
        jsonObject.put("MP", jsonObjectMp);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "komunitas_gw");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    jObjResp.put("ACK", "OK");
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                    jObjResp.put("idtrx", jObjRespMpo.get("trxid").toString());
                    jObjResp.put("timestamp", utilities.getDate().toString());
                    jObjResp.put("status", "SUKSES");
                } else {
                    jObjResp.put("ACK", "NOK");
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                    jObjResp.put("status", "GAGAL");
                }
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal pay komunitas");
                jObjResp.put("status", "GAGAL");
            }
        } catch (ParseException e) {
            jObjResp.put("ACK", "NOK");
            jObjResp.put("pesan", "gagal pay komunitas");
            jObjResp.put("status", "GAGAL");

            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jObjResp.toString();
    }

    @Override
    public void sendSms(JSONObject jsonReq, JSONObject respObj) {
        String smsScript = null;
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        //<editor-fold defaultstate="collapsed" desc="query kirim sms">
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String sql = "select *"
                    + "    from"
                    + "        \"MsSMSNotification\""
                    + "    where"
                    + "        id_tipetransaksi=? "
                    + "        and id_tipeaplikasi=? "
                    + "        and account_type=? "
                    + "        and trx_status=?";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(jsonReq.get("idTipeTransaksi").toString()));
            stPgsql.setLong(2, 3);
            stPgsql.setString(3, "AGENT");
            stPgsql.setString(4, "SUKSES");
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                smsScript = rsPgsql.getString("sms_script");
            }
        } catch (Exception e){
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        //</editor-fold>

        JSONObject jObjRespMp = (JSONObject) respObj.get("MP");
        String uniqueId = jObjRespMp.get("unique_id").toString();
        JSONObject jObjRespMpo = (JSONObject) respObj.get("MPO");
        String idTrx = jObjRespMpo.get("trxid").toString();

        if (smsScript != null) {
            String pesan = String.format(smsScript,
                    "Komunitas " + jObjRespMp.get("communityName").toString(),
                    uniqueId,
                    idTrx);

            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            new SmsHelper().sent(uuid, jsonReq.get("noHp").toString() , pesan, "PAY_KOMUNITAS", entityConfig);
        }
    }

}
