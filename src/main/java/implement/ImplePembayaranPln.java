/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcPembayaranPln;
import koneksi.DatabaseUtilitiesPgsql;

/**
 * @author Hasan
 */
public class ImplePembayaranPln implements InterfcPembayaranPln {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembayaranPln(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public int getIdSupplier(JSONObject jsonReq) {
        int idSupplier = 0;
        //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "select b.id_supplier"
                    + " from \"MsFee\" a, \"MsFeeSupplier\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_tipeaplikasi = ?"
                    + " and a.id_tipetransaksi = ?"
                    + " and b.id_operator = ?"
                    + " and a.\"FlagActive\" = TRUE"
                    + " and b.\"FlagActive\" = TRUE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(jsonReq.get("id_TipeAplikasi").toString()));
            stPgsql.setLong(2, Long.parseLong(jsonReq.get("ID_TipeTransaksi").toString()));
            stPgsql.setLong(3, Long.parseLong(jsonReq.get("id_Operator").toString()));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idSupplier = rsPgsql.getInt("id_supplier");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return idSupplier;
    }

    @Override
    public String getTipeOperator(String idoperator) {
        String tipeOperator = "";
        //<editor-fold defaultstate="collapsed" desc="query getTipeOperator">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"TipeOperator\""
                    + " FROM \"MsOperator\""
                    + " WHERE id_operator = ?"
                    + " AND \"FlagActive\" = 'TRUE'";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(idoperator));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                tipeOperator = rsPgsql.getString("TipeOperator");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return tipeOperator;
    }

    @Override
    public String getProductCode(String idoperator, int idSupplier) {
        String productCode = "";
        //<editor-fold defaultstate="collapsed" desc="query getProductCode">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"ProductCode\" " + "FROM \"MsFeeSupplier\" b INNER JOIN \"MsFee\" d ON b.id_fee = d.id_fee "
                    + " WHERE b.id_operator = ? AND d.id_tipeaplikasi = 3 "
                    + " AND b.\"FlagActive\" = 'TRUE' AND b.id_supplier = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(idoperator));
            stPgsql.setLong(2, idSupplier);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                productCode = rsPgsql.getString("ProductCode");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return productCode;
    }

    @Override
    public String getUrl(String string) {
        String url = "";
        //<editor-fold defaultstate="collapsed" desc="query getUrl">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, string);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return url;
    }

    @Override
    public String curlPost(String url, String data, boolean encode) {
        JSONObject responseObj = null;
        try {
            BufferedReader rd = null;
            String line = null;
            if (encode) {
                data = URLEncoder.encode(data.toString(), "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

            controllerLog.logStreamWriter("req plnPasca switcher url : " + url + " data : " + data);

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");
            con.setRequestProperty("Content-Type", "text/plain");

            con.setConnectTimeout(120000);
            con.setReadTimeout(120000);

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String code = String.valueOf(con.getResponseCode());

            //System.err.println(code);
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));

            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            if (response.toString().trim().length() > 10) {
                JSONParser parser = new JSONParser();
                controllerLog.logStreamWriter("response from otp generator : " + response.toString());
                Object obj = parser.parse(response.toString());
                responseObj = (JSONObject) obj;
                responseObj.put("ResponseCode", code);
            } else {
                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "500");
                responseObj.put("ACK", "NOK");
                responseObj.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                responseObj.put("MSG", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
            }
        } catch (java.net.SocketTimeoutException ex) {
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "504");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "TIME OUT!!");
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            return (responseObj.toString());
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "500");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "EXT : INTERNAL SERVER ERROR");
            return (responseObj.toString());
        }
        return (responseObj.toString());
    }

    @Override
    public int getTrxId(String idtrx) {
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query getTrxId">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select count(*) from \"TrTransaksi\" where id_trx=?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idtrx);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return count;
    }

    @Override
    public String[] getStatusTrx(String idtrx) {
        String status[] = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT "
                    + "  \"TrTransaksi\".\"CustomerReference\", \"TrTransaksi\".\"NoHPTujuan\", \"TrTransaksi\".\"NoRekTujuan\","
                    + "  \"TrTransaksi\".\"StatusTRX\", \"TrTransaksi\".\"id_pelanggan\" FROM "
                    + "  public.\"TrTransaksi\" "
                    + "WHERE    \"TrTransaksi\".id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idtrx);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("StatusTRX");
                status[1] = rsPgsql.getString("CustomerReference");
                status[2] = rsPgsql.getString("NoHPTujuan");
                status[3] = rsPgsql.getString("NoRekTujuan");
                status[4] = rsPgsql.getString("id_pelanggan");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public boolean CekIsEmptyPostpaid(String idpelanggan, String idoperator, String username) {
        boolean status = false;
        //<editor-fold defaultstate="collapsed" desc="query CekIsEmptyPostpaid">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        int count = 0;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\""
                    + " WHERE \"id_pelanggan\" = ?"
                    + " and id_operator = ?"
                    + " AND \"TimeStamp\"::DATE = now()::DATE "
                    + " AND (id_memberaccount = ? OR id_agentaccount = ?)"
                    + " AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idpelanggan);
            stPgsql.setLong(2, Long.parseLong(idoperator));
            stPgsql.setString(3, username);
            stPgsql.setString(4, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
            if (count == 0) {
                status = true;
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String username) {
        String[] statusRekMember = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \n" + "\"MsAccountStatus\".\"NamaAccountStatus\" "
                    + ",\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\" "
                    + ",\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\" "
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\" WHERE "
                    + "\"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus "
                    + "AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                statusRekMember[0] = rsPgsql.getString("NamaAccountStatus");
                statusRekMember[1] = rsPgsql.getString("LastBalance");
                statusRekMember[2] = rsPgsql.getString("StatusKartu");
                statusRekMember[3] = rsPgsql.getString("id_agentaccount");
                statusRekMember[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return statusRekMember;
    }

    @Override
    public int[] getComission(int idOperator) {
        int[] comission = new int[7];
        //<editor-fold defaultstate="collapsed" desc="query getComission">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsFeeSupplier\" a, \"MsFee\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_operator = ?"
                    + " and b.id_tipeaplikasi = '3'"
                    + " and a.\"FlagActive\" = true"
                    + " and b.\"FlagActive\" = true";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, idOperator);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                comission[0] = rsPgsql.getInt("BiayaAdmin");    // BiayaAdmin
                comission[1] = rsPgsql.getInt("TrueSpareFee");    // TrueSpareFee
                comission[2] = rsPgsql.getInt("SupplierPrice"); // SupplierPrice
                comission[3] = rsPgsql.getInt("AgentCommNom");    // AgentCommNom
                comission[4] = rsPgsql.getInt("DealerCommNom"); // DealerCommNom
                comission[5] = rsPgsql.getInt("TrueFeeNom");    // TrueFeeNom
                comission[6] = rsPgsql.getInt("MemberDiscNom"); // MemberDiscNom
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return comission;
    }

    @Override
    public String isSaldoCukup(String idAccount, int total) {
        String hasil = "--";
        //<editor-fold defaultstate="collapsed" desc="query isSaldoCukup">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            String bit7 = idAccount.substring(6, 7);
            String tipeAkun = "AGENT";
            String q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            if (q.trim().length() != 0) {
                long saldo = 0;
                stPgsql = connPgsql.prepareStatement(q);
                stPgsql.setString(1, idAccount);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    saldo = rsPgsql.getLong("saldo");
                }
                if (saldo >= total) {
                    hasil = "00";        //BALANCEINSUFICIENT CUKUP
                } else {
                    hasil = "01";        //BALANCEINSUFICIENT TIDAK CUKUP
                }
            }
            connPgsql.commit();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return hasil;
    }

    @Override
    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] statRekAgen, int[] com) {
        int idSupplier = 0;
        String username = jsonReq.get("username").toString();
        String idpelanggan = jsonReq.get("id_Pelanggan").toString();
        String nominal = jsonReq.get("tagihan").toString();
        String biayaadmin = jsonReq.get("biayaBayar").toString();
        String hargacetak = jsonReq.get("hargaCetak").toString();
        String idtrx = jsonReq.get("idtrx").toString();
        String idoperator = jsonReq.get("id_Operator").toString();
        String pin = jsonReq.get("PIN").toString();
        String idtipetransaksi = jsonReq.get("ID_TipeTransaksi").toString();
        String idtipeaplikasi = jsonReq.get("id_TipeAplikasi").toString();

        String stattrx = "";
        String strstatdeduct = "";
        String productcode = "";

        String statusRekening = statRekAgen[0];
        int saldo = Integer.parseInt(statRekAgen[1]);
        String id_agentaccount = statRekAgen[3];
        String id_transaksi = null;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql2 = new DatabaseUtilitiesPgsql();
        Connection connPgsql2 = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
            String sql = "select b.id_supplier"
                    + " from \"MsFee\" a, \"MsFeeSupplier\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_tipeaplikasi = ?"
                    + " and a.id_tipetransaksi = ?"
                    + " and b.id_operator = ?"
                    + " and a.\"FlagActive\" = TRUE"
                    + " and b.\"FlagActive\" = TRUE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(idtipeaplikasi));
            stPgsql.setLong(2, Long.parseLong(idtipetransaksi));
            stPgsql.setLong(3, Long.parseLong(idoperator));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idSupplier = rsPgsql.getInt("id_supplier");
            }
            //</editor-fold>
            if (idSupplier == 0) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                return jsonResp;
            }

            //<editor-fold defaultstate="collapsed" desc="query getProductCode">
            String query = "SELECT \"ProductCode\" " + "FROM \"MsFeeSupplier\" " + "WHERE id_operator = ?"
                    + " AND \"FlagActive\" = 'TRUE'"
                    + " AND id_supplier = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(idoperator));
            stPgsql.setLong(2, idSupplier);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                productcode = rsPgsql.getString("ProductCode");
            }
            //</editor-fold>
            if (productcode == null && productcode.isEmpty()) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                return jsonResp;
            }

            // deduct agent
            //<editor-fold defaultstate="collapsed" desc="query deduct agent">
            sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setString(1, id_agentaccount);
            rsPgsql = stPgsql.executeQuery();
            long lastBalance = 0;
            while (rsPgsql.next()) {
                lastBalance = rsPgsql.getLong("LastBalance");
            }
            if (lastBalance < Long.parseLong(hargacetak)) {
                strstatdeduct = "NOK";
            } else {
                //check negative number
                if (Integer.signum(Integer.parseInt(hargacetak)) > 0) {
                    String deductBalance = "UPDATE \"MsAgentAccount\""
                            + " SET \"LastBalance\"= \"LastBalance\" - ? WHERE \"id_agentaccount\"=?;";
                    stPgsql = connPgsql.prepareStatement(deductBalance);
                    stPgsql.setLong(1, Long.parseLong(hargacetak));
                    stPgsql.setString(2, id_agentaccount);
                    int status = stPgsql.executeUpdate();
                    if (status == 1) {
                        strstatdeduct = "OK";
                    } else {
                        strstatdeduct = "NOK";
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Transaksi gagal");
                    return jsonResp;
                }
            }
            //</editor-fold>
            if (strstatdeduct.equalsIgnoreCase("OK")) {
                String id_dealer = "0";
                //<editor-fold defaultstate="collapsed" desc="query getIdDealer">
                sql = "SELECT \"MsAgentAccount\".id_dealer"
                        + " FROM \"MsAgentAccount\""
                        + " JOIN \"MsAgent\" ON \"MsAgentAccount\".id_agent = \"MsAgent\".id_agent"
                        + " WHERE \"MsAgentAccount\".id_agentaccount = ?"
                        + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, username);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    id_dealer = rsPgsql.getString("id_dealer");
                }

                //</editor-fold>
                String Keterangan = "Pembayaran PLN " + idpelanggan + " sebesar " + hargacetak;
                //<editor-fold defaultstate="collapsed" desc="query updateTrTransaksi">
                String updateTransaksi = "INSERT INTO \"TrTransaksi\""
                        + " ( id_tipetransaksi, id_agentaccount, id_pelanggan, id_supplier, \"Nominal\", \"StatusTRX\", \"StatusDeduct\", \"StatusRefund\", \"TypeTRX\", \"OpsVerifikasi\", \"Total\", id_trx, \"StatusKomisi\", \"TimeStamp\", \"Keterangan\", id_tipeaplikasi, id_operator, \"Biaya\", id_dealer)"
                        + " VALUES (?,?,?,?,?,'OPEN',?,'OPEN','WITHDRAW','PIN',?,?,'OPEN',now(),?,3,?,?,?)";

                stPgsql = connPgsql.prepareStatement(updateTransaksi, Statement.RETURN_GENERATED_KEYS);
                stPgsql.setLong(1, Long.parseLong(idtipetransaksi));
                stPgsql.setString(2, id_agentaccount);
                stPgsql.setString(3, idpelanggan);
                stPgsql.setLong(4, idSupplier);
                stPgsql.setString(5, nominal);
                stPgsql.setString(6, strstatdeduct);
                stPgsql.setLong(7, Long.parseLong(hargacetak));
                stPgsql.setString(8, idtrx);
                stPgsql.setString(9, Keterangan);
                stPgsql.setLong(10, Long.parseLong(idoperator));
                stPgsql.setString(11, biayaadmin);
                stPgsql.setLong(12, Long.parseLong(id_dealer));
                int hasil = stPgsql.executeUpdate();
                id_transaksi = null;
                if (hasil > 0) {
                    rsPgsql = stPgsql.getGeneratedKeys();
                    while (rsPgsql.next()) {
                        id_transaksi = rsPgsql.getString(1);
                    }
                }
                //</editor-fold>
                int status = 0;
                // UpdateTMNStock agent deduct
                //<editor-fold defaultstate="collapsed" desc="query TrStock">
                String sqlInsertMsTMNStock = "INSERT"
                        + " INTO \"TrStock\" (\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                        + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";
                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                stPgsql.setLong(1, (saldo - Integer.parseInt(hargacetak)));
                stPgsql.setLong(2, Integer.parseInt(hargacetak));
                stPgsql.setString(3, id_agentaccount);
                stPgsql.setLong(4, Long.parseLong(id_transaksi));
                status = stPgsql.executeUpdate();
                //</editor-fold>
            }
            connPgsql.commit();

            connPgsql2 = databaseUtilitiesPgsql2.getConnection(this.entityConfig);
            connPgsql2.setAutoCommit(false);
            if (strstatdeduct.equals("OK")) {
                String hargaBeli = null;
                //<editor-fold defaultstate="collapsed" desc="query getHargaBeli">
                sql = "SELECT id_supplier, \"ProductCode\", \"HargaBeli\", \"HargaJual\", \"HargaCetak\", \"HargaCetakMember\""
                        + " FROM \"MsFeeSupplier\"b INNER JOIN \"MsFee\"d ON b.id_fee = d.id_fee"
                        + " WHERE d.id_tipeaplikasi = 3"
                        + " AND d.id_tipetransaksi = ?"
                        + " AND id_operator = ?"
                        + " AND id_denom = '0'"
                        + " AND b.\"FlagActive\" = TRUE"
                        + " AND d.\"FlagActive\" = TRUE";
                //System.err.println(sql);
                stPgsql = connPgsql2.prepareStatement(sql);
                stPgsql.setLong(1, Long.parseLong(idtipetransaksi));
                stPgsql.setLong(2, Long.parseLong(idoperator));
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    hargaBeli = rsPgsql.getString("HargaBeli");
                }
                //</editor-fold>
                JSONObject jObjCurl = new JSONObject();
                jObjCurl.put("trxid", idtrx);
                jObjCurl.put("type", "PAYMENT");
                jObjCurl.put("product", productcode);
                jObjCurl.put("idpelanggan", idpelanggan);
                jObjCurl.put("idpartner", id_agentaccount);
                jObjCurl.put("interface", "MOBILE");
                jObjCurl.put("harga_beli", hargaBeli);
                String url = this.getUrl("postpaid_pln_pay_" + idSupplier);
                //insert log
                int idLog = utilities.insertLog(idtrx, jObjCurl);
                String respon = this.curlPost(url, jObjCurl.toString(), false);
                //updateLog
                utilities.updateLog(idLog, respon);
                JSONParser jSONParser = new JSONParser();
                JSONObject jsonRespCurl = new JSONObject();
                try {
                    jsonRespCurl = (JSONObject) jSONParser.parse(respon);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                Boolean statpayment = false;
                if (jsonRespCurl.get("ResponseCode").toString().equals("200")) {
                    int komAgent = 0, komTrue = 0, komDealer = 0;
                    String swReffNum = "";
                    int periodeBayar = 0;
                    if (jsonRespCurl.get("ACK").toString().equals("OK")) {
                        stattrx = "SUKSES";

                        if (jsonRespCurl.containsKey("SW_reff_num")) {
                            swReffNum = jsonRespCurl.get("SW_reff_num").toString();
                        }

                        periodeBayar = StringUtils.countMatches(jsonRespCurl.get("bulan_thn").toString(), ",");
                        periodeBayar = periodeBayar + 1;
                        komTrue = com[5] * periodeBayar;
                        komAgent = com[3] * periodeBayar;
                        komDealer = com[4] * periodeBayar;
                        // insert tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String tempkomisi = "INSERT INTO \"TempKomisi\" "
                                + " (\"id_trx\", \"Agent\", \"True\", \"Status\", \"StatusTransaksi\", \"Dealer\") "
                                + " VALUES (?, ?, ?, 'OPEN', 'OPEN', ?)";
                        stPgsql = connPgsql2.prepareStatement(tempkomisi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.setFloat(2, (int) Math.ceil(komAgent));
                        stPgsql.setFloat(3, (int) Math.floor(komTrue));
                        stPgsql.setFloat(4, komDealer);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (jsonRespCurl.get("ACK").toString().equals("PENDING")) {
                        stattrx = "PENDING";

                        // insert tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String tempkomisi = "INSERT INTO \"TempKomisi\" "
                                + "(\"id_trx\", \"Agent\", \"True\", \"Status\", \"StatusTransaksi\") "
                                + "VALUES (?, ?, ?, 'OPEN', 'OPEN')";
                        stPgsql = connPgsql2.prepareStatement(tempkomisi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.setFloat(2, (int) Math.ceil(komAgent));
                        stPgsql.setFloat(3, (int) Math.floor(komTrue));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (jsonRespCurl.get("ACK").toString().equals("NOK")) {
                        stattrx = "GAGAL";
                    }

                    if (stattrx.equals("SUKSES")) {
                        // update tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String updateTemp = "UPDATE \"TempKomisi\" "
                                + "SET \"Status\" = 'OK', \"StatusTransaksi\" = 'OK' "
                                + "WHERE \"id_trx\" = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTemp);
                        stPgsql.setString(1, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update tr transaksi
                        //<editor-fold defaultstate="collapsed" desc="query insert trtransaksi">
                        String updateTransaksi = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = ?, \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK' , \"StatusDana\" = 'DEDUCT', \"hargabeli\" = ?, \"hargajual\" = ?, \"Biaya\" = ?, timestampupdatetrx = now(), id_trxsupplier = ?"
                                + " WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, stattrx);
                        long totalHargaBeli = Long.parseLong(jsonRespCurl.get("jumlahtagihan").toString());
                        stPgsql.setLong(2, totalHargaBeli);
                        stPgsql.setLong(3, Long.parseLong(jsonRespCurl.get("jumlahbayar").toString()));
                        stPgsql.setString(4, biayaadmin);
                        stPgsql.setString(5, swReffNum);
                        stPgsql.setString(6, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock true
                        //<editor-fold defaultstate="collapsed" desc="query update tmnstock">
                        String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (123456, now(), 0, True, ?, 0, 1, 'DEPOSIT', ?, 0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Integer.parseInt(jsonRespCurl.get("jumlahtagihan").toString()));
                        stPgsql.setLong(2, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // komisi agent
                        //<editor-fold defaultstate="collapsed" desc="query komisi agent">
                        String hitungkomisi = null;
                        sql = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, id_agentaccount);
                        stPgsql.executeQuery();

                        hitungkomisi = "UPDATE \"MsAgentAccount\" SET  \"LastBalance\" = \"LastBalance\" + ? WHERE id_agentaccount = ?;";
                        stPgsql = connPgsql2.prepareStatement(hitungkomisi);
                        stPgsql.setLong(1, (int) Math.ceil(komAgent));
                        stPgsql.setString(2, id_agentaccount);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock agent
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock agent">
                        //get lastbalance agent
                        String lb = "";
                        String getAmount = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\" WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";

                        stPgsql = connPgsql2.prepareStatement(getAmount);
                        stPgsql.setString(1, id_agentaccount);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            lb = rsPgsql.getString("LastBalance");
                        }

                        if (lb == null && lb.isEmpty()) {
                            lb = "0";
                        }

                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (?, now(), 0, 'Agent', ?, ?, ?, 'DEPOSIT', ?, 0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Long.parseLong(lb));
                        stPgsql.setLong(2, (int) Math.ceil(komAgent));
                        stPgsql.setLong(3, komAgent);
                        stPgsql.setString(4, username);
                        stPgsql.setLong(5, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock true
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock true">
                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES ('123456', now(), ?, 'True', ?,  0, 1, 'DEPOSIT', ?, 0);";

                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, komTrue);
                        stPgsql.setLong(2, (int) Math.ceil(komTrue));
                        stPgsql.setLong(3, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock supplier
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock supplier">
                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES ('123456', now(), 0, 'Supplier', ?,  0, ?, 'WITHDRAW', ?, 0);";

                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Integer.parseInt(jsonRespCurl.get("jumlahtagihan").toString()));
                        stPgsql.setString(2, String.valueOf(idSupplier));
                        stPgsql.setLong(3, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // komisi dealer
                        //<editor-fold defaultstate="collapsed" desc="query update komisi dealer">
                        String id_dealer = "111111111111";
                        //get id dealer account
                        String getID_memberaccount = "SELECT"
                                + " \"MsDealer\".id_dealer_account"
                                + " FROM"
                                + " \"MsDealer\""
                                + " JOIN \"MsAgentAccount\" ON \"MsDealer\".id_dealer = \"MsAgentAccount\".id_dealer"
                                + " JOIN \"MsAgent\" ON \"MsAgent\".id_agent = \"MsAgentAccount\".id_agent"
                                + " WHERE"
                                + " \"MsAgentAccount\".id_agentaccount = ?"
                                + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer;";
                        stPgsql = connPgsql2.prepareStatement(getID_memberaccount);
                        stPgsql.setString(1, username);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            id_dealer = rsPgsql.getString("id_dealer_account");
                        }

                        sql = "SELECT * FROM \"MsDealer\" WHERE id_dealer_account = ? FOR UPDATE";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, id_dealer);
                        rsPgsql = stPgsql.executeQuery();

                        //get id dealer account
                        hitungkomisi = "UPDATE \"MsDealer\" SET \"Wallet\" = \"Wallet\" + " + (int) Math.ceil(komDealer) + " WHERE id_dealer_account = ?;";
                        stPgsql = connPgsql2.prepareStatement(hitungkomisi);
                        stPgsql.setString(1, id_dealer);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock dealer
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock dealer">
                        lb = "0";
                        getAmount = "SELECT \"MsDealer\".\"Wallet\" FROM \"MsDealer\" WHERE id_dealer_account = ?";
                        stPgsql = connPgsql2.prepareStatement(getAmount);
                        stPgsql.setString(1, id_dealer);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            lb = rsPgsql.getString("Wallet");
                        }

                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (?, now(), 0, 'Dealer', ?,  0, ?,'DEPOSIT', ?, ?);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Long.parseLong(lb));
                        stPgsql.setLong(2, (int) Math.ceil(komDealer));
                        stPgsql.setString(3, id_dealer);
                        stPgsql.setLong(4, Long.parseLong(id_transaksi));
                        stPgsql.setLong(5, komDealer);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // get timestamp
                        String getTime = "SELECT \"TimeStamp\" FROM \"TrTransaksi\" WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(getTime);
                        stPgsql.setString(1, idtrx);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            String strtime = rsPgsql.getString("TimeStamp");
                            jsonRespCurl.put("timestamp", utilities.formatTanggal(strtime));
                        }

                        String pesan = "Transaksi pembayaran tagihan PLN Anda sukses dengan nomor transaksi " + idtrx;
                        jsonResp.put("id_Trx", idtrx);
                        jsonResp.put("ACK", jsonRespCurl.get("ACK").toString());
                        jsonResp.put("pesan", pesan);
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("statusTRX", stattrx);
                        jsonResp.put("bulan_thn", jsonRespCurl.get("bulan_thn").toString());
                        jsonResp.put("daya", jsonRespCurl.get("tarif").toString() + "/" + jsonRespCurl.get("daya").toString() + "VA");
                        jsonResp.put("idpelanggan", jsonRespCurl.get("idpelanggan").toString());

                        double dbl = Double.parseDouble(jsonRespCurl.get("jumlahadm").toString());
                        String dblStr = String.format("%,.0f", dbl);
                        dblStr = dblStr.replaceAll(",", ".");
                        jsonResp.put("jumlahadm", dblStr);

                        dbl = Double.parseDouble(jsonRespCurl.get("jumlahbayar").toString());
                        dblStr = String.format("%,.0f", dbl);
                        dblStr = dblStr.replaceAll(",", ".");
                        jsonResp.put("jumlahbayar", dblStr);

                        dbl = Double.parseDouble(jsonRespCurl.get("jumlahtagihan").toString());
                        dblStr = String.format("%,.0f", dbl);
                        dblStr = dblStr.replaceAll(",", ".");
                        jsonResp.put("jumlahtagihan", dblStr);

                        jsonResp.put("lwbp", jsonRespCurl.get("lwbp").toString());
                        jsonResp.put("namapelanggan", jsonRespCurl.get("namapelanggan").toString());
                        jsonResp.put("refnumber", jsonRespCurl.get("refnumber").toString());
                        jsonResp.put("infoText", jsonRespCurl.get("info_text").toString());
                        jsonResp.put("standMeter", jsonRespCurl.get("lwbp").toString());
                        jsonResp.put("bulanTahun", jsonRespCurl.get("bulan_thn").toString());

                        //hit api iklan
                        String respIklan = utilities.getIklan(idtipetransaksi, productcode, idtrx);
                        JSONObject jObjRespIklan = (JSONObject) parser.parse(respIklan);
                        jsonResp.put("iklan", jObjRespIklan);

                        //insert notif
                        //<editor-fold defaultstate="collapsed" desc="query insert notif">
                        sql = "INSERT INTO \"Notifikasi\" "
                                + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                + " VALUES (?, ?, ?,'FALSE',now())";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, pesan);
                        stPgsql.setString(2, id_agentaccount);
                        stPgsql.setString(3, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (stattrx.equals("PENDING")) {
                        // update tr transaksi
                        String updateTransaksi = "UPDATE \"TrTransaksi\" " + "SET \"StatusTRX\" = '"
                                + stattrx
                                + "', \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'PENDING' "
                                + "WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.executeUpdate();
                        String pesan = "Transaksi Anda sedang diproses";
                        jsonResp.put("id_Trx", idtrx);
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("statusTRX", stattrx);
                        jsonResp.put("pesan", pesan);

                    } else if (stattrx.equals("GAGAL")) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("id_Transaksi", idtrx);
                        jsonResp.put("pesan", "Transaksi Anda gagal");
                        String pesan = "Transaksi pembayaran tagihan PLN Anda gagal dengan nomor transaksi " + idtrx;
                        jsonResp.put("pesan", pesan);
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("statusTRX", stattrx);

                        //insert notif
                        //<editor-fold defaultstate="collapsed" desc="query insert notif">
                        sql = "INSERT INTO \"Notifikasi\" "
                                + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                + " VALUES " + "(?, ?, ?,'FALSE',now())";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, pesan);
                        stPgsql.setString(2, id_agentaccount);
                        stPgsql.setString(3, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                        String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                        stPgsql = connPgsql2.prepareStatement(sql1);
                        stPgsql.setString(1, id_agentaccount);
                        rsPgsql = stPgsql.executeQuery();

                        sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + hargacetak + " "
                                + "WHERE id_agentaccount = ?";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, id_agentaccount);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        // update tr transaksi
                        //<editor-fold defaultstate="collapsed" desc="query update trtransaksi">
                        String updateTransaksi = "UPDATE"
                                + " \"TrTransaksi\" SET \"StatusTRX\" = ?, \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK', \"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', timestamprefund = now()"
                                + " WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, stattrx);
                        stPgsql.setString(2, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        // UpdateTMNStock agent REFUND
                        //<editor-fold defaultstate="collapsed" desc="query agent refund">
                        String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (?, now(),0, 'Agent', ?,  0, ?, 'REFUND', ?,0);";

                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, saldo);
                        stPgsql.setLong(2, Integer.parseInt(hargacetak));
                        stPgsql.setString(3, id_agentaccount);
                        stPgsql.setLong(4, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    }
                } else {
                    //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                    String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                    stPgsql = connPgsql2.prepareStatement(sql1);
                    stPgsql.setString(1, id_agentaccount);
                    rsPgsql = stPgsql.executeQuery();

                    sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + hargacetak + " "
                            + "WHERE id_agentaccount = ?";
                    stPgsql = connPgsql2.prepareStatement(sql);
                    stPgsql.setString(1, id_agentaccount);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="query update tr transaksi">
                    String updateTransaksi = "UPDATE \"TrTransaksi\" "
                            + "SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK',"
                            + "\"StatusDeduct\" = 'NOK', "
                            + "\"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', "
                            + "timestamprefund = now() " + "WHERE id_trx = ?";
                    stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                    stPgsql.setString(1, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    // UpdateTMNStock agent refund
                    //<editor-fold defaultstate="collapsed" desc="query update tmn stock agent refund">
                    String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"(\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                            + " VALUES (?, now(), 0, 'Agent', ?,  0, ?, 'REFUND', ?, 0);";
                    stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                    stPgsql.setLong(1, saldo);
                    stPgsql.setLong(2, Long.parseLong(hargacetak));
                    stPgsql.setLong(3, Long.parseLong(id_agentaccount));
                    stPgsql.setLong(4, Long.parseLong(id_transaksi));
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query update komisi">
                    String updateTemp = "UPDATE \"TempKomisi\" "
                            + "SET \"Status\" = 'NOK', \"StatusTransaksi\" = 'NOK' "
                            + "WHERE \"id_trx\" = ?";
                    stPgsql = connPgsql2.prepareStatement(updateTemp);
                    stPgsql.setString(1, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("id_Transaksi", idtrx);
                    String pesan = "Transaksi pembayaran tagihan PLN Anda gagal dengan nomor transaksi " + idtrx;
                    jsonResp.put("pesan", pesan);
                    jsonResp.put("statusTRX", "GAGAL");
                    //insert notif
                    //<editor-fold defaultstate="collapsed" desc="query insert notif">
                    sql = "INSERT INTO \"Notifikasi\" "
                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                            + " VALUES (?, ?, ?,'FALSE',now())";
                    stPgsql = connPgsql2.prepareStatement(sql);
                    stPgsql.setString(1, pesan);
                    stPgsql.setString(2, id_agentaccount);
                    stPgsql.setString(3, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                }
            } else {
                String updateTransaksi = "UPDATE \"TrTransaksi\" "
                        + "SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK',"
                        + "\"StatusDeduct\" = 'NOK', "
                        + "\"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', "
                        + "timestamprefund = now() WHERE id_trx = ?";
                stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                stPgsql.setString(1, idtrx);
                stPgsql.executeUpdate();

                jsonResp.put("id_Transaksi", idtrx);
                jsonResp.put("ACK", "NOK");
                String pesan = "Transaksi pembayaran tagihan PLN Anda gagal dengan nomor transaksi " + idtrx;
                jsonResp.put("pesan", pesan);
                jsonResp.put("statusTRX", "GAGAL");
                //insert notif
                //<editor-fold defaultstate="collapsed" desc="query insert notif">
                sql = "INSERT INTO \"Notifikasi\" "
                        + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                        + " VALUES (?, ?, ?,'FALSE',now())";
                stPgsql = connPgsql2.prepareStatement(sql);
                stPgsql.setString(1, pesan);
                stPgsql.setString(2, id_agentaccount);
                stPgsql.setString(3, idtrx);
                stPgsql.executeUpdate();
                //</editor-fold>
            }
            connPgsql2.commit();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
                connPgsql2.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
                if (connPgsql2 != null) {
                    connPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return jsonResp;
    }
}
