/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcPembayaranTv;
import koneksi.DatabaseUtilitiesPgsql;

/**
 * @author Hasan
 */
public class ImplePembayaranTv implements InterfcPembayaranTv {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembayaranTv(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public int getIdSupplier(JSONObject jsonReq) {
        int idSupplier = 0;
        //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "select b.id_supplier"
                    + " from \"MsFee\" a, \"MsFeeSupplier\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_tipeaplikasi = ?"
                    + " and a.id_tipetransaksi = ?"
                    + " and b.id_operator = ?"
                    + " and a.\"FlagActive\" = TRUE"
                    + " and b.\"FlagActive\" = TRUE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(jsonReq.get("id_TipeAplikasi").toString()));
            stPgsql.setLong(2, Long.parseLong(jsonReq.get("ID_TipeTransaksi").toString()));
            stPgsql.setLong(3, Long.parseLong(jsonReq.get("id_Operator").toString()));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idSupplier = rsPgsql.getInt("id_supplier");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return idSupplier;
    }

    @Override
    public String getTv(JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select a.\"NamaOperator\", a.id_operator, b.\"HargaCetak\", b.id_denom, b.url_image, c.\"Nominal\", d.id_tipetransaksi"
                    + " from \"MsOperator\" a, \"MsFeeSupplier\" b, \"MsDenom\" c, \"MsFee\" d"
                    + " where a.id_operator = b.id_operator"
                    + " and b.id_denom = c.id_denom"
                    + " and b.id_fee = d.id_fee"
                    + " and d.id_tipetransaksi = '13'"
                    + " and d.id_tipeaplikasi = '3'"
                    + " and a.\"FlagActive\" = 't'"
                    + " and b.\"FlagActive\" = 't'"
                    + " and c.\"FlagActive\" = 't'"
                    + " and d.\"FlagActive\" = 't'"
                    + " and a.\"TipeOperator\" = 'TVKABEL-PASCA'"
                    + " ORDER BY a.\"NamaOperator\"::CHAR ASC";
            st = conn.prepareStatement(query);
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idOperator", rs.getString("id_operator"));
                jObj.put("urlImage", rs.getString("url_image"));
                jObj.put("namaOperator", rs.getString("NamaOperator"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("operator", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String tipeOperator(String idoperator) {
        String tipeOperator = "";
        //<editor-fold defaultstate="collapsed" desc="query tipeOperator">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"TipeOperator\" FROM \"MsOperator\" WHERE id_operator =" + idoperator
                    + " AND \"FlagActive\" = 'TRUE'";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                tipeOperator = rsPgsql.getString("TipeOperator");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return tipeOperator;
    }

    @Override
    public String getProductCode(String idoperator, int idSupplier) {
        String productCode = "";
        //<editor-fold defaultstate="collapsed" desc="query getProductCode">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"ProductCode\" " + "FROM \"MsFeeSupplier\" b INNER JOIN \"MsFee\" d ON b.id_fee = d.id_fee "
                    + " WHERE b.id_operator = " + idoperator + " AND d.id_tipeaplikasi = 3 "
                    + " AND b.\"FlagActive\" = 'TRUE' AND b.id_supplier = " + idSupplier;
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                productCode = rsPgsql.getString("ProductCode");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return productCode;
    }

    @Override
    public String getUrl(String param) {
        String url = "";
        //<editor-fold defaultstate="collapsed" desc="query getUrlMsParameter">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, param);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return url;
    }

    @Override
    public String curlPost(String url, String data, boolean encode) {
        JSONObject responseObj = null;
        try {
            BufferedReader rd = null;
            String line = null;
            if (encode) {
                data = URLEncoder.encode(data.toString(), "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

            this.controllerLog.logStreamWriter("req to otp generator : " + url + " with data : " + data);

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");
            con.setRequestProperty("Content-Type", "text/plain");

            con.setConnectTimeout(120000);
            con.setReadTimeout(120000);

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String code = String.valueOf(con.getResponseCode());

            //System.err.println(code);
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));

            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            if (response.toString().trim().length() > 10) {
                JSONParser parser = new JSONParser();
                this.controllerLog.logStreamWriter("response from otp generator : " + response.toString());
                Object obj = parser.parse(response.toString());
                responseObj = (JSONObject) obj;
                responseObj.put("ResponseCode", code);
            } else {
                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "500");
                responseObj.put("ACK", "NOK");
                responseObj.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                responseObj.put("MSG", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
            }

        } catch (java.net.SocketTimeoutException ex) {
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "504");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "TIME OUT!!");
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);

            return (responseObj.toString());
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);

            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "500");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "EXT : INTERNAL SERVER ERROR");

            return (responseObj.toString());

        }
        return (responseObj.toString());
    }

    @Override
    public int gettrxid(String trxid) {
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query gettrxid">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select count(*) from \"TrTransaksi\" where id_trx = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return count;
    }

    @Override
    public String[] getStatusTrx(String trxid) {
        String status[] = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT"
                    + "  \"TrTransaksi\".\"CustomerReference\", \"TrTransaksi\".\"NoHPTujuan\", \"TrTransaksi\".\"NoRekTujuan\","
                    + "  \"TrTransaksi\".\"StatusTRX\", \"TrTransaksi\".\"id_pelanggan\" FROM"
                    + "  public.\"TrTransaksi\""
                    + "  WHERE \"TrTransaksi\".id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("StatusTRX");
                status[1] = rsPgsql.getString("CustomerReference");
                status[2] = rsPgsql.getString("NoHPTujuan");
                status[3] = rsPgsql.getString("NoRekTujuan");
                status[4] = rsPgsql.getString("id_pelanggan");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public boolean CekIsEmptyPostpaid(String idPelanggan, String id_operator, String idaccount) {
        boolean cek = false;
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query CekIsEmptyPostpaid">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\""
                    + " WHERE \"id_pelanggan\" = ?"
                    + " and id_operator = ?"
                    + " AND \"TimeStamp\"::DATE = now()::DATE "
                    + " AND (id_memberaccount = ? OR id_agentaccount = ?)"
                    + " AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idPelanggan);
            stPgsql.setLong(2, Long.parseLong(id_operator));
            stPgsql.setString(3, idaccount);
            stPgsql.setString(4, idaccount);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
            if (count == 0) {
                cek = true;
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return cek;
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String NoKartu) {
        String[] status = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"MsAccountStatus\".\"NamaAccountStatus\""
                    + " ,\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\""
                    + " ,\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\""
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\""
                    + " WHERE \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus"
                    + " AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, NoKartu);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("NamaAccountStatus");
                status[1] = rsPgsql.getString("LastBalance");
                status[2] = rsPgsql.getString("StatusKartu");
                status[3] = rsPgsql.getString("id_agentaccount");
                status[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String[] getComission(int idTipeTransaksi, int idTipeAplikasi, int idOperator) {
        String[] comission = new String[3];
        //<editor-fold defaultstate="collapsed" desc="query getComission">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPosgre = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connPosgre = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String sqlGetRangeComission = "select * from \"MsFeeSupplier\" a, \"MsFee\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and b.id_tipetransaksi = ?"
                    + " and b.id_tipeaplikasi = ?"
                    + " and a.id_operator = ?"
                    + " and a.\"FlagActive\" = true";

            ps = connPosgre.prepareStatement(sqlGetRangeComission);
            ps.setLong(1, idTipeTransaksi);
            ps.setLong(2, idTipeAplikasi);
            ps.setLong(3, idOperator);
            rs = ps.executeQuery();
            if (rs.next()) {
                comission[0] = rs.getString("TrueFeeNom");
                comission[1] = rs.getString("AgentCommNom");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connPosgre != null) {
                    connPosgre.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return comission;
    }

    @Override
    public String isSaldoCukup(String idAccount, int total) {
        String hasil = "--";
        //<editor-fold defaultstate="collapsed" desc="query isSaldoCukup">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            String bit7 = idAccount.substring(6, 7);
            String tipeAkun = "AGENT";
            String q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            if (q.trim().length() != 0) {
                long saldo = 0;
                stPgsql = connPgsql.prepareStatement(q);
                stPgsql.setString(1, idAccount);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    saldo = rsPgsql.getLong("saldo");
                }
                if (saldo >= total) {
                    hasil = "00";        //BALANCEINSUFICIENT CUKUP
                } else {
                    hasil = "01";        //BALANCEINSUFICIENT TIDAK CUKUP
                }
            }
            connPgsql.commit();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return hasil;
    }

    @Override
    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, String[] com) {
        String username = jsonReq.get("username").toString();
        String idPelanggan = jsonReq.get("id_Pelanggan").toString();
        String nominal = jsonReq.get("tagihan").toString();
        String biayaAdmin = jsonReq.get("biayaBayar").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        String idtrx = jsonReq.get("idtrx").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String pin = jsonReq.get("PIN").toString();
        String idTipeTransaksi = jsonReq.get("ID_TipeTransaksi").toString();
        String idTipeAplikasi = jsonReq.get("id_TipeAplikasi").toString();
        int saldo = Integer.parseInt(rekeningAgentNonEDC[1]);

        String stattrx = "";
        String strstatdeduct;
        String productcode = null;
        String namaOperator = null;
        int idSupplier = 0;
        String id_transaksi = null;

        //<editor-fold defaultstate="collapsed" desc="query prosesBayar">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql2 = new DatabaseUtilitiesPgsql();
        Connection connPgsql2 = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
            String sql = "select b.id_supplier"
                    + " from \"MsFee\" a, \"MsFeeSupplier\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_tipeaplikasi = ?"
                    + " and a.id_tipetransaksi = ?"
                    + " and b.id_operator = ?"
                    + " and a.\"FlagActive\" = TRUE"
                    + " and b.\"FlagActive\" = TRUE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(idTipeAplikasi));
            stPgsql.setLong(2, Long.parseLong(idTipeTransaksi));
            stPgsql.setLong(3, Long.parseLong(idOperator));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idSupplier = rsPgsql.getInt("id_supplier");
            }
            //</editor-fold>
            if (idSupplier == 0) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                return jsonResp;
            }

            //<editor-fold defaultstate="collapsed" desc="query getProductCode">
            String query = "SELECT a.\"ProductCode\", b.\"NamaOperator\"" +
                    " FROM \"MsFeeSupplier\" a, \"MsOperator\" b" +
                    " WHERE a.id_operator = ?" +
                    " AND a.\"FlagActive\" = 'TRUE'" +
                    " AND a.id_supplier = ?" +
                    " AND a.id_operator = b.id_operator";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(idOperator));
            stPgsql.setLong(2, idSupplier);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                productcode = rsPgsql.getString("ProductCode");
                namaOperator = rsPgsql.getString("NamaOperator");
            }
            //</editor-fold>
            if (productcode == null && productcode.isEmpty()) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                return jsonResp;
            }

            // deduct agent
            //<editor-fold defaultstate="collapsed" desc="query deduct agent">
            sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            long lastBalance = 0;
            while (rsPgsql.next()) {
                lastBalance = rsPgsql.getLong("LastBalance");
            }
            if (lastBalance < Long.parseLong(hargaCetak)) {
                strstatdeduct = "NOK";
            } else {
                //check negative number
                if (Integer.signum(Integer.parseInt(hargaCetak)) > 0) {
                    String deductBalance = "UPDATE \"MsAgentAccount\""
                            + " SET \"LastBalance\"= \"LastBalance\" - ? WHERE \"id_agentaccount\"=?;";
                    stPgsql = connPgsql.prepareStatement(deductBalance);
                    stPgsql.setLong(1, Long.parseLong(hargaCetak));
                    stPgsql.setString(2, username);
                    int status = stPgsql.executeUpdate();
                    if (status == 1) {
                        strstatdeduct = "OK";
                    } else {
                        strstatdeduct = "NOK";
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Transaksi gagal");
                    return jsonResp;
                }
            }
            //</editor-fold>
            if (strstatdeduct.equalsIgnoreCase("OK")) {
                String id_dealer = "0";
                //<editor-fold defaultstate="collapsed" desc="query getIdDealer">
                sql = "SELECT \"MsAgentAccount\".id_dealer"
                        + " FROM \"MsAgentAccount\""
                        + " JOIN \"MsAgent\" ON \"MsAgentAccount\".id_agent = \"MsAgent\".id_agent"
                        + " WHERE \"MsAgentAccount\".id_agentaccount = ?"
                        + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, username);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    id_dealer = rsPgsql.getString("id_dealer");
                }

                //</editor-fold>
                String Keterangan = "Pembayaran TV POSTPAID " + namaOperator + " " + idPelanggan + " sebesar " + hargaCetak;
                //<editor-fold defaultstate="collapsed" desc="query updateTrTransaksi">
                String updateTransaksi = "INSERT INTO \"TrTransaksi\""
                        + " ( id_tipetransaksi, id_agentaccount, id_pelanggan, id_supplier, \"Nominal\", \"StatusTRX\", \"StatusDeduct\", \"StatusRefund\", \"TypeTRX\", \"OpsVerifikasi\", \"Total\", id_trx, \"StatusKomisi\", \"TimeStamp\", \"Keterangan\", id_tipeaplikasi, id_operator, \"Biaya\", id_dealer)"
                        + " VALUES (?,?,?,?,?,'OPEN',?,'OPEN','WITHDRAW','PIN',?,?,'OPEN',now(),?,3,?,?,?)";

                stPgsql = connPgsql.prepareStatement(updateTransaksi, Statement.RETURN_GENERATED_KEYS);
                stPgsql.setLong(1, Long.parseLong(idTipeTransaksi));
                stPgsql.setString(2, username);
                stPgsql.setString(3, idPelanggan);
                stPgsql.setLong(4, idSupplier);
                stPgsql.setString(5, nominal);
                stPgsql.setString(6, strstatdeduct);
                stPgsql.setLong(7, Long.parseLong(hargaCetak));
                stPgsql.setString(8, idtrx);
                stPgsql.setString(9, Keterangan);
                stPgsql.setLong(10, Long.parseLong(idOperator));
                stPgsql.setString(11, biayaAdmin);
                stPgsql.setLong(12, Long.parseLong(id_dealer));
                int hasil = stPgsql.executeUpdate();
                id_transaksi = null;
                if (hasil > 0) {
                    rsPgsql = stPgsql.getGeneratedKeys();
                    while (rsPgsql.next()) {
                        id_transaksi = rsPgsql.getString(1);
                    }
                }
                //</editor-fold>
                int status = 0;
                // UpdateTMNStock agent deduct
                //<editor-fold defaultstate="collapsed" desc="query TrStock">
                String sqlInsertMsTMNStock = "INSERT"
                        + " INTO \"TrStock\" (\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                        + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";
                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                stPgsql.setLong(1, (saldo - Integer.parseInt(hargaCetak)));
                stPgsql.setLong(2, Long.parseLong(hargaCetak));
                stPgsql.setString(3, username);
                stPgsql.setLong(4, Long.parseLong(id_transaksi));
                status = stPgsql.executeUpdate();
                //</editor-fold>
            }
            connPgsql.commit();

            connPgsql2 = databaseUtilitiesPgsql2.getConnection(this.entityConfig);
            connPgsql2.setAutoCommit(false);
            if (strstatdeduct.equals("OK")) {
                String hargaBeli = null;
                //<editor-fold defaultstate="collapsed" desc="query getHargaBeli">
                sql = "SELECT id_supplier, \"ProductCode\", \"HargaBeli\", \"HargaJual\", \"HargaCetak\", \"HargaCetakMember\""
                        + " FROM \"MsFeeSupplier\"b INNER JOIN \"MsFee\"d ON b.id_fee = d.id_fee"
                        + " WHERE d.id_tipeaplikasi = 3"
                        + " AND d.id_tipetransaksi = ?"
                        + " AND id_operator = ?"
                        + " AND id_denom = '0'"
                        + " AND b.\"FlagActive\" = TRUE"
                        + " AND d.\"FlagActive\" = TRUE";
                //System.err.println(sql);
                stPgsql = connPgsql2.prepareStatement(sql);
                stPgsql.setLong(1, Long.parseLong(idTipeTransaksi));
                stPgsql.setLong(2, Long.parseLong(idOperator));
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    hargaBeli = rsPgsql.getString("HargaBeli");
                }
                //</editor-fold>
                JSONObject jObjCurl = new JSONObject();
                jObjCurl.put("trxid", idtrx);
                jObjCurl.put("type", "PAYMENT");
                jObjCurl.put("product", productcode);
                jObjCurl.put("idpelanggan", idPelanggan);
                jObjCurl.put("idpartner", username);
                jObjCurl.put("interface", "MOBILE");
                jObjCurl.put("harga_beli", hargaBeli);
                String url = this.getUrl("url_paytv_postpaid_payment_" + idSupplier);
                //insert log
                int idLog = utilities.insertLog(idtrx, jObjCurl);
                String respon = this.curlPost(url, jObjCurl.toString(), false);
                //updateLog
                utilities.updateLog(idLog, respon);
                JSONParser jSONParser = new JSONParser();
                JSONObject jsonRespCurl = new JSONObject();
                try {
                    jsonRespCurl = (JSONObject) jSONParser.parse(respon);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                if (jsonRespCurl.get("ResponseCode").toString().equals("200")) {
                    int margin = 0;
                    double komAgent = 0, komTrue = 0;
                    String swReffNum = "";
                    if (jsonRespCurl.get("ACK").toString().equals("OK")) {
                        stattrx = "SUKSES";

                        if (jsonRespCurl.containsKey("SW_reff_num")) {
                            swReffNum = jsonRespCurl.get("SW_reff_num").toString();
                        }

                        margin = Integer.parseInt(jsonRespCurl.get("jumlahbayar").toString())
                                - Integer.parseInt(jsonRespCurl.get("jumlahtagihan").toString());

                        komTrue = (Double.parseDouble(com[0]) * margin) / 100;
                        komAgent = (Double.parseDouble(com[1]) * margin) / 100;
                        // insert tempkomisi
                        //System.out.println("komisi agent " + komAgent);
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String tempkomisi = "INSERT INTO \"TempKomisi\" "
                                + " (\"id_trx\", \"Agent\", \"True\", \"Status\", \"StatusTransaksi\") "
                                + " VALUES (?, ?, ?, 'OPEN', 'OPEN')";
                        stPgsql = connPgsql2.prepareStatement(tempkomisi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.setFloat(2, (int) Math.ceil(komAgent));
                        stPgsql.setFloat(3, (int) Math.floor(komTrue));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (jsonRespCurl.get("ACK").toString().equals("PENDING")) {
                        stattrx = "PENDING";

                        // insert tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String tempkomisi = "INSERT INTO \"TempKomisi\" "
                                + "(\"id_trx\", \"Agent\", \"True\", \"Status\", \"StatusTransaksi\") "
                                + "VALUES (?, ?, ?, 'OPEN', 'OPEN')";
                        stPgsql = connPgsql2.prepareStatement(tempkomisi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.setFloat(2, (int) Math.ceil(komAgent));
                        stPgsql.setFloat(3, (int) Math.floor(komTrue));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (jsonRespCurl.get("ACK").toString().equals("NOK")) {
                        stattrx = "GAGAL";
                    }

                    if (stattrx.equals("SUKSES")) {
                        // update tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String updateTemp = "UPDATE \"TempKomisi\" "
                                + "SET \"Status\" = 'OK', \"StatusTransaksi\" = 'OK' "
                                + "WHERE \"id_trx\" = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTemp);
                        stPgsql.setString(1, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update tr transaksi
                        //<editor-fold defaultstate="collapsed" desc="query insert trtransaksi">
                        String updateTransaksi = "UPDATE \"TrTransaksi\""
                                + " SET \"StatusTRX\" = ?, \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK' , \"StatusDana\" = 'DEDUCT', \"hargabeli\" = ?, \"hargajual\" = ?, \"Biaya\" = ?, timestampupdatetrx = now(), id_trxsupplier = ?"
                                + " WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, stattrx);
                        stPgsql.setLong(2, Long.parseLong(jsonRespCurl.get("jumlahtagihan").toString()));
                        stPgsql.setLong(3, Long.parseLong(jsonRespCurl.get("jumlahbayar").toString()));
                        stPgsql.setString(4, biayaAdmin);
                        stPgsql.setString(5, swReffNum);
                        stPgsql.setString(6, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock true
                        //<editor-fold defaultstate="collapsed" desc="query update tmnstock">
                        String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (123456, now(), 0, True, ?, 0, 1, 'DEPOSIT', ?, 0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Integer.parseInt(jsonRespCurl.get("jumlahtagihan").toString()));
                        stPgsql.setLong(2, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // komisi agent
                        //<editor-fold defaultstate="collapsed" desc="query komisi agent">
                        String hitungkomisi = null;
                        sql = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, username);
                        stPgsql.executeQuery();

                        hitungkomisi = "UPDATE \"MsAgentAccount\" SET  \"LastBalance\" = \"LastBalance\" + ? WHERE id_agentaccount = ?;";
                        stPgsql = connPgsql2.prepareStatement(hitungkomisi);
                        stPgsql.setLong(1, (int) Math.ceil(komAgent));
                        stPgsql.setString(2, username);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock agent
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock agent">
                        //get lastbalance agent
                        String lb = "";
                        String getAmount = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\" WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";

                        stPgsql = connPgsql2.prepareStatement(getAmount);
                        stPgsql.setString(1, username);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            lb = rsPgsql.getString("LastBalance");
                        }

                        if (lb == null && lb.isEmpty()) {
                            lb = "0";
                        }

                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (?, now(), 0, 'Agent', ?, ?, ?, 'DEPOSIT', ?, 0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Long.parseLong(lb));
                        stPgsql.setLong(2, (int) Math.ceil(komAgent));
                        stPgsql.setLong(3, Long.parseLong(String.valueOf((int) Math.ceil(komAgent))));
                        stPgsql.setString(4, username);
                        stPgsql.setLong(5, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock true
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock true">
                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES ('123456', now(), ?, 'True', ?,  0, 1, 'DEPOSIT', ?, 0);";

                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Long.parseLong(String.valueOf((int) Math.ceil(komTrue))));
                        stPgsql.setLong(2, (int) Math.ceil(komTrue));
                        stPgsql.setLong(3, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock supplier
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock supplier">
                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES ('123456', now(), 0, 'Supplier', ?,  0, ?, 'WITHDRAW', ?, 0);";

                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Integer.parseInt(jsonRespCurl.get("jumlahtagihan").toString()));
                        stPgsql.setString(2, String.valueOf(idSupplier));
                        stPgsql.setLong(3, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // get timestamp
                        String getTime = "SELECT \"TimeStamp\" FROM \"TrTransaksi\" WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(getTime);
                        stPgsql.setString(1, idtrx);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            String strtime = rsPgsql.getString("TimeStamp");
                            jsonRespCurl.put("timestamp", utilities.formatTanggal(strtime));
                        }

                        String pesan = "Transaksi pembayaran tagihan TV POSTPAID Anda sukses dengan nomor transaksi " + idtrx;
                        jsonResp.put("ACK", jsonRespCurl.get("ACK").toString());
                        jsonResp.put("pesan", pesan);
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("statusTRX", stattrx);
                        jsonResp.put("idtrx", idtrx);
//                        jsonResp.put("bulan_thn", jsonRespCurl.get("bulan_thn").toString());
//                        jsonResp.put("idpelanggan", jsonRespCurl.get("idpelanggan").toString());
//                        jsonResp.put("jumlahadm", jsonRespCurl.get("jumlahadm").toString());
//                        jsonResp.put("jumlahbayar", jsonRespCurl.get("jumlahbayar").toString());
//                        jsonResp.put("jumlahtagihan", jsonRespCurl.get("jumlahtagihan").toString());
//                        jsonResp.put("namapelanggan", jsonRespCurl.get("namapelanggan").toString());
//                        jsonResp.put("refnumber", jsonRespCurl.get("refnumber").toString());
                        //insert notif
                        //<editor-fold defaultstate="collapsed" desc="query insert notif">
                        sql = "INSERT INTO \"Notifikasi\" "
                                + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                + " VALUES (?, ?, ?,'FALSE',now())";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, pesan);
                        stPgsql.setString(2, username);
                        stPgsql.setString(3, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (stattrx.equals("PENDING")) {
                        // update tr transaksi
                        String updateTransaksi = "UPDATE \"TrTransaksi\" " + "SET \"StatusTRX\" = '"
                                + stattrx
                                + "', \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'PENDING' "
                                + "WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.executeUpdate();
                        String pesan = "Transaksi Anda sedang diproses";
                        jsonResp.put("id_Trx", idtrx);
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("statusTRX", stattrx);
                        jsonResp.put("pesan", pesan);

                    } else if (stattrx.equals("GAGAL")) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("id_Transaksi", idtrx);
                        jsonResp.put("pesan", "Transaksi Anda gagal");
                        String pesan = "Transaksi pembayaran tagihan TV POSTPAID Anda gagal dengan nomor transaksi " + idtrx;
                        jsonResp.put("pesan", pesan);
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("statusTRX", stattrx);

                        //insert notif
                        //<editor-fold defaultstate="collapsed" desc="query insert notif">
                        sql = "INSERT INTO \"Notifikasi\" "
                                + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                + " VALUES " + "(?, ?, ?,'FALSE',now())";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, pesan);
                        stPgsql.setString(2, username);
                        stPgsql.setString(3, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                        String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                        stPgsql = connPgsql2.prepareStatement(sql1);
                        stPgsql.setString(1, username);
                        rsPgsql = stPgsql.executeQuery();

                        sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + hargaCetak + " "
                                + "WHERE id_agentaccount = ?";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, username);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        // update tr transaksi
                        //<editor-fold defaultstate="collapsed" desc="query update trtransaksi">
                        String updateTransaksi = "UPDATE"
                                + " \"TrTransaksi\" SET \"StatusTRX\" = ?, \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK', \"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', timestamprefund = now()"
                                + " WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, stattrx);
                        stPgsql.setString(2, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        // UpdateTMNStock agent REFUND
                        //<editor-fold defaultstate="collapsed" desc="query agent refund">
                        String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (?, now(),0, 'Agent', ?,  0, ?, 'REFUND', ?,0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, saldo);
                        stPgsql.setLong(2, Integer.parseInt(hargaCetak));
                        stPgsql.setString(3, username);
                        stPgsql.setLong(4, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    }
                } else {
                    //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                    String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                    stPgsql = connPgsql2.prepareStatement(sql1);
                    stPgsql.setString(1, username);
                    rsPgsql = stPgsql.executeQuery();

                    sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + hargaCetak + " "
                            + "WHERE id_agentaccount = ?";
                    stPgsql = connPgsql2.prepareStatement(sql);
                    stPgsql.setString(1, username);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="query update tr transaksi">
                    String updateTransaksi = "UPDATE \"TrTransaksi\" "
                            + "SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK',"
                            + "\"StatusDeduct\" = 'NOK', "
                            + "\"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', "
                            + "timestamprefund = now() " + "WHERE id_trx = ?";
                    stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                    stPgsql.setString(1, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    // UpdateTMNStock agent refund
                    //<editor-fold defaultstate="collapsed" desc="query update tmn stock agent refund">
                    String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"(\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                            + " VALUES (?, now(), 0, 'Agent', ?,  0, ?, 'REFUND', ?, 0);";
                    stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                    stPgsql.setLong(1, saldo);
                    stPgsql.setLong(2, Long.parseLong(hargaCetak));
                    stPgsql.setLong(3, Long.parseLong(username));
                    stPgsql.setLong(4, Long.parseLong(id_transaksi));
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query update komisi">
                    String updateTemp = "UPDATE \"TempKomisi\" "
                            + "SET \"Status\" = 'NOK', \"StatusTransaksi\" = 'NOK' "
                            + "WHERE \"id_trx\" = ?";
                    stPgsql = connPgsql2.prepareStatement(updateTemp);
                    stPgsql.setString(1, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("id_Transaksi", idtrx);
                    String pesan = "Transaksi pembayaran tagihan TV POSTPAID Anda gagal dengan nomor transaksi " + idtrx;
                    jsonResp.put("pesan", pesan);
                    jsonResp.put("statusTRX", "GAGAL");
                    //insert notif
                    //<editor-fold defaultstate="collapsed" desc="query insert notif">
                    sql = "INSERT INTO \"Notifikasi\" "
                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                            + " VALUES (?, ?, ?,'FALSE',now())";
                    stPgsql = connPgsql2.prepareStatement(sql);
                    stPgsql.setString(1, pesan);
                    stPgsql.setString(2, username);
                    stPgsql.setString(3, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                }
            } else {
                String updateTransaksi = "UPDATE \"TrTransaksi\" "
                        + "SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK',"
                        + "\"StatusDeduct\" = 'NOK', "
                        + "\"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', "
                        + "timestamprefund = now() WHERE id_trx = ?";
                stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                stPgsql.setString(1, idtrx);
                stPgsql.executeUpdate();

                jsonResp.put("id_Transaksi", idtrx);
                jsonResp.put("ACK", "NOK");
                String pesan = "Transaksi pembayaran tagihan TV POSTPAID Anda gagal dengan nomor transaksi " + idtrx;
                jsonResp.put("pesan", pesan);
                jsonResp.put("statusTRX", "GAGAL");
                //insert notif
                //<editor-fold defaultstate="collapsed" desc="query insert notif">
                sql = "INSERT INTO \"Notifikasi\" "
                        + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                        + " VALUES (?, ?, ?,'FALSE',now())";
                stPgsql = connPgsql2.prepareStatement(sql);
                stPgsql.setString(1, pesan);
                stPgsql.setString(2, username);
                stPgsql.setString(3, idtrx);
                stPgsql.executeUpdate();
                //</editor-fold>
            }
            connPgsql2.commit();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
                connPgsql2.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
                connPgsql2.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
                if (connPgsql2 != null) {
                    connPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return jsonResp;
    }
}
