/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

import helper.SmsHelper;
import org.json.simple.JSONObject;

import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcPembelianGame;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Hasan
 */
public class ImplePembelianGame implements InterfcPembelianGame {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembelianGame(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getGame(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select id_operator, \"NamaOperator\""
                    + " from \"MsOperator\""
                    + " where \"TipeOperator\" = 'VOUCHER GAMES'"
                    + " and \"FlagActive\" = TRUE"
                    + " ORDER BY \"NamaOperator\" ASC";
            st = conn.prepareStatement(query);
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idOperator", rs.getString("id_operator"));
                jObj.put("namaOperator", rs.getString("NamaOperator"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("operator", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String getNominal(JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select a.\"NamaOperator\", b.\"HargaCetak\", b.id_denom, b.url_image, c.\"Nominal\", d.id_tipetransaksi"
                    + " from \"MsOperator\" a, \"MsFeeSupplier\" b, \"MsDenom\" c, \"MsFee\" d"
                    + " where a.\"id_operator\" = ?"
                    + " and a.id_operator = b.id_operator"
                    + " and b.id_denom = c.id_denom"
                    + " and b.id_fee = d.id_fee"
                    + " and d.id_tipetransaksi = '19'"
                    + " and d.id_tipeaplikasi = '3'"
                    + " and b.\"FlagActive\" = 't'"
                    + " and a.\"TipeOperator\" = 'VOUCHER GAMES'"
                    + " ORDER BY b.\"HargaCetak\"::int ASC";
            st = conn.prepareStatement(query);
            st.setLong(1, Long.parseLong(jsonReq.get("idOperator").toString()));
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idNominal", rs.getString("id_denom"));

                double hargaCetak = Double.parseDouble(rs.getString("HargaCetak"));
                String hargaCetakStr = String.format("%,.0f", hargaCetak);
                hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                jObj.put("hargaCetak", hargaCetakStr);

                jObj.put("nominal", rs.getString("Nominal"));
                jObj.put("urlImage", rs.getString("url_image"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("nominal", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String getInquiry(JSONObject jsonReq) {
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "permintaan gagal");

        JSONObject jObjCurl = new JSONObject();
        jObjCurl.put("CC","3");
        jObjCurl.put("PC","CEK");
        jObjCurl.put("MC", "UNI01");
        jObjCurl.put("MT", "2000");

        JSONObject jObjMP = new JSONObject();
        jObjMP.put("idTipeTransaksi", "19");
        jObjMP.put("TipeUser", "agent");
        jObjMP.put("denom", jsonReq.get("nominal").toString());
        jObjMP.put("trxid", jsonReq.get("idtrx").toString());
        jObjMP.put("id_Operator", jsonReq.get("idOperator"));

        jObjCurl.put("MP", jObjMP);

        //System.out.println("req to switch gw ==== " + jObjCurl);

        Helper helper = new Helper();
        String resp = helper.requestToGwSocket(jObjCurl, "url_unipin");

        //System.out.println("resp switch gw ==== " + resp);

        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonParsed = (JSONObject) parser.parse(resp);
            if (jsonParsed.get("RC").equals("0000")) {
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "SUKSES");
                JSONObject jMP = (JSONObject) jsonParsed.get("MP");
                jobjResp.put("nominal", jMP.get("denom").toString());
                jobjResp.put("idtrx", jMP.get("trxid").toString());
                jobjResp.put("idOperator", jMP.get("id_Operator").toString());
                JSONObject jMPO = (JSONObject) jsonParsed.get("MPO");
                jobjResp.put("hargaCetak", jMPO.get("hargaCetakNM").toString());
                jobjResp.put("remark", jMPO.get("Remark").toString());
            } else {
                JSONObject jMPO = (JSONObject) jsonParsed.get("MPO");
                jobjResp.put("pesan", jMPO.get("pesan").toString());
            }
        } catch (ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return jobjResp.toString();
    }

    @Override
    public String getNewPay(JSONObject jsonReq) {
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "permintaan gagal");


        String noKartuAgent = "";
        String pinFromDb = "";
        ArrayList<HashMap<String, Object>> row; // = new ArrayList<>();
        row = this.getMsAgentAccountDetil(jsonReq);
        for (HashMap<String, Object> map : row) {
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                int j = 0;
                Map.Entry pair = (Map.Entry) it.next();
                if (pair.getKey().equals("NomorKartu")) {
                    noKartuAgent = pair.getValue().toString();
                }
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        JSONObject jObjCurl = new JSONObject();
        jObjCurl.put("CC","3");
        jObjCurl.put("PC","REQ");
        jObjCurl.put("MC", "UNI01");
        jObjCurl.put("MT", "2200");

        JSONObject jObjMP = new JSONObject();
        jObjMP.put("idTipeTransaksi", "19");
        jObjMP.put("TipeUser", "agent");
        String noHp = jsonReq.get("noHp").toString();
        jObjMP.put("no_HP", noHp);
        jObjMP.put("hargaCetak", jsonReq.get("hargaCetak"));
        jObjMP.put("denom", jsonReq.get("nominal"));
        jObjMP.put("trxid", jsonReq.get("idtrx"));
        jObjMP.put("Tipe", "RequestUnipin");
        jObjMP.put("no_KartuAgent", noKartuAgent);
        jObjMP.put("id_Operator", jsonReq.get("idOperator"));
        jObjMP.put("id_Account", jsonReq.get("username"));

        jObjCurl.put("MP", jObjMP);

        System.out.println("req to switch gw ==== " + jObjCurl);

        Helper helper = new Helper();
        String resp = helper.requestToGwSocket(jObjCurl, "url_unipin");

        System.out.println("resp switch gw ==== " + resp);

        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonParsed = (JSONObject) parser.parse(resp);
            if (jsonParsed.get("RC").equals("0000")) {
                String pin = "";
                String serial = "";
                String idtrx = "";
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", jsonParsed.get("RCM"));
                jobjResp.put("status", "SUKSES");
                JSONObject jMP = (JSONObject) jsonParsed.get("MP");
                jobjResp.put("nominal", jMP.get("denom"));
                idtrx = jMP.get("trxid").toString();
                jobjResp.put("idtrx", idtrx);
                jobjResp.put("idOperator", jMP.get("id_Operator"));
                JSONObject jMPO = (JSONObject) jsonParsed.get("MPO");
                pin = jMPO.get("pin").toString();
                serial = jMPO.get("serial").toString();
                jobjResp.put("pin", pin);
                jobjResp.put("serial", serial);
                jobjResp.put("balance", jMPO.get("balance"));
                jobjResp.put("total_amount", jMPO.get("total_amount"));
                jobjResp.put("reference_no", jMPO.get("reference_no"));
                jobjResp.put("order", jMPO.get("order"));

                String pesan = "Transaksi pembelian voucher Games SUKSES.\nPIN: " + pin + ".\nSERIAL: " + serial + ".\nID Transaksi: " + idtrx;
                jobjResp.put("pesan", pesan);

                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                new SmsHelper().sent(uuid, noHp, pesan, "Games", entityConfig);
            } else {
                jobjResp.put("pesan", jsonParsed.get("RCM"));
                jobjResp.put("status", "GAGAL");
            }
        } catch (ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
        return jobjResp.toString();
    }

    @Override
    public int getCountTrxByIdtrx(String idTrx) {
        int countTrxByIdTrx = 0;
        //<editor-fold defaultstate="collapsed" desc="query getCountTrxByIdtrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT count(*) as juml FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idTrx);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                countTrxByIdTrx = rsPgsql.getInt("juml");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return countTrxByIdTrx;
    }

    @Override
    public String getStatusTrxByIdtrx(JSONObject jsonReq) {
        String statusTrx = "";
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrxByIdtrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"StatusTRX\" FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("idTrx").toString());
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                statusTrx = rsPgsql.getString("StatusTRX");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return statusTrx;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getMsAgentAccountDetil(JSONObject jsonReq) {
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        //<editor-fold defaultstate="collapsed" desc="query getMsAgentAccountDetil">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsAgentAccount\" "
                    + "where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("username").toString());
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            while (rsPgsql.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                }
                row.add(columns);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return row;
    }

    @Override
    public int checkDuplicateTransaction(JSONObject jsonReq) {
        String nohp = jsonReq.get("noHp").toString();
        String nominal = jsonReq.get("nominal").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idtrx").toString();

        int checkDuplicateTransaction = 0;
        //<editor-fold defaultstate="collapsed" desc="query checkDuplicateTransaction">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\" "
                    + "WHERE \"NoHPTujuan\" = ? "
                    + "AND \"Nominal\" = ? "
                    + "and id_operator = ?  "
                    + "AND \"TimeStamp\"::DATE = now()::DATE "
                    + "AND (id_memberaccount = ? OR id_agentaccount = ?)  "
                    + "AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nohp);
            stPgsql.setString(2, nominal);
            stPgsql.setInt(3, Integer.parseInt(idOperator));
            stPgsql.setString(4, username);
            stPgsql.setString(5, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                checkDuplicateTransaction = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return checkDuplicateTransaction;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getDetilHarga(JSONObject jsonReq) {
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        String nohp = jsonReq.get("noHp").toString();
        String nominal = jsonReq.get("nominal").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idtrx").toString();
        //<editor-fold defaultstate="collapsed" desc="query getDetilHarga">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT "
                    + "\"MsFeeSupplier\".\"HargaCetak\", "
                    + "\"MsOperator\".\"NamaOperator\", "
                    + "\"MsDenom\".\"Nominal\", "
                    + "\"MsFeeSupplier\".\"HargaJual\", "
                    + "\"MsFeeSupplier\".\"ProductCode\", "
                    + "\"MsDenom\".id_denom, "
                    + "\"MsFeeSupplier\".id_supplier, "
                    + "\"MsFeeSupplier\".\"HargaBeli\" "
                    + "FROM PUBLIC.\"MsOperator\", PUBLIC.\"MsDenom\", PUBLIC.\"MsFeeSupplier\", PUBLIC.\"MsFee\" "
                    + "WHERE \"MsFeeSupplier\".id_operator = \"MsOperator\".id_operator "
                    + "AND \"MsFeeSupplier\".id_fee = \"MsFee\".id_fee "
                    + "AND \"MsFeeSupplier\".id_denom = \"MsDenom\".id_denom "
                    + "AND \"MsDenom\".\"Nominal\" = ? "
                    + "AND \"MsFee\".id_tipeaplikasi = 3 "
                    + "AND \"MsOperator\".id_operator = ? "
                    + "AND \"MsFeeSupplier\".\"FlagActive\" = TRUE "
                    + "AND \"MsFee\".\"FlagActive\" = TRUE;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nominal);
            stPgsql.setInt(2, Integer.parseInt(idOperator));
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            while (rsPgsql.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                }
                row.add(columns);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return row;
    }

    @Override
    public JSONObject deductBalance(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");

        String nohp = jsonReq.get("noHp").toString();
        String nominal = jsonReq.get("nominal").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idtrx").toString();
        //<editor-fold defaultstate="collapsed" desc="query deduct balance">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            if (Integer.signum(Integer.parseInt(hargaCetak)) > 0) {
                connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                String query = "update \"MsAgentAccount\" set \"LastBalance\" = \"LastBalance\" - ? "
                        + "where "
                        + "id_agentaccount = ?";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setInt(1, Integer.parseInt(hargaCetak));
                stPgsql.setString(2, username);
                stPgsql.executeUpdate();
                jsonResp.put("ACK", "OK");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return jsonResp;
    }

    @Override
    public void insertTrTransTrStock(JSONObject jsonReq, ArrayList<HashMap<String, Object>> row) {
        String nohp = jsonReq.get("noHp").toString();
        String nominal = jsonReq.get("nominal").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idtrx").toString();

        int hargaBeli = 0;
        int idDenom = 0;
        int idSupplier = 0;
        String namaOperator = null;
        for (HashMap<String, Object> map : row) {
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                int j = 0;
                Map.Entry pair = (Map.Entry) it.next();
                if (pair.getKey().equals("HargaBeli")) {
                    hargaBeli = Integer.parseInt(pair.getValue().toString());
                } else if (pair.getKey().equals("id_denom")) {
                    idDenom = Integer.parseInt(pair.getValue().toString());
                } else if (pair.getKey().equals("id_supplier")) {
                    idSupplier = Integer.parseInt(pair.getValue().toString());
                } else if (pair.getKey().equals("NamaOperator")) {
                    namaOperator = (String) pair.getValue();
                }
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        String keterangan = "Pembelian Game " + namaOperator + " sebesar " + nominal + " Ke Nomor " + nohp;
        int idTipeTransaksi = 19;

        String idTransaksi = "0";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            //<editor-fold defaultstate="collapsed" desc="query insertTrTransaksi">
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);
            String query = "INSERT INTO \"TrTransaksi\" ("
                    + "	\"id_tipetransaksi\","
                    + "	\"id_agentaccount\","
                    + "	\"id_denom\","
                    + "	\"NoHPTujuan\","
                    + "	\"Nominal\","
                    + "	\"StatusTRX\","
                    + "	\"StatusDeduct\","
                    + "	\"TypeTRX\","
                    + "	\"TimeStamp\","
                    + "	\"OpsVerifikasi\","
                    + "	\"Total\","
                    + "	\"id_trx\","
                    + "	\"StatusKomisi\","
                    + "	\"id_tipeaplikasi\","
                    + "	\"id_operator\","
                    + "	\"Keterangan\","
                    + "	\"StatusRefund\","
                    + "	\"StatusDana\","
                    + "	\"id_supplier\","
                    + "	\"Biaya\""
                    + ")"
                    + "VALUES"
                    + "	(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setLong(1, idTipeTransaksi);
            stPgsql.setLong(2, Long.parseLong(username));
            stPgsql.setLong(3, idDenom);
            stPgsql.setString(4, nohp);
            stPgsql.setString(5, nominal);
            stPgsql.setString(6, "OPEN");
            stPgsql.setString(7, "OK");
            stPgsql.setString(8, "WITHDRAW");
            stPgsql.setTimestamp(9, this.utilities.getDate());
            stPgsql.setString(10, "PIN");
            stPgsql.setInt(11, Integer.parseInt(hargaCetak));
            stPgsql.setString(12, idtrx);
            stPgsql.setString(13, "PENDING");
            stPgsql.setLong(14, 3);
            stPgsql.setLong(15, Long.parseLong(idOperator));
            stPgsql.setString(16, keterangan);
            stPgsql.setString(17, "NOK");
            stPgsql.setString(18, "DEDUCT");
            stPgsql.setLong(19, idSupplier);
            stPgsql.setString(20, String.valueOf((Integer.parseInt(hargaCetak) - hargaBeli)));
            stPgsql.executeUpdate();
            rsPgsql = stPgsql.getGeneratedKeys();
            while (rsPgsql.next()) {
                idTransaksi = rsPgsql.getString(1);
            }

            //</editor-fold>
            long lastBalance = 0;
            //<editor-fold defaultstate="collapsed" desc="query getLastBalanceAgent">
            DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
            Connection connPgsqlLb = null;
            PreparedStatement stPgsqlLb = null;
            ResultSet rsPgsqlLb = null;
            try {
                connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
                query = "select \"LastBalance\" from \"MsAgentAccount\" "
                        + "where id_agentaccount = ?";
                stPgsqlLb = connPgsqlLb.prepareStatement(query);
                stPgsqlLb.setString(1, username);
                rsPgsqlLb = stPgsqlLb.executeQuery();
                while (rsPgsqlLb.next()) {
                    lastBalance = rsPgsqlLb.getLong("LastBalance");
                }
            } catch (ParserConfigurationException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (rsPgsqlLb != null) {
                        rsPgsqlLb.close();
                    }
                    if (stPgsqlLb != null) {
                        stPgsqlLb.close();
                    }
                    if (connPgsqlLb != null) {
                        connPgsqlLb.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="query insertTrStock">
            query = "INSERT INTO \"TrStock\" ("
                    + "	\"LastBalance\","
                    + "	\"TimeStamp\","
                    + "	\"StokType\","
                    + "	\"Nominal\","
                    + "	\"id_transaksi\","
                    + "	\"id_stock\","
                    + "	\"TypeTrx\","
                    + "	\"True\","
                    + "	\"Member\","
                    + "	\"Agent\","
                    + "	\"Dealer\""
                    + ")"
                    + "VALUES"
                    + "	(?,?,?,?,?,?,?,?,?,?,?);";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, lastBalance);
            stPgsql.setTimestamp(2, this.utilities.getDate());
            stPgsql.setString(3, "Agent");
            stPgsql.setLong(4, Long.valueOf(hargaCetak));
            stPgsql.setLong(5, Long.valueOf(idTransaksi));
            stPgsql.setString(6, username);
            stPgsql.setString(7, "WITHDRAW");
            stPgsql.setLong(8, (long) 0);
            stPgsql.setLong(9, (long) 0);
            stPgsql.setLong(10, (long) 0);
            stPgsql.setLong(11, (long) 0);
            stPgsql.executeUpdate();
            //</editor-fold>
            connPgsql.commit();
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } catch (ParserConfigurationException ex) {
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                String s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    @Override
    public void updateStatusAccount(JSONObject jsonReq) {
        //<editor-fold defaultstate="collapsed" desc="query updateStatusAccount">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "update \"MsAgentAccount\" set id_accountstatus = ?, "
                    + "\"Remark\" = ? "
                    + "where "
                    + "id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.valueOf(1));
            stPgsql.setTimestamp(2, null);
            stPgsql.setString(3, jsonReq.get("username").toString());
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }

    @Override
    public void hitApiSwitcing(final JSONObject jsonReq) {
        //hit api switching
        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("id_trx", jsonReq.get("idtrx").toString());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                jSONObject.put("waktu", sdf.format(new Date()).toString());
                String data = jSONObject.toString();

                String url = "";
                int idSupplier = 0;

                DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                Connection connPgsql = null;
                PreparedStatement stPgsql = null;
                ResultSet rsPgsql = null;
                try {
                    //get msparam
                    connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
                    String paramName = "url_api_switching_prepaid";
                    String query = "SELECT * FROM ms_parameter "
                            + "WHERE parameter_name = ?";
                    stPgsql = connPgsql.prepareStatement(query);
                    stPgsql.setString(1, paramName);
                    rsPgsql = stPgsql.executeQuery();
                    while (rsPgsql.next()) {
                        url = rsPgsql.getString("parameter_value");
                    }

                    //insert log
                    int idLog = utilities.insertLog(jsonReq.get("idtrx").toString(), jSONObject);
                    JSONObject curlResp = utilities.curlPost(url, data, false);
                    //updateLog
                    utilities.updateLog(idLog, curlResp.toString());
                    //</editor-fold>
                } catch (Exception ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                } finally {
                    try {
                        if (rsPgsql != null) {
                            rsPgsql.close();
                        }
                        if (stPgsql != null) {
                            stPgsql.close();
                        }
                        if (connPgsql != null) {
                            connPgsql.close();
                        }
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    }
                }
            }
        }).start();
    }

}
