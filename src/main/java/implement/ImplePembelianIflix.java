/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.*;
import interfc.InterfcPembelianIflix;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * @author Hasan
 */
public class ImplePembelianIflix implements InterfcPembelianIflix {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembelianIflix(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getDenom(JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select a.\"HargaCetak\", a.\"ProductCode\", c.\"Nominal\", c.id_denom " +
                    " from \"MsFeeSupplier\" a, \"MsFee\" b, \"MsDenom\" c " +
                    " where a.id_operator = '214' and a.\"FlagActive\" = 't' " +
                    " and a.id_fee = b.id_fee " +
                    " and a.id_denom = c.id_denom " +
                    " and b.id_tipeaplikasi = '3' " +
                    " and b.id_tipetransaksi = '58' " +
                    " ORDER BY c.\"Nominal\"";
            st = conn.prepareStatement(query);
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idDenom", rs.getString("id_denom"));

                double hargaCetak = Double.parseDouble(rs.getString("HargaCetak"));
                String hargaCetakStr = String.format("%,.0f", hargaCetak);
                hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                jObj.put("hargaCetak", hargaCetakStr);

                jObj.put("nominal", rs.getString("Nominal"));
                jObj.put("productCode", rs.getString("ProductCode"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("nominal", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public int getCountTrxByIdtrx(String idTrx) {
        int countTrxByIdTrx = 0;
        //<editor-fold defaultstate="collapsed" desc="query getCountTrxByIdtrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT count(*) as juml FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idTrx);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                countTrxByIdTrx = rsPgsql.getInt("juml");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return countTrxByIdTrx;
    }

    @Override
    public String getStatusTrxByIdtrx(JSONObject jsonReq) {
        String statusTrx = "";
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrxByIdtrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"StatusTRX\" FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("idTrx").toString());
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                statusTrx = rsPgsql.getString("StatusTRX");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return statusTrx;
    }

    @Override
    public int checkDuplicateTransaction(JSONObject jsonReq) {
        String nohp = jsonReq.get("noHp").toString();
        String nominal = jsonReq.get("nominal").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idtrx").toString();

        int checkDuplicateTransaction = 0;
        //<editor-fold defaultstate="collapsed" desc="query checkDuplicateTransaction">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\" "
                    + "WHERE \"NoHPTujuan\" = ? "
                    + "AND \"Nominal\" = ? "
                    + "and id_operator = ?  "
                    + "AND \"TimeStamp\"::DATE = now()::DATE "
                    + "AND id_agentaccount = ?  "
                    + "AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nohp);
            stPgsql.setString(2, nominal);
            stPgsql.setInt(3, Integer.parseInt(idOperator));
            stPgsql.setString(4, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                checkDuplicateTransaction = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return checkDuplicateTransaction;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getMsAgentAccountDetil(JSONObject jsonReq) {
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        //<editor-fold defaultstate="collapsed" desc="query getMsAgentAccountDetil">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsAgentAccount\" "
                    + "where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("username").toString());
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            while (rsPgsql.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                }
                row.add(columns);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return row;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getDetilHarga(JSONObject jsonReq) {
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        String nohp = jsonReq.get("noHp").toString();
        String nominal = jsonReq.get("nominal").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idtrx").toString();
        //<editor-fold defaultstate="collapsed" desc="query getDetilHarga">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT "
                    + "\"MsFeeSupplier\".\"HargaCetak\", "
                    + "\"MsOperator\".\"NamaOperator\", "
                    + "\"MsDenom\".\"Nominal\", "
                    + "\"MsFeeSupplier\".\"HargaJual\", "
                    + "\"MsFeeSupplier\".\"ProductCode\", "
                    + "\"MsDenom\".id_denom, "
                    + "\"MsFeeSupplier\".id_supplier, "
                    + "\"MsFeeSupplier\".\"HargaBeli\" "
                    + "FROM PUBLIC.\"MsOperator\", PUBLIC.\"MsDenom\", PUBLIC.\"MsFeeSupplier\", PUBLIC.\"MsFee\" "
                    + "WHERE \"MsFeeSupplier\".id_operator = \"MsOperator\".id_operator "
                    + "AND \"MsFeeSupplier\".id_fee = \"MsFee\".id_fee "
                    + "AND \"MsFeeSupplier\".id_denom = \"MsDenom\".id_denom "
                    + "AND \"MsDenom\".\"Nominal\" = ? "
                    + "AND \"MsFee\".id_tipeaplikasi = 3 "
                    + "AND \"MsOperator\".id_operator = ? "
                    + "AND \"MsFeeSupplier\".\"FlagActive\" = TRUE "
                    + "AND \"MsFee\".\"FlagActive\" = TRUE;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nominal);
            stPgsql.setInt(2, Integer.parseInt(idOperator));
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            while (rsPgsql.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                }
                row.add(columns);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return row;
    }

    @Override
    public String getUrl() {
        String url = "";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String paramName = "iflix_getvoucher";
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, paramName);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return url;
    }

    @Override
    public JSONObject getPay(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        String nohp = jsonReq.get("noHp").toString();
        String nominal = jsonReq.get("nominal").toString();
        String idOperator = jsonReq.get("id_Operator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idtrx").toString();
        String idTipeTransaksi = jsonReq.get("ID_TipeTransaksi").toString();

        String hargaJual = "", productCode = "";
        int idDenom = 0, hargaBeli = 0, idFeeSupplier = 0, idSupplier = 0;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql2 = new DatabaseUtilitiesPgsql();
        Connection connPgsql2 = null;
        PreparedStatement stPgsql2 = null;
        ResultSet rsPgsql2 = null;

        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            // Deduct Balance
            String statusDeduct = "NOK";
            //<editor-fold defaultstate="collapsed" desc="query deductBalance">
            String sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();

            long lastBalance = 0;
            while (rsPgsql.next()) {
                lastBalance = rsPgsql.getLong("LastBalance");
            }

            if (lastBalance < Long.parseLong(hargaCetak)) {
                statusDeduct = "NOK";
            } else {
                if (Integer.signum(Integer.parseInt(hargaCetak)) > 0) {
                    String deductBalance = "UPDATE \"MsAgentAccount\" SET \"LastBalance\"= \"LastBalance\" - ? "
                            + "WHERE \"id_agentaccount\"=?;";
                    stPgsql = connPgsql.prepareStatement(deductBalance);
                    stPgsql.setLong(1, Long.parseLong(hargaCetak));
                    stPgsql.setString(2, username);
                    int status = stPgsql.executeUpdate();
                    if (status == 1) {
                        statusDeduct = "OK";
                    } else {
                        statusDeduct = "NOK";
                    }
                } else {
                    statusDeduct = "NOK";
                }
            }
            //</editor-fold>
            if (statusDeduct.equalsIgnoreCase("OK")) {
                //<editor-fold defaultstate="collapsed" desc="query getHargaJual">
                String cekHargaJual = "SELECT"
                        + " \"MsFeeSupplier\".id_supplier, \"MsOperator\".id_operator, \"MsDenom\".\"Nominal\", \"MsFeeSupplier\".\"HargaJual\", \"MsFeeSupplier\".\"ProductCode\", \"MsDenom\".id_denom, \"MsFeeSupplier\".id_supplier, \"MsFeeSupplier\".id_feesupplier, \"MsFeeSupplier\".\"HargaBeli\""
                        + " FROM PUBLIC .\"MsOperator\", PUBLIC .\"MsDenom\", PUBLIC .\"MsFeeSupplier\", PUBLIC .\"MsFee\""
                        + " WHERE "
                        + "\"MsFeeSupplier\".id_operator = \"MsOperator\".id_operator"
                        + " AND \"MsFeeSupplier\".id_fee = \"MsFee\".id_fee"
                        + " AND \"MsFeeSupplier\".id_denom = \"MsDenom\".id_denom"
                        + " AND \"MsDenom\".\"Nominal\" = ?"
                        + " AND \"MsFee\".id_tipeaplikasi = 3"
                        + " AND \"MsOperator\".id_operator = ?"
                        + " AND \"MsFeeSupplier\".\"FlagActive\" = TRUE"
                        + " AND \"MsFee\".\"FlagActive\" = TRUE;";
                stPgsql = connPgsql.prepareStatement(cekHargaJual);
                stPgsql.setString(1, nominal);
                stPgsql.setLong(2, Long.parseLong(idOperator));
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    hargaJual = rsPgsql.getString("HargaJual");
                    idDenom = rsPgsql.getInt("id_denom");
                    idFeeSupplier = rsPgsql.getInt("id_feesupplier");
                    idSupplier = rsPgsql.getInt("id_supplier");
                    productCode = rsPgsql.getString("ProductCode");
                    hargaBeli = rsPgsql.getInt("HargaBeli");
                }
                //</editor-fold>
                String keterangan = "Pembelian Voucher Iflix " + nominal + " sebesar Rp " + hargaCetak + " Ke Nomor " + nohp;
                // Update Transaksi
                String idTrTransaksi = null;
                //<editor-fold defaultstate="collapsed" desc="query updateTransaksi">
                String query = "INSERT INTO \"TrTransaksi\" ("
                        + "	\"id_tipetransaksi\","
                        + "	\"id_agentaccount\","
                        + "	\"id_denom\","
                        + "	\"NoHPTujuan\","
                        + "	\"Nominal\","
                        + "	\"StatusTRX\","
                        + "	\"StatusDeduct\","
                        + "	\"TypeTRX\","
                        + "	\"TimeStamp\","
                        + "	\"OpsVerifikasi\","
                        + "	\"Total\","
                        + "	\"id_trx\","
                        + "	\"StatusKomisi\","
                        + "	\"id_tipeaplikasi\","
                        + "	\"id_operator\","
                        + "	\"Keterangan\","
                        + "	\"StatusRefund\","
                        + "	\"StatusDana\","
                        + "	\"id_supplier\","
                        + "	\"Biaya\","
                        + "	\"id_feesupplier\","
                        + "	\"hargabeli\","
                        + "	\"hargajual\""
                        + ")"
                        + "VALUES"
                        + "	(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                stPgsql.setLong(1, Long.parseLong(idTipeTransaksi));
                stPgsql.setLong(2, Long.parseLong(username));
                stPgsql.setLong(3, idDenom);
                stPgsql.setString(4, nohp);
                stPgsql.setString(5, nominal);
                stPgsql.setString(6, "OPEN");
                stPgsql.setString(7, "OK");
                stPgsql.setString(8, "WITHDRAW");
                stPgsql.setTimestamp(9, this.utilities.getDate());
                stPgsql.setString(10, "PIN");
                stPgsql.setInt(11, Integer.parseInt(hargaCetak));
                stPgsql.setString(12, idtrx);
                stPgsql.setString(13, "PENDING");
                stPgsql.setLong(14, 3);
                stPgsql.setLong(15, Long.parseLong(idOperator));
                stPgsql.setString(16, keterangan);
                stPgsql.setString(17, "NOK");
                stPgsql.setString(18, "DEDUCT");
                stPgsql.setLong(19, idSupplier);
                stPgsql.setString(20, String.valueOf((Integer.parseInt(hargaCetak) - hargaBeli)));
                stPgsql.setLong(21, idFeeSupplier);
                stPgsql.setLong(22, hargaBeli);
                stPgsql.setLong(23, Long.parseLong(hargaCetak));
                stPgsql.executeUpdate();
                rsPgsql = stPgsql.getGeneratedKeys();
                while (rsPgsql.next()) {
                    idTrTransaksi = rsPgsql.getString(1);
                }
                //</editor-fold>
                String lb = "";
                //<editor-fold defaultstate="collapsed" desc="query getLastBalanceAgent">
                String getAmount = "SELECT"
                        + " \"MsAgentAccount\".\"LastBalance\""
                        + " FROM public.\"MsAgentAccount\""
                        + " WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";

                stPgsql = connPgsql.prepareStatement(getAmount);
                stPgsql.setString(1, username);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    lb = rsPgsql.getString("LastBalance");
                }
                if (lb == null && lb.isEmpty()) {
                    lb = "0";
                }
                //</editor-fold>
                // Update TMNStock Agent
                //<editor-fold defaultstate="collapsed" desc="query insert trstock">
                String updateTMNStock = "INSERT"
                        + " INTO \"TrStock\"(\"LastBalance\",\"TimeStamp\",\"StokType\",\"Nominal\",id_transaksi,id_stock,\"TypeTrx\",\"True\",\"Agent\",\"Member\",\"Dealer\")"
                        + " VALUES (?,now(),'Agent',?,?,?,'WITHDRAW', 0, 0, 0, 0);";
                stPgsql = connPgsql.prepareStatement(updateTMNStock);
                stPgsql.setLong(1, Long.parseLong(lb));
                stPgsql.setLong(2, Long.parseLong(hargaCetak));
                stPgsql.setInt(3, Integer.parseInt(idTrTransaksi));
                stPgsql.setString(4, username);

                int status = stPgsql.executeUpdate();
                //</editor-fold>

                int b[] = new int[8];
                //<editor-fold defaultstate="collapsed" desc="query getIDAgenKomisi">
                query = "select * from \"MsFeeSupplier\" a, \"MsFee\" b"
                        + " where a.id_fee = b.id_fee"
                        + " and a.id_denom = ?"
                        + " and b.id_tipetransaksi = ?"
                        + " and b.id_tipeaplikasi = '3'"
                        + " and a.id_operator = ?"
                        + " and a.\"FlagActive\" = true";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setLong(1, idDenom);
                stPgsql.setLong(2, Long.parseLong(idTipeTransaksi));
                stPgsql.setLong(3, Long.parseLong(idOperator));
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    b[0] = rsPgsql.getInt("BiayaAdmin");    // BiayaAdmin
                    b[1] = rsPgsql.getInt("TrueSpareFee");    // TrueSpareFee
                    b[2] = rsPgsql.getInt("SupplierPrice"); // SupplierPrice
                    b[3] = rsPgsql.getInt("AgentCommNom");    // AgentCommNom
                    b[4] = rsPgsql.getInt("DealerCommNom"); // DealerCommNom
                    b[5] = rsPgsql.getInt("TrueFeeNom");    // TrueFeeNom
                    b[6] = rsPgsql.getInt("MemberDiscNom"); // MemberDiscNom
                    b[7] = rsPgsql.getInt("id_supplier");    // idSupplier
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="query insertTmpKomisi">
                int Dealer = b[4];
                int True = b[1] + b[5] + b[6];
                int Agent = b[3];
                String SQL = "INSERT"
                        + " INTO \"TempKomisi\"(\"id_trx\",\"Agent\",\"True\",\"Status\",\"Dealer\",\"StatusTransaksi\")"
                        + " VALUES (?,?,?,'OPEN',?,'PENDING');";

                stPgsql = connPgsql.prepareStatement(SQL);
                stPgsql.setString(1, idtrx);
                stPgsql.setFloat(2, (int) Math.ceil(Agent));
                stPgsql.setFloat(3, (int) Math.ceil(True));
                stPgsql.setFloat(4, (int) Math.ceil(Dealer));

                stPgsql.executeUpdate();
                //</editor-fold>
                connPgsql.commit();

                connPgsql2 = databaseUtilitiesPgsql2.getConnection(this.entityConfig);
                connPgsql2.setAutoCommit(false);
                JSONObject jSONObjCurl = new JSONObject();
                jSONObjCurl.put("providerTransactionId", jsonReq.get("idtrx").toString());
                jSONObjCurl.put("product_sku", jsonReq.get("productCode").toString().replaceAll("\\s+", ""));
                jSONObjCurl.put("denomination", jsonReq.get("hargaCetak").toString());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                jSONObjCurl.put("waktu", sdf.format(new Date()).toString());
                String json = jSONObjCurl.toString();

                //get parameter url
                String url = "";
                //<editor-fold defaultstate="collapsed" desc="query getUrl">
                query = "SELECT * FROM ms_parameter "
                        + "WHERE parameter_name = ?";
                stPgsql2 = connPgsql2.prepareStatement(query);
                stPgsql2.setString(1, "url_api_iflix");
                rsPgsql2 = stPgsql2.executeQuery();
                while (rsPgsql2.next()) {
                    url = rsPgsql2.getString("parameter_value");
                }
                //</editor-fold>
                //insert log
                int idLog = 0;
                //<editor-fold defaultstate="collapsed" desc="query insertLog">
                query = "insert "
                        + "    into"
                        + "        tr_trx_token_log"
                        + "        (id_trx, request, request_time, response, response_time) "
                        + "    values"
                        + "        (?, ?, ?, ?, ?)";
                stPgsql2 = connPgsql2.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                stPgsql2.setString(1, idtrx);
                stPgsql2.setString(2, jsonReq.toString());
                stPgsql2.setTimestamp(3, utilities.getDate());
                stPgsql2.setString(4, null);
                stPgsql2.setTimestamp(5, null);
                int hasil = stPgsql2.executeUpdate();
                if (hasil != 0) {
                    rsPgsql2 = stPgsql2.getGeneratedKeys();
                    while (rsPgsql2.next()) {
                        idLog = Integer.parseInt(rsPgsql2.getString(1));
                    }
                }
                //</editor-fold>
                String responseCurl = "";
                //<editor-fold defaultstate="collapsed" desc="query curlPost">
                JSONObject responseObj = null;
                try {
                    BufferedReader rd = null;
                    String line = null;
                    HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

                    controllerLog.logStreamWriter("req to otp generator : " + url + " data : " + json);

                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Length", "" + Integer.toString(json.getBytes().length));
                    con.setRequestProperty("Content-Language", "en-US");
                    con.setRequestProperty("Content-Type", "text/plain");

                    con.setUseCaches(false);
                    con.setDoInput(true);
                    con.setDoOutput(true);

                    // Send request
                    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                    wr.writeBytes(json);
                    wr.flush();
                    wr.close();

                    String code = String.valueOf(con.getResponseCode());
                    // Get Response
                    InputStream is = con.getInputStream();
                    rd = new BufferedReader(new InputStreamReader(is));

                    StringBuffer response = new StringBuffer();
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();

                    if (response.toString().trim().length() > 10) {
                        JSONParser parser = new JSONParser();
                        //System.out.println(response.toString());
                        controllerLog.logStreamWriter("response otp generator : " + response.toString());
                        Object obj = parser.parse(response.toString());
                        responseObj = (JSONObject) obj;
                        responseObj.put("ResponseCode", code);
                    } else {
                        responseObj = new JSONObject();
                        responseObj.put("ResponseCode", "500");
                        responseObj.put("ACK", "NOK");
                        responseObj.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                    }
                    responseCurl = responseObj.toString();
                } catch (Exception ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                    responseObj.put("ResponseCode", "200");
                    responseObj.put("ACK", "PENDING");
                    responseObj.put("pesan", "EXT : INTERNAL SERVER ERROR");
                    responseCurl = responseObj.toString();
                }
                //</editor-fold>
                //update log
                //<editor-fold defaultstate="collapsed" desc="query updateLog">
                query = "update"
                        + "        \"tr_trx_token_log\" "
                        + "    set"
                        + "        \"response\"=?,"
                        + "        \"response_time\"=? "
                        + "    where"
                        + "        id_trx_token_log=?";
                stPgsql2 = connPgsql2.prepareStatement(query);
                stPgsql2.setString(1, responseCurl);
                stPgsql2.setTimestamp(2, utilities.getDate());
                stPgsql2.setLong(3, idLog);
                hasil = stPgsql2.executeUpdate();
                //</editor-fold>

                JsonObject Jresponse = new JsonParser().parse(responseCurl).getAsJsonObject();
                if (Jresponse.get("resultCode").getAsString().equalsIgnoreCase("0")) {
                    String voucher = Jresponse.has("pin")? Jresponse.get("pin").getAsString() : "";
                    String CustRef = voucher + "/" + idtrx;

                    String swReffNum = "";
                    if (Jresponse.has("transactionId")) {
                        swReffNum = Jresponse.get("transactionId").getAsString();
                    }

                    // Update TrTransaksi
                    //<editor-fold defaultstate="collapsed" desc="query TrTransaksi">
                    String updateTransaksi = "UPDATE \"TrTransaksi\""
                            + " SET \"StatusDana\" = 'DEDUCT', "
                            + "\"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK', \"CustomerReference\" = ?, "
                            + "\"StatusTRX\" = 'SUKSES', timestampupdatetrx = now(), id_trxsupplier = ? WHERE id_trx = ?";

                    stPgsql2 = connPgsql2.prepareStatement(updateTransaksi);
                    stPgsql2.setString(1, CustRef);
                    stPgsql2.setString(2, swReffNum);
                    stPgsql2.setString(3, idtrx);
                    hasil = stPgsql2.executeUpdate();
                    //</editor-fold>

                    // Update TMNStock True
                    //<editor-fold defaultstate="collapsed" desc="query updateTrStock">
                    String updateTMNStockTrue = "INSERT INTO \"TrStock\"("
                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", "
                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                            + " VALUES ('123456',now(),'True',?,?,1,'DEPOSIT',0,0,0,0);";

                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockTrue);
                    stPgsql2.setLong(1, Long.parseLong(String.valueOf(hargaBeli)));
                    stPgsql2.setLong(2, Long.parseLong(idTrTransaksi));

                    status = stPgsql2.executeUpdate();
                    //</editor-fold>

                    // Update komisi TMNStock True
                    //<editor-fold defaultstate="collapsed" desc="query komisi TrStock">
                    updateTMNStockTrue = "INSERT INTO \"TrStock\"("
                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", "
                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                            + " VALUES ('123456',now(),'True',?,?,1,'DEPOSIT',0,0,0,0);";

                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockTrue);
                    stPgsql2.setFloat(1, (int) Math.floor(True));
                    stPgsql2.setLong(2, Long.parseLong(idTrTransaksi));
                    status = stPgsql2.executeUpdate();
                    //</editor-fold>

                    // lastbalance
                    //<editor-fold defaultstate="collapsed" desc="query update last balance">
                    String topUPBalance = "UPDATE \"MsAgentAccount\" SET \"LastBalance\"= \"LastBalance\" + " + (int) Math.ceil(Agent) + "   WHERE \"id_agentaccount\" = '" + username + "';";
                    stPgsql2 = connPgsql2.prepareStatement(topUPBalance);
                    stPgsql2.executeUpdate();
                    //</editor-fold>

                    // Update komisi TMNStock Agent
                    String Lb = "";
                    //<editor-fold defaultstate="collapsed" desc="query get LastBalace">
                    getAmount = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\" WHERE  \"MsAgentAccount\".\"id_agentaccount\" = ?;";
                    stPgsql2 = connPgsql2.prepareStatement(getAmount);
                    stPgsql2.setString(1, username);
                    rsPgsql2 = stPgsql2.executeQuery();
                    while (rsPgsql2.next()) {
                        Lb = rsPgsql2.getString("LastBalance");
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query update TrStock">
                    updateTMNStockTrue = "INSERT INTO \"TrStock\"( \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\") VALUES (?,now(),'Agent', ?, ?, ?, 'DEPOSIT', 0, 0, 0, 0);";
                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockTrue);
                    stPgsql2.setLong(1, Long.parseLong(Lb));
                    stPgsql2.setFloat(2, (int) Math.ceil(Agent));
                    stPgsql2.setLong(3, Long.parseLong(idTrTransaksi));
                    stPgsql2.setString(4, username);

                    status = stPgsql2.executeUpdate();
                    //</editor-fold>

                    // update tempkomisi
                    //<editor-fold defaultstate="collapsed" desc="query update tempKomisi">
                    String updateTemp = "UPDATE \"TempKomisi\" SET \"Status\" = 'OK', \"StatusTransaksi\" = 'OK' WHERE \"id_trx\" = ?";
                    stPgsql2 = connPgsql2.prepareStatement(updateTemp);
                    stPgsql2.setString(1, idtrx);
                    stPgsql2.executeUpdate();
                    //</editor-fold>

                    // Update TMNStock Supplier
                    //<editor-fold defaultstate="collapsed" desc="query update trStock">
                    String updateTMNStockSupplier = "INSERT INTO \"TrStock\"("
                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\","
                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                            + " VALUES ('123456', now(), 'Supplier',?, ?, ?, 'WITHDRAW', 0, 0, 0, 0);";

                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockSupplier);
                    stPgsql2.setLong(1, Long.parseLong(String.valueOf(hargaBeli)));
                    stPgsql2.setLong(2, Long.parseLong(idTrTransaksi));
                    stPgsql2.setString(3, String.valueOf(idSupplier));

                    status = stPgsql2.executeUpdate();
                    //</editor-fold>

                    // hitung komisi dealer
                    String idDealer = "";
                    //<editor-fold defaultstate="collapsed" desc="query getIdDealerAccount">
                    String getID_memberaccount = "SELECT"
                            + "	\"MsDealer\".id_dealer_account"
                            + " FROM"
                            + "	\"MsDealer\""
                            + " JOIN \"MsAgentAccount\" ON \"MsDealer\".id_dealer = \"MsAgentAccount\".id_dealer"
                            + " JOIN \"MsAgent\" ON \"MsAgent\".id_agent = \"MsAgentAccount\".id_agent"
                            + " WHERE"
                            + "	\"MsAgentAccount\".id_agentaccount = ?"
                            + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer;";
                    stPgsql2 = connPgsql2.prepareStatement(getID_memberaccount);
                    stPgsql2.setString(1, username);
                    rsPgsql2 = stPgsql2.executeQuery();
                    while (rsPgsql2.next()) {
                        idDealer = rsPgsql2.getString("id_dealer_account");
                    }
                    if (idDealer == null && idDealer.isEmpty()) {
                        idDealer = "111111111111";
                    }

                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query update MsDealer">
                    sql = "SELECT * FROM \"MsDealer\" WHERE id_dealer_account = ? FOR UPDATE";
                    stPgsql2 = connPgsql2.prepareStatement(sql);
                    stPgsql2.setString(1, idDealer);
                    rsPgsql2 = stPgsql2.executeQuery();

                    String hitungkomisi = "UPDATE \"MsDealer\" SET \"Wallet\" = \"Wallet\" + " + (int) Math.ceil(Dealer) + " WHERE id_dealer_account = ?;";
                    stPgsql2 = connPgsql2.prepareStatement(hitungkomisi);
                    stPgsql2.setString(1, idDealer);
                    status = stPgsql2.executeUpdate();
                    //</editor-fold>

                    // Update TMNStock Dealer
                    idDealer = "0";
                    //<editor-fold defaultstate="collapsed" desc="query getIdDealer">
                    sql = "SELECT"
                            + "	\"MsAgentAccount\".id_dealer"
                            + " FROM"
                            + "	\"MsAgentAccount\""
                            + " JOIN \"MsAgent\" ON \"MsAgentAccount\".id_agent = \"MsAgent\".id_agent"
                            + " WHERE"
                            + "	\"MsAgentAccount\".id_agentaccount = ?"
                            + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer";

                    stPgsql2 = connPgsql2.prepareStatement(sql);
                    stPgsql2.setString(1, username);
                    rsPgsql2 = stPgsql2.executeQuery();
                    while (rsPgsql2.next()) {
                        idDealer = rsPgsql2.getString("id_dealer");
                    }
                    //</editor-fold>
                    String lastBalanceDealer = "0";
                    //<editor-fold defaultstate="collapsed" desc="query getLastBalanceDealer">
                    sql = "SELECT \"MsDealer\".\"Wallet\" FROM \"MsDealer\""
                            + " WHERE id_dealer_account = ?";

                    stPgsql2 = connPgsql2.prepareStatement(sql);
                    stPgsql2.setString(1, idDealer);
                    rsPgsql2 = stPgsql2.executeQuery();
                    while (rsPgsql2.next()) {
                        lastBalanceDealer = rsPgsql2.getString("Wallet");
                    }
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query update trStock dealer">
                    String updateTMNStockDealer = "INSERT INTO \"TrStock\"("
                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\","
                            + " id_transaksi, id_stock, \"TypeTrx\", \"Dealer\", \"True\", \"Agent\", \"Member\")"
                            + " VALUES (?, now(), 'Dealer',?, ?, ?, 'DEPOSIT', 0, 0, 0, 0);";

                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockDealer);
                    stPgsql2.setLong(1, Long.parseLong(lastBalanceDealer));
                    stPgsql2.setFloat(2, (int) Math.ceil(Dealer));
                    stPgsql2.setLong(3, Long.parseLong(idTrTransaksi));
                    stPgsql2.setString(4, idDealer);

                    status = stPgsql2.executeUpdate();
                    //</editor-fold>

                    jsonResp.put("ACK", "OK");
                    jsonResp.put("id_Transaksi", idtrx);
                    jsonResp.put("pesan", keterangan + "\nVoucher : \n" + voucher);
                    jsonResp.put("id_Operator", idOperator);
                    jsonResp.put("denom", nominal);
                    jsonResp.put("timestamp", utilities.getDate().toString());
                    jsonResp.put("statusTRX", "SUKSES");

                    String pesan = "Transaksi pembelian iflix Anda berhasil dengan voucher " + voucher
                            + ". Nomor transaksi " + idtrx + ".\n Voucher : \n" + voucher;

                    //insert notif
                    //<editor-fold defaultstate="collapsed" desc="query insert notif">
                    sql = "INSERT INTO \"Notifikasi\" "
                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                            + " VALUES (?, ?, ?,'FALSE',now())";
                    stPgsql2 = connPgsql2.prepareStatement(sql);
                    stPgsql2.setString(1, pesan);
                    stPgsql2.setString(2, username);
                    stPgsql2.setString(3, idtrx);

                    stPgsql2.executeUpdate();
                    //</editor-fold>

                    //send sms
                    String smsScript = "Voucher Iflix %s ke %s BERHASIL, SN: %s, IDTRX: %s. Terimakasih.";
                    if (smsScript != null) {
                        pesan = String.format(smsScript, nominal, nohp, voucher, idtrx);
                        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                        new SmsHelper().sent(uuid, nohp, pesan, "PAY_IFLIX", entityConfig);
                    }

                    String a[] = new String[4];
                    //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgent">
                    query = "SELECT  \"MsAccountStatus\".\"NamaAccountStatus\","
                            + "  \"MsAgentAccount\".\"StatusKartu\",\"MsAgentAccount\".\"PIN\","
                            + "    \"MsAgentAccount\".\"LastBalance\" FROM   public.\"MsAgentAccount\", "
                            + "  public.\"MsAccountStatus\" WHERE "
                            + "  \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus AND"
                            + "  \"MsAgentAccount\".\"id_agentaccount\" = ?;";
                    stPgsql = connPgsql.prepareStatement(query);
                    stPgsql.setString(1, username);
                    rsPgsql = stPgsql.executeQuery();
                    while (rsPgsql.next()) {
                        a[0] = rsPgsql.getString("NamaAccountStatus");
                        a[1] = rsPgsql.getString("LastBalance");
                        a[2] = rsPgsql.getString("StatusKartu");
                        a[3] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
                    }
                    //</editor-fold>
                    // Update Status Account
                    if (a[0].equals("INACTIVE") || a[0].equals("DORMANT")) {
                        SQL = "UPDATE \"MsAgentAccount\" SET id_accountstatus=1 WHERE \"id_agentaccount\"=?;";
                        stPgsql2 = connPgsql2.prepareStatement(SQL);
                        stPgsql2.setString(1, username);
                        stPgsql2.executeUpdate();
                    }
                } else {
                    //<editor-fold defaultstate="collapsed" desc="query update trtransaksi">
                    String updateTransaksi = "UPDATE \"TrTransaksi\" "
                            + " SET \"StatusTRX\"='GAGAL', timestamprefund = now(), "
                            + " \"StatusDeduct\"='NOK', \"StatusRefund\"='OK', \"StatusKomisi\"='NOK' WHERE id_trx=?;";
                    stPgsql2 = connPgsql2.prepareStatement(updateTransaksi);
                    stPgsql2.setString(1, idtrx);
                    hasil = stPgsql2.executeUpdate();
                    //</editor-fold>

                    //refund agent
                    //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                    String sql1 = "SELECT * FROM \"MsAgentAccount\""
                            + " WHERE id_agentaccount = ? FOR UPDATE";
                    stPgsql2 = connPgsql2.prepareStatement(sql1);
                    stPgsql2.setString(1, username);

                    rsPgsql2 = stPgsql2.executeQuery();

                    sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + hargaCetak + " "
                            + "WHERE id_agentaccount = ?";
                    stPgsql2 = connPgsql2.prepareStatement(sql);
                    stPgsql2.setString(1, username);
                    hasil = stPgsql2.executeUpdate();
                    //</editor-fold>

                    // Update TMNStock Agent refund
                    lb = "";
                    //<editor-fold defaultstate="collapsed" desc="query getLastBalance">
                    sql = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\" WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";
                    stPgsql2 = connPgsql2.prepareStatement(sql);
                    stPgsql2.setString(1, username);
                    rsPgsql2 = stPgsql2.executeQuery();
                    while (rsPgsql2.next()) {
                        lb = rsPgsql2.getString("LastBalance");
                    }

                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query update trStock refund">
                    updateTMNStock = "INSERT INTO \"TrStock\"("
                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", "
                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                            + " VALUES (?, now(), 'Agent', ?, ?, ?, 'REFUND', 0, 0, 0, 0);";
                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStock);
                    stPgsql2.setLong(1, Long.parseLong(lb));
                    stPgsql2.setLong(2, Long.parseLong(hargaCetak));
                    stPgsql2.setLong(3, Long.parseLong(idTrTransaksi));
                    stPgsql2.setString(4, username);

                    stPgsql2.executeUpdate();
                    //</editor-fold>

                    jsonResp.put("id_Transaksi", idtrx);
                    jsonResp.put("statusTRX", "GAGAL");
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", Jresponse.get("pesan").getAsString());

                    String pesan = "Transaksi pembelian token listrik Anda gagal. Nomor transaksi " + idtrx + ".";

                    //insert notif
                    //<editor-fold defaultstate="collapsed" desc="query insert notif">
                    sql = "INSERT INTO \"Notifikasi\" "
                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                            + " VALUES (?, ?, ?,'FALSE',now())";
                    stPgsql2 = connPgsql2.prepareStatement(sql);
                    stPgsql2.setString(1, pesan);
                    stPgsql2.setString(2, username);
                    stPgsql2.setString(3, idtrx);
                    stPgsql2.executeUpdate();
                    //</editor-fold>

                    //send sms
                    String smsScript = "Voucher Iflix %s ke %s GAGAL, IDTRX: %s. Terimakasih.";
                    if (smsScript != null) {
                        pesan = String.format(smsScript, nominal, nohp, idtrx);
                        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                        new SmsHelper().sent(uuid, nohp, pesan, "PAY_IFLIX", entityConfig);
                    }
                }

                connPgsql2.commit();
            }
        } catch (Exception ex) {
            try {
                connPgsql.rollback();
                connPgsql2.rollback();
            } catch (SQLException e) {
                String s = Throwables.getStackTraceAsString(e);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
                if (rsPgsql2 != null) {
                    rsPgsql2.close();
                }
                if (stPgsql2 != null) {
                    stPgsql2.close();
                }
                if (connPgsql2 != null) {
                    connPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jsonResp;
    }


}
