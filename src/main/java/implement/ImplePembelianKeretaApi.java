/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.SendEmailKereta;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcPembayaranKomunitas;
import interfc.InterfcPembelianKeretaApi;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Hasan
 */
public class ImplePembelianKeretaApi implements InterfcPembelianKeretaApi {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembelianKeretaApi(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getStasiun(JSONObject objJson) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("request", "getstation");

        //insert log
        int idLog = utilities.insertLog(objJson.get("idTrx").toString(), jsonObject);
        String url = hlp.getParamMsParameter("url_padicity") + "GetStation";
        JSONObject jObjResp = this.utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, jObjResp.toString());
        if (jObjResp.containsKey("respCode")) {
            if (jObjResp.get("respCode").toString().equals("0")) {
                jObjResp.put("ACK", "OK");
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal mengambil list stasiun");
            }
        } else {
            jObjResp.put("ACK", "NOK");
            jObjResp.put("pesan", "gagal mengambil list stasiun");
        }
        return jObjResp.toString();
    }

    @Override
    public String getSchedule(JSONObject objJson) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("trnType", objJson.get("trnType").toString());
        jsonObject.put("org", objJson.get("org").toString());
        jsonObject.put("des", objJson.get("des").toString());
        jsonObject.put("depDate", objJson.get("depDate").toString());
        jsonObject.put("returnDate", objJson.get("returnDate").toString());

        //insert log
        int idLog = utilities.insertLog(objJson.get("idTrx").toString(), jsonObject);
        String url = hlp.getParamMsParameter("url_padicity") + "GetInquirySchedule";
        JSONObject jObjResp = this.utilities.curlPost(url, jsonObject.toString(), false);
        //updateLog
        utilities.updateLog(idLog, jObjResp.toString());
        //System.out.println("asli : " + jObjResp);
        if (jObjResp.containsKey("respCode")) {
            if (jObjResp.get("respCode").toString().equals("0")) {
                //rebuild json response
                JSONObject objJsonRecreate = new JSONObject();
                for (Iterator iterator = jObjResp.keySet().iterator(); iterator.hasNext(); ) {
                    String key = (String) iterator.next();
                    if (!key.equalsIgnoreCase("departureList") && !key.equalsIgnoreCase("returnList")) {
                        objJsonRecreate.put(key, jObjResp.get(key));
                    } else if (key.equalsIgnoreCase("departureList")) {
                        JSONObject jObjDepList = (JSONObject) jObjResp.get(key);
                        //System.out.println("1 : " + jObjDepList.toString());
                        JSONObject newJObjDepList = new JSONObject();
                        String destStationCode = jObjDepList.get("destStationCode").toString();
                        String orgStationCode = jObjDepList.get("orgStationCode").toString();
                        String depDate = jObjDepList.get("depDate").toString();

                        newJObjDepList.put("destStationCode", destStationCode);
                        newJObjDepList.put("orgStationCode", orgStationCode);
                        newJObjDepList.put("depDate", depDate);

                        JSONArray jArrTrainSchedule = (JSONArray) jObjDepList.get("trainSchedule");
                        JSONArray newJArrTrainSchedule = new JSONArray();
                        //System.out.println("2 : " + jArrTrainSchedule.toString());
                        int k = 0;
                        for (int i = jArrTrainSchedule.size() - 1; i >= 0; i--) {
                            JSONObject jObjTrainSchedule = (JSONObject) jArrTrainSchedule.get(i);
                            String arrvDate = jObjTrainSchedule.get("arrvDate").toString();
                            String trainNm = jObjTrainSchedule.get("trainNm").toString();
                            String arrvTime = jObjTrainSchedule.get("arrvTime").toString();
                            String depTime = jObjTrainSchedule.get("depTime").toString();
                            String trainNo = jObjTrainSchedule.get("trainNo").toString();
                            String depDateTrain = jObjTrainSchedule.get("depDate").toString();
                            JSONArray trainClassArr = (JSONArray) jObjTrainSchedule.get("trainClassLst");

                            for (int j = trainClassArr.size() - 1; j >= 0; j--) {
                                JSONObject trainClassObj = (JSONObject) trainClassArr.get(j);
                                String infantFare = trainClassObj.get("infantFare").toString();
                                String trainSubClass = trainClassObj.get("trainSubClass").toString();

                                double dbl = Double.parseDouble(trainClassObj.get("adultFare").toString());
                                String dblStr = String.format("%,.0f", dbl);
                                dblStr = dblStr.replaceAll(",", ".");
                                String adultFare = dblStr;

                                String trainClass = trainClassObj.get("trainClass").toString();
                                String trainNmNew = "";
                                if (trainClass.equalsIgnoreCase("k")) {
                                    trainNmNew = trainNm + " \nEkonomi (" + trainSubClass + ")";
                                } else if (trainClass.equalsIgnoreCase("b")) {
                                    trainNmNew = trainNm + " \nBisnis (" + trainSubClass + ")";
                                } else if (trainClass.equalsIgnoreCase("e")) {
                                    trainNmNew = trainNm + " \nEksekutif (" + trainSubClass + ")";
                                }
                                String trainClassId = trainClassObj.get("trainClassId").toString();
                                String avb = trainClassObj.get("avb").toString();

                                //get time travel
                                String diffTime = utilities.calculateDiffDate(depDate + depTime, arrvDate + arrvTime, "yyyyMMddHHmm");
                                //get time different between timenow and departure time
                                String timeNowString = new SimpleDateFormat("yyyyMMddHHmm").format(Calendar.getInstance().getTime());
                                long difference = utilities.calculateDiffDateOnMiliSecond(timeNowString, depDate + depTime, "yyyyMMddHHmm");
                                long hourDiffTimeNow = TimeUnit.MILLISECONDS.toHours(difference);

                                JSONObject newObjTrainScedule = new JSONObject();
                                newObjTrainScedule.put("arrvDate", arrvDate);
                                newObjTrainScedule.put("trainNm", trainNmNew);
                                newObjTrainScedule.put("arrvTime", arrvTime.substring(0, 2) + ":" + arrvTime.substring(2, 4));
                                newObjTrainScedule.put("depTime", depTime.substring(0, 2) + ":" + depTime.substring(2, 4));
                                newObjTrainScedule.put("travelTime", diffTime);
                                newObjTrainScedule.put("trainNo", trainNo);
                                newObjTrainScedule.put("depDate", depDateTrain);
                                newObjTrainScedule.put("hourDiffTimeNow", String.valueOf(hourDiffTimeNow));

                                newObjTrainScedule.put("infantFare", infantFare);
                                newObjTrainScedule.put("trainSubClass", trainSubClass);
                                newObjTrainScedule.put("adultFare", adultFare);
                                newObjTrainScedule.put("trainClass", trainClass);
                                newObjTrainScedule.put("trainClassId", trainClassId);
                                newObjTrainScedule.put("avb", avb);
                                newObjTrainScedule.put("orgStationCode", orgStationCode);
                                newObjTrainScedule.put("destStationCode", destStationCode);

                                newJArrTrainSchedule.add(k, newObjTrainScedule);
                                k = k++;
                            }
                        }
                        newJObjDepList.put("trainSchedule", newJArrTrainSchedule);
                        //System.out.println("3 : " + newJObjDepList.toString());
                        objJsonRecreate.put("departureList", newJObjDepList);
                        //System.out.println("4 : " + objJsonRecreate.toString());
                    } else if (key.equalsIgnoreCase("returnList")) {
                        JSONObject jObjRtnList = (JSONObject) jObjResp.get(key);
                        //System.out.println("1 : " + jObjDepList.toString());
                        JSONObject newJObjRtnList = new JSONObject();
                        String destStationCode = jObjRtnList.get("destStationCode").toString();
                        String orgStationCode = jObjRtnList.get("orgStationCode").toString();
                        String depDate = jObjRtnList.get("depDate").toString();

                        newJObjRtnList.put("destStationCode", destStationCode);
                        newJObjRtnList.put("orgStationCode", orgStationCode);
                        newJObjRtnList.put("depDate", depDate);

                        JSONArray jArrTrainSchedule = (JSONArray) jObjRtnList.get("trainSchedule");
                        JSONArray newJArrTrainSchedule = new JSONArray();
                        //System.out.println("2 : " + jArrTrainSchedule.toString());
                        int k = 0;
                        for (int i = jArrTrainSchedule.size() - 1; i >= 0; i--) {
                            JSONObject jObjTrainSchedule = (JSONObject) jArrTrainSchedule.get(i);
                            String arrvDate = jObjTrainSchedule.get("arrvDate").toString();
                            String trainNm = jObjTrainSchedule.get("trainNm").toString();
                            String arrvTime = jObjTrainSchedule.get("arrvTime").toString();
                            String depTime = jObjTrainSchedule.get("depTime").toString();
                            String trainNo = jObjTrainSchedule.get("trainNo").toString();
                            String depDateTrain = jObjTrainSchedule.get("depDate").toString();
                            JSONArray trainClassArr = (JSONArray) jObjTrainSchedule.get("trainClassLst");

                            for (int j = trainClassArr.size() - 1; j >= 0; j--) {
                                JSONObject trainClassObj = (JSONObject) trainClassArr.get(j);
                                String infantFare = trainClassObj.get("infantFare").toString();
                                String trainSubClass = trainClassObj.get("trainSubClass").toString();

                                double dbl = Double.parseDouble(trainClassObj.get("adultFare").toString());
                                String dblStr = String.format("%,.0f", dbl);
                                dblStr = dblStr.replaceAll(",", ".");
                                String adultFare = dblStr;

                                String trainClass = trainClassObj.get("trainClass").toString();
                                String trainNmNew = "";
                                if (trainClass.equalsIgnoreCase("k")) {
                                    trainNmNew = trainNm + " \nEkonomi (" + trainSubClass + ")";
                                } else if (trainClass.equalsIgnoreCase("b")) {
                                    trainNmNew = trainNm + " \nBisnis (" + trainSubClass + ")";
                                } else if (trainClass.equalsIgnoreCase("e")) {
                                    trainNmNew = trainNm + " \nEksekutif (" + trainSubClass + ")";
                                }
                                String trainClassId = trainClassObj.get("trainClassId").toString();
                                String avb = trainClassObj.get("avb").toString();

                                //get time travel
                                String diffTime = utilities.calculateDiffDate(depDate + depTime, arrvDate + arrvTime, "yyyyMMddHHmm");

                                JSONObject newObjTrainScedule = new JSONObject();
                                newObjTrainScedule.put("arrvDate", arrvDate);
                                newObjTrainScedule.put("trainNm", trainNmNew);
                                newObjTrainScedule.put("arrvTime", arrvTime.substring(0, 2) + ":" + arrvTime.substring(2, 4));
                                newObjTrainScedule.put("depTime", depTime.substring(0, 2) + ":" + depTime.substring(2, 4));
                                newObjTrainScedule.put("travelTime", diffTime);
                                newObjTrainScedule.put("trainNo", trainNo);
                                newObjTrainScedule.put("depDate", depDateTrain);

                                newObjTrainScedule.put("infantFare", infantFare);
                                newObjTrainScedule.put("trainSubClass", trainSubClass);
                                newObjTrainScedule.put("adultFare", adultFare);
                                newObjTrainScedule.put("trainClass", trainClass);
                                newObjTrainScedule.put("trainClassId", trainClassId);
                                newObjTrainScedule.put("avb", avb);
                                newObjTrainScedule.put("orgStationCode", orgStationCode);
                                newObjTrainScedule.put("destStationCode", destStationCode);

                                newJArrTrainSchedule.add(k, newObjTrainScedule);
                                k = k++;
                            }
                        }
                        newJObjRtnList.put("trainSchedule", newJArrTrainSchedule);
                        //System.out.println("3 : " + newJObjDepList.toString());
                        objJsonRecreate.put("returnList", newJObjRtnList);
                        //System.out.println("4 : " + objJsonRecreate.toString());
                    }
                }
                jObjResp = objJsonRecreate;
                jObjResp.put("ACK", "OK");
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "tidak ada jadwal kereta atau gagal mengambil jadwal kereta");
            }
        } else {
            jObjResp.put("ACK", "NOK");
            jObjResp.put("pesan", "tidak ada jadwal kereta atau gagal mengambil jadwal kereta");
        }
        return jObjResp.toString();
    }

    @Override
    public String getInq(JSONObject jsonReq) {
        JSONObject jObjResp = new JSONObject();
        jObjResp.put("ACK", "NOK");
        jObjResp.put("pesan", "Gagal inquiry");
        //<editor-fold defaultstate="collapsed" desc="query getComission">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsFeeSupplier\" a, \"MsFee\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_operator = ?"
                    + " and b.id_tipeaplikasi = '3'"
                    + " and a.\"FlagActive\" = true"
                    + " and b.\"FlagActive\" = true";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, 225);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                String jmlPenumpang = jsonReq.get("paxAdult").toString();
                int jumlahPenumpang = Integer.parseInt(jmlPenumpang);
                int biayaAdmin = jumlahPenumpang * rsPgsql.getInt("BiayaAdmin");
                int komisiAgen = jumlahPenumpang * rsPgsql.getInt("AgentCommNom");
                int komisiTrue = jumlahPenumpang * rsPgsql.getInt("TrueFeeNom");

                jObjResp.put("BiayaAdmin", biayaAdmin);
                jObjResp.put("AgentCommNom", komisiAgen);
                jObjResp.put("TrueFeeNom", komisiTrue);
                jObjResp.put("ACK", "OK");
                jObjResp.put("pesan", "Sukses inquiry");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        //bypass get komisi
        jObjResp.put("BiayaAdmin", "0");
        jObjResp.put("AgentCommNom", "0");
        jObjResp.put("TrueFeeNom", "0");
        jObjResp.put("ACK", "OK");
        jObjResp.put("pesan", "Sukses inquiry");
        //hit train gateway
        int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonReq);
        String url = hlp.getParamMsParameter("url_padicity") + "getFareScheduleSelected";
        JSONObject jObjRespCurl = this.utilities.curlPost(url, jsonReq.toString(), false);
        //updateLog
        utilities.updateLog(idLog, jObjRespCurl.toString());
        if (jObjRespCurl.containsKey("respCode")) {
            if (jObjRespCurl.get("respCode").toString().equals("0")) {
                jObjResp.put("gwResponseFareSchedule", jObjRespCurl);
                //execute booking
                //hit train gateway
                idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonReq);
                url = hlp.getParamMsParameter("url_padicity") + "ExecuteBooking";
                JSONObject jObjBookRespCurl = this.utilities.curlPost(url, jsonReq.toString(), false);
                utilities.updateLog(idLog, jObjBookRespCurl.toString());
                if (jObjBookRespCurl.containsKey("respCode")) {
                    if (jObjBookRespCurl.get("respCode").toString().equals("0")) {
                        jObjResp.put("gwResponseBooking", jObjBookRespCurl);
                    } else {
                        jObjResp.put("ACK", jObjRespCurl.get("ACK").toString());
                        jObjResp.put("pesan", jObjRespCurl.get("pesan").toString());
                        return jObjResp.toString();
                    }
                }
            } else {
                jObjResp.put("ACK", jObjRespCurl.get("ACK").toString());
                jObjResp.put("pesan", jObjRespCurl.get("pesan").toString());
                return jObjResp.toString();
            }
        }
        return jObjResp.toString();
    }

    @Override
    public String setManualSeat(JSONObject jsonReq) {
        JSONObject jObjResp = new JSONObject();
        jObjResp.put("ACK", "NOK");
        jObjResp.put("pesan", "Gagal manual set seat");

        JSONObject jObjDataBooking = (JSONObject) jsonReq.get("dataBooking");
        String suppTrxId = jObjDataBooking.get("suppTrxId").toString();
        String bookCodeDepart = jObjDataBooking.get("departureBookCode").toString();
        String bookCodeReturn = jObjDataBooking.get("returnBookCode").toString();

        //execute manualSeatPergi
        if (jsonReq.containsKey("manualSeatPergi")) {
            JSONObject jsonReqSeatPergi = (JSONObject) jsonReq.get("manualSeatPergi");
            if(jsonReqSeatPergi.containsKey("seatLst")) {
                jsonReqSeatPergi.put("trxId", suppTrxId);
                jsonReqSeatPergi.put("bookCode", bookCodeDepart);
                int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonReqSeatPergi);
                String url = hlp.getParamMsParameter("url_padicity") + "ExecuteManualSeat2";
                JSONObject jObjSeatPergiRespCurl = this.utilities.curlPost(url, jsonReqSeatPergi.toString(), false);
                utilities.updateLog(idLog, jObjSeatPergiRespCurl.toString());
                if (jObjSeatPergiRespCurl.containsKey("respCode")) {
                    if (jObjSeatPergiRespCurl.get("respCode").toString().equals("0")) {
                        jObjResp.put("ACK", "OK");
                        jObjResp.put("pesan", "Berhasil manual set seat");
                        jObjResp.put("manualSeatPergiResponse", jObjSeatPergiRespCurl);
                    } else {
                        jObjResp.put("ACK", jObjSeatPergiRespCurl.get("ACK").toString());
                        jObjResp.put("pesan", jObjSeatPergiRespCurl.get("pesan").toString());
                        return jObjResp.toString();
                    }
                }
            }
        }
        //execute manualSeatPulang
        if (jsonReq.containsKey("manualSeatPulang")) {
            JSONObject jsonReqSeatPulang = (JSONObject) jsonReq.get("manualSeatPulang");
            if(jsonReqSeatPulang.containsKey("seatLst")) {
                jsonReqSeatPulang.put("trxId", suppTrxId);
                jsonReqSeatPulang.put("bookCode", bookCodeReturn);
                int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jsonReqSeatPulang);
                String url = hlp.getParamMsParameter("url_padicity") + "ExecuteManualSeat2";
                JSONObject jObjSeatPulangRespCurl = this.utilities.curlPost(url, jsonReqSeatPulang.toString(), false);
                utilities.updateLog(idLog, jObjSeatPulangRespCurl.toString());
                if (jObjSeatPulangRespCurl.containsKey("respCode")) {
                    if (jObjSeatPulangRespCurl.get("respCode").toString().equals("0")) {
                        jObjResp.put("ACK", "OK");
                        jObjResp.put("pesan", "Berhasil manual set seat");
                        jObjResp.put("manualSeatPulangResponse", jObjSeatPulangRespCurl);
                    } else {
                        jObjResp.put("ACK", jObjSeatPulangRespCurl.get("ACK").toString());
                        jObjResp.put("pesan", jObjSeatPulangRespCurl.get("pesan").toString());
                        return jObjResp.toString();
                    }
                }
            }
        }
        return jObjResp.toString();
    }

    @Override
    public String getCancelBook(JSONObject jsonReq) {
        JSONObject jObjResp = new JSONObject();
        jObjResp.put("ACK", "NOK");
        jObjResp.put("pesan", "Gagal cancel book");

        //hit train gateway
        int idLog = utilities.insertLog(jsonReq.get("trxId").toString(), jsonReq);
        String url = hlp.getParamMsParameter("url_padicity") + "ExecuteCancelBook";
        JSONObject jObjRespCurl = this.utilities.curlPost(url, jsonReq.toString(), false);
        //updateLog
        utilities.updateLog(idLog, jObjRespCurl.toString());
        if (jObjRespCurl.containsKey("respCode")) {
            if (jObjRespCurl.get("respCode").toString().equals("0")) {
                jObjResp.put("ACK", "OK");
                jObjResp.put("pesan", "Sukses cancel book");
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "Gagal cancel book");
            }
        }
        return jObjResp.toString();
    }

    @Override
    public String getPay(final JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, String[] com) {
        //<editor-fold defaultstate="collapsed" desc="query prosesBayar">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql2 = new DatabaseUtilitiesPgsql();
        Connection connPgsql2 = null;

        String idTipeAplikasi = "3";
        String idTipeTransaksi = "67";
        String idOperator = "225";
        String username = jsonReq.get("username").toString();
        String idtrx = jsonReq.get("tmnIdTrx").toString();
        String pin = jsonReq.get("PIN").toString();
        String hargaCetak = jsonReq.get("totalBayar").toString();
        String totalBiaya = jsonReq.get("totalBiaya").toString();
        String totalDiskon = jsonReq.get("totalDiskon").toString();
        String totalAdminSupplier = jsonReq.get("totalAdmin").toString();
        String noHp = jsonReq.get("noHp").toString();
        String jmlDewasa = jsonReq.get("jmlDewasa").toString();
        String jmlAnak = jsonReq.get("jmlAnak").toString();
        String trnType = jsonReq.get("trnType").toString();
        int saldo = Integer.parseInt(rekeningAgentNonEDC[1]);
        Long nominal = 0L;
        Long totalBiayaAdmin = 0L;
        int komisiPp = 1;

        String stattrx = "";
        String strstatdeduct;
        String productcode = null;
        int idSupplier = 0;
        int idFeeSupplier = 0;
        String id_transaksi = null;

        try {
            //money movement
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);
            //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
            String sql = "select b.id_supplier, b.id_feesupplier"
                    + " from \"MsFee\" a, \"MsFeeSupplier\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_tipeaplikasi = ?"
                    + " and a.id_tipetransaksi = ?"
                    + " and b.id_operator = ?"
                    + " and a.\"FlagActive\" = TRUE"
                    + " and b.\"FlagActive\" = TRUE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(idTipeAplikasi));
            stPgsql.setLong(2, Long.parseLong(idTipeTransaksi));
            stPgsql.setLong(3, Long.parseLong(idOperator));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idSupplier = rsPgsql.getInt("id_supplier");
                idFeeSupplier = rsPgsql.getInt("id_feesupplier");
            }
            //</editor-fold>
            if (idSupplier == 0) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, supplier tidak tersedia");
                return jsonResp.toString();
            }

            //<editor-fold defaultstate="collapsed" desc="query getProductCode">
            String query = "SELECT \"ProductCode\" " + "FROM \"MsFeeSupplier\" " + "WHERE id_operator = ?"
                    + " AND \"FlagActive\" = 'TRUE'"
                    + " AND id_supplier = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(idOperator));
            stPgsql.setLong(2, idSupplier);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                productcode = rsPgsql.getString("ProductCode");
            }
            //</editor-fold>
            if (productcode == null && productcode.isEmpty()) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                return jsonResp.toString();
            }

            // deduct agent
            //<editor-fold defaultstate="collapsed" desc="query deduct agent">
            sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            long lastBalance = 0;
            while (rsPgsql.next()) {
                lastBalance = rsPgsql.getLong("LastBalance");
            }
            if (lastBalance < Long.parseLong(hargaCetak)) {
                strstatdeduct = "NOK";
            } else {
                //check negative number
                if (Integer.signum(Integer.parseInt(hargaCetak)) > 0) {
                    String deductBalance = "UPDATE \"MsAgentAccount\""
                            + " SET \"LastBalance\"= \"LastBalance\" - ? WHERE \"id_agentaccount\"=?;";
                    stPgsql = connPgsql.prepareStatement(deductBalance);
                    stPgsql.setLong(1, Long.parseLong(hargaCetak));
                    stPgsql.setString(2, username);
                    int status = stPgsql.executeUpdate();
                    if (status == 1) {
                        strstatdeduct = "OK";
                    } else {
                        strstatdeduct = "NOK";
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Transaksi gagal");
                    return jsonResp.toString();
                }
            }
            //</editor-fold>
            if (strstatdeduct.equalsIgnoreCase("OK")) {
                String id_dealer = "0";
                //<editor-fold defaultstate="collapsed" desc="query getIdDealer">
                sql = "SELECT \"MsAgentAccount\".id_dealer"
                        + " FROM \"MsAgentAccount\""
                        + " JOIN \"MsAgent\" ON \"MsAgentAccount\".id_agent = \"MsAgent\".id_agent"
                        + " WHERE \"MsAgentAccount\".id_agentaccount = ?"
                        + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, username);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    id_dealer = rsPgsql.getString("id_dealer");
                }

                String hargaBeli = null;
                String biayaAdmin = null;
                //<editor-fold defaultstate="collapsed" desc="query getHargaBeli">
                sql = "SELECT id_supplier, \"ProductCode\", \"HargaBeli\", \"HargaJual\", \"HargaCetak\", \"HargaCetakMember\", \"BiayaAdmin\""
                        + " FROM \"MsFeeSupplier\"b INNER JOIN \"MsFee\"d ON b.id_fee = d.id_fee"
                        + " WHERE d.id_tipeaplikasi = 3"
                        + " AND d.id_tipetransaksi = ?"
                        + " AND id_operator = ?"
                        + " AND id_denom = '210'"
                        + " AND b.\"FlagActive\" = TRUE"
                        + " AND d.\"FlagActive\" = TRUE";
                //System.err.println(sql);
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setLong(1, Long.parseLong(idTipeTransaksi));
                stPgsql.setLong(2, Long.parseLong(idOperator));
                rsPgsql = stPgsql.executeQuery();

                komisiPp = 1;
                if (!trnType.equals("one-way")) {
                    komisiPp = 2;
                }
                while (rsPgsql.next()) {
                    hargaBeli = rsPgsql.getString("HargaBeli");
                    biayaAdmin = rsPgsql.getString("BiayaAdmin");
                    totalBiayaAdmin = Long.parseLong(biayaAdmin) * Long.parseLong(jmlDewasa) * komisiPp;
                }
                //</editor-fold>

                //</editor-fold>
//                String Keterangan = "Pembayaran Kereta Api " + noHp + " sebesar Rp." + hargaCetak + "\n"
//                        + " Stasiun Pergi : " + jsonReq.get("stasiunPergi").toString() + "\n"
//                        + " Kereta Pergi: " + jsonReq.get("keretaPergi").toString() + "\n"
//                        + " Seat Pergi: " + jsonReq.get("seatPergi").toString() + "\n"
//                        + " jam Pergi: " + jsonReq.get("jamPergi").toString() + "\n"
//                        + " Stasiun Pulang : " + jsonReq.get("stasiunPulang").toString() + "\n"
//                        + " Kereta Pulang: " + jsonReq.get("keretaPulang").toString() + "\n"
//                        + " Seat Pulang: " + jsonReq.get("seatPulang").toString() + "\n"
//                        + " jam Pulang: " + jsonReq.get("jamPulang").toString();
                String Keterangan = "Pembayaran Kereta Api " + noHp + " sebesar Rp." + hargaCetak;
                nominal = Long.parseLong(hargaCetak) - totalBiayaAdmin;
                //<editor-fold defaultstate="collapsed" desc="query updateTrTransaksi">
                String updateTransaksi = "INSERT INTO \"TrTransaksi\""
                        + " ( id_tipetransaksi, id_agentaccount, id_pelanggan, id_supplier, \"Nominal\", \"StatusTRX\", \"StatusDeduct\", \"StatusRefund\", \"TypeTRX\", \"OpsVerifikasi\", \"Total\", id_trx, \"StatusKomisi\", \"TimeStamp\", \"Keterangan\", id_tipeaplikasi, id_operator, \"Biaya\", id_dealer, id_feesupplier)"
                        + " VALUES (?,?,?,?,?,'OPEN',?,'OPEN','WITHDRAW','PIN',?,?,'OPEN',now(),?,3,?,?,?,?)";

                stPgsql = connPgsql.prepareStatement(updateTransaksi, Statement.RETURN_GENERATED_KEYS);
                stPgsql.setLong(1, Long.parseLong(idTipeTransaksi));
                stPgsql.setString(2, username);
                stPgsql.setString(3, noHp);
                stPgsql.setLong(4, idSupplier);
                stPgsql.setString(5, String.valueOf(nominal));
                stPgsql.setString(6, strstatdeduct);
                stPgsql.setLong(7, Long.parseLong(hargaCetak));
                stPgsql.setString(8, idtrx);
                stPgsql.setString(9, Keterangan);
                stPgsql.setLong(10, Long.parseLong(idOperator));
                stPgsql.setString(11, biayaAdmin);
                stPgsql.setLong(12, Long.parseLong(id_dealer));
                stPgsql.setLong(13, idFeeSupplier);
                int hasil = stPgsql.executeUpdate();
                id_transaksi = null;
                if (hasil > 0) {
                    rsPgsql = stPgsql.getGeneratedKeys();
                    while (rsPgsql.next()) {
                        id_transaksi = rsPgsql.getString(1);
                    }
                }
                //</editor-fold>
                int status = 0;
                // UpdateTMNStock agent deduct
                //<editor-fold defaultstate="collapsed" desc="query TrStock">
                String sqlInsertMsTMNStock = "INSERT"
                        + " INTO \"TrStock\" (\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                        + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";
                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                stPgsql.setLong(1, (saldo - Integer.parseInt(hargaCetak)));
                stPgsql.setLong(2, Long.parseLong(hargaCetak));
                stPgsql.setString(3, username);
                stPgsql.setLong(4, Long.parseLong(id_transaksi));
                status = stPgsql.executeUpdate();
                //</editor-fold>
            }
            connPgsql.commit();

            connPgsql2 = databaseUtilitiesPgsql2.getConnection(this.entityConfig);
            connPgsql2.setAutoCommit(false);
            if (strstatdeduct.equals("OK")) {
                //hit train gateway
                int idLog = utilities.insertLog(jsonReq.get("trxId").toString(), jsonReq);
                String url = hlp.getParamMsParameter("url_padicity") + "ExecuteIssuedTicket";
                JSONObject jsonRespCurl = this.utilities.curlPost(url, jsonReq.toString(), false);
                //updateLog
                utilities.updateLog(idLog, jsonRespCurl.toString());
                //simulate refund on timeout
                //jsonRespCurl.put("ResponseCode", "500");
                if (jsonRespCurl.get("ResponseCode").toString().equals("200")) {
                    int margin = 0;
                    double komAgent = 0, komTrue = 0;
                    String swReffNum = "";
                    //simulate refund on failed transaction
                    //jsonRespCurl.put("respCode", "1");
                    //jsonRespCurl.put("ACK", "NOK");
                    if (jsonRespCurl.get("respCode").toString().equals("0")) {
                        stattrx = "SUKSES";

                        if (jsonRespCurl.containsKey("trxId")) {
                            swReffNum = jsonRespCurl.get("trxId").toString();
                        }

                        komTrue = (Double.parseDouble(com[0]) * Integer.parseInt(jmlDewasa)) * komisiPp;
                        komAgent = (Double.parseDouble(com[1]) * Integer.parseInt(jmlDewasa)) * komisiPp;

                        // insert tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String tempkomisi = "INSERT INTO \"TempKomisi\" "
                                + " (\"id_trx\", \"Agent\", \"True\", \"Status\", \"StatusTransaksi\") "
                                + " VALUES (?, ?, ?, 'OPEN', 'OPEN')";
                        stPgsql = connPgsql2.prepareStatement(tempkomisi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.setFloat(2, (int) Math.ceil(komAgent));
                        stPgsql.setFloat(3, (int) Math.floor(komTrue));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (jsonRespCurl.get("ACK").toString().equals("PENDING")) {
                        stattrx = "PENDING";

                        // insert tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String tempkomisi = "INSERT INTO \"TempKomisi\" "
                                + "(\"id_trx\", \"Agent\", \"True\", \"Status\", \"StatusTransaksi\") "
                                + "VALUES (?, ?, ?, 'OPEN', 'OPEN')";
                        stPgsql = connPgsql2.prepareStatement(tempkomisi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.setFloat(2, (int) Math.ceil(komAgent));
                        stPgsql.setFloat(3, (int) Math.floor(komTrue));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    } else if (jsonRespCurl.get("ACK").toString().equals("NOK")) {
                        stattrx = "GAGAL";
                    }

                    if (stattrx.equals("SUKSES")) {
                        // update tempkomisi
                        //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                        String updateTemp = "UPDATE \"TempKomisi\" "
                                + "SET \"Status\" = 'OK', \"StatusTransaksi\" = 'OK' "
                                + "WHERE \"id_trx\" = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTemp);
                        stPgsql.setString(1, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update tr transaksi
                        //<editor-fold defaultstate="collapsed" desc="query insert trtransaksi">
                        String updateTransaksi = "UPDATE \"TrTransaksi\""
                                + " SET \"StatusTRX\" = ?, \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK' , \"StatusDana\" = 'DEDUCT', \"hargabeli\" = ?, \"hargajual\" = ?, \"Biaya\" = ?, timestampupdatetrx = now(), id_trxsupplier = ?"
                                + " WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, stattrx);
                        stPgsql.setLong(2, 0);
                        stPgsql.setLong(3, Long.parseLong(hargaCetak));
                        stPgsql.setString(4, String.valueOf(totalBiayaAdmin));
                        stPgsql.setString(5, swReffNum);
                        stPgsql.setString(6, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock true
                        //<editor-fold defaultstate="collapsed" desc="query update tmnstock">
                        String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (123456, now(), 0, True, ?, 0, 1, 'DEPOSIT', ?, 0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, nominal);
                        stPgsql.setLong(2, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // komisi agent
                        //<editor-fold defaultstate="collapsed" desc="query komisi agent">
                        String hitungkomisi = null;
                        sql = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, username);
                        stPgsql.executeQuery();

                        hitungkomisi = "UPDATE \"MsAgentAccount\" SET  \"LastBalance\" = \"LastBalance\" + ? WHERE id_agentaccount = ?;";
                        stPgsql = connPgsql2.prepareStatement(hitungkomisi);
                        stPgsql.setLong(1, (int) Math.ceil(komAgent));
                        stPgsql.setString(2, username);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock agent
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock agent">
                        //get lastbalance agent
                        String lb = "";
                        String getAmount = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\" WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";

                        stPgsql = connPgsql2.prepareStatement(getAmount);
                        stPgsql.setString(1, username);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            lb = rsPgsql.getString("LastBalance");
                        }

                        if (lb == null && lb.isEmpty()) {
                            lb = "0";
                        }

                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (?, now(), 0, 'Agent', ?, ?, ?, 'DEPOSIT', ?, 0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Long.parseLong(lb));
                        stPgsql.setLong(2, (int) Math.ceil(komAgent));
                        stPgsql.setLong(3, (int) Math.ceil(komAgent));
                        stPgsql.setString(4, username);
                        stPgsql.setLong(5, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock true
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock true">
                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES ('123456', now(), ?, 'True', ?,  0, 1, 'DEPOSIT', ?, 0);";

                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, (int) Math.ceil(komTrue));
                        stPgsql.setLong(2, (int) Math.ceil(komTrue));
                        stPgsql.setLong(3, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // update TMNStock supplier
                        //<editor-fold defaultstate="collapsed" desc="query update TMNStock supplier">
                        sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES ('123456', now(), 0, 'Supplier', ?,  0, ?, 'WITHDRAW', ?, 0);";

                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, Integer.parseInt(hargaCetak));
                        stPgsql.setString(2, String.valueOf(idSupplier));
                        stPgsql.setLong(3, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        // get timestamp
                        String getTime = "SELECT \"TimeStamp\" FROM \"TrTransaksi\" WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(getTime);
                        stPgsql.setString(1, idtrx);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            String strtime = rsPgsql.getString("TimeStamp");
                            jsonRespCurl.put("timestamp", utilities.formatTanggal(strtime));
                        }

                        String pesan = "Transaksi pembayaran Kereta Api Anda sukses dengan nomor transaksi " + idtrx;
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("pesan", pesan);
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("status", stattrx);
                        jsonResp.put("idtrx", idtrx);
                        //hit api iklan
                        String respIklan = utilities.getIklan(idTipeTransaksi, productcode, idtrx);
                        JSONObject jObjRespIklan = (JSONObject) parser.parse(respIklan);
                        jsonResp.put("iklan", jObjRespIklan);
                        //insert notif
                        //<editor-fold defaultstate="collapsed" desc="query insert notif">
                        sql = "INSERT INTO \"Notifikasi\" "
                                + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                + " VALUES (?, ?, ?,'FALSE',now())";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, pesan);
                        stPgsql.setString(2, username);
                        stPgsql.setString(3, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        //sms
                        String smsScript = null;
                        //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                        sql = "select"
                                + "        this_.id,"
                                + "        this_.account_type,"
                                + "        this_.created_by,"
                                + "        this_.flag_active,"
                                + "        this_.id_tipeaplikasi,"
                                + "        this_.id_tipetransaksi,"
                                + "        this_.sms_script,"
                                + "        this_.timestamp,"
                                + "        this_.trx_status "
                                + "    from"
                                + "        public.\"MsSMSNotification\" this_ "
                                + "    where"
                                + "        this_.id_tipetransaksi=? "
                                + "        and this_.id_tipeaplikasi=? "
                                + "        and this_.account_type=? "
                                + "        and this_.trx_status=?";
                        stPgsql = connPgsql.prepareStatement(sql);
                        stPgsql.setLong(1, 67);
                        stPgsql.setLong(2, 3);
                        stPgsql.setString(3, "AGENT");
                        stPgsql.setString(4, "SUKSES");
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            //Transaksi %s %s. Kode Book %s,%s|%s, %s %s, %s Penumpang.Ref %s
                            //Transaksi Kereta Api 2018-05-15 17:44. Kode Book 123456, GMBR-SBY|SBY-SENEN, 4Tiket, 2Penumpang. Ref 123456789012345678901234567890
                            smsScript = rsPgsql.getString("sms_script");
                        }
                        String pulang = "";
                        int jumlahTiket = Integer.parseInt(jmlDewasa);
                        if (smsScript != null) {
                            if (komisiPp == 2) {
                                jumlahTiket = Integer.parseInt(jmlDewasa) * 2;
                                pulang = jsonReq.get("stasiunPulang").toString() + "-" + jsonReq.get("stasiunPergi").toString();
                            }
                            int jumlahPenumpang = Integer.parseInt(jmlDewasa) + Integer.parseInt(jmlAnak);
                            pesan = String.format(smsScript,
                                    "Kereta api",
                                    utilities.convertFormatDate("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", utilities.getDate().toString()),
                                    jsonReq.get("depBookCode").toString(),
                                    jsonReq.get("stasiunPergi").toString() + "-" + jsonReq.get("stasiunPulang").toString(),
                                    jsonReq.get("returnBookCode").toString(),
                                    pulang,
                                    jumlahTiket + " Tiket",
                                    jumlahPenumpang,
                                    idtrx);
                            controllerLog.logStreamWriter("pesan to sms : " + pesan);

                            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                            new SmsHelper().sent(uuid, noHp, pesan, "KERETA", entityConfig);
                        }
                        //</editor-fold>

                        //email
                        JSONObject jObjBundle = (JSONObject) jsonReq.get("jObjBundle");
                        JSONArray jArrDataDewasa = (JSONArray) jObjBundle.get("dataDewasa");
                        JSONObject jArrDataDewasa1 = (JSONObject) jArrDataDewasa.get(0);
                        JSONObject jObjTrainPergi = (JSONObject) jObjBundle.get("trainPergi");
                        JSONObject jObjTrainPulang = (jObjBundle.containsKey("trainPulang")) ? (JSONObject) jObjBundle.get("trainPulang") : new JSONObject();

                        final JSONObject jsonReqEmail = new JSONObject();
                        jsonReqEmail.put("jObjReq", jsonReq);
                        jsonReqEmail.put("nama", jArrDataDewasa1.get("nama").toString());
                        jsonReqEmail.put("bookPergi", jsonReq.get("depBookCode").toString());
                        jsonReqEmail.put("noKeretaPergi", jObjTrainPergi.get("trainNo").toString());
                        jsonReqEmail.put("namaKeretaPergi", jObjTrainPergi.get("trainNm").toString());
                        jsonReqEmail.put("stasiunPergi", jObjBundle.get("orgName").toString());
                        jsonReqEmail.put("stasiunPergiTujuan", jObjBundle.get("desName").toString());

                        if (jObjTrainPulang.containsKey("trainNo")) {
                            jsonReqEmail.put("bookPulang", jsonReq.get("returnBookCode").toString());
                            jsonReqEmail.put("noKeretaPulang", jObjTrainPulang.get("trainNo").toString());
                            jsonReqEmail.put("namaKeretaPulang", jObjTrainPulang.get("trainNm").toString());
                            jsonReqEmail.put("stasiunPulang", jObjBundle.get("desName").toString());
                            jsonReqEmail.put("stasiunPulangTujuan", jObjBundle.get("orgName").toString());
                        }

//                        jObjDataAgent.put("biaya", totalBiaya);
//                        jObjDataAgent.put("diskon", totalDiskon);
//                        jObjDataAgent.put("total", hargaCetak);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(10000);
                                    new SendEmailKereta().send(jsonReq.get("email").toString(), jsonReqEmail.toString());
                                } catch (InterruptedException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }).start();

                    } else if (stattrx.equals("PENDING")) {
                        // update tr transaksi
                        String updateTransaksi = "UPDATE \"TrTransaksi\" " + "SET \"StatusTRX\" = '"
                                + stattrx
                                + "', \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'PENDING' "
                                + "WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, idtrx);
                        stPgsql.executeUpdate();
                        String pesan = "Transaksi Anda sedang diproses";
                        jsonResp.put("id_Trx", idtrx);
                        jsonResp.put("ACK", "OK");
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("status", stattrx);
                        jsonResp.put("pesan", pesan);

                    } else if (stattrx.equals("GAGAL")) {
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("id_Transaksi", idtrx);
                        String pesan = "Transaksi pembayaran Kereta Api  Anda gagal dengan nomor transaksi " + idtrx;
                        jsonResp.put("pesan", pesan);
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("status", stattrx);

                        //insert notif
                        //<editor-fold defaultstate="collapsed" desc="query insert notif">
                        sql = "INSERT INTO \"Notifikasi\" "
                                + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                + " VALUES " + "(?, ?, ?,'FALSE',now())";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, pesan);
                        stPgsql.setString(2, username);
                        stPgsql.setString(3, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                        String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                        stPgsql = connPgsql2.prepareStatement(sql1);
                        stPgsql.setString(1, username);
                        rsPgsql = stPgsql.executeQuery();

                        sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + hargaCetak + " "
                                + "WHERE id_agentaccount = ?";
                        stPgsql = connPgsql2.prepareStatement(sql);
                        stPgsql.setString(1, username);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        // update tr transaksi
                        //<editor-fold defaultstate="collapsed" desc="query update trtransaksi">
                        String updateTransaksi = "UPDATE"
                                + " \"TrTransaksi\" SET \"StatusTRX\" = ?, \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK', \"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', timestamprefund = now()"
                                + " WHERE id_trx = ?";
                        stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                        stPgsql.setString(1, stattrx);
                        stPgsql.setString(2, idtrx);
                        stPgsql.executeUpdate();
                        //</editor-fold>

                        // UpdateTMNStock agent REFUND
                        //<editor-fold defaultstate="collapsed" desc="query agent refund">
                        String sqlInsertMsTMNStock = "INSERT"
                                + " INTO \"TrStock\"( \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                + " VALUES (?, now(),0, 'Agent', ?,  0, ?, 'REFUND', ?,0);";
                        stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                        stPgsql.setLong(1, saldo);
                        stPgsql.setLong(2, Integer.parseInt(hargaCetak));
                        stPgsql.setString(3, username);
                        stPgsql.setLong(4, Long.parseLong(id_transaksi));
                        stPgsql.executeUpdate();
                        //</editor-fold>
                    }
                } else {
                    //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                    String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                    stPgsql = connPgsql2.prepareStatement(sql1);
                    stPgsql.setString(1, username);
                    rsPgsql = stPgsql.executeQuery();

                    sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + hargaCetak + " "
                            + "WHERE id_agentaccount = ?";
                    stPgsql = connPgsql2.prepareStatement(sql);
                    stPgsql.setString(1, username);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="query update tr transaksi">
                    String updateTransaksi = "UPDATE \"TrTransaksi\" "
                            + "SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK',"
                            + "\"StatusDeduct\" = 'NOK', "
                            + "\"StatusDana\" = 'REFUND', "
                            + "timestamprefund = now() " + "WHERE id_trx = ?";
                    stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                    stPgsql.setString(1, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    // UpdateTMNStock agent refund
                    //<editor-fold defaultstate="collapsed" desc="query update tmn stock agent refund">
                    String sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"(\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                            + " VALUES (?, now(), 0, 'Agent', ?,  0, ?, 'REFUND', ?, 0);";
                    stPgsql = connPgsql2.prepareStatement(sqlInsertMsTMNStock);
                    stPgsql.setLong(1, saldo);
                    stPgsql.setLong(2, Long.parseLong(hargaCetak));
                    stPgsql.setLong(3, Long.parseLong(username));
                    stPgsql.setLong(4, Long.parseLong(id_transaksi));
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    //<editor-fold defaultstate="collapsed" desc="query update komisi">
                    String updateTemp = "UPDATE \"TempKomisi\" "
                            + "SET \"Status\" = 'NOK', \"StatusTransaksi\" = 'NOK' "
                            + "WHERE \"id_trx\" = ?";
                    stPgsql = connPgsql2.prepareStatement(updateTemp);
                    stPgsql.setString(1, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("id_Transaksi", idtrx);
                    String pesan = "Transaksi pembayaran Kereta Api Anda gagal dengan nomor transaksi " + idtrx;
                    jsonResp.put("pesan", pesan);
                    jsonResp.put("status", "GAGAL");
                    //insert notif
                    //<editor-fold defaultstate="collapsed" desc="query insert notif">
                    sql = "INSERT INTO \"Notifikasi\" "
                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                            + " VALUES (?, ?, ?,'FALSE',now())";
                    stPgsql = connPgsql2.prepareStatement(sql);
                    stPgsql.setString(1, pesan);
                    stPgsql.setString(2, username);
                    stPgsql.setString(3, idtrx);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                }
            } else {
                String updateTransaksi = "UPDATE \"TrTransaksi\" "
                        + "SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK', \"StatusKomisi\" = 'NOK',"
                        + "\"StatusDeduct\" = 'NOK', "
                        + "\"StatusDana\" = 'REFUND', \"StatusDeduct\"='NOK', "
                        + "timestamprefund = now() WHERE id_trx = ?";
                stPgsql = connPgsql2.prepareStatement(updateTransaksi);
                stPgsql.setString(1, idtrx);
                stPgsql.executeUpdate();

                jsonResp.put("id_Transaksi", idtrx);
                jsonResp.put("ACK", "NOK");
                String pesan = "Transaksi pembayaran Kereta Api Anda gagal dengan nomor transaksi " + idtrx;
                jsonResp.put("pesan", pesan);
                jsonResp.put("status", "GAGAL");
                //insert notif
                //<editor-fold defaultstate="collapsed" desc="query insert notif">
                sql = "INSERT INTO \"Notifikasi\" "
                        + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                        + " VALUES (?, ?, ?,'FALSE',now())";
                stPgsql = connPgsql2.prepareStatement(sql);
                stPgsql.setString(1, pesan);
                stPgsql.setString(2, username);
                stPgsql.setString(3, idtrx);
                stPgsql.executeUpdate();
                //</editor-fold>
            }
            connPgsql2.commit();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
                connPgsql2.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
            jsonResp.put("id_Transaksi", idtrx);
            jsonResp.put("ACK", "NOK");
            String pesan = "Transaksi pembayaran Kereta Api Anda gagal dengan nomor transaksi " + idtrx;
            jsonResp.put("pesan", pesan);
            jsonResp.put("status", "GAGAL");
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
                if (connPgsql2 != null) {
                    connPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jsonResp.toString();
    }

    @Override
    public int gettrxid(String trxid) {
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query gettrxid">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select count(*) from \"TrTransaksi\" where id_trx = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return count;
    }

    @Override
    public String[] getStatusTrx(String trxid) {
        String status[] = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT"
                    + "  \"TrTransaksi\".\"CustomerReference\", \"TrTransaksi\".\"NoHPTujuan\", \"TrTransaksi\".\"NoRekTujuan\","
                    + "  \"TrTransaksi\".\"StatusTRX\", \"TrTransaksi\".\"id_pelanggan\" FROM"
                    + "  public.\"TrTransaksi\""
                    + "  WHERE \"TrTransaksi\".id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("StatusTRX");
                status[1] = rsPgsql.getString("CustomerReference");
                status[2] = rsPgsql.getString("NoHPTujuan");
                status[3] = rsPgsql.getString("NoRekTujuan");
                status[4] = rsPgsql.getString("id_pelanggan");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String NoKartu) {
        String[] status = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"MsAccountStatus\".\"NamaAccountStatus\""
                    + " ,\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\""
                    + " ,\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\""
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\""
                    + " WHERE \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus"
                    + " AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, NoKartu);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("NamaAccountStatus");
                status[1] = rsPgsql.getString("LastBalance");
                status[2] = rsPgsql.getString("StatusKartu");
                status[3] = rsPgsql.getString("id_agentaccount");
                status[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String[] getComission(int idTipeTransaksi, int idTipeAplikasi, int idOperator) {
        String[] comission = new String[3];
        //<editor-fold defaultstate="collapsed" desc="query getComission">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPosgre = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connPosgre = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String sqlGetRangeComission = "select * from \"MsFeeSupplier\" a, \"MsFee\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and b.id_tipetransaksi = ?"
                    + " and b.id_tipeaplikasi = ?"
                    + " and a.id_operator = ?"
                    + " and a.\"FlagActive\" = true";

            ps = connPosgre.prepareStatement(sqlGetRangeComission);
            ps.setLong(1, idTipeTransaksi);
            ps.setLong(2, idTipeAplikasi);
            ps.setLong(3, idOperator);
            rs = ps.executeQuery();
            if (rs.next()) {
                comission[0] = rs.getString("TrueFeeNom");
                comission[1] = rs.getString("AgentCommNom");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connPosgre != null) {
                    connPosgre.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return comission;
    }

    @Override
    public String isSaldoCukup(String idAccount, int total) {
        String hasil = "--";
        //<editor-fold defaultstate="collapsed" desc="query isSaldoCukup">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            String bit7 = idAccount.substring(6, 7);
            String tipeAkun = "AGENT";
            String q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            if (q.trim().length() != 0) {
                long saldo = 0;
                stPgsql = connPgsql.prepareStatement(q);
                stPgsql.setString(1, idAccount);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    saldo = rsPgsql.getLong("saldo");
                }
                if (saldo >= total) {
                    hasil = "00";        //BALANCEINSUFICIENT CUKUP
                } else {
                    hasil = "01";        //BALANCEINSUFICIENT TIDAK CUKUP
                }
            }
            connPgsql.commit();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return hasil;
    }

    @Override
    public String getSeatMap(JSONObject objJson) {
        //insert log
        int idLog = utilities.insertLog(objJson.get("idTrx").toString(), objJson);
        String url = hlp.getParamMsParameter("url_padicity") + "GetSeatMap";
        JSONObject jObjResp = this.utilities.curlPost(url, objJson.toString(), false);
        //updateLog
        utilities.updateLog(idLog, jObjResp.toString());
        if (jObjResp.containsKey("respCode")) {
            if (jObjResp.get("respCode").toString().equals("")) {
                jObjResp.put("ACK", "OK");
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal mengambil seat map");
            }
        } else {
            jObjResp.put("ACK", "NOK");
            jObjResp.put("pesan", "gagal mengambil seat map");
        }
        return jObjResp.toString();
    }

}
