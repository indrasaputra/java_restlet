/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import helper.Constant;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.simple.JSONObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.GcmNew;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcPembelianPln;
import koneksi.DatabaseUtilitiesPgsql;

/**
 *
 * @author Hasan
 */
public class ImplePembelianPln implements InterfcPembelianPln {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembelianPln(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getDenom(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select a.\"HargaCetak\", c.\"Nominal\", c.id_denom "
                    + "from \"MsFeeSupplier\" a, \"MsFee\" b, \"MsDenom\" c "
                    + "where a.id_operator = '13' "
                    + "and a.\"FlagActive\" = 't' "
                    + "and a.id_fee = b.id_fee "
                    + "and a.id_denom = c.id_denom "
                    + "and b.id_tipeaplikasi = '3' "
                    + "and b.id_tipetransaksi = '18' "
                    + "ORDER BY c.\"Nominal\"::NUMERIC";
            st = conn.prepareStatement(query);
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idDenom", rs.getString("id_denom"));

                double hargaCetak = Double.parseDouble(rs.getString("HargaCetak"));
                String hargaCetakStr = String.format("%,.0f", hargaCetak);
                hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                jObj.put("hargaCetak", hargaCetakStr);

                double nominal = Double.parseDouble(rs.getString("Nominal"));
                String nominalStr = String.format("%,.0f", nominal);
                nominalStr = nominalStr.replaceAll(",", ".");
                jObj.put("nominal", nominalStr);
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("denom", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public ArrayList<HashMap<String, Object>> getIDAgenKomisi(JSONObject jsonReq) {
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        String denom = jsonReq.get("denom").toString();
        denom = denom.replace(".", "");
        //<editor-fold defaultstate="collapsed" desc="query getIDAgenKomisi">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT "
                    + " A.id_supplier, "
                    + " A.\"ProductCode\", "
                    + " A.\"HargaBeli\", "
                    + " A.\"HargaJual\", "
                    + " A.\"HargaCetak\", "
                    + " A.\"HargaCetakMember\", "
                    + " A.\"BiayaAdmin\", "
                    + " A.\"TrueSpareFee\", "
                    + " A.\"SupplierPrice\", "
                    + " A.\"AgentCommType\", "
                    + " A.\"AgentCommNom\", "
                    + " A.\"DealerCommType\", "
                    + " A.\"DealerCommNom\", "
                    + " A.\"TrueFeeType\", "
                    + " A.\"TrueFeeNom\", "
                    + " A.\"MemberDiscType\", "
                    + " CASE WHEN A.\"MemberDiscType\" = 'PERCENTAGE' THEN ((A.\"BiayaAdmin\" - A.\"SupplierPrice\") * A.\"MemberDiscNom\") / 100 ELSE A.\"MemberDiscNom\" END, "
                    + " A.\"BiayaAdmin\" - A.\"TrueSpareFee\" AS \"MarginGross\" "
                    + "FROM "
                    + " \"MsFeeSupplier\" A "
                    + "JOIN \"MsDenom\" ON A.id_denom = \"MsDenom\".id_denom "
                    + "JOIN \"MsOperator\" ON A.id_operator = \"MsOperator\".id_operator "
                    + "JOIN \"MsFee\" ON \"MsFee\".id_fee = A.id_fee "
                    + "WHERE\n"
                    + " \"MsDenom\".\"Nominal\" = ? "
                    + "AND \"MsOperator\".id_operator = 13 "
                    + "AND A.\"FlagActive\" = 'TRUE' "
                    + "AND A.\"HargaBeli\" IS NOT NULL "
                    + "AND \"MsFee\".id_tipeaplikasi = 3";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, denom);
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            while (rsPgsql.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                }
                row.add(columns);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return row;
    }

    @Override
    public String getURL(String param) {
        String url = "";
        //<editor-fold defaultstate="collapsed" desc="query getUrlMsParameter">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT * FROM ms_parameter "
                    + "WHERE parameter_name = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, param);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return url;
    }

    @Override
    public int insertLog(JSONObject jsonReq) {
        int idLog = 0;
        //<editor-fold defaultstate="collapsed" desc="query insertLog">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "insert "
                    + "into "
                    + "tr_trx_token_log "
                    + "(id_trx, request, request_time, response, response_time) "
                    + "values "
                    + "(?, ?, ?, ?, ?);";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setString(1, jsonReq.get("idTrx").toString());
            stPgsql.setString(2, jsonReq.toString());
            stPgsql.setTimestamp(3, this.utilities.getDate());
            stPgsql.setString(4, null);
            stPgsql.setTimestamp(5, null);
            stPgsql.executeUpdate();
            rsPgsql = stPgsql.getGeneratedKeys();
            while (rsPgsql.next()) {
                idLog = Integer.parseInt(rsPgsql.getString(1));
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return idLog;
    }

    @Override
    public String curlPost(String url, JSONObject jsonReq, boolean encode) {
        JSONObject responseObj = null;
        jsonReq.put("Tipe", "inqTokenPLN");
        String data = jsonReq.toString();
        data = data.replace(".", "");
        data = data.replace("idTrx", "idtrx");
        data = data.replace("idPelanggan", "idpelanggan");

        this.controllerLog.logStreamWriter("SEND REQUEST TO : " + url + " with data : " + data);
        try {
            BufferedReader rd = null;
            String line = null;
            if (encode) {
                data = URLEncoder.encode(data.toString(), "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");
            con.setRequestProperty("Content-Type", "text/plain");

            con.setConnectTimeout(120000);
            con.setReadTimeout(120000);

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String code = String.valueOf(con.getResponseCode());

            //System.out.println(code);
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));

            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            if (response.toString().trim().length() > 10) {
                JSONParser parser = new JSONParser();
                this.controllerLog.logStreamWriter("response from otp generator : " + response.toString());
                Object obj = parser.parse(response.toString());
                responseObj = (JSONObject) obj;
                responseObj.put("ResponseCode", code);
            } else {
                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "500");
                responseObj.put("ACK", "NOK");
                responseObj.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                responseObj.put("MSG", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
            }

        } catch (java.net.SocketTimeoutException ex) {
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "504");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "TIME OUT!!");
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);

            return (responseObj.toString());
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "500");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "EXT : INTERNAL SERVER ERROR");

            return (responseObj.toString());

        }
        return (responseObj.toString());
    }

    @Override
    public void updateLog(String resp, int idLog) {
        //<editor-fold defaultstate="collapsed" desc="query updateLog">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "update "
                    + "tr_trx_token_log "
                    + "set "
                    + "response=?, "
                    + "response_time=? "
                    + "where "
                    + "id_trx_token_log=?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, resp);
            stPgsql.setTimestamp(2, this.utilities.getDate());
            stPgsql.setInt(3, idLog);
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }

    @Override
    public String bayarPlnPrabayar(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        String idPelanggan = null;
        String Nominal = null;
        String idDenom = null;
        String id_operator = null;
        String NoKartuAgent = null;
        String pin = null;
        String hp = null;
        String Hargajual = null;
        String Interface = "3";
        String idtrx = null;

        String jsonReqTmp = jsonReq.toString();
        jsonReqTmp = jsonReqTmp.replace(".", "");
        JSONParser jSONParser = new JSONParser();
        try {
            jsonReq = (JSONObject) jSONParser.parse(jsonReqTmp);
        } catch (ParseException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }

        try {
            idPelanggan = jsonReq.get("id_Pelanggan").toString();
            Nominal = jsonReq.get("denom").toString();
            idDenom = jsonReq.get("idDenom").toString();
            id_operator = jsonReq.get("id_Operator").toString();
            NoKartuAgent = jsonReq.get("username").toString();
            pin = jsonReq.get("pin").toString();
            hp = jsonReq.get("Handphone").toString();
            Hargajual = jsonReq.get("hargaCetak").toString();
            idtrx = jsonReq.get("idtrx").toString();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Missing parameter");
            return jsonResp.toString();
        }

        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query getCountIdTrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select count(*) from \"TrTransaksi\" where id_trx=?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idtrx);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        if (count == 1) {
            String status[] = new String[5];
            //<editor-fold defaultstate="collapsed" desc="query getStatusTrx">
            databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
            connPgsql = null;
            stPgsql = null;
            rsPgsql = null;
            try {
                connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                String query = "SELECT "
                        + "  \"TrTransaksi\".\"CustomerReference\", \"TrTransaksi\".\"NoHPTujuan\", \"TrTransaksi\".\"NoRekTujuan\","
                        + "  \"TrTransaksi\".\"StatusTRX\", \"TrTransaksi\".\"id_pelanggan\" FROM "
                        + "  public.\"TrTransaksi\" WHERE  \"TrTransaksi\".id_trx = ?;";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setString(1, idtrx);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    status[0] = rsPgsql.getString("StatusTRX");
                    status[1] = rsPgsql.getString("CustomerReference");
                    status[2] = rsPgsql.getString("NoHPTujuan");
                    status[3] = rsPgsql.getString("NoRekTujuan");
                    status[4] = rsPgsql.getString("id_pelanggan");
                }
            } catch (ParserConfigurationException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (rsPgsql != null) {
                        rsPgsql.close();
                    }
                    if (stPgsql != null) {
                        stPgsql.close();
                    }
                    if (connPgsql != null) {
                        connPgsql.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            String m = "";
            String ack = "";
            if (status[0].equalsIgnoreCase("SUKSES")) {
                m = "Transaksi Anda sukses";
                ack = "OK";
            } else if (status[0].equalsIgnoreCase("GAGAL")) {
                m = "Transaksi Anda gagal";
                ack = "NOK";
            } else {
                status[0] = "PENDING";
                m = "Transaksi Anda sedang diproses";
                ack = "OK";
            }
            jsonResp.put("statusTRX", status[0]);
            jsonResp.put("ACK", ack);
            jsonResp.put("pesan", m);
            return jsonResp.toString();
        } else if (count > 1) {
            jsonResp.put("statusTRX", "GAGAL");
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Transaksi Anda gagal");
            return jsonResp.toString();
        }

        String[] key = new String[4];
        String[] value = new String[4];
        String HargaJual = "";
        int HargaBeli = 0;
        int id_denom = 0;
        int id_supplier = 0;
        int id_feesupplier = 0;
        String ProductCode = null;

        String a[] = new String[4];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgent">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT  \"MsAccountStatus\".\"NamaAccountStatus\","
                    + "  \"MsAgentAccount\".\"StatusKartu\",\"MsAgentAccount\".\"PIN\","
                    + "    \"MsAgentAccount\".\"LastBalance\" FROM   public.\"MsAgentAccount\", "
                    + "  public.\"MsAccountStatus\" WHERE "
                    + "  \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus AND"
                    + "  \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, NoKartuAgent);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                a[0] = rsPgsql.getString("NamaAccountStatus");
                a[1] = rsPgsql.getString("LastBalance");
                a[2] = rsPgsql.getString("StatusKartu");
                a[3] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        int b[] = new int[8];
        //<editor-fold defaultstate="collapsed" desc="query getIDAgenKomisi">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsFeeSupplier\" a, \"MsFee\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_denom = ?"
                    + " and b.id_tipetransaksi = '18'"
                    + " and b.id_tipeaplikasi = '3'"
                    + " and a.id_operator = '13'"
                    + " and a.\"FlagActive\" = true";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(idDenom));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                b[0] = rsPgsql.getInt("BiayaAdmin"); 	// BiayaAdmin
                b[1] = rsPgsql.getInt("TrueSpareFee"); 	// TrueSpareFee
                b[2] = rsPgsql.getInt("SupplierPrice"); // SupplierPrice
                b[3] = rsPgsql.getInt("AgentCommNom"); 	// AgentCommNom
                b[4] = rsPgsql.getInt("DealerCommNom"); // DealerCommNom
                b[5] = rsPgsql.getInt("TrueFeeNom"); 	// TrueFeeNom
                b[6] = rsPgsql.getInt("MemberDiscNom"); // MemberDiscNom
                b[7] = rsPgsql.getInt("id_supplier"); 	// idSupplier
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        boolean cekisemptytoken = false;
        //<editor-fold defaultstate="collapsed" desc="query cekisemptytoken">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\" "
                    + " WHERE \"id_pelanggan\" = ?"
                    + " AND \"Nominal\" = ?"
                    + " AND id_operator = ?"
                    + " AND \"TimeStamp\"::DATE = now()::DATE"
                    + " AND (id_memberaccount = ? OR id_agentaccount = ?)"
                    + " AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            //System.out.println("query cekisemptytoken : " + query);
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idPelanggan);
            stPgsql.setString(2, Nominal);
            stPgsql.setLong(3, Long.parseLong(id_operator));
            stPgsql.setString(4, NoKartuAgent);
            stPgsql.setString(5, NoKartuAgent);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
            if (count == 0) {
                cekisemptytoken = true;
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        if (!cekisemptytoken) {
            jsonResp.put("ACK", "NOK");
            jsonResp.put("pesan", "Anda tidak dapat melakukan pembelian, silakan coba 24 jam kemudian");
            return jsonResp.toString();
        }

        try {
            databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
            connPgsql = null;
            stPgsql = null;
            rsPgsql = null;
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            jsonResp.put("ID", "payment");

            //<editor-fold defaultstate="collapsed" desc="query cekHargaJual">
            String cekHargaJual = "SELECT"
                    + " \"MsFeeSupplier\".id_supplier, \"MsOperator\".id_operator, \"MsDenom\".\"Nominal\", \"MsFeeSupplier\".\"HargaJual\", \"MsFeeSupplier\".\"ProductCode\", \"MsDenom\".id_denom, \"MsFeeSupplier\".id_supplier, \"MsFeeSupplier\".id_feesupplier, \"MsFeeSupplier\".\"HargaBeli\""
                    + " FROM PUBLIC .\"MsOperator\", PUBLIC .\"MsDenom\", PUBLIC .\"MsFeeSupplier\", PUBLIC .\"MsFee\""
                    + " WHERE "
                    + "\"MsFeeSupplier\".id_operator = \"MsOperator\".id_operator"
                    + " AND \"MsFeeSupplier\".id_fee = \"MsFee\".id_fee"
                    + " AND \"MsFeeSupplier\".id_denom = \"MsDenom\".id_denom"
                    + " AND \"MsDenom\".\"Nominal\" = ?"
                    + " AND \"MsFee\".id_tipeaplikasi = 3"
                    + " AND \"MsOperator\".id_operator = ?"
                    + " AND \"MsFeeSupplier\".\"FlagActive\" = TRUE"
                    + " AND \"MsFee\".\"FlagActive\" = TRUE;";
            stPgsql = connPgsql.prepareStatement(cekHargaJual);
            stPgsql.setString(1, Nominal);
            stPgsql.setLong(2, Long.parseLong(id_operator));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                HargaJual = rsPgsql.getString("HargaJual");
                id_denom = rsPgsql.getInt("id_denom");
                id_feesupplier = rsPgsql.getInt("id_feesupplier");
                id_supplier = rsPgsql.getInt("id_supplier");
                ProductCode = rsPgsql.getString("ProductCode");
                HargaBeli = rsPgsql.getInt("HargaBeli");
            }
            //</editor-fold>

            String idagentaccount = NoKartuAgent;

            // Cek Status Rekening, Status Kartu & Saldo
            if (a[0].equals("ACTIVE") || a[0].equals("INACTIVE") || a[0].equals("DORMANT")) {
                if (pin.equals(a[3])) {
                    String validasiSaldo = "";
                    String q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo"
                            + " FROM \"MsAgentAccount\""
                            + " WHERE id_agentaccount = ?"
                            + " FOR UPDATE";
                    if (q.trim().length() != 0) {
                        long saldo = 0;
                        stPgsql = connPgsql.prepareStatement(q);
                        stPgsql.setString(1, idagentaccount);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            saldo = rsPgsql.getLong("saldo");
                        }
                        boolean lanjut = true;
                        if (lanjut) {
                            if (saldo >= Integer.parseInt(Hargajual)) {
                                validasiSaldo = "00";		//BALANCEINSUFICIENT CUKUP
                            } else {
                                validasiSaldo = "01";		//BALANCEINSUFICIENT TIDAK CUKUP
                            }
                        }
                    }
                    if (validasiSaldo.equalsIgnoreCase("00")) {
                        // Deduct Balance
                        String statusDeduct = "NOK";
                        //<editor-fold defaultstate="collapsed" desc="query deductBalance">
                        String sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                        stPgsql = connPgsql.prepareStatement(sql);
                        stPgsql.setString(1, NoKartuAgent);
                        rsPgsql = stPgsql.executeQuery();

                        long lastBalance = 0;
                        while (rsPgsql.next()) {
                            lastBalance = rsPgsql.getLong("LastBalance");
                        }

                        if (lastBalance < Long.parseLong(Hargajual)) {
                            statusDeduct = "NOK";
                        } else {
                            if (Integer.signum(Integer.parseInt(Hargajual)) > 0) {
                                String deductBalance = "UPDATE \"MsAgentAccount\" SET \"LastBalance\"= \"LastBalance\" - ? "
                                        + "WHERE \"id_agentaccount\"=?;";
                                stPgsql = connPgsql.prepareStatement(deductBalance);
                                stPgsql.setLong(1, Long.parseLong(Hargajual));
                                stPgsql.setString(2, NoKartuAgent);
                                int status = stPgsql.executeUpdate();
                                if (status == 1) {
                                    statusDeduct = "OK";
                                } else {
                                    statusDeduct = "NOK";
                                }
                            } else{
                                statusDeduct = "NOK";
                            }
                        }
                        //</editor-fold>
                        if (statusDeduct.equalsIgnoreCase("OK")) {
                            String Keterangan = "Pembelian token PLN untuk ID Pelanggan " + idPelanggan + " sebesar Rp " + Nominal;
                            // Update Transaksi
                            String id_transaksi = null;
                            //<editor-fold defaultstate="collapsed" desc="query updateTransaksi">
                            String updateTransaksi = "INSERT INTO \"TrTransaksi\"("
                                    + " id_tipetransaksi, id_agentaccount, id_denom,id_pelanggan, \"Nominal\", \"StatusTRX\", "
                                    + " \"StatusDeduct\", \"TypeTRX\", \"TimeStamp\", \"OpsVerifikasi\", \"Total\", "
                                    + " id_trx, \"StatusKomisi\",id_tipeaplikasi, id_operator,\"Keterangan\","
                                    + "	\"StatusRefund\",\"NoHPTujuan\",\"StatusDana\", \"Biaya\", id_supplier,"
                                    + "	\"hargabeli\", \"hargajual\", id_feesupplier)"
                                    + " VALUES (18,?,?,?,?,'PENDING',?,'WITHDRAW',now(),'PIN',?,?,'PENDING',3,?,?,'NOK',?,'DEDUCT',?,?,?,?,?);";

                            stPgsql = connPgsql.prepareStatement(updateTransaksi, Statement.RETURN_GENERATED_KEYS);
                            stPgsql.setString(1, NoKartuAgent);
                            stPgsql.setLong(2, id_denom);
                            stPgsql.setString(3, idPelanggan);
                            stPgsql.setString(4, Nominal);
                            stPgsql.setString(5, statusDeduct);
                            stPgsql.setLong(6, Long.parseLong(Hargajual));
                            stPgsql.setString(7, idtrx);
                            stPgsql.setLong(8, Long.parseLong(id_operator));
                            stPgsql.setString(9, Keterangan);
                            stPgsql.setString(10, hp);
                            stPgsql.setString(11, String.valueOf((Long.parseLong(Hargajual) - Long.parseLong(Nominal))));
                            stPgsql.setLong(12, id_supplier);
                            stPgsql.setLong(13, HargaBeli);
                            stPgsql.setLong(14, Long.parseLong(Hargajual));
                            stPgsql.setLong(15, id_feesupplier);

                            int hasil = stPgsql.executeUpdate();
                            if (hasil > 0) {
                                rsPgsql = stPgsql.getGeneratedKeys();
                                while (rsPgsql.next()) {
                                    id_transaksi = rsPgsql.getString(1);
                                }
                            }
                            //</editor-fold>
                            String lb = "";
                            //<editor-fold defaultstate="collapsed" desc="query getLastBalanceAgent">
                            String getAmount = "SELECT"
                                    + " \"MsAgentAccount\".\"LastBalance\""
                                    + " FROM public.\"MsAgentAccount\""
                                    + " WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";

                            stPgsql = connPgsql.prepareStatement(getAmount);
                            stPgsql.setString(1, NoKartuAgent);
                            rsPgsql = stPgsql.executeQuery();
                            while (rsPgsql.next()) {
                                lb = rsPgsql.getString("LastBalance");
                            }
                            if (lb == null && lb.isEmpty()) {
                                lb = "0";
                            }
                            //</editor-fold>
                            // Update TMNStock Agent
                            //<editor-fold defaultstate="collapsed" desc="query updateTrStock">
                            String updateTMNStock = "INSERT"
                                    + " INTO \"TrStock\"(\"LastBalance\",\"TimeStamp\",\"StokType\",\"Nominal\",id_transaksi,id_stock,\"TypeTrx\",\"True\",\"Agent\",\"Member\",\"Dealer\")"
                                    + " VALUES (?,now(),'Agent',?,?,?,'WITHDRAW', 0, 0, 0, 0);";
                            stPgsql = connPgsql.prepareStatement(updateTMNStock);
                            stPgsql.setLong(1, Long.parseLong(lb));
                            stPgsql.setLong(2, Long.parseLong(Hargajual));
                            stPgsql.setInt(3, Integer.parseInt(id_transaksi));
                            stPgsql.setString(4, NoKartuAgent);

                            int status = stPgsql.executeUpdate();
                            //</editor-fold>
                            //<editor-fold defaultstate="collapsed" desc="query insertTmpKomisi">
                            int Dealer = b[4];
                            int True = b[1] + b[5] + b[6];
                            int Agent = b[3];
                            String SQL = "INSERT"
                                    + " INTO \"TempKomisi\"(\"id_trx\",\"Agent\",\"True\",\"Status\",\"Dealer\",\"StatusTransaksi\")"
                                    + " VALUES (?,?,?,'OPEN',?,'PENDING');";

                            stPgsql = connPgsql.prepareStatement(SQL);
                            stPgsql.setString(1, idtrx);
                            stPgsql.setFloat(2, (int) Math.ceil(Agent));
                            stPgsql.setFloat(3, (int) Math.ceil(True));
                            stPgsql.setFloat(4, (int) Math.ceil(Dealer));

                            stPgsql.executeUpdate();
                            //</editor-fold>

                            connPgsql.commit();

                            DatabaseUtilitiesPgsql databaseUtilitiesPgsql2 = new DatabaseUtilitiesPgsql();
                            Connection connPgsql2 = null;
                            PreparedStatement stPgsql2 = null;
                            ResultSet rsPgsql2 = null;
                            try {
                                connPgsql2 = databaseUtilitiesPgsql2.getConnection(this.entityConfig);
                                connPgsql2.setAutoCommit(false);
                                JSONObject jObject = new JSONObject();
                                jObject.put("idtrx", idtrx);
                                jObject.put("msisdn", hp);
                                jObject.put("idpelanggan", idPelanggan);
                                jObject.put("denom", Nominal);

                                String json = jObject.toString();
                                //get parameter url
                                String url = "";
                                //<editor-fold defaultstate="collapsed" desc="query getUrl">
                                String query = "SELECT * FROM ms_parameter "
                                        + "WHERE parameter_name = ?";
                                stPgsql2 = connPgsql2.prepareStatement(query);
                                stPgsql2.setString(1, "prepaid_pln_pay_" + b[7]);
                                rsPgsql2 = stPgsql2.executeQuery();
                                while (rsPgsql2.next()) {
                                    url = rsPgsql2.getString("parameter_value");
                                }
                                //</editor-fold>
                                //insert log
                                int idLog = 0;
                                //<editor-fold defaultstate="collapsed" desc="query insertLog">
                                query = "insert "
                                        + "    into"
                                        + "        tr_trx_token_log"
                                        + "        (id_trx, request, request_time, response, response_time) "
                                        + "    values"
                                        + "        (?, ?, ?, ?, ?)";
                                stPgsql2 = connPgsql2.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                                stPgsql2.setString(1, idtrx);
                                stPgsql2.setString(2, jsonReq.toString());
                                stPgsql2.setTimestamp(3, utilities.getDate());
                                stPgsql2.setString(4, null);
                                stPgsql2.setTimestamp(5, null);
                                hasil = stPgsql2.executeUpdate();
                                if (hasil != 0) {
                                    rsPgsql2 = stPgsql2.getGeneratedKeys();
                                    while (rsPgsql2.next()) {
                                        idLog = Integer.parseInt(rsPgsql2.getString(1));
                                    }
                                }
                                //</editor-fold>
                                String responseCurl = "";
                                //<editor-fold defaultstate="collapsed" desc="query curlPost">
                                JSONObject responseObj = null;
                                try {
                                    BufferedReader rd = null;
                                    String line = null;
                                    HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

                                    controllerLog.logStreamWriter("req to otp generator : " + url + " data : " + json);

                                    con.setRequestMethod("POST");
                                    con.setRequestProperty("Content-Length", "" + Integer.toString(json.getBytes().length));
                                    con.setRequestProperty("Content-Language", "en-US");
                                    con.setRequestProperty("Content-Type", "text/plain");

                                    con.setUseCaches(false);
                                    con.setDoInput(true);
                                    con.setDoOutput(true);

                                    // Send request
                                    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                                    wr.writeBytes(json);
                                    wr.flush();
                                    wr.close();

                                    String code = String.valueOf(con.getResponseCode());
                                    // Get Response
                                    InputStream is = con.getInputStream();
                                    rd = new BufferedReader(new InputStreamReader(is));

                                    StringBuffer response = new StringBuffer();
                                    while ((line = rd.readLine()) != null) {
                                        response.append(line);
                                        response.append('\r');
                                    }
                                    rd.close();

                                    if (response.toString().trim().length() > 10) {
                                        JSONParser parser = new JSONParser();
                                        //System.out.println(response.toString());
                                        controllerLog.logStreamWriter("response otp generator : " + response.toString());
                                        Object obj = parser.parse(response.toString());
                                        responseObj = (JSONObject) obj;
                                        responseObj.put("ResponseCode", code);
                                    } else {
                                        responseObj = new JSONObject();
                                        responseObj.put("ResponseCode", "500");
                                        responseObj.put("ACK", "NOK");
                                        responseObj.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                                    }
                                    responseCurl = responseObj.toString();
                                } catch (Exception ex) {
                                    String s = Throwables.getStackTraceAsString(ex);
                                    controllerLog.logErrorWriter(s);
                                    responseObj.put("ResponseCode", "200");
                                    responseObj.put("ACK", "PENDING");
                                    responseObj.put("pesan", "EXT : INTERNAL SERVER ERROR");
                                    responseCurl = responseObj.toString();
                                }
                                //</editor-fold>
                                //update log
                                //<editor-fold defaultstate="collapsed" desc="query updateLog">
                                query = "update"
                                        + "        \"tr_trx_token_log\" "
                                        + "    set"
                                        + "        \"response\"=?,"
                                        + "        \"response_time\"=? "
                                        + "    where"
                                        + "        id_trx_token_log=?";
                                stPgsql2 = connPgsql2.prepareStatement(query);
                                stPgsql2.setString(1, responseCurl);
                                stPgsql2.setTimestamp(2, utilities.getDate());
                                stPgsql2.setLong(3, idLog);
                                hasil = stPgsql2.executeUpdate();
                                //</editor-fold>

                                JsonObject Jresponse = new JsonParser().parse(responseCurl).getAsJsonObject();
                                if (Jresponse.get("ACK").getAsString().equalsIgnoreCase("OK")) {
                                    String token = Jresponse.has("token") ? Jresponse.get("token").getAsString().replace(" ", "-") : "";
                                    String kwh = Jresponse.has("kwh") ? Jresponse.get("kwh").getAsString() : "";
                                    String Nama = Jresponse.has("nama") ? Jresponse.get("nama").getAsString() : "";
                                    String daya = Jresponse.has("daya") ? Jresponse.get("daya").getAsString() : "";

                                    String CustRef = token + "/" + Nama + "/" + daya + "/" + kwh;

                                    String swReffNum = "";
                                    if (Jresponse.has("SW_reff_num")) {
                                        swReffNum = Jresponse.get("SW_reff_num").getAsString();
                                    }

                                    // Update TrTransaksi
                                    //<editor-fold defaultstate="collapsed" desc="query TrTransaksi">
                                    updateTransaksi = "UPDATE \"TrTransaksi\""
                                            + " SET \"StatusDana\" = 'DEDUCT', "
                                            + "\"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK', \"CustomerReference\" = ?, "
                                            + "\"StatusTRX\" = 'SUKSES', timestampupdatetrx = now(), id_trxsupplier = ? WHERE id_trx = ?";

                                    stPgsql2 = connPgsql2.prepareStatement(updateTransaksi);
                                    stPgsql2.setString(1, CustRef);
                                    stPgsql2.setString(2, swReffNum);
                                    stPgsql2.setString(3, idtrx);
                                    hasil = stPgsql2.executeUpdate();
                                    //</editor-fold>

                                    //point
                                    JSONObject jObjTrTransaksi = new JSONObject();
                                    jObjTrTransaksi.put("tglTransaksi", new Date());
                                    jObjTrTransaksi.put("tipeTransaksi", 18);
                                    jObjTrTransaksi.put("idAgentAccount", NoKartuAgent);
                                    jObjTrTransaksi.put("idTransaksi", Integer.parseInt(id_transaksi));
                                    jObjTrTransaksi.put("subject", Constant.AGENT);

                                    JSONObject jObjPromo = new JSONObject();
                                    //<editor-fold defaultstate="collapsed" desc="query getDataPromo">
                                    sql = "SELECT * FROM \"public\".\"MsPromo\" WHERE id_promo = ?;";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setLong(1, Constant.idPromo);
                                    rsPgsql2 = stPgsql2.executeQuery();
                                    while (rsPgsql2.next()) {
                                        jObjPromo.put("idPromo", rsPgsql2.getInt("id_promo"));
                                        jObjPromo.put("tglMulai", rsPgsql2.getDate("tgl_mulai"));
                                        jObjPromo.put("tglSelesai", rsPgsql2.getDate("tgl_selesai"));
                                        jObjPromo.put("namaPromo", rsPgsql2.getString("nama_promo"));
                                    }
                                    //</editor-fold>

                                    JSONObject jObjPoint = new JSONObject();
                                    //<editor-fold defaultstate="collapsed" desc="query getPoint">
                                    sql = "SELECT id_point, point_agent, point_member FROM mspoint WHERE id_tipetransaksi = ? AND id_promo = ? AND flagactive = TRUE;";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setLong(1, Long.parseLong(jObjTrTransaksi.get("tipeTransaksi").toString()));
                                    stPgsql2.setLong(2, Constant.idPromo);
                                    rsPgsql2 = stPgsql2.executeQuery();
                                    while (rsPgsql2.next()) {
                                        jObjPoint.put("idPoint", rsPgsql2.getInt("id_point"));
                                        jObjPoint.put("pointAgent", rsPgsql2.getString("point_agent"));
                                        jObjPoint.put("pointMember", rsPgsql2.getString("point_member"));
                                    }
                                    //</editor-fold>
                                    boolean isEmployee = false;
                                    boolean isInRange = false;
                                    Date startDate = (Date) jObjPromo.get("tglMulai");
                                    Date endDate = (Date) jObjPromo.get("tglSelesai");
                                    if (((Date) jObjTrTransaksi.get("tglTransaksi")).getTime() >= startDate.getTime()
                                            && ((Date) jObjTrTransaksi.get("tglTransaksi")).getTime() <= endDate.getTime()) {
                                        isInRange = true;
                                    }
                                    // Jika Masuk PROMO ~
                                    if (isInRange) {
                                        if (jObjPoint.get("pointAgent") != null && jObjPoint.get("pointMember") != null) {
                                            if (jObjTrTransaksi.get("subject").toString().equalsIgnoreCase(Constant.KARTU_MEMBER)) {
                                                // Berhubungan dengan Member dan Agent

                                                if (isEmployee) {
                                                    // Insert Point Agent
                                                    if (jObjPoint.get("pointAgent") != null) {
                                                        this.insertPointAgent(connPgsql2, jObjTrTransaksi, jObjPoint);

                                                    }

                                                    int TotalPoint = Integer.parseInt(jObjPoint.get("pointAgent").toString());
                                                    this.updatePoint(connPgsql2, jObjTrTransaksi, TotalPoint, jObjPromo);
                                                } else {
                                                    // Insert Point Agent AND Member
                                                    this.insertPointAgent(connPgsql2, jObjTrTransaksi, jObjPoint);
                                                    this.insertPointMember(connPgsql2, jObjTrTransaksi, jObjPoint);
                                                    int TotalPoint = Integer.parseInt(jObjPoint.get("pointAgent").toString()) + Integer.parseInt(jObjPoint.get("pointMember").toString());
                                                    this.updatePoint(connPgsql2, jObjTrTransaksi, TotalPoint, jObjPromo);
                                                }
                                            } else {
                                                // Berhubungan dengan Agent saja
                                                if (jObjPoint.get("pointAgent") != null) {
                                                    this.insertPointAgent(connPgsql2, jObjTrTransaksi, jObjPoint);
                                                    int TotalPoint = Integer.parseInt(jObjPoint.get("pointAgent").toString());
                                                    this.updatePoint(connPgsql2, jObjTrTransaksi, TotalPoint, jObjPromo);
                                                }
                                            }
                                        }
                                    }

                                    // Update TMNStock True
                                    String updateTMNStockTrue = "INSERT INTO \"TrStock\"("
                                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", "
                                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                                            + " VALUES ('123456',now(),'True',?,?,1,'DEPOSIT',0,0,0,0);";

                                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockTrue);
                                    stPgsql2.setLong(1, Long.parseLong(String.valueOf(HargaBeli)));
                                    stPgsql2.setLong(2, Long.parseLong(id_transaksi));

                                    status = stPgsql2.executeUpdate();

                                    // Update komisi TMNStock True
                                    updateTMNStockTrue = "INSERT INTO \"TrStock\"("
                                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", "
                                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                                            + " VALUES ('123456',now(),'True',?,?,1,'DEPOSIT',0,0,0,0);";

                                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockTrue);
                                    stPgsql2.setFloat(1, (int) Math.floor(True));
                                    stPgsql2.setLong(2, Long.parseLong(id_transaksi));
                                    status = stPgsql2.executeUpdate();

                                    // komisi agent
                                    String topUPBalance = "UPDATE \"MsAgentAccount\" " + " SET \"LastBalance\"= \"LastBalance\" + " + (int) Math.ceil(Agent) + "   WHERE \"id_agentaccount\" = '" + idagentaccount + "';";
                                    stPgsql2 = connPgsql2.prepareStatement(topUPBalance);
                                    stPgsql2.executeUpdate();

                                    // Update komisi TMNStock Agent
                                    String Lb = "";
                                    //<editor-fold defaultstate="collapsed" desc="query LastBalace">
                                    getAmount = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM  "
                                            + "  public.\"MsAgentAccount\" WHERE  \"MsAgentAccount\".\"id_agentaccount\" = ?;";
                                    stPgsql2 = connPgsql2.prepareStatement(getAmount);
                                    stPgsql2.setString(1, NoKartuAgent);
                                    rsPgsql2 = stPgsql2.executeQuery();
                                    while (rsPgsql2.next()) {
                                        Lb = rsPgsql2.getString("LastBalance");
                                    }
                                    //</editor-fold>
                                    updateTMNStockTrue = "INSERT INTO \"TrStock\"("
                                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", "
                                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\") "
                                            + "VALUES (?,now(),'Agent', ?, ?, ?, 'DEPOSIT', 0, 0, 0, 0);";

                                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockTrue);
                                    stPgsql2.setLong(1, Long.parseLong(Lb));
                                    stPgsql2.setFloat(2, (int) Math.ceil(Agent));
                                    stPgsql2.setLong(3, Long.parseLong(id_transaksi));
                                    stPgsql2.setString(4, idagentaccount);

                                    status = stPgsql2.executeUpdate();

                                    // update tempkomisi
                                    String updateTemp = "UPDATE \"TempKomisi\" "
                                            + "SET \"Status\" = 'OK', \"StatusTransaksi\" = 'OK' WHERE \"id_trx\" = ?";
                                    stPgsql2 = connPgsql2.prepareStatement(updateTemp);
                                    stPgsql2.setString(1, idtrx);
                                    stPgsql2.executeUpdate();

                                    // Update TMNStock Supplier
                                    String updateTMNStockSupplier = "INSERT INTO \"TrStock\"("
                                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\","
                                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                                            + " VALUES ('123456', now(), 'Supplier',?, ?, ?, 'WITHDRAW', 0, 0, 0, 0);";

                                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockSupplier);
                                    stPgsql2.setLong(1, Long.parseLong(String.valueOf(HargaBeli)));
                                    stPgsql2.setLong(2, Long.parseLong(id_transaksi));
                                    stPgsql2.setString(3, String.valueOf(id_supplier));

                                    status = stPgsql2.executeUpdate();

                                    // hitung komisi dealer
                                    String idDealer = "";
                                    //<editor-fold defaultstate="collapsed" desc="query getIdDealerAccount">
                                    String getID_memberaccount = "SELECT"
                                            + "	\"MsDealer\".id_dealer_account"
                                            + " FROM"
                                            + "	\"MsDealer\""
                                            + " JOIN \"MsAgentAccount\" ON \"MsDealer\".id_dealer = \"MsAgentAccount\".id_dealer"
                                            + " JOIN \"MsAgent\" ON \"MsAgent\".id_agent = \"MsAgentAccount\".id_agent"
                                            + " WHERE"
                                            + "	\"MsAgentAccount\".id_agentaccount = ?"
                                            + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer;";
                                    stPgsql2 = connPgsql2.prepareStatement(getID_memberaccount);
                                    stPgsql2.setString(1, NoKartuAgent);
                                    rsPgsql2 = stPgsql2.executeQuery();
                                    while (rsPgsql2.next()) {
                                        idDealer = rsPgsql2.getString("id_dealer_account");
                                    }
                                    if (idDealer == null && idDealer.isEmpty()) {
                                        idDealer = "111111111111";
                                    }

                                    //</editor-fold>
                                    sql = "SELECT * FROM \"MsDealer\" WHERE id_dealer_account = ? FOR UPDATE";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setString(1, idDealer);
                                    rsPgsql2 = stPgsql2.executeQuery();

                                    String hitungkomisi = "UPDATE \"MsDealer\" SET \"Wallet\" = \"Wallet\" + " + (int) Math.ceil(Dealer) + " WHERE id_dealer_account = ?;";
                                    stPgsql2 = connPgsql2.prepareStatement(hitungkomisi);
                                    stPgsql2.setString(1, idDealer);
                                    status = stPgsql2.executeUpdate();

                                    // Update TMNStock Dealer
                                    idDealer = "0";
                                    //<editor-fold defaultstate="collapsed" desc="query getIdDealer">
                                    sql = "SELECT"
                                            + "	\"MsAgentAccount\".id_dealer"
                                            + " FROM"
                                            + "	\"MsAgentAccount\""
                                            + " JOIN \"MsAgent\" ON \"MsAgentAccount\".id_agent = \"MsAgent\".id_agent"
                                            + " WHERE"
                                            + "	\"MsAgentAccount\".id_agentaccount = ?"
                                            + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer";

                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setString(1, NoKartuAgent);
                                    rsPgsql2 = stPgsql2.executeQuery();
                                    while (rsPgsql2.next()) {
                                        idDealer = rsPgsql2.getString("id_dealer");
                                    }
                                    //</editor-fold>
                                    String lastBalanceDealer = "0";
                                    //<editor-fold defaultstate="collapsed" desc="query getLastBalanceDealer">
                                    sql = "SELECT \"MsDealer\".\"Wallet\" FROM \"MsDealer\""
                                            + " WHERE id_dealer_account = ?";

                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setString(1, idDealer);
                                    rsPgsql2 = stPgsql2.executeQuery();
                                    while (rsPgsql2.next()) {
                                        lastBalanceDealer = rsPgsql2.getString("Wallet");
                                    }
                                    //</editor-fold>
                                    String updateTMNStockDealer = "INSERT INTO \"TrStock\"("
                                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\","
                                            + " id_transaksi, id_stock, \"TypeTrx\", \"Dealer\", \"True\", \"Agent\", \"Member\")"
                                            + " VALUES (?, now(), 'Dealer',?, ?, ?, 'DEPOSIT', 0, 0, 0, 0);";

                                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStockDealer);
                                    stPgsql2.setLong(1, Long.parseLong(lastBalanceDealer));
                                    stPgsql2.setFloat(2, (int) Math.ceil(Dealer));
                                    stPgsql2.setLong(3, Long.parseLong(id_transaksi));
                                    stPgsql2.setString(4, idDealer);

                                    status = stPgsql2.executeUpdate();

                                    //set json response
                                    jsonResp.put("ACK", "OK");
                                    jsonResp.put("id_Transaksi", idtrx);
                                    jsonResp.put("pesan", Keterangan);
                                    jsonResp.put("id_Operator", id_operator);
                                    jsonResp.put("denom", Nominal);
                                    jsonResp.put("statusTopUP", "SUKSES");
                                    jsonResp.put("timestamp", utilities.getDate().toString());
                                    jsonResp.put("statusTRX", "SUKSES");

                                    jsonResp.put("id_Pelanggan", idPelanggan);
                                    jsonResp.put("nama", Nama);
                                    jsonResp.put("daya", daya);
                                    jsonResp.put("maxKWHlimit", Jresponse.get("maxKWHlimit").getAsString());
                                    jsonResp.put("totalRepeatToken", Jresponse.get("totalRepeatToken").getAsString());

                                    double dbl = Double.parseDouble(Jresponse.get("biayaAdmin").getAsString());
                                    String dblStr = String.format("%,.0f", dbl);
                                    dblStr = dblStr.replaceAll(",", ".");
                                    jsonResp.put("biayaAdmin", dblStr);

                                    jsonResp.put("noMeter", Jresponse.get("noMeter").getAsString());
                                    jsonResp.put("kwh", Jresponse.get("kwh").getAsString());
                                    jsonResp.put("token", Jresponse.get("token").getAsString());
                                    jsonResp.put("nomorMeter", Jresponse.get("nomorMeter").getAsString());

                                    dbl = Double.parseDouble(Jresponse.get("meterai").getAsString());
                                    dblStr = String.format("%,.0f", dbl);
                                    dblStr = dblStr.replaceAll(",", ".");
                                    jsonResp.put("meterai", dblStr);

                                    dbl = Double.parseDouble(Jresponse.get("ppn").getAsString());
                                    dblStr = String.format("%,.0f", dbl);
                                    dblStr = dblStr.replaceAll(",", ".");
                                    jsonResp.put("ppn", dblStr);

                                    dbl = Double.parseDouble(Jresponse.get("ppj").getAsString());
                                    dblStr = String.format("%,.0f", dbl);
                                    dblStr = dblStr.replaceAll(",", ".");
                                    jsonResp.put("ppj", dblStr);

                                    dbl = Double.parseDouble(Jresponse.get("angsuran").getAsString());
                                    dblStr = String.format("%,.0f", dbl);
                                    dblStr = dblStr.replaceAll(",", ".");
                                    jsonResp.put("angsuran", dblStr);

                                    dbl = Double.parseDouble(Jresponse.get("stroom").getAsString());
                                    dblStr = String.format("%,.0f", dbl);
                                    dblStr = dblStr.replaceAll(",", ".");
                                    jsonResp.put("stroom", dblStr);

                                    jsonResp.put("infoText", Jresponse.get("info_text").getAsString());
                                    jsonResp.put("swReffNum", swReffNum);

                                    dbl = Double.parseDouble(Hargajual);
                                    dblStr = String.format("%,.0f", dbl);
                                    dblStr = dblStr.replaceAll(",", ".");
                                    jsonResp.put("hargaCetak", dblStr);

                                    //hit api iklan
                                    String respIklan = utilities.getIklan("18",ProductCode, idtrx);
                                    JSONObject jObjRespIklan = (JSONObject) parser.parse(respIklan);
                                    jsonResp.put("iklan", jObjRespIklan);

                                    String pesan = "Transaksi pembelian token listrik Anda berhasil dengan Token " + token
                                            + ". Nomor transaksi " + idtrx + ".";

                                    //insert notif
                                    sql = "INSERT INTO \"Notifikasi\" "
                                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                            + " VALUES (?, ?, ?,'FALSE',now())";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setString(1, pesan);
                                    stPgsql2.setString(2, NoKartuAgent);
                                    stPgsql2.setString(3, idtrx);

                                    stPgsql2.executeUpdate();

                                    //send sms
                                    //get MsSmsNotification
                                    ArrayList<HashMap<String, Object>> rowMsSmsNotifikasi = new ArrayList<>();
                                    //<editor-fold defaultstate="collapsed" desc="query MsSmsNotification">
                                    query = "SELECT * FROM \"MsSMSNotification\" "
                                            + "WHERE \"id_tipetransaksi\" = 18 "
                                            + "and id_tipeaplikasi = 3 "
                                            + "and account_type = ? "
                                            + "and trx_status = ?";
                                    stPgsql = connPgsql.prepareStatement(query);
                                    stPgsql.setString(1, Constant.MEMBER);
                                    stPgsql.setString(2, Constant.StatusTrx.SUKSES.toString().toUpperCase());
                                    rsPgsql = stPgsql.executeQuery();
                                    ResultSetMetaData metaData = rsPgsql.getMetaData();
                                    int colCount = metaData.getColumnCount();
                                    while (rsPgsql.next()) {
                                        HashMap<String, Object> columns = new HashMap<>();
                                        for (int i = 1; i <= colCount; i++) {
                                            columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                                        }
                                        rowMsSmsNotifikasi.add(columns);
                                    }
                                    //</editor-fold>
                                    String smsScript = "";
                                    for (HashMap<String, Object> map : rowMsSmsNotifikasi) {
                                        Iterator it = map.entrySet().iterator();
                                        while (it.hasNext()) {
                                            int j = 0;
                                            Map.Entry pair = (Map.Entry) it.next();
                                            if (pair.getKey().equals("sms_script")) {
                                                smsScript = pair.getValue().toString();
                                            }
                                            it.remove(); // avoids a ConcurrentModificationException
                                        }
                                    }
                                    if (smsScript != null) {
                                        pesan = String.format(smsScript, Nominal, hp, idPelanggan, token, idtrx);
                                        new GcmNew().sent(idtrx, hp, pesan, 18, false, this.entityConfig);
                                    }

                                    // Update Status Account
                                    if (a[0].equals("INACTIVE") || a[0].equals("DORMANT")) {
                                        SQL = "UPDATE \"MsAgentAccount\" SET id_accountstatus=1 WHERE \"id_agentaccount\"=?;";
                                        stPgsql2 = connPgsql2.prepareStatement(SQL);
                                        stPgsql2.setString(1, NoKartuAgent);
                                        stPgsql2.executeUpdate();
                                    }
                                } else if (Jresponse.get("ACK").getAsString().equalsIgnoreCase("PENDING")) {
                                    jsonResp.put("id_Transaksi", idtrx);
                                    jsonResp.put("statusTRX", "PENDING");
                                    jsonResp.put("ACK", "OK");
                                    jsonResp.put("pesan", Jresponse.get("pesan").getAsString());
                                } else {
                                    updateTransaksi = "UPDATE \"TrTransaksi\" "
                                            + " SET \"StatusTRX\"='GAGAL', timestamprefund = now(), "
                                            + " \"StatusDeduct\"='NOK', \"StatusRefund\"='OK', \"StatusKomisi\"='NOK' WHERE id_trx=?;";
                                    stPgsql2 = connPgsql2.prepareStatement(updateTransaksi);
                                    stPgsql2.setString(1, idtrx);
                                    hasil = stPgsql2.executeUpdate();

                                    //refund agent
                                    //<editor-fold defaultstate="collapsed" desc="query refundAgent">
                                    String sql1 = "SELECT * FROM \"MsAgentAccount\""
                                            + " WHERE id_agentaccount = ? FOR UPDATE";
                                    stPgsql2 = connPgsql2.prepareStatement(sql1);
                                    stPgsql2.setString(1, NoKartuAgent);

                                    rsPgsql2 = stPgsql2.executeQuery();

                                    sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + Hargajual + " "
                                            + "WHERE id_agentaccount = ?";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setString(1, idagentaccount);
                                    hasil = stPgsql2.executeUpdate();
                                    //</editor-fold>

                                    // Update TMNStock Agent refund
                                    lb = "";
                                    //<editor-fold defaultstate="collapsed" desc="query getLastBalance">
                                    sql = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\" WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setString(1, NoKartuAgent);
                                    rsPgsql2 = stPgsql2.executeQuery();
                                    while (rsPgsql2.next()) {
                                        lb = rsPgsql2.getString("LastBalance");
                                    }

                                    //</editor-fold>
                                    updateTMNStock = "INSERT INTO \"TrStock\"("
                                            + " \"LastBalance\", \"TimeStamp\", \"StokType\", \"Nominal\", "
                                            + " id_transaksi, id_stock, \"TypeTrx\", \"True\", \"Agent\", \"Member\", \"Dealer\")"
                                            + " VALUES (?, now(), 'Agent', ?, ?, ?, 'REFUND', 0, 0, 0, 0);";
                                    stPgsql2 = connPgsql2.prepareStatement(updateTMNStock);
                                    stPgsql2.setLong(1, Long.parseLong(lb));
                                    stPgsql2.setLong(2, Long.parseLong(Hargajual));
                                    stPgsql2.setLong(3, Long.parseLong(id_transaksi));
                                    stPgsql2.setString(4, NoKartuAgent);

                                    stPgsql2.executeUpdate();

                                    jsonResp.put("id_Transaksi", idtrx);
                                    jsonResp.put("statusTRX", "GAGAL");
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan", Jresponse.get("pesan").getAsString());

                                    String pesan = "Transaksi pembelian token listrik Anda gagal. Nomor transaksi " + idtrx + ".";
                                    //insert notif
                                    sql = "INSERT INTO \"Notifikasi\" "
                                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                            + " VALUES (?, ?, ?,'FALSE',now())";
                                    stPgsql2 = connPgsql2.prepareStatement(sql);
                                    stPgsql2.setString(1, pesan);
                                    stPgsql2.setString(2, NoKartuAgent);
                                    stPgsql2.setString(3, idtrx);
                                    stPgsql2.executeUpdate();

                                    //send sms
                                    //get MsSmsNotification
                                    ArrayList<HashMap<String, Object>> rowMsSmsNotifikasi = new ArrayList<>();
                                    //<editor-fold defaultstate="collapsed" desc="query MsSmsNotification">
                                    query = "SELECT * FROM \"MsSMSNotification\" "
                                            + "WHERE \"id_tipetransaksi\" = 18 "
                                            + "and id_tipeaplikasi = 3 "
                                            + "and account_type = ? "
                                            + "and trx_status = ?";
                                    stPgsql = connPgsql.prepareStatement(query);
                                    stPgsql.setString(1, Constant.MEMBER);
                                    stPgsql.setString(2, Constant.StatusTrx.GAGAL.toString().toUpperCase());
                                    rsPgsql = stPgsql.executeQuery();
                                    ResultSetMetaData metaData = rsPgsql.getMetaData();
                                    int colCount = metaData.getColumnCount();
                                    while (rsPgsql.next()) {
                                        HashMap<String, Object> columns = new HashMap<>();
                                        for (int i = 1; i <= colCount; i++) {
                                            columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                                        }
                                        rowMsSmsNotifikasi.add(columns);
                                    }
                                    //</editor-fold>
                                    String smsScript = "";
                                    for (HashMap<String, Object> map : rowMsSmsNotifikasi) {
                                        Iterator it = map.entrySet().iterator();
                                        while (it.hasNext()) {
                                            int j = 0;
                                            Map.Entry pair = (Map.Entry) it.next();
                                            if (pair.getKey().equals("sms_script")) {
                                                smsScript = pair.getValue().toString();
                                            }
                                            it.remove(); // avoids a ConcurrentModificationException
                                        }
                                    }
                                    if (smsScript != null) {
                                        pesan = String.format(smsScript, Nominal, hp, idPelanggan, idtrx);
                                        new GcmNew().sent(idtrx, hp, pesan, 18, false, this.entityConfig);
                                    }
                                }

                                connPgsql2.commit();
                            } catch (Exception ex) {
                                try {
                                    connPgsql2.rollback();
                                } catch (SQLException ex1) {
                                    String s = Throwables.getStackTraceAsString(ex1);
                                    controllerLog.logErrorWriter(s);
                                }
                                String s = Throwables.getStackTraceAsString(ex);
                                controllerLog.logErrorWriter(s);
                            } finally {
                                try {
                                    if (rsPgsql2 != null) {
                                        rsPgsql2.close();
                                    }
                                    if (stPgsql2 != null) {
                                        stPgsql2.close();
                                    }
                                    if (connPgsql2 != null) {
                                        connPgsql2.close();
                                    }
                                } catch (SQLException ex) {
                                    String s = Throwables.getStackTraceAsString(ex);
                                    controllerLog.logErrorWriter(s);
                                }
                            }
                        } else {
                            jsonResp.put("id_Transaksi", idtrx);
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Transaksi Anda gagal");
                        }
                    } else if (validasiSaldo.equalsIgnoreCase("01")) { // Saldo tidak cukup
                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Saldo Anda tidak cukup");
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "PIN Anda salah");

                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Rekening Anda Diblokir. Silakan Hubungi Customer Care Kami");
            }
        } catch (Exception ex) {
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                String s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }

        return jsonResp.toString();
    }

    public void insertPointAgent(Connection connPgsql2, JSONObject jObjTrTransaksi, JSONObject jObjPoint) {
        JSONObject jObjTrPoint = new JSONObject();
        jObjTrPoint.put("accountType", "Agent");
        jObjTrPoint.put("typeTrx", "Deposit");
        jObjTrPoint.put("point", Integer.parseInt(jObjPoint.get("pointAgent").toString()));
        jObjTrPoint.put("idTrx", String.valueOf(jObjTrTransaksi.get("idTransaksi")));

        int hasil = 0;
        //<editor-fold defaultstate="collapsed" desc="query getLastPoint">
        PreparedStatement stPgsql2 = null;
        ResultSet rsPgsql2 = null;
        try {
            String sql = "SELECT COALESCE(\"max\"(id_trpoint), 0) AS jumlah FROM trpoint WHERE id_account = '" + jObjTrTransaksi.get("idAgentAccount").toString() + "'";
            stPgsql2 = connPgsql2.prepareStatement(sql);
            rsPgsql2 = stPgsql2.executeQuery();
            while (rsPgsql2.next()) {
                hasil = rsPgsql2.getInt("jumlah");
            }
            //System.out.println("Max ID" + hasil);
            if (hasil > 0) {
                sql = "SELECT * FROM trpoint WHERE id_trpoint = (SELECT COALESCE(\"max\"(id_trpoint), 0) FROM trpoint WHERE id_account = '" + jObjTrTransaksi.get("idAgentAccount").toString() + "');";
                stPgsql2 = connPgsql2.prepareStatement(sql);
                rsPgsql2 = stPgsql2.executeQuery();
                jObjTrPoint.put("idAccount", jObjTrTransaksi.get("idAgentAccount").toString());
                while (rsPgsql2.next()) {
                    jObjTrPoint.put("postPoint", 0);
                    jObjTrPoint.put("prePoint", rsPgsql2.getInt("lastpoint"));
                }

            } else {
                jObjTrPoint.put("idAccount", jObjTrTransaksi.get("idAgentAccount").toString());
                jObjTrPoint.put("prePoint", 0);
            }
            //</editor-fold>
            int LastPoint = Integer.parseInt(jObjTrPoint.get("prePoint").toString()) + Integer.parseInt(jObjTrPoint.get("point").toString());
            jObjTrPoint.put("postPoint", LastPoint);
            //insert TrPoint
            //<editor-fold defaultstate="collapsed" desc="query insertTrPoint">
            sql = "INSERT INTO \"public\".\"trpoint\" ( "
                    + "	\"id_transaksi\", "
                    + "	\"id_account\", "
                    + "	\"account_type\", "
                    + "	\"point\", "
                    + "	\"typetrx\", "
                    + "	\"point_before\", "
                    + "	\"lastpoint\", "
                    + "	\"timestamp\", "
                    + "	\"remark\", "
                    + "	\"id_prize\" "
                    + ") "
                    + "VALUES "
                    + "	( "
                    + "		  " + Integer.parseInt(jObjTrPoint.get("idTransaksi").toString()) + ", "
                    + "		  '" + jObjTrPoint.get("idAccount").toString() + "', "
                    + "		  '" + jObjTrPoint.get("accountType").toString() + "', "
                    + "		  '" + jObjTrPoint.get("point").toString() + "', "
                    + "		  '" + jObjTrPoint.get("typeTrx").toString() + "', "
                    + "		  '" + jObjTrPoint.get("prePoint").toString() + "', "
                    + "		  '" + jObjTrPoint.get("postPoint").toString() + "', "
                    + "		now(), "
                    + "		NULL, "
                    + "		NULL "
                    + "	);";

            stPgsql2 = connPgsql2.prepareStatement(sql);
            hasil = stPgsql2.executeUpdate();
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="query UpdateLastPoint">
            sql = "UPDATE \"MsAgentAccount\" SET lastpoint = ? WHERE id_agentaccount = ?";
            stPgsql2 = connPgsql2.prepareStatement(sql);
            stPgsql2.setInt(1, Integer.parseInt(jObjTrPoint.get("postPoint").toString()));
            stPgsql2.setString(2, jObjTrPoint.get("idAccount").toString());
            hasil = stPgsql2.executeUpdate();
            //</editor-fold>
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql2 != null) {
                    rsPgsql2.close();
                }
                if (stPgsql2 != null) {
                    stPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    public void insertPointMember(Connection connPgsql2, JSONObject jObjTrTransaksi, JSONObject jObjPoint) {
        JSONObject jObjTrPoint = new JSONObject();
        jObjTrPoint.put("accountType", "Member");
        jObjTrPoint.put("typeTrx", "Deposit");
        jObjTrPoint.put("point", Integer.parseInt(jObjPoint.get("pointMember").toString()));
        jObjTrPoint.put("idTrx", String.valueOf(jObjTrTransaksi.get("idTransaksi")));

        int hasil = 0;
        //<editor-fold defaultstate="collapsed" desc="query getLastPoint">
        PreparedStatement stPgsql2 = null;
        ResultSet rsPgsql2 = null;
        try {
            String sql = "SELECT COALESCE(\"max\"(id_trpoint), 0) AS jumlah FROM trpoint WHERE id_account = '" + jObjTrTransaksi.get("idMemberAccount").toString() + "'";
            stPgsql2 = connPgsql2.prepareStatement(sql);
            rsPgsql2 = stPgsql2.executeQuery();
            while (rsPgsql2.next()) {
                hasil = rsPgsql2.getInt("jumlah");
            }
            //System.out.println("Max ID" + hasil);
            if (hasil > 0) {
                sql = "SELECT * FROM trpoint WHERE id_trpoint = (SELECT COALESCE(\"max\"(id_trpoint), 0) FROM trpoint WHERE id_account = '" + jObjTrTransaksi.get("idMemberAccount").toString() + "');";
                stPgsql2 = connPgsql2.prepareStatement(sql);
                rsPgsql2 = stPgsql2.executeQuery();
                jObjTrPoint.put("idAccount", jObjTrTransaksi.get("idMemberAccount").toString());
                while (rsPgsql2.next()) {
                    jObjTrPoint.put("postPoint", 0);
                    jObjTrPoint.put("prePoint", rsPgsql2.getInt("lastpoint"));
                }

            } else {
                jObjTrPoint.put("idAccount", jObjTrTransaksi.get("idMemberAccount").toString());
                jObjTrPoint.put("prePoint", 0);
            }
            //</editor-fold>
            int LastPoint = Integer.parseInt(jObjTrPoint.get("prePoint").toString()) + Integer.parseInt(jObjTrPoint.get("point").toString());
            jObjTrPoint.put("postPoint", LastPoint);
            //insert TrPoint
            //<editor-fold defaultstate="collapsed" desc="query insertTrPoint">
            sql = "INSERT INTO \"public\".\"trpoint\" ( "
                    + "	\"id_transaksi\", "
                    + "	\"id_account\", "
                    + "	\"account_type\", "
                    + "	\"point\", "
                    + "	\"typetrx\", "
                    + "	\"point_before\", "
                    + "	\"lastpoint\", "
                    + "	\"timestamp\", "
                    + "	\"remark\", "
                    + "	\"id_prize\" "
                    + ") "
                    + "VALUES "
                    + "	( "
                    + "		  " + Integer.parseInt(jObjTrPoint.get("idTransaksi").toString()) + ", "
                    + "		  '" + jObjTrPoint.get("idAccount").toString() + "', "
                    + "		  '" + jObjTrPoint.get("accountType").toString() + "', "
                    + "		  '" + jObjTrPoint.get("point").toString() + "', "
                    + "		  '" + jObjTrPoint.get("typeTrx").toString() + "', "
                    + "		  '" + jObjTrPoint.get("prePoint").toString() + "', "
                    + "		  '" + jObjTrPoint.get("postPoint").toString() + "', "
                    + "		now(), "
                    + "		NULL, "
                    + "		NULL "
                    + "	);";

            stPgsql2 = connPgsql2.prepareStatement(sql);
            hasil = stPgsql2.executeUpdate();
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="query UpdateLastPoint">
            sql = "UPDATE \"MsMemberAccount\" SET lastpoint = ? WHERE id_memberaccount = ?";
            stPgsql2 = connPgsql2.prepareStatement(sql);
            stPgsql2.setInt(1, Integer.parseInt(jObjTrPoint.get("postPoint").toString()));
            stPgsql2.setString(2, jObjTrPoint.get("idAccount").toString());
            hasil = stPgsql2.executeUpdate();
            //</editor-fold>
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql2 != null) {
                    rsPgsql2.close();
                }
                if (stPgsql2 != null) {
                    stPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    public void updatePoint(Connection connPgsql2, JSONObject jObjTrTransaksi, int totalPoint, JSONObject jObjPromo) {
        //<editor-fold defaultstate="collapsed" desc="query UpdateTrTransaksi">
        PreparedStatement stPgsql2 = null;
        try {
            String Stringsql = "UPDATE \"TrTransaksi\" SET point = " + totalPoint + " , \"Remark\" = '" + jObjPromo.get("namaPromo").toString() + "' WHERE id_transaksi = " + jObjTrTransaksi.get("idTransaksi").toString();
            stPgsql2 = connPgsql2.prepareStatement(Stringsql);
            int UpdateTrTransaksi = stPgsql2.executeUpdate();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (stPgsql2 != null) {
                    stPgsql2.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }

    @Override
    public JSONArray getDetilharga(String idOperator, String nominal) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONArray jArrRow = new JSONArray();
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT " +
                    "	A.\"id_feesupplier\", " +
                    "	A.\"ProductCode\", " +
                    "	A.\"id_denom\", " +
                    "	A.\"HargaBeli\", " +
                    "	A.\"HargaJual\", " +
                    "	A.\"HargaCetak\", " +
                    "	A.\"HargaCetakMember\", " +
                    "	A.\"BiayaAdmin\", " +
                    "	A.\"TrueSpareFee\", " +
                    "	A.\"SupplierPrice\", " +
                    "	A.\"AgentCommType\", " +
                    "	A.\"AgentCommNom\", " +
                    "	A.\"DealerCommType\", " +
                    "	A.\"DealerCommNom\", " +
                    "	A.\"TrueFeeType\", " +
                    "	A.\"TrueFeeNom\", " +
                    "	A.\"MemberDiscType\", " +
                    "	A.\"id_supplier\", " +
                    "	\"MsOperator\".\"NamaOperator\", " +
                    "	CASE WHEN A.\"MemberDiscType\" = 'PERCENTAGE' THEN ((A.\"BiayaAdmin\" - A.\"SupplierPrice\") * A.\"MemberDiscNom\") / 100 ELSE A.\"MemberDiscNom\" END, " +
                    " 	A.\"BiayaAdmin\" - A.\"SupplierPrice\" AS \"MarginGross\" " +
                    "FROM " +
                    "	\"MsFeeSupplier\" A " +
                    "JOIN \"MsDenom\" ON A.id_denom = \"MsDenom\".id_denom " +
                    "JOIN \"MsOperator\" ON A.id_operator = \"MsOperator\".id_operator " +
                    "JOIN \"MsFee\" ON \"MsFee\".id_fee = A.id_fee " +
                    "WHERE " +
                    "	\"MsDenom\".\"Nominal\" = ? " +
                    "AND \"MsOperator\".id_operator = ? " +
                    "AND A.\"FlagActive\" = 'TRUE' " +
                    "AND A.\"HargaBeli\" IS NOT NULL " +
                    "AND \"MsFee\".id_tipeaplikasi = 3;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nominal);
            stPgsql.setLong(2, Long.parseLong(idOperator));
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            //convert resultset to jsonArray
            jsonObject = new JSONObject();
            int j = 0;
            while (rsPgsql.next()) {
                JSONObject jObjColumn = new JSONObject();
                for (int i = 1; i <= colCount; i++) {
                    if (rsPgsql.getObject(i) == null) {
                        jObjColumn.put(metaData.getColumnLabel(i), "");
                    } else {
                        jObjColumn.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                    }
                }
                jArrRow.add(j, jObjColumn);
                j = j++;
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jArrRow;
    }
}
