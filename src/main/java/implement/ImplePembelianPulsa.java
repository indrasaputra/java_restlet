/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import helper.Constant;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.simple.JSONObject;

import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcPembelianPulsa;
import koneksi.DatabaseUtilitiesPgsql;

/**
 *
 * @author Hasan
 */
public class ImplePembelianPulsa implements InterfcPembelianPulsa {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImplePembelianPulsa(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getDenom(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "select a.id_prefix, a.id_operator, b.\"NamaOperator\", c.\"HargaCetak\", d.\"Nominal\", e.id_tipetransaksi "
                    + "from \"MsPrefix\" a, \"MsOperator\" b, \"MsFeeSupplier\" c, \"MsDenom\" d, \"MsFee\" e "
                    + "where a.\"Prefix\" = ? "
                    + "and a.id_operator = b.id_operator "
                    + "and a.id_operator = c.id_operator "
                    + "and c.id_denom = d.id_denom "
                    + "and c.id_fee = e.id_fee "
                    + "and e.id_tipetransaksi = '17' "
                    + "and e.id_tipeaplikasi = '3' "
                    + "and a.\"FlagActive\" = 't' "
                    + "and b.\"FlagActive\" = 't' "
                    + "and c.\"FlagActive\" = 't' "
                    + "and d.\"FlagActive\" = 't' "
                    + "and e.\"FlagActive\" = 't' "
                    + "and b.\"TipeOperator\" = 'PULSA-PRA' "
                    + "ORDER BY d.\"Nominal\"::int";
            st = conn.prepareStatement(query);
            st.setString(1, objJson.get("prefix").toString());
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idPrefix", rs.getString("id_prefix"));
                jObj.put("idOperator", rs.getString("id_operator"));
                jObj.put("namaOperator", rs.getString("NamaOperator"));

                double hargaCetak = Double.parseDouble(rs.getString("HargaCetak"));
                String hargaCetakStr = String.format("%,.0f", hargaCetak);
                hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                jObj.put("hargaCetak", hargaCetakStr);

                double nominal = Double.parseDouble(rs.getString("Nominal"));
                String nominalStr = String.format("%,.0f", nominal);
                nominalStr = nominalStr.replaceAll(",", ".");
                jObj.put("nominal", nominalStr);
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("denom", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String bayarPulsaPrabayar(final JSONObject jsonReq) {
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        String nohp = jsonReq.get("handphone").toString();
        String nominal = jsonReq.get("denom").toString();
        nominal = nominal.replace(".", "");
        String idOperator = jsonReq.get("idOperator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("pin").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idTrx").toString();

        int countTrxByIdTrx = 0;
        //<editor-fold defaultstate="collapsed" desc="query getCountTrxByIdtrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT count(*) as juml FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idtrx);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                countTrxByIdTrx = rsPgsql.getInt("juml");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        if (countTrxByIdTrx == 1) {
            String statusTrx = "";
            //<editor-fold defaultstate="collapsed" desc="query getStatusTrxByIdtrx">
            databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
            connPgsql = null;
            stPgsql = null;
            rsPgsql = null;
            try {
                connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                String query = "SELECT \"StatusTRX\" FROM \"TrTransaksi\" where id_trx = ?;";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setString(1, idtrx);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    statusTrx = rsPgsql.getString("StatusTRX");
                }
            } catch (ParserConfigurationException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (rsPgsql != null) {
                        rsPgsql.close();
                    }
                    if (stPgsql != null) {
                        stPgsql.close();
                    }
                    if (connPgsql != null) {
                        connPgsql.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            if (statusTrx.equalsIgnoreCase("SUKSES")) {
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "Transaksi Anda sukes");
                jobjResp.put("status", statusTrx);
            } else if (statusTrx.equalsIgnoreCase("GAGAL")) {
                jobjResp.put("ACK", "NOK");
                jobjResp.put("pesan", "Transaksi Anda gagal");
                jobjResp.put("status", statusTrx);
            } else if (statusTrx.equalsIgnoreCase("PENDING")) {
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "Transaksi Anda sedang diproses");
                jobjResp.put("status", statusTrx);
            }
            return jobjResp.toString();
        } else if (countTrxByIdTrx > 1) {
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "Transaksi Anda gagal");
            jobjResp.put("status", "GAGAL");
            return jobjResp.toString();
        }

        int checkDuplicateTransaction = 0;
        //<editor-fold defaultstate="collapsed" desc="query checkDuplicateTransaction">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\" "
                    + "WHERE \"NoHPTujuan\" = ? "
                    + "AND \"Nominal\" = ? "
                    + "and id_operator = ?  "
                    + "AND \"TimeStamp\"::DATE = now()::DATE "
                    + "AND (id_memberaccount = ? OR id_agentaccount = ?)  "
                    + "AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nohp);
            stPgsql.setString(2, nominal);
            stPgsql.setInt(3, Integer.parseInt(idOperator));
            stPgsql.setString(4, username);
            stPgsql.setString(5, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                checkDuplicateTransaction = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        if (checkDuplicateTransaction > 0) {
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "Anda tidak dapat melakukan transaksi, silakan coba 24 jam kemudian");
            return jobjResp.toString();
        }

        long accountStatus = 0;
        String pinFromDb = "";
        long lastBalance = 0;
        //<editor-fold defaultstate="collapsed" desc="query getMsAgentAccountDetil">
        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        connPgsql = null;
        stPgsql = null;
        rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsAgentAccount\" "
                    + "where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                accountStatus = rsPgsql.getInt("id_accountstatus");
                pinFromDb = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
                lastBalance = rsPgsql.getLong("LastBalance");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        if (accountStatus == 1 || accountStatus == 2 || accountStatus == 3) {
            if (pin.equals(pinFromDb)) {
                if (lastBalance >= Integer.parseInt(hargaCetak)) {
                    int hargaBeli = 0;
                    int idDenom = 0;
                    int idSupplier = 0;
                    String namaOperator = null;
                    //<editor-fold defaultstate="collapsed" desc="query getDetilHarga">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "SELECT "
                                + "\"MsFeeSupplier\".\"HargaCetak\", "
                                + "\"MsOperator\".\"NamaOperator\", "
                                + "\"MsDenom\".\"Nominal\", "
                                + "\"MsFeeSupplier\".\"HargaJual\", "
                                + "\"MsFeeSupplier\".\"ProductCode\", "
                                + "\"MsDenom\".id_denom, "
                                + "\"MsFeeSupplier\".id_supplier, "
                                + "\"MsFeeSupplier\".\"HargaBeli\" "
                                + "FROM PUBLIC.\"MsOperator\", PUBLIC.\"MsDenom\", PUBLIC.\"MsFeeSupplier\", PUBLIC.\"MsFee\" "
                                + "WHERE \"MsFeeSupplier\".id_operator = \"MsOperator\".id_operator "
                                + "AND \"MsFeeSupplier\".id_fee = \"MsFee\".id_fee "
                                + "AND \"MsFeeSupplier\".id_denom = \"MsDenom\".id_denom "
                                + "AND \"MsDenom\".\"Nominal\" = ? "
                                + "AND \"MsFee\".id_tipeaplikasi = 3 "
                                + "AND \"MsOperator\".id_operator = ? "
                                + "AND \"MsFeeSupplier\".\"FlagActive\" = TRUE "
                                + "AND \"MsFee\".\"FlagActive\" = TRUE;";
                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setString(1, nominal);
                        stPgsql.setInt(2, Integer.parseInt(idOperator));
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            idDenom = rsPgsql.getInt("id_denom");
                            idSupplier = rsPgsql.getInt("id_supplier");
                            hargaBeli = rsPgsql.getInt("HargaBeli");
                            namaOperator = rsPgsql.getString("NamaOperator");
                        }
                    } catch (ParserConfigurationException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="query deduct balance">
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        String query = "update \"MsAgentAccount\" set \"LastBalance\" = \"LastBalance\" - ? "
                                + "where "
                                + "id_agentaccount = ?";
                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setInt(1, Integer.parseInt(hargaCetak));
                        stPgsql.setString(2, username);
                        stPgsql.executeUpdate();
                    } catch (ParserConfigurationException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>
                    String keterangan = "Pembelian pulsa " + namaOperator + " sebesar " + nominal + " Ke Nomor " + nohp;
                    int idTipeTransaksi = 17;

                    String idTransaksi = "0";
                    databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                    connPgsql = null;
                    stPgsql = null;
                    rsPgsql = null;
                    try {
                        //<editor-fold defaultstate="collapsed" desc="query insertTrTransaksi">
                        connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                        connPgsql.setAutoCommit(false);
                        String query = "INSERT INTO \"TrTransaksi\" ("
                                + "	\"id_tipetransaksi\","
                                + "	\"id_agentaccount\","
                                + "	\"id_denom\","
                                + "	\"NoHPTujuan\","
                                + "	\"Nominal\","
                                + "	\"StatusTRX\","
                                + "	\"StatusDeduct\","
                                + "	\"TypeTRX\","
                                + "	\"TimeStamp\","
                                + "	\"OpsVerifikasi\","
                                + "	\"Total\","
                                + "	\"id_trx\","
                                + "	\"StatusKomisi\","
                                + "	\"id_tipeaplikasi\","
                                + "	\"id_operator\","
                                + "	\"Keterangan\","
                                + "	\"StatusRefund\","
                                + "	\"StatusDana\","
                                + "	\"id_supplier\","
                                + "	\"Biaya\""
                                + ")"
                                + "VALUES"
                                + "	(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                        stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                        stPgsql.setLong(1, idTipeTransaksi);
                        stPgsql.setLong(2, Long.parseLong(username));
                        stPgsql.setLong(3, idDenom);
                        stPgsql.setString(4, nohp);
                        stPgsql.setString(5, nominal);
                        stPgsql.setString(6, "OPEN");
                        stPgsql.setString(7, "OK");
                        stPgsql.setString(8, "WITHDRAW");
                        stPgsql.setTimestamp(9, this.utilities.getDate());
                        stPgsql.setString(10, "PIN");
                        stPgsql.setInt(11, Integer.parseInt(hargaCetak));
                        stPgsql.setString(12, idtrx);
                        stPgsql.setString(13, "PENDING");
                        stPgsql.setLong(14, 3);
                        stPgsql.setLong(15, Long.parseLong(idOperator));
                        stPgsql.setString(16, keterangan);
                        stPgsql.setString(17, "NOK");
                        stPgsql.setString(18, "DEDUCT");
                        stPgsql.setLong(19, idSupplier);
                        stPgsql.setString(20, String.valueOf((Integer.parseInt(hargaCetak) - hargaBeli)));
                        stPgsql.executeUpdate();
                        rsPgsql = stPgsql.getGeneratedKeys();
                        while (rsPgsql.next()) {
                            idTransaksi = rsPgsql.getString(1);
                        }

                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="query getLastBalanceAgent">
                        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
                        Connection connPgsqlLb = null;
                        PreparedStatement stPgsqlLb = null;
                        ResultSet rsPgsqlLb = null;
                        try {
                            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
                            query = "select \"LastBalance\" from \"MsAgentAccount\" "
                                    + "where id_agentaccount = ?";
                            stPgsqlLb = connPgsqlLb.prepareStatement(query);
                            stPgsqlLb.setString(1, username);
                            rsPgsqlLb = stPgsqlLb.executeQuery();
                            while (rsPgsqlLb.next()) {
                                lastBalance = rsPgsqlLb.getLong("LastBalance");
                            }
                        } catch (ParserConfigurationException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        } catch (Exception ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        } finally {
                            try {
                                if (rsPgsqlLb != null) {
                                    rsPgsqlLb.close();
                                }
                                if (stPgsqlLb != null) {
                                    stPgsqlLb.close();
                                }
                                if (connPgsqlLb != null) {
                                    connPgsqlLb.close();
                                }
                            } catch (SQLException ex) {
                                String s = Throwables.getStackTraceAsString(ex);
                                controllerLog.logErrorWriter(s);
                            }
                        }
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="query insertTrStock">
                        query = "INSERT INTO \"TrStock\" ("
                                + "	\"LastBalance\","
                                + "	\"TimeStamp\","
                                + "	\"StokType\","
                                + "	\"Nominal\","
                                + "	\"id_transaksi\","
                                + "	\"id_stock\","
                                + "	\"TypeTrx\","
                                + "	\"True\","
                                + "	\"Member\","
                                + "	\"Agent\","
                                + "	\"Dealer\""
                                + ")"
                                + "VALUES"
                                + "	(?,?,?,?,?,?,?,?,?,?,?);";
                        stPgsql = connPgsql.prepareStatement(query);
                        stPgsql.setLong(1, lastBalance);
                        stPgsql.setTimestamp(2, this.utilities.getDate());
                        stPgsql.setString(3, "Agent");
                        stPgsql.setLong(4, Long.valueOf(hargaCetak));
                        stPgsql.setLong(5, Long.valueOf(idTransaksi));
                        stPgsql.setString(6, username);
                        stPgsql.setString(7, "WITHDRAW");
                        stPgsql.setLong(8, (long) 0);
                        stPgsql.setLong(9, (long) 0);
                        stPgsql.setLong(10, (long) 0);
                        stPgsql.setLong(11, (long) 0);
                        stPgsql.executeUpdate();
                        //</editor-fold>
                        connPgsql.commit();
                    } catch (SQLException ex) {
                        try {
                            connPgsql.rollback();
                        } catch (SQLException ex1) {
                            String s = Throwables.getStackTraceAsString(ex1);
                            controllerLog.logErrorWriter(s);
                        }
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } catch (ParserConfigurationException ex) {
                        try {
                            connPgsql.rollback();
                        } catch (SQLException ex1) {
                            String s = Throwables.getStackTraceAsString(ex1);
                            controllerLog.logErrorWriter(s);
                        }
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (rsPgsql != null) {
                                rsPgsql.close();
                            }
                            if (stPgsql != null) {
                                stPgsql.close();
                            }
                            if (connPgsql != null) {
                                connPgsql.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }

                    // Update Status Account
                    if (accountStatus == Constant.idStatusAccount.INACTIVE || accountStatus == Constant.idStatusAccount.DORMANT) {
                        //<editor-fold defaultstate="collapsed" desc="query updateStatusAccount">
                        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                        connPgsql = null;
                        stPgsql = null;
                        rsPgsql = null;
                        try {
                            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                            String query = "update \"MsAgentAccount\" set id_accountstatus = ?, "
                                    + "\"Remark\" = ? "
                                    + "where "
                                    + "id_agentaccount = ?";
                            stPgsql = connPgsql.prepareStatement(query);
                            stPgsql.setLong(1, Long.valueOf(1));
                            stPgsql.setTimestamp(2, null);
                            stPgsql.setString(3, username);
                            stPgsql.executeUpdate();
                        } catch (ParserConfigurationException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        } finally {
                            try {
                                if (rsPgsql != null) {
                                    rsPgsql.close();
                                }
                                if (stPgsql != null) {
                                    stPgsql.close();
                                }
                                if (connPgsql != null) {
                                    connPgsql.close();
                                }
                            } catch (SQLException ex) {
                                String s = Throwables.getStackTraceAsString(ex);
                                controllerLog.logErrorWriter(s);
                            }
                        }
                        //</editor-fold>
                    }

                    //hit api switching
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("id_trx", jsonReq.get("idTrx").toString());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            jSONObject.put("waktu", sdf.format(new Date()).toString());
                            String data = jSONObject.toString();
                            try {
                                String url = "";
                                //<editor-fold defaultstate="collapsed" desc="query getUrlMsParameter">
                                DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                                Connection connPgsql = null;
                                PreparedStatement stPgsql = null;
                                ResultSet rsPgsql = null;
                                try {
                                    connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
                                    String query = "SELECT * FROM ms_parameter "
                                            + "WHERE parameter_name = 'url_api_switching_prepaid'";
                                    stPgsql = connPgsql.prepareStatement(query);
                                    rsPgsql = stPgsql.executeQuery();
                                    while (rsPgsql.next()) {
                                        url = rsPgsql.getString("parameter_value");
                                    }
                                } catch (ParserConfigurationException ex) {
                                    String s = Throwables.getStackTraceAsString(ex);
                                    controllerLog.logErrorWriter(s);
                                } catch (SQLException ex) {
                                    String s = Throwables.getStackTraceAsString(ex);
                                    controllerLog.logErrorWriter(s);
                                } finally {
                                    try {
                                        if (rsPgsql != null) {
                                            rsPgsql.close();
                                        }
                                        if (stPgsql != null) {
                                            stPgsql.close();
                                        }
                                        if (connPgsql != null) {
                                            connPgsql.close();
                                        }
                                    } catch (SQLException ex) {
                                        String s = Throwables.getStackTraceAsString(ex);
                                        controllerLog.logErrorWriter(s);
                                    }
                                }
                                //</editor-fold>
                                //insert log
                                int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jSONObject);
                                JSONObject curlResp = utilities.curlPost(url, data, false);
                                //updateLog
                                utilities.updateLog(idLog, curlResp.toString());
                            } catch (Exception ex) {
                                String s = Throwables.getStackTraceAsString(ex);
                                controllerLog.logErrorWriter(s);
                            }
                        }
                    }).start();

                    jobjResp.put("ACK", "OK");
                    jobjResp.put("pesan", "Transaksi Anda sedang diproses");
                    jobjResp.put("status", "PENDING");
                    jobjResp.put("idtrx", idtrx);
                } else {
                    jobjResp.put("ACK", "NOK");
                    jobjResp.put("pesan", "Saldo Anda tidak cukup");
                    jobjResp.put("idtrx", idtrx);
                }
            } else {
                jobjResp.put("ACK", "NOK");
                jobjResp.put("pesan", "PIN Anda salah");
                jobjResp.put("idtrx", idtrx);
            }
        } else {
            jobjResp.put("ACK", "NOK");
            jobjResp.put("pesan", "Rekening Anda diblokir. Silakan hubungi customer care kami");
            jobjResp.put("idtrx", idtrx);
        }

        return jobjResp.toString();
    }

    @Override
    public int getCountIdTrx(JSONObject jsonReq) {
        int countTrxByIdTrx = 0;
        //<editor-fold defaultstate="collapsed" desc="query getCountTrxByIdtrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT count(*) as juml FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("idTrx").toString());
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                countTrxByIdTrx = rsPgsql.getInt("juml");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return countTrxByIdTrx;
    }

    @Override
    public String getStatusTrxByIdtrx(JSONObject jsonReq) {
        String statusTrx = "";
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrxByIdtrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"StatusTRX\" FROM \"TrTransaksi\" where id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("idTrx").toString());
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                statusTrx = rsPgsql.getString("StatusTRX");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return statusTrx;
    }

    @Override
    public int checkDuplicateTransaction(JSONObject jsonReq) {
        String nohp = jsonReq.get("handphone").toString();
        String nominal = jsonReq.get("denom").toString();
        nominal = nominal.replace(".", "");
        String idOperator = jsonReq.get("idOperator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("pin").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idTrx").toString();

        int checkDuplicateTransaction = 0;
        //<editor-fold defaultstate="collapsed" desc="query checkDuplicateTransaction">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\" "
                    + "WHERE \"NoHPTujuan\" = ? "
                    + "AND \"Nominal\" = ? "
                    + "and id_operator = ?  "
                    + "AND \"TimeStamp\"::DATE = now()::DATE "
                    + "AND (id_memberaccount = ? OR id_agentaccount = ?)  "
                    + "AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nohp);
            stPgsql.setString(2, nominal);
            stPgsql.setInt(3, Integer.parseInt(idOperator));
            stPgsql.setString(4, username);
            stPgsql.setString(5, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                checkDuplicateTransaction = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return checkDuplicateTransaction;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getMsAgentAccountDetil(JSONObject jsonReq) {
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        //<editor-fold defaultstate="collapsed" desc="query getMsAgentAccountDetil">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsAgentAccount\" "
                    + "where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, jsonReq.get("username").toString());
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            while (rsPgsql.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                }
                row.add(columns);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return row;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getDetilHarga(JSONObject jsonReq) {
        ArrayList<HashMap<String, Object>> row = new ArrayList<>();
        String nohp = jsonReq.get("handphone").toString();
        String nominal = jsonReq.get("denom").toString();
        nominal = nominal.replace(".", "");
        String idOperator = jsonReq.get("idOperator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("pin").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idTrx").toString();
        //<editor-fold defaultstate="collapsed" desc="query getDetilHarga">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT "
                    + "\"MsFeeSupplier\".\"HargaCetak\", "
                    + "\"MsOperator\".\"NamaOperator\", "
                    + "\"MsDenom\".\"Nominal\", "
                    + "\"MsFeeSupplier\".\"HargaJual\", "
                    + "\"MsFeeSupplier\".\"ProductCode\", "
                    + "\"MsDenom\".id_denom, "
                    + "\"MsFeeSupplier\".id_supplier, "
                    + "\"MsFeeSupplier\".\"HargaBeli\" "
                    + "FROM PUBLIC.\"MsOperator\", PUBLIC.\"MsDenom\", PUBLIC.\"MsFeeSupplier\", PUBLIC.\"MsFee\" "
                    + "WHERE \"MsFeeSupplier\".id_operator = \"MsOperator\".id_operator "
                    + "AND \"MsFeeSupplier\".id_fee = \"MsFee\".id_fee "
                    + "AND \"MsFeeSupplier\".id_denom = \"MsDenom\".id_denom "
                    + "AND \"MsDenom\".\"Nominal\" = ? "
                    + "AND \"MsFee\".id_tipeaplikasi = 3 "
                    + "AND \"MsOperator\".id_operator = ? "
                    + "AND \"MsFeeSupplier\".\"FlagActive\" = TRUE "
                    + "AND \"MsFee\".\"FlagActive\" = TRUE;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nominal);
            stPgsql.setInt(2, Integer.parseInt(idOperator));
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            while (rsPgsql.next()) {
                HashMap<String, Object> columns = new HashMap<>();
                for (int i = 1; i <= colCount; i++) {
                    columns.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                }
                row.add(columns);
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return row;
    }

    @Override
    public JSONObject deductBalance(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        
        String nohp = jsonReq.get("handphone").toString();
        String nominal = jsonReq.get("denom").toString();
        nominal = nominal.replace(".", "");
        String idOperator = jsonReq.get("idOperator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("pin").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idTrx").toString();
        //<editor-fold defaultstate="collapsed" desc="query deduct balance">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            if (Integer.signum(Integer.parseInt(hargaCetak)) > 0) {
                connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
                String query = "update \"MsAgentAccount\" set \"LastBalance\" = \"LastBalance\" - ? "
                        + "where "
                        + "id_agentaccount = ?";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setInt(1, Integer.parseInt(hargaCetak));
                stPgsql.setString(2, username);
                stPgsql.executeUpdate();
                jsonResp.put("ACK", "OK");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return jsonResp;
    }

    @Override
    public void insertTrTransTrStock(JSONObject jsonReq, ArrayList<HashMap<String, Object>> row) {
        String nohp = jsonReq.get("handphone").toString();
        String nominal = jsonReq.get("denom").toString();
        nominal = nominal.replace(".", "");
        String idOperator = jsonReq.get("idOperator").toString();
        String username = jsonReq.get("username").toString();
        String pin = jsonReq.get("pin").toString();
        String hargaCetak = jsonReq.get("hargaCetak").toString();
        hargaCetak = hargaCetak.replace(".", "");
        String idtrx = jsonReq.get("idTrx").toString();

        int hargaBeli = 0;
        int idDenom = 0;
        int idSupplier = 0;
        String namaOperator = null;
        for (HashMap<String, Object> map : row) {
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                int j = 0;
                Map.Entry pair = (Map.Entry) it.next();
                if (pair.getKey().equals("HargaBeli")) {
                    hargaBeli = Integer.parseInt(pair.getValue().toString());
                } else if (pair.getKey().equals("id_denom")) {
                    idDenom = Integer.parseInt(pair.getValue().toString());
                } else if (pair.getKey().equals("id_supplier")) {
                    idSupplier = Integer.parseInt(pair.getValue().toString());
                } else if (pair.getKey().equals("NamaOperator")) {
                    namaOperator = (String) pair.getValue();
                }
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        String keterangan = "Pembelian pulsa " + namaOperator + " sebesar " + nominal + " Ke Nomor " + nohp;
        int idTipeTransaksi = 17;

        String idTransaksi = "0";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            //<editor-fold defaultstate="collapsed" desc="query insertTrTransaksi">
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);
            String query = "INSERT INTO \"TrTransaksi\" ("
                    + "	\"id_tipetransaksi\","
                    + "	\"id_agentaccount\","
                    + "	\"id_denom\","
                    + "	\"NoHPTujuan\","
                    + "	\"Nominal\","
                    + "	\"StatusTRX\","
                    + "	\"StatusDeduct\","
                    + "	\"TypeTRX\","
                    + "	\"TimeStamp\","
                    + "	\"OpsVerifikasi\","
                    + "	\"Total\","
                    + "	\"id_trx\","
                    + "	\"StatusKomisi\","
                    + "	\"id_tipeaplikasi\","
                    + "	\"id_operator\","
                    + "	\"Keterangan\","
                    + "	\"StatusRefund\","
                    + "	\"StatusDana\","
                    + "	\"id_supplier\","
                    + "	\"Biaya\""
                    + ")"
                    + "VALUES"
                    + "	(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            stPgsql = connPgsql.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stPgsql.setLong(1, idTipeTransaksi);
            stPgsql.setLong(2, Long.parseLong(username));
            stPgsql.setLong(3, idDenom);
            stPgsql.setString(4, nohp);
            stPgsql.setString(5, nominal);
            stPgsql.setString(6, "OPEN");
            stPgsql.setString(7, "OK");
            stPgsql.setString(8, "WITHDRAW");
            stPgsql.setTimestamp(9, this.utilities.getDate());
            stPgsql.setString(10, "PIN");
            stPgsql.setInt(11, Integer.parseInt(hargaCetak));
            stPgsql.setString(12, idtrx);
            stPgsql.setString(13, "PENDING");
            stPgsql.setLong(14, 3);
            stPgsql.setLong(15, Long.parseLong(idOperator));
            stPgsql.setString(16, keterangan);
            stPgsql.setString(17, "NOK");
            stPgsql.setString(18, "DEDUCT");
            stPgsql.setLong(19, idSupplier);
            stPgsql.setString(20, String.valueOf((Integer.parseInt(hargaCetak) - hargaBeli)));
            stPgsql.executeUpdate();
            rsPgsql = stPgsql.getGeneratedKeys();
            while (rsPgsql.next()) {
                idTransaksi = rsPgsql.getString(1);
            }

            //</editor-fold>
            long lastBalance = 0;
            //<editor-fold defaultstate="collapsed" desc="query getLastBalanceAgent">
            DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
            Connection connPgsqlLb = null;
            PreparedStatement stPgsqlLb = null;
            ResultSet rsPgsqlLb = null;
            try {
                connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
                query = "select \"LastBalance\" from \"MsAgentAccount\" "
                        + "where id_agentaccount = ?";
                stPgsqlLb = connPgsqlLb.prepareStatement(query);
                stPgsqlLb.setString(1, username);
                rsPgsqlLb = stPgsqlLb.executeQuery();
                while (rsPgsqlLb.next()) {
                    lastBalance = rsPgsqlLb.getLong("LastBalance");
                }
            } catch (ParserConfigurationException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } catch (Exception ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            } finally {
                try {
                    if (rsPgsqlLb != null) {
                        rsPgsqlLb.close();
                    }
                    if (stPgsqlLb != null) {
                        stPgsqlLb.close();
                    }
                    if (connPgsqlLb != null) {
                        connPgsqlLb.close();
                    }
                } catch (SQLException ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="query insertTrStock">
            query = "INSERT INTO \"TrStock\" ("
                    + "	\"LastBalance\","
                    + "	\"TimeStamp\","
                    + "	\"StokType\","
                    + "	\"Nominal\","
                    + "	\"id_transaksi\","
                    + "	\"id_stock\","
                    + "	\"TypeTrx\","
                    + "	\"True\","
                    + "	\"Member\","
                    + "	\"Agent\","
                    + "	\"Dealer\""
                    + ")"
                    + "VALUES"
                    + "	(?,?,?,?,?,?,?,?,?,?,?);";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, lastBalance);
            stPgsql.setTimestamp(2, this.utilities.getDate());
            stPgsql.setString(3, "Agent");
            stPgsql.setLong(4, Long.valueOf(hargaCetak));
            stPgsql.setLong(5, Long.valueOf(idTransaksi));
            stPgsql.setString(6, username);
            stPgsql.setString(7, "WITHDRAW");
            stPgsql.setLong(8, (long) 0);
            stPgsql.setLong(9, (long) 0);
            stPgsql.setLong(10, (long) 0);
            stPgsql.setLong(11, (long) 0);
            stPgsql.executeUpdate();
            //</editor-fold>
            connPgsql.commit();
        } catch (SQLException ex) {
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                String s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (ParserConfigurationException ex) {
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                String s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    @Override
    public void updateStatusAccount(JSONObject jsonReq) {
        //<editor-fold defaultstate="collapsed" desc="query updateStatusAccount">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "update \"MsAgentAccount\" set id_accountstatus = ?, "
                    + "\"Remark\" = ? "
                    + "where "
                    + "id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.valueOf(1));
            stPgsql.setTimestamp(2, null);
            stPgsql.setString(3, jsonReq.get("username").toString());
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }

    @Override
    public void hitApiSwitcing(final JSONObject jsonReq) {
        //hit api switching
        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("id_trx", jsonReq.get("idTrx").toString());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                jSONObject.put("waktu", sdf.format(new Date()).toString());
                String data = jSONObject.toString();

                String url = "";
                int idSupplier = 0;

                DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                Connection connPgsql = null;
                PreparedStatement stPgsql = null;
                ResultSet rsPgsql = null;
                try {
                    //<editor-fold defaultstate="collapsed" desc="query getmsparam and hit apiSwitching">
                    connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
                    String paramName = "url_api_switching_prepaid";
                    String query = "SELECT * FROM ms_parameter "
                            + "WHERE parameter_name = ?";
                    stPgsql = connPgsql.prepareStatement(query);
                    stPgsql.setString(1, paramName);
                    rsPgsql = stPgsql.executeQuery();
                    while (rsPgsql.next()) {
                        url = rsPgsql.getString("parameter_value");
                    }

                    //insert log
                    int idLog = utilities.insertLog(jsonReq.get("idTrx").toString(), jSONObject);
                    JSONObject curlResp = utilities.curlPost(url, data, false);
                    //updateLog
                    utilities.updateLog(idLog, curlResp.toString());
                    //</editor-fold>
                } catch (Exception ex) {
                    String s = Throwables.getStackTraceAsString(ex);
                    controllerLog.logErrorWriter(s);
                } finally {
                    try {
                        if (rsPgsql != null) {
                            rsPgsql.close();
                        }
                        if (stPgsql != null) {
                            stPgsql.close();
                        }
                        if (connPgsql != null) {
                            connPgsql.close();
                        }
                    } catch (SQLException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    }
                }
            }
        }).start();
    }

    @Override
    public JSONArray getDetilharga(String idOperator, String nominal) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        JSONArray jArrRow = new JSONArray();
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT " +
                    "	A.\"id_feesupplier\", " +
                    "	A.\"ProductCode\", " +
                    "	A.\"id_denom\", " +
                    "	A.\"HargaBeli\", " +
                    "	A.\"HargaJual\", " +
                    "	A.\"HargaCetak\", " +
                    "	A.\"HargaCetakMember\", " +
                    "	A.\"BiayaAdmin\", " +
                    "	A.\"TrueSpareFee\", " +
                    "	A.\"SupplierPrice\", " +
                    "	A.\"AgentCommType\", " +
                    "	A.\"AgentCommNom\", " +
                    "	A.\"DealerCommType\", " +
                    "	A.\"DealerCommNom\", " +
                    "	A.\"TrueFeeType\", " +
                    "	A.\"TrueFeeNom\", " +
                    "	A.\"MemberDiscType\", " +
                    "	A.\"id_supplier\", " +
                    "	\"MsOperator\".\"NamaOperator\", " +
                    "	CASE WHEN A.\"MemberDiscType\" = 'PERCENTAGE' THEN ((A.\"BiayaAdmin\" - A.\"SupplierPrice\") * A.\"MemberDiscNom\") / 100 ELSE A.\"MemberDiscNom\" END, " +
                    " 	A.\"BiayaAdmin\" - A.\"SupplierPrice\" AS \"MarginGross\" " +
                    "FROM " +
                    "	\"MsFeeSupplier\" A " +
                    "JOIN \"MsDenom\" ON A.id_denom = \"MsDenom\".id_denom " +
                    "JOIN \"MsOperator\" ON A.id_operator = \"MsOperator\".id_operator " +
                    "JOIN \"MsFee\" ON \"MsFee\".id_fee = A.id_fee " +
                    "WHERE " +
                    "	\"MsDenom\".\"Nominal\" = ? " +
                    "AND \"MsOperator\".id_operator = ? " +
                    "AND A.\"FlagActive\" = 'TRUE' " +
                    "AND A.\"HargaBeli\" IS NOT NULL " +
                    "AND \"MsFee\".id_tipeaplikasi = 3;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nominal);
            stPgsql.setLong(2, Long.parseLong(idOperator));
            rsPgsql = stPgsql.executeQuery();
            ResultSetMetaData metaData = rsPgsql.getMetaData();
            int colCount = metaData.getColumnCount();
            //convert resultset to jsonArray
            jsonObject = new JSONObject();
            int j = 0;
            while (rsPgsql.next()) {
                JSONObject jObjColumn = new JSONObject();
                for (int i = 1; i <= colCount; i++) {
                    if (rsPgsql.getObject(i) == null) {
                        jObjColumn.put(metaData.getColumnLabel(i), "");
                    } else {
                        jObjColumn.put(metaData.getColumnLabel(i), rsPgsql.getObject(i));
                    }
                }
                jArrRow.add(j, jObjColumn);
                j = j++;
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jArrRow;
    }
}
