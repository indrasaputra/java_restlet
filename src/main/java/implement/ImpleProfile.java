/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONObject;

import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Constant;
import helper.Helper;
import helper.Utilities;
import interfc.InterfcProfile;
import koneksi.DatabaseUtilitiesPgsql;

/**
 *
 * @author Hasan
 */
public class ImpleProfile implements InterfcProfile {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleProfile(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getProfile(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        //from request
        String reqUserName = (String) objJson.get("username");
        String nama, TGL, HP, alamat, idAgent, fotoDiri;
        nama = TGL = HP = alamat = idAgent = fotoDiri = null;

        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String datadiri = null;
            String query = "";
            //cari info diri
            if (reqUserName.charAt(6) == '1') {
                query = "SELECT"
                        + " \"MsInformasiAgent\".\"Nama\","
                        + " \"MsInformasiAgent\".\"TanggalLahir\","
                        + " \"MsInformasiAgent\".\"Alamat\","
                        + " \"MsInformasiAgent\".\"id_agent\","
                        + " \"MsInformasiAgent\".\"fotodiri\","
                        + " \"MsInformasiAgent\".\"Handphone\""
                        + " FROM"
                        + " \"MsInformasiAgent\""
                        + " INNER JOIN \"MsAgentAccount\" ON \"MsInformasiAgent\".id_agent = \"MsAgentAccount\".id_agent"
                        + " WHERE"
                        + " \"MsAgentAccount\".id_agentaccount = ? AND \"MsInformasiAgent\".id_tipeinformasiagent = 1;";

            } //cari info toko
            else {
                query = "SELECT"
                        + " \"MsInformasiAgent\".\"Nama\","
                        + " \"MsInformasiAgent\".\"TanggalLahir\","
                        + " \"MsInformasiAgent\".\"Alamat\","
                        + " \"MsInformasiAgent\".\"id_agent\","
                        + " \"MsInformasiAgent\".\"fotodiri\","
                        + " \"MsInformasiAgent\".\"Handphone\""
                        + " FROM"
                        + " \"MsInformasiAgent\""
                        + " INNER JOIN \"MsAgentAccount\" ON \"MsInformasiAgent\".id_agent = \"MsAgentAccount\".id_agent"
                        + " WHERE"
                        + " \"MsAgentAccount\".id_agent = ? AND \"MsInformasiAgent\".id_tipeinformasiagent = 2;";
            }
            st = conn.prepareStatement(query);
            st.setString(1, reqUserName);
            rs = st.executeQuery();
            while (rs.next()) {
                HP = rs.getString("Handphone");
                TGL = rs.getString("TanggalLahir");
                nama = rs.getString("Nama");
                alamat = rs.getString("Alamat");
                idAgent = rs.getString("id_agent");
                fotoDiri = rs.getString("fotodiri");
            }

            if (HP != null && !HP.isEmpty()) {
            } else {
                HP = "-";
            }
            if (TGL != null && !TGL.isEmpty()) {
            } else {
                TGL = "-";
            }
            if (nama != null && !nama.isEmpty()) {
            } else {
                nama = "-";
            }
            if (alamat != null && !alamat.isEmpty()) {
            } else {
                alamat = "-";
            }

            jobjResp.put("Username", idAgent);
            jobjResp.put("Alamat", alamat);
            jobjResp.put("idAgentAccount", reqUserName);
            jobjResp.put("Nama", nama);
            jobjResp.put("TanggalLahir", TGL);
            jobjResp.put("NoHandphone", HP);
            jobjResp.put("fotoDiri", fotoDiri);
            jobjResp.put("ACK", "OK");
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String[] StatusRekeningAgent(String username) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String[] status = new String[4];
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String datadiri = null;

            String cekStatusRekening = "SELECT \"MsAccountStatus\".\"NamaAccountStatus\","
                    + " \"MsAgentAccount\".\"StatusKartu\",\"MsAgentAccount\".\"PIN\","
                    + " \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\","
                    + " public.\"MsAccountStatus\" WHERE"
                    + " \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus AND"
                    + " \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            st = conn.prepareStatement(cekStatusRekening);
            st.setString(1, username);
            rs = st.executeQuery();
            while (rs.next()) {
                status[0] = rs.getString("NamaAccountStatus");
                status[1] = rs.getString("LastBalance");
                status[2] = rs.getString("StatusKartu");
                status[3] = this.encDec.decryptDbVal(rs.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return status;
    }

    @Override
    public String changePin(String username, String pinBaru) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        JSONObject jObj = new JSONObject();
        jObj.put("ACK", "NOK");
        jObj.put("pesan", "Gagal mengganti PIN");
        jObj.put("timestamp", utilities.getDate().toString());
        jObj.put("statusTRX", "GAGAL");

        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String datadiri = null;

            String updatepin = "UPDATE \"MsAgentAccount\" SET \"PIN\" = ?,"
                    + "\"flag_change_pin\" = TRUE WHERE id_agentaccount = ?";
            st = conn.prepareStatement(updatepin);
            st.setString(1, this.encDec.encryptDbVal(pinBaru, this.entityConfig.getEncKeyDb()));
            st.setString(2, username);
            st.executeUpdate();

            jObj.put("ACK", "OK");
            jObj.put("pesan", utilities.getWording("CHANGE_PIN_OK"));
            jObj.put("timestamp", utilities.getDate().toString());
            jObj.put("statusTRX", "SUKSES");
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jObj.toString();
    }

    @Override
    public String requestToken(String requestBody) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        JSONObject response = new JSONObject();
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String datadiri = null;

            String sql = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'tokenotp_url';";
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            String url = "";
            while (rs.next()) {
                url = rs.getString("parameter_value");
            }

            response = utilities.curlPost(url, requestBody, false);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            response.put("ACK", String.valueOf(Constant.ACK.NOK));
            response.put("pesan", "Terjadi kesalahan, silakan request token kembali");
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            response.put("ACK", String.valueOf(Constant.ACK.NOK));
            response.put("pesan", "Terjadi kesalahan, silakan request token kembali");
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return response.toString();
    }

    @Override
    public String validasiTokenReguler(String username, String otp) {
        JSONObject curlResponse = new JSONObject();
        //<editor-fold defaultstate="collapsed" desc="query validasiTokenReguler">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'tokenotp_url';";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            String url = "";
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
            String requestBody = "{\"id_account\":\"" + username + "\",\"otp\":\"" + otp + "\",\"type\":\"token_otentifikasi_reguler\"}";
            curlResponse = utilities.curlPost(url, requestBody, false);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return curlResponse.toString();
    }

    @Override
    public void updatePin(String username, String pinEncrypt) {
        //<editor-fold defaultstate="collapsed" desc="query updatePin">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "UPDATE \"MsAgentAccount\" SET \"PIN\"='" + pinEncrypt + "'"
                    + " WHERE \"MsAgentAccount\".id_agentaccount = '" + username + "'";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.executeUpdate();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
    }
}
