package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcQurban;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.*;
import java.util.UUID;

/**
 * Created by Fino Indrasaputra on 11/07/2018.
 */
public class ImpleQurban implements InterfcQurban {
    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleQurban(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getKelurahan(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        try {
            //System.out.println("pass default 123456 : " + this.encDec.encryptDbVal(reqPass, this.entityConfig.getEncKeyDb()));

            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //1, check usernya
            String query = "select "
                    + " a.id_kelurahan, a.\"NamaKelurahan\", "
                    + " b.id_kecamatan, b.\"NamaKecamatan\", "
                    + " c.id_kota, c.\"NamaKota\", "
                    + " d.id_provinsi, d.\"NamaProvinsi\" "
                    + " from \"MsKelurahan\" a, \"MsKecamatan\" b, \"MsKota\" c, \"MsProvinsi\" d "
                    + " where a.id_kecamatan = b.id_kecamatan "
                    + " and b.id_kota = c.id_kota "
                    + " and c.id_provinsi = d.id_provinsi "
                    + " and a.\"NamaKelurahan\" ILIKE ?"
                    + " ORDER BY a.\"NamaKelurahan\"  ASC;";
            st = conn.prepareStatement(query);
            st.setString(1, objJson.get("search").toString() + "%");
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idKelurahan", rs.getString("id_kelurahan"));
                jObj.put("namaKelurahan", rs.getString("NamaKelurahan"));
                jObj.put("idKecamatan", rs.getString("id_kecamatan"));
                jObj.put("namaKecamatan", rs.getString("NamaKecamatan"));
                jObj.put("idKota", rs.getString("id_kota"));
                jObj.put("namaKota", rs.getString("NamaKota"));
                jObj.put("idProvinsi", rs.getString("id_provinsi"));
                jObj.put("namaProvinsi", rs.getString("NamaProvinsi"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("kelurahan", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String getProduk(JSONObject jsonReq) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "NOK");
        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            String query = "SELECT" +
                    " a .id_operator," +
                    " a.\"NamaOperator\"," +
                    " b.\"id_feesupplier\"," +
                    " b.\"HargaCetak\"," +
                    " b.\"HargaBeli\"," +
                    " b.\"HargaJual\"," +
                    " b.\"BiayaAdmin\"," +
                    " b.\"ProductCode\"," +
                    " b.\"id_denom\"," +
                    " c.\"Nominal\"," +
                    " c.\"Description\"," +
                    " c.\"id_denom\"," +
                    " d.id_tipetransaksi," +
                    " d.id_tipeaplikasi" +
                    " FROM" +
                    " \"MsOperator\" a," +
                    " \"MsFeeSupplier\" b," +
                    " \"MsDenom\" c," +
                    " \"MsFee\" d" +
                    " WHERE" +
                    " a.id_operator = '228'" +
                    " AND d.id_tipetransaksi = '71'" +
                    " AND d.id_tipeaplikasi = '3'" +
                    " And a.id_operator = b.id_operator" +
                    " AND b.id_denom = c.id_denom" +
                    " And b.id_fee = d.id_fee" +
                    " AND a.\"FlagActive\" = 't'" +
                    " AND b.\"FlagActive\" = 't'" +
                    " AND c.\"FlagActive\" = 't'" +
                    " AND d.\"FlagActive\" = 't'"; //+
                    //" ORDER BY" +
                    //" c.\"Nominal\" "; //:: INT";
            st = conn.prepareStatement(query);
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj;
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idOperator", rs.getString("id_operator"));
                jObj.put("namaOperator", rs.getString("NamaOperator"));
                jObj.put("idFeeSupplier", rs.getString("id_feesupplier"));
                jObj.put("productCode", rs.getString("ProductCode"));
                jObj.put("description", rs.getString("Nominal")); //("Description"));
                jObj.put("idDenom", rs.getString("id_denom"));

                double hargaCetak = Double.parseDouble(rs.getString("HargaCetak"));
                String hargaCetakStr = String.format("%,.0f", hargaCetak);
                hargaCetakStr = hargaCetakStr.replaceAll(",", ".");
                jObj.put("hargaCetak", hargaCetakStr);

                double hargaBeli = Double.parseDouble(rs.getString("HargaBeli"));
                String hargaBeliStr = String.format("%,.0f", hargaBeli);
                hargaBeliStr = hargaBeliStr.replaceAll(",", ".");
                jObj.put("hargaBeli", hargaBeliStr);

                double biayaAdmin = Double.parseDouble(rs.getString("BiayaAdmin"));
                String biayaAdminStr = String.format("%,.0f", biayaAdmin);
                biayaAdminStr = biayaAdminStr.replaceAll(",", ".");
                jObj.put("biayaAdmin", biayaAdminStr);

                /*double nominal = Double.parseDouble(rs.getString("Nominal"));
                String nominalStr = String.format("%,.0f", nominal);
                nominalStr = nominalStr.replaceAll(",", ".");
                jObj.put("nominal", nominalStr);*/

                jArr.add(jObj);
            }
            jobjResp.put("produk", jArr);

            jobjResp.put("ACK", "OK");
            jobjResp.put("pesan", "OK");
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public int gettrxid(String trxid) {
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query gettrxid">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select count(*) from \"TrTransaksi\" where id_trx = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return count;
    }

    @Override
    public String[] getStatusTrx(String trxid) {
        String status[] = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT"
                    + "  \"TrTransaksi\".\"CustomerReference\", \"TrTransaksi\".\"NoHPTujuan\", \"TrTransaksi\".\"NoRekTujuan\","
                    + "  \"TrTransaksi\".\"StatusTRX\", \"TrTransaksi\".\"id_pelanggan\" FROM"
                    + "  public.\"TrTransaksi\""
                    + "  WHERE \"TrTransaksi\".id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("StatusTRX");
                status[1] = rsPgsql.getString("CustomerReference");
                status[2] = rsPgsql.getString("NoHPTujuan");
                status[3] = rsPgsql.getString("NoRekTujuan");
                status[4] = rsPgsql.getString("id_pelanggan");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public boolean CekIsDuplicate(String idPelanggan, String id_operator, String idaccount) {
        boolean cek = false;
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query CekIsEmptyPostpaid">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"count\"(*) from \"TrTransaksi\""
                    + " WHERE \"id_pelanggan\" = ?"
                    + " and id_operator = ?"
                    + " AND \"TimeStamp\"::DATE = now()::DATE "
                    + " AND (id_memberaccount = ? OR id_agentaccount = ?)"
                    + " AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, idPelanggan);
            stPgsql.setLong(2, Long.parseLong(id_operator));
            stPgsql.setString(3, idaccount);
            stPgsql.setString(4, idaccount);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
            if (count == 0) {
                cek = true;
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return cek;
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String NoKartu) {
        String[] status = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"MsAccountStatus\".\"NamaAccountStatus\""
                    + " ,\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\""
                    + " ,\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\""
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\""
                    + " WHERE \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus"
                    + " AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, NoKartu);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("NamaAccountStatus");
                status[1] = rsPgsql.getString("LastBalance");
                status[2] = rsPgsql.getString("StatusKartu");
                status[3] = rsPgsql.getString("id_agentaccount");
                status[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public int[] getComission(int idOperator, int idDenom) {
        int[] comission = new int[7];
        //<editor-fold defaultstate="collapsed" desc="query getComission">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsFeeSupplier\" a, \"MsFee\" b, \"MsDenom\" c"
                    + " where a.id_fee = b.id_fee"
                    + " AND a.id_denom = c.id_denom"
                    + " and a.id_operator = ?"
                    + " and b.id_tipeaplikasi = '3'"
                    + " and a.\"FlagActive\" = true"
                    + " and b.\"FlagActive\" = true"
                    + " AND c.id_denom = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, idOperator);
            stPgsql.setLong(2, idDenom);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                comission[0] = rsPgsql.getInt("BiayaAdmin");    // BiayaAdmin
                comission[1] = rsPgsql.getInt("TrueSpareFee");    // TrueSpareFee
                comission[2] = rsPgsql.getInt("SupplierPrice"); // SupplierPrice
                comission[3] = rsPgsql.getInt("AgentCommNom");    // AgentCommNom
                comission[4] = rsPgsql.getInt("DealerCommNom"); // DealerCommNom
                comission[5] = rsPgsql.getInt("TrueFeeNom");    // TrueFeeNom
                comission[6] = rsPgsql.getInt("MemberDiscNom"); // MemberDiscNom
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return comission;
    }

    @Override
    public String isSaldoCukup(String idAccount, int total) {
        String hasil = "--";
        //<editor-fold defaultstate="collapsed" desc="query isSaldoCukup">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            String bit7 = idAccount.substring(6, 7);
            String tipeAkun = "AGENT";
            String q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            if (q.trim().length() != 0) {
                long saldo = 0;
                stPgsql = connPgsql.prepareStatement(q);
                stPgsql.setString(1, idAccount);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    saldo = rsPgsql.getLong("saldo");
                }
                if (saldo >= total) {
                    hasil = "00";        //BALANCEINSUFICIENT CUKUP
                } else {
                    hasil = "01";        //BALANCEINSUFICIENT TIDAK CUKUP
                }
            }
            connPgsql.commit();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return hasil;
    }

    @Override
    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, int[] com) {
        String username = jsonReq.get("username").toString();
        String idtrx = jsonReq.get("idtrx").toString();
        String produkCode = jsonReq.get("produk").toString();
        String hargaCetak = jsonReq.get("produkCetak").toString();
        String nominal = jsonReq.get("produkBeli").toString();
        String biayaAdmin = jsonReq.get("produkAdmin").toString();
        String nama = jsonReq.get("nama").toString();
        //String ktp = jsonReq.get("ktp").toString();
        String alamat = jsonReq.get("alamat").toString();
        String kelurahan = jsonReq.get("kelurahan").toString();
        //String jenisKelamin = jsonReq.get("jenisKelamin").toString();
        String noHp = jsonReq.get("noHp").toString();
        //String tglLahir = jsonReq.get("tglLahir").toString();
        //String email = "";
        String email = jsonReq.get("email").toString();
        String idFeeSupplier = jsonReq.get("idFeeSupplier").toString();

        int saldo = Integer.parseInt(rekeningAgentNonEDC[1]);
        String stattrx = "SUKSES";
        String strstatdeduct;
        String productcode = null;
        String id_transaksi = null;
        int idSupplier = 0;
        //int idFeeSupplier = 0;
        String idOperator = "228";
        String idTipeTransaksi = "71";
        String idTipeAplikasi = "3";

        //<editor-fold defaultstate="collapsed" desc="query prosesBayar">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
            String sql = "select b.id_supplier, b.id_feesupplier"
                    + " from \"MsFee\" a, \"MsFeeSupplier\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_tipeaplikasi = ?"
                    + " and a.id_tipetransaksi = ?"
                    + " and b.id_operator = ?"
                    + " and a.\"FlagActive\" = TRUE"
                    + " and b.\"FlagActive\" = TRUE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(idTipeAplikasi));
            stPgsql.setLong(2, Long.parseLong(idTipeTransaksi));
            stPgsql.setLong(3, Long.parseLong(idOperator));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idSupplier = rsPgsql.getInt("id_supplier");
                //idFeeSupplier = rsPgsql.getInt("id_feesupplier");
            }
            //</editor-fold>
            if (idSupplier == 0) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                return jsonResp;
            }

            //<editor-fold defaultstate="collapsed" desc="query getProductCode">
            String query = "SELECT \"ProductCode\" " + "FROM \"MsFeeSupplier\" " + "WHERE id_operator = ?"
                    + " AND \"FlagActive\" = 'TRUE'"
                    + " AND id_supplier = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setLong(1, Long.parseLong(idOperator));
            stPgsql.setLong(2, idSupplier);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                productcode = rsPgsql.getString("ProductCode");
            }
            //</editor-fold>
            if (productcode == null && productcode.isEmpty()) {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Mohon maaf, produk tidak tersedia");
                return jsonResp;
            }

            // deduct agent
            //<editor-fold defaultstate="collapsed" desc="query deduct agent">
            sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            long lastBalance = 0;
            while (rsPgsql.next()) {
                lastBalance = rsPgsql.getLong("LastBalance");
            }
            if (lastBalance < Long.parseLong(hargaCetak)) {
                strstatdeduct = "NOK";
            } else {
                //check negative number
                if (Integer.signum(Integer.parseInt(hargaCetak)) > 0) {
                    String deductBalance = "UPDATE \"MsAgentAccount\""
                            + " SET \"LastBalance\"= \"LastBalance\" - ? WHERE \"id_agentaccount\"=?;";
                    stPgsql = connPgsql.prepareStatement(deductBalance);
                    stPgsql.setLong(1, Long.parseLong(hargaCetak));
                    stPgsql.setString(2, username);
                    int status = stPgsql.executeUpdate();
                    if (status == 1) {
                        strstatdeduct = "OK";
                    } else {
                        strstatdeduct = "NOK";
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Transaksi gagal");
                    return jsonResp;
                }
            }
            //</editor-fold>
            if (strstatdeduct.equalsIgnoreCase("OK")) {
                String id_dealer = "0";
                //<editor-fold defaultstate="collapsed" desc="query getIdDealer">
                sql = "SELECT \"MsAgentAccount\".id_dealer"
                        + " FROM \"MsAgentAccount\""
                        + " JOIN \"MsAgent\" ON \"MsAgentAccount\".id_agent = \"MsAgent\".id_agent"
                        + " WHERE \"MsAgentAccount\".id_agentaccount = ?"
                        + " AND \"MsAgentAccount\".id_dealer = \"MsAgent\".id_dealer";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, username);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    id_dealer = rsPgsql.getString("id_dealer");
                }

                //</editor-fold>
                String Keterangan = "Pembelian " + produkCode + " atas nama " + nama + " " + noHp + " seharga Rp." + hargaCetak;
                //<editor-fold defaultstate="collapsed" desc="query insert TrTransaksi">
                String updateTransaksi = "INSERT INTO \"TrTransaksi\""
                        + " ( id_tipetransaksi, id_agentaccount, id_pelanggan, id_supplier, \"Nominal\", \"StatusTRX\", \"StatusDeduct\", \"StatusRefund\", \"TypeTRX\", \"OpsVerifikasi\", \"Total\", id_trx, \"StatusKomisi\", \"TimeStamp\", \"Keterangan\", id_tipeaplikasi, id_operator, \"Biaya\", id_dealer, id_feesupplier)"
                        + " VALUES (?,?,?,?,?,'OPEN',?,'OPEN','WITHDRAW','PIN',?,?,'OPEN',now(),?,3,?,?,?,?)";

                stPgsql = connPgsql.prepareStatement(updateTransaksi, Statement.RETURN_GENERATED_KEYS);
                stPgsql.setLong(1, Long.parseLong(idTipeTransaksi));
                stPgsql.setString(2, username);
                stPgsql.setString(3, noHp);
                stPgsql.setLong(4, idSupplier);
                stPgsql.setString(5, nominal);
                stPgsql.setString(6, strstatdeduct);
                stPgsql.setLong(7, Long.parseLong(hargaCetak));
                stPgsql.setString(8, idtrx);
                stPgsql.setString(9, Keterangan);
                stPgsql.setLong(10, Long.parseLong(idOperator));
                stPgsql.setString(11, biayaAdmin);
                stPgsql.setLong(12, Long.parseLong(id_dealer));
                stPgsql.setLong(13, Long.parseLong(idFeeSupplier));
                int hasil = stPgsql.executeUpdate();
                id_transaksi = null;
                if (hasil > 0) {
                    rsPgsql = stPgsql.getGeneratedKeys();
                    while (rsPgsql.next()) {
                        id_transaksi = rsPgsql.getString(1);
                    }
                }
                //</editor-fold>
                // update tr transaksi
                //<editor-fold defaultstate="collapsed" desc="query update trtransaksi">
                updateTransaksi = "UPDATE \"TrTransaksi\""
                        + " SET \"StatusTRX\" = ?, \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'OK' , \"StatusDana\" = 'DEDUCT', \"hargabeli\" = ?, \"hargajual\" = ?, \"Biaya\" = ?, timestampupdatetrx = now(), id_trxsupplier = ?"
                        + " WHERE id_trx = ?";
                stPgsql = connPgsql.prepareStatement(updateTransaksi);
                stPgsql.setString(1, stattrx);
                stPgsql.setLong(2, Long.parseLong(nominal));
                stPgsql.setLong(3, Long.parseLong(hargaCetak));
                stPgsql.setString(4, biayaAdmin);
                stPgsql.setString(5, "");
                stPgsql.setString(6, idtrx);
                stPgsql.executeUpdate();
                int status = 0;
                //</editor-fold>
                // UpdateTMNStock agent deduct
                //<editor-fold defaultstate="collapsed" desc="query TrStock">
                String sqlInsertMsTMNStock = "INSERT"
                        + " INTO \"TrStock\" (\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                        + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";
                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                stPgsql.setLong(1, (saldo - Integer.parseInt(hargaCetak)));
                stPgsql.setLong(2, Long.parseLong(hargaCetak));
                stPgsql.setString(3, username);
                stPgsql.setLong(4, Long.parseLong(id_transaksi));
                status = stPgsql.executeUpdate();
                //</editor-fold>

                int margin = 0;
                int komAgent = 0, komTrue = 0, komDealer = 0;
                margin = Integer.parseInt(hargaCetak)
                        - Integer.parseInt(nominal);
                int periodeBayar = 1;
                komTrue = com[5] * periodeBayar;
                komAgent = com[3] * periodeBayar;
                komDealer = com[4] * periodeBayar;

                // update TMNStock true
                //<editor-fold defaultstate="collapsed" desc="query update tmnstock">
                sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                        + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                        + " VALUES (123456, now(), 0, True, ?, 0, 1, 'DEPOSIT', ?, 0);";
                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                stPgsql.setLong(1, Integer.parseInt(nominal));
                stPgsql.setLong(2, Long.parseLong(id_transaksi));
                stPgsql.executeUpdate();
                //</editor-fold>

                String pesan = "Transaksi pembelian " + produkCode + " atas nama " + nama + " Sukses dengan nomor transaksi " + idtrx;
                jsonResp.put("ACK", "OK");
                jsonResp.put("pesan", pesan);
                jsonResp.put("timestamp", utilities.getDate().toString());
                jsonResp.put("statusTRX", stattrx);
                jsonResp.put("idtrx", idtrx);
                jsonResp.put("noHp", noHp);
                jsonResp.put("jumlahtagihan", nominal);
                jsonResp.put("jumlahadm", biayaAdmin);
                jsonResp.put("jumlahbayar", hargaCetak);
                //hit api iklan
                String respIklan = utilities.getIklan(idTipeTransaksi,productcode, idtrx);
                JSONObject jObjRespIklan = (JSONObject) parser.parse(respIklan);
                jsonResp.put("iklan", jObjRespIklan);
                //insert notif
                //<editor-fold defaultstate="collapsed" desc="query insert notif">
                sql = "INSERT INTO \"Notifikasi\" "
                        + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                        + " VALUES (?, ?, ?,'FALSE',now())";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, pesan);
                stPgsql.setString(2, username);
                stPgsql.setString(3, idtrx);
                stPgsql.executeUpdate();
                //</editor-fold>

                //insert data customer to db
                sqlInsertMsTMNStock = "insert " +
                        " into tr_customer_data (company_id, applicant_full_name, applicant_hp_number, applicant_address, applied_timestamp, applicant_email, id_feesupplier, id_trx) " +
                        " VALUES ('3', ?, ?, ?, now(),?,?,?);";

                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                stPgsql.setString(1, nama);
                stPgsql.setString(2, noHp);
                stPgsql.setString(3, alamat);
                //stPgsql.setString(4, ktp);
                //tglLahir = utilities.convertFormatDate("dd-MM-yyyy","yyyy-MM-dd",tglLahir);
                //stPgsql.setDate(5, Date.valueOf(tglLahir));
                stPgsql.setString(4, email);
                stPgsql.setLong(5, Long.parseLong(idFeeSupplier));
                stPgsql.setString(6, idtrx);
                stPgsql.executeUpdate();

                //sms
                String smsScript = null;
                //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                sql = "select"
                        + "        this_.id,"
                        + "        this_.account_type,"
                        + "        this_.created_by,"
                        + "        this_.flag_active,"
                        + "        this_.id_tipeaplikasi,"
                        + "        this_.id_tipetransaksi,"
                        + "        this_.sms_script,"
                        + "        this_.timestamp,"
                        + "        this_.trx_status "
                        + "    from"
                        + "        public.\"MsSMSNotification\" this_ "
                        + "    where"
                        + "        this_.id_tipetransaksi=? "
                        + "        and this_.id_tipeaplikasi=? "
                        + "        and this_.account_type=? "
                        + "        and this_.trx_status=?";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setLong(1, 71);
                stPgsql.setLong(2, 3);
                stPgsql.setString(3, "AGENT");
                stPgsql.setString(4, "SUKSES");
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    smsScript = rsPgsql.getString("sms_script");
                }
                if (smsScript != null) {
                    pesan = String.format(smsScript,
                            "Qurban",
                            utilities.convertFormatDate("yyyy-MM-dd HH:mm:ss","yyyy-MM-dd",utilities.getDate().toString()),
                            idtrx);

                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    new SmsHelper().sent(uuid, noHp, pesan, "Qurban", entityConfig);
                }
                //</editor-fold>
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Transaksi gagal");
            }
            connPgsql.commit();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return jsonResp;
    }

}
