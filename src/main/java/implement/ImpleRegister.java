/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.json.simple.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Constant;
import helper.GcmNew;
import helper.Helper;
import helper.SendEmail;
import helper.SendEmailAMSAgentReg;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcRegister;
import koneksi.DatabaseUtilitiesMysql;
import koneksi.DatabaseUtilitiesPgsql;
import koneksi.DatabaseUtilitiesPgsqlSales;

/**
 * @author Hasan
 */
public class ImpleRegister implements InterfcRegister {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleRegister(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getReferral(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");
        jobjResp.put("pesan", "Fail");

        try {
            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //1, check usernya
            String query = "SELECT * FROM \"MsAgentAccount\" " +
                    "WHERE id_agentaccount = '" + objJson.get("referralCode").toString() + "';";
            st = conn.prepareStatement(query);
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            if (rs.next()) {
                jobjResp = new JSONObject();
                jobjResp.put("ACK", "OK");
                jobjResp.put("pesan", "referralCode valid");
                jobjResp.put("referralCode", objJson.get("referralCode").toString());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String getKelurahan(JSONObject objJson) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        try {
            //System.out.println("pass default 123456 : " + this.encDec.encryptDbVal(reqPass, this.entityConfig.getEncKeyDb()));

            conn = databaseUtilitiesPgsql.getConnection(entityConfig);
            //1, check usernya 
            String query = "select "
                    + " a.id_kelurahan, a.\"NamaKelurahan\", "
                    + " b.id_kecamatan, b.\"NamaKecamatan\", "
                    + " c.id_kota, c.\"NamaKota\", "
                    + " d.id_provinsi, d.\"NamaProvinsi\" "
                    + " from \"MsKelurahan\" a, \"MsKecamatan\" b, \"MsKota\" c, \"MsProvinsi\" d "
                    + " where a.id_kecamatan = b.id_kecamatan "
                    + " and b.id_kota = c.id_kota "
                    + " and c.id_provinsi = d.id_provinsi "
                    + " and a.\"NamaKelurahan\" ILIKE ?"
                    + " ORDER BY a.\"NamaKelurahan\"  ASC;";
            st = conn.prepareStatement(query);
            st.setString(1, objJson.get("search").toString() + "%");
            rs = st.executeQuery();
            JSONArray jArr = new JSONArray();
            JSONObject jObj = new JSONObject();
            while (rs.next()) {
                jObj = new JSONObject();
                jObj.put("idKelurahan", rs.getString("id_kelurahan"));
                jObj.put("namaKelurahan", rs.getString("NamaKelurahan"));
                jObj.put("idKecamatan", rs.getString("id_kecamatan"));
                jObj.put("namaKecamatan", rs.getString("NamaKecamatan"));
                jObj.put("idKota", rs.getString("id_kota"));
                jObj.put("namaKota", rs.getString("NamaKota"));
                jObj.put("idProvinsi", rs.getString("id_provinsi"));
                jObj.put("namaProvinsi", rs.getString("NamaProvinsi"));
                jArr.add(jObj);
            }
            jobjResp.put("ACK", "OK");
            jobjResp.put("kelurahan", jArr);
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jobjResp.toString();
    }

    @Override
    public String getOtp(JSONObject jsonReq) {
        JSONObject jobjResp = new JSONObject();
        jobjResp.put("ACK", "NOK");

        String handphone = jsonReq.get("handphone").toString();
        String ktp = jsonReq.get("ktp").toString();
        String email = jsonReq.get("email").toString();
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String sql = "WITH detail1 AS ("
                    + "	SELECT "
                    + "		\"count\" (*) AS hp "
                    + "	FROM "
                    + "		\"MsInformasiAgent\" "
                    + "	WHERE "
                    + "		\"Handphone\" = ? "
                    + "), "
                    + " detail2 AS ( "
                    + "	SELECT "
                    + "		\"count\" (*) AS ktp "
                    + "	FROM "
                    + "		\"MsInformasiAgent\" "
                    + "	WHERE "
                    + "		\"NomorKTP\" = ? "
                    + ") SELECT "
                    + "	detail1.hp, "
                    + "	detail2.ktp "
                    + "FROM "
                    + "	detail1, "
                    + "	detail2";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setString(1, handphone);
            stPgsql.setString(2, ktp);
            rsPgsql = stPgsql.executeQuery();
            int countHP = 0;
            int countKTP = 0;

            while (rsPgsql.next()) {
                countHP = rsPgsql.getInt("hp");
                countKTP = rsPgsql.getInt("ktp");
            }

            if (countHP == 0) {
                if (countKTP == 0) {
                    boolean validEmail = false;
                    //<editor-fold defaultstate="collapsed" desc="query validEmail">
                    sql = "SELECT \"lower\"(content_input) FROM \"MsContentBlacklist\" WHERE content_type = 'EMAIL' AND flag_active = 'T'";
                    stPgsql = connPgsql.prepareStatement(sql);
                    rsPgsql = stPgsql.executeQuery();
                    String Email = null;
                    while (rsPgsql.next()) {
                        if (email.toLowerCase().contains(rsPgsql.getString("lower"))) {
                            validEmail = false;
                            break;
                        } else {
                            validEmail = true;
                        }
                    }
                    //</editor-fold>
                    if (validEmail) {
                        int cekKtpAgent = 1;
                        //<editor-fold defaultstate="collapsed" desc="query validEmail">
                        sql = "SELECT \"count\"(*) FROM \"MsAgent\" WHERE \"upper\"(\"Email\") = '\" + _email.toUpperCase() + \"'";
                        stPgsql = connPgsql.prepareStatement(sql);
                        rsPgsql = stPgsql.executeQuery();
                        while (rsPgsql.next()) {
                            cekKtpAgent = rsPgsql.getInt("count");
                        }
                        //</editor-fold>
                        if (cekKtpAgent == 0) {

                            String requestBody = "{\"id_reg\":\"" + uuid + "\",\"no_hp\":\"" + handphone + "\",\"type\":\"token_register\"}";
                            String responseBody = "";
                            //<editor-fold defaultstate="collapsed" desc="query getOtp">
                            sql = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'tokenotp_url';";
                            stPgsql = connPgsql.prepareStatement(sql);
                            rsPgsql = stPgsql.executeQuery();
                            String url = "";
                            while (rsPgsql.next()) {
                                url = rsPgsql.getString("parameter_value");
                            }
                            //</editor-fold>
                            responseBody = this.curl_post_inq(url, requestBody, false);
                            //{"ACK":"OK","no_hp":"082197046466","otp":"848742","pesan":"Token TrueMoney Anda 848742, berlaku hingga 04-07-2018 14:35, berlaku 1x transaksi. Ayo Download Aplikasi Android TrueMoney Indonesia. Info hub: 08041000100","type":"token_register","id_reg":"47eba46790cc41a1a164105c3c1f2a02"}
                            JSONParser jSONParser = new JSONParser();
                            JSONObject jObjRespCurl = (JSONObject) jSONParser.parse(responseBody);

                            //send sms
                            String uuidSms = UUID.randomUUID().toString().replaceAll("-", "");
                            new SmsHelper().sent(uuidSms, jObjRespCurl.get("no_hp").toString(), jObjRespCurl.get("pesan").toString(), "OTP", entityConfig);
                            if (jObjRespCurl.get("ACK").toString().equalsIgnoreCase(String.valueOf(Constant.ACK.NOK))) {
                                jobjResp.put("ACK", jObjRespCurl.get("ACK").toString());
                                jobjResp.put("pesan", jObjRespCurl.get("pesan").toString());
                            } else {
                                jobjResp.put("ACK", jObjRespCurl.get("ACK").toString());
                                jobjResp.put("pesan", utilities.getWording("NOTIF_SEND_TOKEN"));
                                jobjResp.put("id_reg", uuid);
                                jobjResp.put("otp", "");
                            }
                        } else {
                            jobjResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                            jobjResp.put("pesan", "Email sudah terdaftar");
                        }
                    } else {
                        jobjResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                        jobjResp.put("pesan", "Domain email tidak diperkenankan untuk dipakai");
                    }
                } else {
                    jobjResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                    jobjResp.put("pesan", "No KTP sudah terdaftar");
                }
            } else {
                jobjResp.put("ACK", String.valueOf(Constant.ACK.NOK));
                jobjResp.put("pesan", "No handphone sudah terdaftar");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }

        return jobjResp.toString();
    }

    private String curl_post_inq(String url, String data, boolean encode) {
        JSONObject responseObj = null;
        try {
            BufferedReader rd = null;
            String line = null;
            if (encode) {
                data = URLEncoder.encode(data.toString(), "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();

            this.controllerLog.logStreamWriter("SEND REQUEST TO : " + url + " with data : " + data);

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");
            con.setRequestProperty("Content-Type", "text/plain");

            con.setConnectTimeout(120000);
            con.setReadTimeout(120000);

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String code = String.valueOf(con.getResponseCode());

            //System.err.println(code);
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));

            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            if (response.toString().trim().length() > 10) {
                JSONParser parser = new JSONParser();
                this.controllerLog.logStreamWriter("response from otp generator : " + response.toString());
                Object obj = parser.parse(response.toString());
                responseObj = (JSONObject) obj;
                responseObj.put("ResponseCode", code);
            } else {
                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "500");
                responseObj.put("ACK", "NOK");
                responseObj.put("pesan", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
                responseObj.put("MSG", "Mohon maaf untuk sementara transaksi tidak dapat dilakukan");
            }
        } catch (java.net.SocketTimeoutException ex) {
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "504");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "TIME OUT!!");
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);

            return (responseObj.toString());
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);

            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "500");
            responseObj.put("ACK", "NOK");
            responseObj.put("pesan", "EXT : INTERNAL SERVER ERROR");

            return (responseObj.toString());

        }
        return (responseObj.toString());
    }

    @Override
    public String getRegister(JSONObject jsonReq) {
        JSONObject jsonResp = new JSONObject();
        jsonResp.put("ACK", "NOK");
        jsonResp.put("pesan", "registrasi gagal");

        JSONObject jObjDataDiri = (JSONObject) jsonReq.get("dataPribadi");
        JSONObject jObjDataToko = (JSONObject) jsonReq.get("dataToko");
        JSONObject jObjDataFile = (JSONObject) jsonReq.get("fileFoto");

        //collect data diri
        String nama = jObjDataDiri.get("namaAgen").toString().toUpperCase();
        String handphone = jObjDataDiri.get("noHp").toString();
        String telepon = jObjDataDiri.get("noTlp").toString();
        String tempatLahir = jObjDataDiri.get("tempatLahir").toString();
        String tanggalLahir = jObjDataDiri.get("tglLahir").toString();
        String jkel = jObjDataDiri.get("jenisKelamin").toString();
        if (jkel.equals("Laki-laki")) {
            jkel = "L";
        } else {
            jkel = "P";
        }
        String email = jObjDataDiri.get("email").toString().toUpperCase();
        String ktp = jObjDataDiri.get("noKtp").toString();
        String npwp = jObjDataDiri.get("npwp").toString();
        String namaIbuKandung = jObjDataDiri.get("namaIbuKandung").toString().toUpperCase();
        String alamat = jObjDataDiri.get("alamat").toString();
        String provinsi = jObjDataDiri.get("provinsi").toString();
        String kota = jObjDataDiri.get("kota").toString();
        String kecamatan = jObjDataDiri.get("kecamatan").toString();
        String kelurahan = jObjDataDiri.get("kelurahan").toString();
        String rw = jObjDataDiri.get("rw").toString();
        String rt = jObjDataDiri.get("rt").toString();
        String kodePos = jObjDataDiri.get("kodePos").toString();
        String longitude = jObjDataDiri.get("longitude").toString();
        String referralCode = jObjDataDiri.get("referralCode").toString();
        //longitude = longitude.equals("") ? "0" : longitude;
        if (longitude.equals("")) {
            longitude = "0";
        }
        String latitude = jObjDataDiri.get("latitude").toString();
        if (latitude.equals("")) {
            latitude = "0";
        }

        //collect data toko
        String alamatToko = jObjDataToko.get("alamat").toString();
        String provinsiToko = jObjDataToko.get("provinsi").toString();
        String kotaToko = jObjDataToko.get("kota").toString();
        String kecamatanToko = jObjDataToko.get("kecamatan").toString();
        String kelurahanToko = jObjDataToko.get("kelurahan").toString();
        String rwToko = jObjDataToko.get("rw").toString();
        String rtToko = jObjDataToko.get("rt").toString();
        String kodeToko = jObjDataToko.get("kodePos").toString();

        //collect data foto
        String fotoDiri = jObjDataFile.get("fileFotoDiri").toString();
        String fotoKTP = jObjDataFile.get("fileKtp").toString();

        //colect common data
        String deviceInfo = jsonReq.get("deviceInfo").toString();
        String otp = jsonReq.get("otp").toString();
        String idReg = jsonReq.get("idReg").toString();

        int tipeAgent = 3;

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        DatabaseUtilitiesPgsqlSales databaseUtilitiesPgsqlSales = new DatabaseUtilitiesPgsqlSales();
        Connection connPgsqlSales = null;
        PreparedStatement stPgsqlSales = null;
        ResultSet rsPgsqlSales = null;

        DatabaseUtilitiesMysql databaseUtilitiesMysql = new DatabaseUtilitiesMysql();
        Connection connMysql = null;
        PreparedStatement stMysql = null;
        ResultSet rsMysql = null;

        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            connPgsql.setAutoCommit(false);

            connPgsqlSales = databaseUtilitiesPgsqlSales.getConnection(entityConfig);
            connPgsqlSales.setAutoCommit(false);

            connMysql = databaseUtilitiesMysql.getConnection(entityConfig);
            connMysql.setAutoCommit(false);

            String responseCurl = "";
            //<editor-fold defaultstate="collapsed" desc="query validasiOtp">
            String sql = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'tokenotp_url';";
            stPgsql = connPgsql.prepareCall(sql);
            rsPgsql = stPgsql.executeQuery();
            String url = "";
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
            String requestBody = "{\"id_reg\":\"" + idReg + "\",\"no_hp\":\"" + handphone + "\",\"otp\":\"" + otp + "\",\"type\":\"token_otentifikasi_register\"}";
            responseCurl = this.curl_post_inq(url, requestBody, false);
            //</editor-fold>
            JSONObject jsObjOtpOtenRegister = (JSONObject) parser.parse(responseCurl);
            String pin = this.encDec.encryptDbVal(utilities.GeneratePIN(), Constant.KUNCI);
            //bypass otp
            jsObjOtpOtenRegister.put("ACK","OK");
            if (jsObjOtpOtenRegister.get("ACK").toString().equalsIgnoreCase("OK")) {
                String arr[] = new String[2];
                boolean isAda = true;
                int noUrut = 0;
                //<editor-fold defaultstate="collapsed" desc="query generateIdAgent">
                while (isAda) {
                    sql = "SELECT nilai FROM \"NoUrut\" WHERE tipe = 'id_agent' FOR UPDATE";
                    stPgsql = connPgsql.prepareStatement(sql);
                    rsPgsql = stPgsql.executeQuery();
                    while (rsPgsql.next()) {
                        noUrut = rsPgsql.getInt("nilai");
                    }
                    //System.out.println("nourut : " + noUrut);
                    noUrut = noUrut + 1;

                    sql = "UPDATE \"NoUrut\" SET nilai = " + noUrut + " WHERE tipe = 'id_agent'";

                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.executeUpdate();

                    DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
                    Date date = new Date();

                    arr[0] = dateFormat.format(date) + "1" + String.format("%05d", noUrut);        //idAgentAcc
                    arr[1] = dateFormat.format(date) + "2" + String.format("%05d", noUrut);        //idAgent

                    //System.out.println("nourut : " + noUrut);
                    //System.out.println("idAgentAcc : " + arr[0]);
                    //System.out.println("idAgent : " + arr[1]);
                    sql = "SELECT \"count\"(*) as jumlah FROM \"MsAgentAccount\" WHERE id_agentaccount = '" + arr[0] + "'";
                    stPgsql = connPgsql.prepareStatement(sql);
                    rsPgsql = stPgsql.executeQuery();
                    int jumAgentAcc = 0;
                    while (rsPgsql.next()) {
                        jumAgentAcc = rsPgsql.getInt("jumlah");
                    }

                    sql = "SELECT \"count\"(*) as jumlah FROM \"MsAgent\" WHERE id_agent = '" + arr[1] + "'";
                    stPgsql = connPgsql.prepareStatement(sql);
                    rsPgsql = stPgsql.executeQuery();
                    int jumAgent = 0;
                    while (rsPgsql.next()) {
                        jumAgent = rsPgsql.getInt("jumlah");
                    }

                    if (jumAgentAcc == 0 && jumAgent == 0) {
                        isAda = false;
                    }
                }
                //</editor-fold>
                String idAgentAcc = arr[0];
                String idAgent = arr[1];
                Long noAgent = null;
                //<editor-fold defaultstate="collapsed" desc="query saveAgent">
                sql = "insert "
                        + "    into "
                        + "        \"MsAgent\""
                        + "        (create_by, \"Email\", \"Ewalet\", \"FlagActive\", id_agent, id_dealer, id_tipeagent, \"Password\", \"PIN\", \"TimeStamp\", update_by, update_date) "
                        + "    values "
                        + "        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
                        //+ " RETURNING no_agent";
                String generatedColumns[] = {"no_agent"};
                stPgsql = connPgsql.prepareStatement(sql, generatedColumns);
                stPgsql.setString(1, null);
                stPgsql.setString(2, email);
                stPgsql.setLong(3, 0);
                stPgsql.setBoolean(4, true);
                stPgsql.setString(5, idAgent);
                stPgsql.setLong(6, 0);
                stPgsql.setLong(7, 3);
                stPgsql.setString(8, null);
                stPgsql.setString(9, pin);
                stPgsql.setTimestamp(10, utilities.getDate());
                stPgsql.setString(11, null);
                stPgsql.setTimestamp(12, null);
                stPgsql.executeUpdate();
                rsPgsql = stPgsql.getGeneratedKeys();
                if (rsPgsql.next()) {
                    noAgent = rsPgsql.getLong(1);
                }
                //</editor-fold>
                java.sql.Date tglAktivasi = new java.sql.Date(Calendar.getInstance().getTime().getTime());
                boolean checkDeviceInfo = false;
                //<editor-fold defaultstate="collapsed" desc="query checkDeviceInfo">
                sql = "select "
                        + "        count(msmemberac0_.no_memberaccount) as col_0_0_  "
                        + "    from "
                        + "        \"MsMemberAccount\" msmemberac0_  "
                        + "    where "
                        + "        msmemberac0_.device_info=?";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, deviceInfo);
                rsPgsql = stPgsql.executeQuery();
                long countMember = 0;
                while (rsPgsql.next()) {
                    countMember = rsPgsql.getLong("col_0_0_");
                }

                sql = "select "
                        + "        count(msagentacc0_.no_agentaccount) as col_0_0_  "
                        + "    from "
                        + "        public.\"MsAgentAccount\" msagentacc0_  "
                        + "    where "
                        + "        msagentacc0_.device_info=?";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, deviceInfo);
                rsPgsql = stPgsql.executeQuery();
                long countAgent = 0;
                while (rsPgsql.next()) {
                    countAgent = rsPgsql.getLong("col_0_0_");
                }

                if (countAgent == 0 && countMember == 0) {
                    checkDeviceInfo = true;
                }
                //</editor-fold>

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse(tanggalLahir);
                String alamatPribadiApp = alamat.toUpperCase();
                int id_kecamatanPribadi = 0;
                int id_kelurahanPribadi = 0;
                int id_kotaPribadi = 0;
                int id_provinsiPribadi = 0;
                //<editor-fold defaultstate="collapsed" desc="query getIdLokasiPribadi">
                sql = "select a.id_kelurahan, a.\"NamaKelurahan\", b.id_kecamatan, b.\"NamaKecamatan\", c.id_kota, c.\"NamaKota\", d.id_provinsi, d.\"NamaProvinsi\" "
                        + " from \"MsKelurahan\" a, \"MsKecamatan\" b, \"MsKota\" c, \"MsProvinsi\" d "
                        + " where a.id_kecamatan = b.id_kecamatan "
                        + " and b.id_kota = c.id_kota "
                        + " and c.id_provinsi = d.id_provinsi "
                        + " and a.\"NamaKelurahan\" = ? "
                        + " and b.\"NamaKecamatan\" = ? "
                        + " and c.\"NamaKota\" = ? "
                        + " and d.\"NamaProvinsi\" = ?";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, kelurahan);
                stPgsql.setString(2, kecamatan);
                stPgsql.setString(3, kota);
                stPgsql.setString(4, provinsi);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    id_kecamatanPribadi = rsPgsql.getInt("id_kecamatan");
                    id_kelurahanPribadi = rsPgsql.getInt("id_kelurahan");
                    id_kotaPribadi = rsPgsql.getInt("id_kota");
                    id_provinsiPribadi = rsPgsql.getInt("id_provinsi");
                }
                //</editor-fold>
                if (checkDeviceInfo) {
                    //<editor-fold defaultstate="collapsed" desc="query saveAgentAccount">
                    sql = "insert"
                            + " into"
                            + " public.\"MsAgentAccount\" (create_by, device_info, \"FlagActive\", flag_change_pin, id_accountstatus, id_agent, id_agentaccount, \"LastBalance\", \"Latitude\", \"Longitude\", \"PIN\", \"Remark\", statusva, \"TanggalAktivasi\", \"TimeStamp\", virtualaccount, no_agent) "
                            + " values "
                            + " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.setString(1, "Generated using android tmw");
                    stPgsql.setString(2, deviceInfo);
                    stPgsql.setBoolean(3, true);
                    stPgsql.setBoolean(4, false);
                    stPgsql.setLong(5, 2);
                    stPgsql.setString(6, idAgent);
                    stPgsql.setString(7, idAgentAcc);
                    stPgsql.setLong(8, 0);
                    stPgsql.setDouble(9, Double.parseDouble(latitude));
                    stPgsql.setDouble(10, Double.parseDouble(longitude));
                    stPgsql.setString(11, pin);
                    stPgsql.setString(12, "Generated using android tmw");
                    stPgsql.setBoolean(13, true);
                    stPgsql.setDate(14, tglAktivasi);
                    stPgsql.setTimestamp(15, utilities.getDate());
                    stPgsql.setString(16, "8" + idAgentAcc);
                    stPgsql.setLong(17, noAgent);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                } else {
                    //<editor-fold defaultstate="collapsed" desc="query saveAgentAccount">
                    sql = "insert"
                            + " into"
                            + " public.\"MsAgentAccount\" (create_by, \"FlagActive\", flag_change_pin, id_accountstatus, id_agent, id_agentaccount, \"LastBalance\", \"Latitude\", \"Longitude\", \"PIN\", \"Remark\", statusva, \"TanggalAktivasi\", \"TimeStamp\", virtualaccount, no_agent) "
                            + " values "
                            + " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.setString(1, "Generated using android tmw");
                    stPgsql.setBoolean(2, true);
                    stPgsql.setBoolean(3, false);
                    stPgsql.setLong(4, 2);
                    stPgsql.setString(5, idAgent);
                    stPgsql.setString(6, idAgentAcc);
                    stPgsql.setLong(7, 0);
                    stPgsql.setDouble(8, Double.parseDouble(latitude));
                    stPgsql.setDouble(9, Double.parseDouble(longitude));
                    stPgsql.setString(10, pin);
                    stPgsql.setString(11, "Generated using android tmw");
                    stPgsql.setBoolean(12, true);
                    stPgsql.setDate(13, tglAktivasi);
                    stPgsql.setTimestamp(14, utilities.getDate());
                    stPgsql.setString(15, "8" + idAgentAcc);
                    stPgsql.setLong(16, noAgent);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                }
                //<editor-fold defaultstate="collapsed" desc="query MsInformasiAgent 1">
                sql = "insert"
                        + " into"
                        + " \"MsInformasiAgent\""
                        + " (\"Alamat\", \"FlagActive\", fotodiri, fotoktp, \"Handphone\", id_agent, id_bank, id_kecamatan, id_kelurahan, id_kota, id_provinsi, id_tipeinformasiagent, \"JenisKelamin\", \"KodePOS\", \"Nama\", \"NamaIbuKandung\", \"NomorKTP\", \"NPWP\", \"RT\", \"RW\", \"TanggalLahir\", \"Telepon\", \"TempatLahir\", \"TimeStamp\")"
                        + " values"
                        + " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, alamatPribadiApp);
                stPgsql.setBoolean(2, true);
                stPgsql.setString(3, fotoDiri);
                stPgsql.setString(4, fotoKTP);
                stPgsql.setString(5, handphone);
                stPgsql.setString(6, idAgent);
                stPgsql.setInt(7, 0);
                stPgsql.setInt(8, id_kecamatanPribadi);
                stPgsql.setInt(9, id_kelurahanPribadi);
                stPgsql.setInt(10, id_kotaPribadi);
                stPgsql.setInt(11, id_provinsiPribadi);
                stPgsql.setInt(12, 1);
                stPgsql.setString(13, jkel);
                stPgsql.setString(14, kodePos);
                stPgsql.setString(15, nama);
                stPgsql.setString(16, namaIbuKandung);
                stPgsql.setString(17, ktp);
                stPgsql.setString(18, npwp);
                stPgsql.setString(19, rt);
                stPgsql.setString(20, rw);
                stPgsql.setTimestamp(21, new Timestamp(date.getTime()));
                stPgsql.setString(22, telepon);
                stPgsql.setString(23, tempatLahir);
                stPgsql.setTimestamp(24, utilities.getDate());

                stPgsql.executeUpdate();
                //</editor-fold>
                int idKecamatanUsaha = 0;
                int idKelurahanUsaha = 0;
                int idKotaUsaha = 0;
                int idProvinsiUsaha = 0;
                //<editor-fold defaultstate="collapsed" desc="query getIdLokasiUsaha">
                sql = "select a.id_kelurahan, a.\"NamaKelurahan\", b.id_kecamatan, b.\"NamaKecamatan\", c.id_kota, c.\"NamaKota\", d.id_provinsi, d.\"NamaProvinsi\" "
                        + " from \"MsKelurahan\" a, \"MsKecamatan\" b, \"MsKota\" c, \"MsProvinsi\" d "
                        + " where a.id_kecamatan = b.id_kecamatan "
                        + " and b.id_kota = c.id_kota "
                        + " and c.id_provinsi = d.id_provinsi "
                        + " and a.\"NamaKelurahan\" = ? "
                        + " and b.\"NamaKecamatan\" = ? "
                        + " and c.\"NamaKota\" = ? "
                        + " and d.\"NamaProvinsi\" = ?";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, kelurahanToko);
                stPgsql.setString(2, kecamatanToko);
                stPgsql.setString(3, kotaToko);
                stPgsql.setString(4, provinsiToko);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    idKecamatanUsaha = rsPgsql.getInt("id_kecamatan");
                    idKelurahanUsaha = rsPgsql.getInt("id_kelurahan");
                    idKotaUsaha = rsPgsql.getInt("id_kota");
                    idProvinsiUsaha = rsPgsql.getInt("id_provinsi");
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="query MsInformationAgent 3">
                String alamatUsahaApp = alamatToko.toUpperCase();
                sql = " insert"
                        + " into"
                        + " \"MsInformasiAgent\""
                        + " (\"Alamat\", \"FlagActive\", fotodiri, fotoktp, id_agent, id_bank, id_kecamatan, id_kelurahan, id_kota, id_provinsi, id_tipeinformasiagent,\"KodePOS\", \"NamaIbuKandung\",\"RT\",\"RW\",\"TimeStamp\") "
                        + " values"
                        + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setString(1, alamatUsahaApp);
                stPgsql.setBoolean(2, true);
                stPgsql.setString(3, fotoDiri);
                stPgsql.setString(4, fotoKTP);
                stPgsql.setString(5, idAgent);
                stPgsql.setLong(6, 0);
                stPgsql.setLong(7, idKecamatanUsaha);
                stPgsql.setLong(8, idKelurahanUsaha);
                stPgsql.setLong(9, idKotaUsaha);
                stPgsql.setLong(10, idProvinsiUsaha);
                stPgsql.setLong(11, 3);
                stPgsql.setString(12, kodeToko);
                stPgsql.setString(13, namaIbuKandung);
                stPgsql.setString(14, rt);
                stPgsql.setString(15, rw);
                stPgsql.setTimestamp(16, utilities.getDate());
                stPgsql.executeUpdate();
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="query insert agent komisi">
                sql = "SELECT id_komisi, id_weekly FROM \"RelKomisiDefault\"";
                stPgsql = connPgsql.prepareStatement(sql);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    sql = "INSERT INTO \"MsAgentKomisi\" (id_komisi, id_weekly, \"FlagActive\","
                            + " \"TimeStamp\", id_agentaccount) " + "VALUES (" + rsPgsql.getString("id_komisi") + ", "
                            + rsPgsql.getString("id_weekly") + ", 'TRUE', now(), '" + idAgentAcc + "')";
                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.executeUpdate();
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="query TrTransaksiAgent Scheme API_SALES">
                sql = "INSERT INTO `API_Sales`.`TrRegistrasiAgent` ("
                        + "	`ID_Agent`, "
                        + "	`ID_TipeAgent`, "
                        + "	`TanggalRegistrasi`, "
                        + "	`Remark`, "
                        + "	`FlagApprove` "
                        + ") "
                        + "VALUES "
                        + "	( "
                        + "		'" + idAgent + "', "
                        + "		'3', "
                        + "		NOW(), "
                        + "		'Registered from android tmw', "
                        + "		'0' "
                        + "	);";
                stMysql = connMysql.prepareStatement(sql);
                stMysql.executeUpdate();
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="query insert tr_referral_history">
                if (!referralCode.equals("") || referralCode.length()!=0) {
                    sql = "INSERT INTO tr_referral_history (id_account, account_type, referral_code,"
                            + " used_by, used_timestamp, approval_status) "
                            + "VALUES (?,?,?,?,now(),?)"; //"'" + idAgentAcc + "', 'AGENT', '" + referralCode + "', '"
                            //+ idAgentAcc + "', now(), 'NEED APPROVAL')";
                    System.out.println("sql referral = " + sql);
                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.setString(1, idAgentAcc);
                    stPgsql.setString(2, "AGENT");
                    stPgsql.setString(3, referralCode);
                    stPgsql.setString(4, idAgentAcc);
                    stPgsql.setString(5, "NEED APPROVAL");
                    stPgsql.executeUpdate();
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="query kirim email ke agent">
                new SendEmail().send(email, "", jsonReq.toString());
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="query kirim email ke AMS">
                Map<String, String> map = new HashMap<>();
                map.put("Tipe", "sendRegisterAgentAndroid");
                map.put("idAgent", idAgent);
                map.put("idAgentAccount", idAgentAcc);
                map.put("VarNama", nama);
                map.put("VarHP", handphone);
                map.put("VarEmail", email);
                map.put("VarKTP", ktp);

                new SendEmailAMSAgentReg().sent(map);
                //</editor-fold>
                String smsScript = null;
                //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                sql = "select"
                        + "        this_.id,"
                        + "        this_.account_type,"
                        + "        this_.created_by,"
                        + "        this_.flag_active,"
                        + "        this_.id_tipeaplikasi,"
                        + "        this_.id_tipetransaksi,"
                        + "        this_.sms_script,"
                        + "        this_.timestamp,"
                        + "        this_.trx_status "
                        + "    from"
                        + "        public.\"MsSMSNotification\" this_ "
                        + "    where"
                        + "        this_.id_tipetransaksi=? "
                        + "        and this_.id_tipeaplikasi=? "
                        + "        and this_.account_type=? "
                        + "        and this_.trx_status=?";
                stPgsql = connPgsql.prepareStatement(sql);
                stPgsql.setLong(1, 1);
                stPgsql.setLong(2, 3);
                stPgsql.setString(3, "AGENT");
                stPgsql.setString(4, "SUKSES");
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    smsScript = rsPgsql.getString("sms_script");
                }
                if (smsScript != null) {
                    pin = encDec.decryptDbVal(pin, Constant.KUNCI);
                    String pesan = String.format(smsScript,
                            pin,
                            idAgentAcc);

                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    new SmsHelper().sent(uuid, handphone, pesan, "INFO_REGISTRASI", entityConfig);
                }
                //</editor-fold>
                //upoad to ftp
                File fotoKTPFile = new File(entityConfig.getMasterFolder() + "uploadFile/register_agent/" + fotoKTP);
                File fotoDiriFile = new File(entityConfig.getMasterFolder() + "uploadFile/register_agent/" + fotoDiri);
                uploadFtp(fotoKTP, fotoKTPFile);
                uploadFtp(fotoDiri, fotoDiriFile);

                //uploadFileViaCommandLine(entityConfig.getMasterFolder() + "uploadFile/register_agent/" + fotoKTP);
                //uploadFileViaCommandLine(entityConfig.getMasterFolder() + "uploadFile/register_agent/" + fotoDiri);
                jsonResp.put("ACK", "OK");
                jsonResp.put("pesan", "Data berhasil diterima, silakan menunggu (Max 2 hari) untuk aktivasi Account");

            } else {
                jsonResp.put("pesan", jsObjOtpOtenRegister.get("pesan").toString());
            }

            connPgsql.commit();
            connPgsqlSales.commit();
            connMysql.commit();
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
                connPgsqlSales.rollback();
                connMysql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }

                if (stPgsqlSales != null) {
                    stPgsqlSales.close();
                }
                if (rsPgsqlSales != null) {
                    rsPgsqlSales.close();
                }
                if (connPgsqlSales != null) {
                    connPgsqlSales.close();
                }

                if (stMysql != null) {
                    stMysql.close();
                }
                if (rsMysql != null) {
                    rsMysql.close();
                }
                if (connMysql != null) {
                    connMysql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }

        return jsonResp.toString();
    }

    public void uploadFtp(String filename, File image) {
        //upload to inventory
        String server = entityConfig.getFtpIp();
        int port = Integer.parseInt(entityConfig.getFtpPort());
        String user = entityConfig.getFtpUser();
        String pass = entityConfig.getFtpPass();
        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            // ftpClient.enterLocalActiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // File firstLocalFile = new File("D://BackGround/Ao8tG.jpg");
            //  devel "/home/development/devel/android/";
            //  prod "/home/development/prod/picture/";
            String path = entityConfig.getFtpPath();
            String firstRemoteFile = path + filename;
            InputStream inputStream = new FileInputStream(image);
            controllerLog.logStreamWriter("Start uploading file " + firstRemoteFile);
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            controllerLog.logStreamWriter(ftpClient.getReplyString());
            inputStream.close();
            if (done) {
                controllerLog.logStreamWriter("The file is uploaded successfully.");
            } else {

            }
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
    }

    public void uploadFileViaCommandLine(String file) {
        String server = entityConfig.getFtpIp();
        int port = Integer.parseInt(entityConfig.getFtpPort());
        String user = entityConfig.getFtpUser();
        String pass = "helloworld..2018\\!\\#";
        String path = entityConfig.getFtpPath();
        try {
            final Process p = Runtime.getRuntime().exec("curl -T " + file + " " + "ftp://" + server + path + " --user " + user + ":" + pass);
            System.out.println("command curl -T " + file + " " + "ftp://" + server + path + " --user " + user + ":" + pass);
            new Thread(new Runnable() {
                public void run() {
                    BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = null;
                    try {
                        while ((line = input.readLine()) != null) {
                            controllerLog.logStreamWriter("Start uploading file " + line);
                        }
                    } catch (IOException ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    }
                }
            }).start();

            p.waitFor();
        } catch (InterruptedException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (IOException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        }
    }
}
