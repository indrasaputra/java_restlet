/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcPembayaranKomunitas;
import interfc.InterfcTransfer;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author Hasan
 */
public class ImpleTransfer implements InterfcTransfer {

    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;

    public ImpleTransfer(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getInq(JSONObject jsonReq) {
        String idAgentAccount = jsonReq.get("username").toString();
        String idAgent = idAgentAccount.substring(0, 6) + '2' + idAgentAccount.substring(7);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "RM-01");
        jsonObject.put("MC", "RM0102");
        jsonObject.put("MT", "2100");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObject.put("MP", jsonReq);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("trxid").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "c2c_gw");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    jObjResp.put("ACK", "OK");

                    String jObjRespMpoString = jObjResp.get("MPO").toString();
                    jObjRespMpoString = jObjRespMpoString.replace("\"{", "{");
                    jObjRespMpoString = jObjRespMpoString.replace("}\"", "}");
                    jObjRespMpoString = jObjRespMpoString.replace("\\", "");
                    JSONObject jObjRespMpo = (JSONObject) parser.parse(jObjRespMpoString);

                    String jumlahadm = jObjRespMpo.get("fee").toString();
                    double dbl = Double.parseDouble(jumlahadm);
                    String dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jObjRespMpo.put("jumlahadm", dblStr);

                    String jumlahbayar = jObjRespMpo.get("hargaCetak").toString();
                    dbl = Double.parseDouble(jumlahbayar);
                    dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jObjRespMpo.put("jumlahbayar", dblStr);
                    jObjResp.put("MPO", jObjRespMpo);
                } else {
                    jObjResp.put("ACK", "NOK");
                    String jObjRespMpoString = jObjResp.get("MPO").toString();
                    jObjRespMpoString = jObjRespMpoString.replace("\"{", "{");
                    jObjRespMpoString = jObjRespMpoString.replace("}\"", "}");
                    jObjRespMpoString = jObjRespMpoString.replace("\\", "");
                    JSONObject jObjRespMpo = (JSONObject) parser.parse(jObjRespMpoString);
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                }
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal inq kirim uang");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jObjResp.toString();
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String NoKartu) {
        String[] status = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"MsAccountStatus\".\"NamaAccountStatus\""
                    + " ,\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\""
                    + " ,\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\""
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\""
                    + " WHERE \"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus"
                    + " AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, NoKartu);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("NamaAccountStatus");
                status[1] = rsPgsql.getString("LastBalance");
                status[2] = rsPgsql.getString("StatusKartu");
                status[3] = rsPgsql.getString("id_agentaccount");
                status[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String getPay(JSONObject jsonReq) {
        String idAgentAccount = jsonReq.get("username").toString();
        String idAgent = idAgentAccount.substring(0, 6) + '2' + idAgentAccount.substring(7);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "RM-02");
        jsonObject.put("MC", "RM0102");
        jsonObject.put("MT", "2200");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObject.put("MP", jsonReq);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("trxid").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "c2c_gw");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jObjResp.put("idtrx", jObjRespMpo.get("trxid").toString());
                    jObjResp.put("timestamp", utilities.getDate().toString());
                    jObjResp.put("status", "SUKSES");

                    String pesan = jObjRespMpo.get("pesan").toString();
                    String idtrx = jObjRespMpo.get("sn").toString();
                    String id_print = "";
                    String keterangan = "";

                    //<editor-fold defaultstate="collapsed" desc="query get id_print">
                    DatabaseUtilitiesPgsql databaseUtilitiesPgsql = null;
                    PreparedStatement st = null;
                    ResultSet rs = null;
                    Connection conn = null;
                    try {
                        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                        conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                        String query = "select id_transaksi, \"Keterangan\" from \"TrTransaksi\" " +
                                " where id_trx = ? ";
                        st = conn.prepareStatement(query);
                        st.setString(1, idtrx);
                        rs = st.executeQuery();
                        while (rs.next()) {
                            id_print = rs.getInt("id_transaksi") + "";
                            //keterangan = rs.getString("Keterangan").replace("Uang", "Tunai");
                        }
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (st != null) {
                                st.close();
                            }
                            if (rs != null) {
                                rs.close();
                            }
                            if (conn != null) {
                                conn.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>

                    /*
                    //<editor-fold defaultstate="collapsed" desc="query update Keterangan TrTransaksi">
                    try {
                        databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
                        conn = databaseUtilitiesPgsql.getConnection(entityConfig);
                        String query = "UPDATE \"TrTransaksi\" SET \"Keterangan\" = ? " +
                                " where id_trx = ? ";
                        st = conn.prepareStatement(query);
                        st.setString(1, keterangan + ", ID Print: " + id_print + ",");
                        st.setString(2, idtrx);
                        st.executeUpdate();
                    } catch (Exception ex) {
                        String s = Throwables.getStackTraceAsString(ex);
                        controllerLog.logErrorWriter(s);
                    } finally {
                        try {
                            if (st != null) {
                                st.close();
                            }
                            if (rs != null) {
                                rs.close();
                            }
                            if (conn != null) {
                                conn.close();
                            }
                        } catch (SQLException ex) {
                            String s = Throwables.getStackTraceAsString(ex);
                            controllerLog.logErrorWriter(s);
                        }
                    }
                    //</editor-fold>
                    */

                    jObjResp.put("ACK", "OK");
                    jObjResp.put("pesan", pesan + ".\nID Print: " + id_print);

                    String jObjStrRespMpi = jObjResp.get("MP").toString();
                    jObjStrRespMpi = jObjStrRespMpi.replace("\"{", "{");
                    jObjStrRespMpi = jObjStrRespMpi.replace("}\"", "}");
                    jObjStrRespMpi = jObjStrRespMpi.replace("\\", "");
                    JSONObject jObjRespMpi = (JSONObject) parser.parse(jObjStrRespMpi);
                    jObjResp.put("MP", jObjRespMpi);
                } else {
                    jObjResp.put("ACK", "NOK");
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                    jObjResp.put("status", "GAGAL");
                }
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal kirim uang");
                jObjResp.put("status", "GAGAL");
            }
        } catch (ParseException e) {
            jObjResp.put("ACK", "NOK");
            jObjResp.put("pesan", "gagal kirim uang");
            jObjResp.put("status", "GAGAL");

            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jObjResp.toString();
    }

    @Override
    public void sendSms(JSONObject jsonReq, JSONObject respObj) {
        String smsScript = null;
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;

        //<editor-fold defaultstate="collapsed" desc="query kirim sms">
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(entityConfig);
            String sql = "select *"
                    + "    from"
                    + "        \"MsSMSNotification\""
                    + "    where"
                    + "        id_tipetransaksi=? "
                    + "        and id_tipeaplikasi=? "
                    + "        and account_type=? "
                    + "        and trx_status=?";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(jsonReq.get("idTipeTransaksi").toString()));
            stPgsql.setLong(2, 3);
            stPgsql.setString(3, "AGENT");
            stPgsql.setString(4, "SUKSES");
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                smsScript = rsPgsql.getString("sms_script");
            }
        } catch (Exception e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        //</editor-fold>

        JSONObject jObjRespMp = (JSONObject) respObj.get("MP");
        String uniqueId = jObjRespMp.get("unique_id").toString();
        JSONObject jObjRespMpo = (JSONObject) respObj.get("MPO");
        String idTrx = jObjRespMpo.get("trxid").toString();

        if (smsScript != null) {
            String pesan = String.format(smsScript,
                    "Komunitas " + jObjRespMp.get("communityName").toString(),
                    uniqueId,
                    idTrx);

            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            new SmsHelper().sent(uuid, jsonReq.get("noHp").toString(), pesan, "PAY_KOMUNITAS", entityConfig);
        }
    }


    @Override
    public String getInqTerimaUang(JSONObject jsonReq) {
        String idAgentAccount = jsonReq.get("username").toString();
        String idAgent = idAgentAccount.substring(0, 6) + '2' + idAgentAccount.substring(7);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "RM-03");
        jsonObject.put("MC", "RM0304");
        jsonObject.put("MT", "2100");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObject.put("MP", jsonReq);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("trxid").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "c2c_gw");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    jObjResp.put("ACK", "OK");

                    String jObjRespMpoString = jObjResp.get("MPO").toString();
                    jObjRespMpoString = jObjRespMpoString.replace("\"{", "{");
                    jObjRespMpoString = jObjRespMpoString.replace("}\"", "}");
                    jObjRespMpoString = jObjRespMpoString.replace("\\", "");
                    JSONObject jObjRespMpo = (JSONObject) parser.parse(jObjRespMpoString);

                    String amount = jObjRespMpo.get("amount").toString();
                    double dbl = Double.parseDouble(amount);
                    String dblStr = String.format("%,.0f", dbl);
                    dblStr = dblStr.replaceAll(",", ".");
                    jObjRespMpo.put("amount", dblStr);

                    jObjResp.put("MPO", jObjRespMpo);
                } else {
                    jObjResp.put("ACK", "NOK");
                    String jObjRespMpoString = jObjResp.get("MPO").toString();
                    jObjRespMpoString = jObjRespMpoString.replace("\"{", "{");
                    jObjRespMpoString = jObjRespMpoString.replace("}\"", "}");
                    jObjRespMpoString = jObjRespMpoString.replace("\\", "");
                    JSONObject jObjRespMpo = (JSONObject) parser.parse(jObjRespMpoString);
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                }
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal inq kirim uang");
            }
        } catch (ParseException e) {
            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jObjResp.toString();
    }

    @Override
    public String getPayTerimaUang(JSONObject jsonReq) {
        String idAgentAccount = jsonReq.get("username").toString();
        String idAgent = idAgentAccount.substring(0, 6) + '2' + idAgentAccount.substring(7);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("CC", "3");
        jsonObject.put("DT", utilities.getDate("yyyyMMddHHmmss"));
        jsonObject.put("ST", utilities.createStan());
        jsonObject.put("PC", "RM-04");
        jsonObject.put("MC", "RM0304");
        jsonObject.put("MT", "2200");
        JSONObject jsonObjectMp = new JSONObject();
        jsonObject.put("MP", jsonReq);
        //insert log
        int idLog = utilities.insertLog(jsonReq.get("trxid").toString(), jsonObject);
        String resp = this.hlp.requestToGwSocket(jsonObject, "c2c_gw");
        //updateLog
        utilities.updateLog(idLog, resp);
        JSONParser parser = new JSONParser();
        JSONObject jObjResp = new JSONObject();
        try {
            jObjResp = (JSONObject) parser.parse(resp);
            if (jObjResp.containsKey("RC")) {
                if (jObjResp.get("RC").toString().equals("0000")) {
                    jObjResp.put("ACK", "OK");
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                    jObjResp.put("idtrx", jObjRespMpo.get("trxid").toString());
                    jObjResp.put("timestamp", utilities.getDate().toString());
                    jObjResp.put("status", "SUKSES");

                    String jObjStrRespMpi = jObjResp.get("MP").toString();
                    jObjStrRespMpi = jObjStrRespMpi.replace("\"{", "{");
                    jObjStrRespMpi = jObjStrRespMpi.replace("}\"", "}");
                    jObjStrRespMpi = jObjStrRespMpi.replace("\\", "");
                    JSONObject jObjRespMpi = (JSONObject) parser.parse(jObjStrRespMpi);
                    jObjResp.put("MP", jObjRespMpi);
                } else {
                    jObjResp.put("ACK", "NOK");
                    JSONObject jObjRespMpo = (JSONObject) jObjResp.get("MPO");
                    jObjResp.put("pesan", jObjRespMpo.get("pesan").toString());
                    jObjResp.put("status", "GAGAL");
                }
            } else {
                jObjResp.put("ACK", "NOK");
                jObjResp.put("pesan", "gagal kirim uang");
                jObjResp.put("status", "GAGAL");
            }
        } catch (ParseException e) {
            jObjResp.put("ACK", "NOK");
            jObjResp.put("pesan", "gagal kirim uang");
            jObjResp.put("status", "GAGAL");

            String s = Throwables.getStackTraceAsString(e);
            controllerLog.logErrorWriter(s);
        }
        return jObjResp.toString();
    }

}
