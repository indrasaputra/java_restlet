package implement;

import com.google.common.base.Throwables;
import controller.ControllerEncDec;
import controller.ControllerLog;
import entity.EntityConfig;
import helper.Helper;
import helper.SmsHelper;
import helper.Utilities;
import interfc.InterfcTransferBank;
import koneksi.DatabaseUtilitiesPgsql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.security.MessageDigest;
import java.sql.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Fino Indrasaputra on 20/07/2018.
 */
public class ImpleTransferBank implements InterfcTransferBank {
    EntityConfig entityConfig;
    Helper hlp;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    private final ControllerEncDec encDec;
    private final Utilities utilities;
    private final ControllerLog controllerLog;
    static SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy");

    public ImpleTransferBank(EntityConfig entityConfig) {
        this.entityConfig = entityConfig;
        this.encDec = new ControllerEncDec(this.entityConfig.getEncIv(), this.entityConfig.getEncKey());
        this.hlp = new Helper();
        this.utilities = new Utilities();
        this.controllerLog = new ControllerLog();
    }

    @Override
    public String getBank() {
        JSONObject obj = new JSONObject();
        JSONObject jobjResp = new JSONObject();
        JSONArray jArr = new JSONArray();
        //<editor-fold defaultstate="collapsed" desc="query getBank">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String tgl = format.format(new Date());
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
//            String sql = "SELECT " + "	\"MsBank\".* FROM " + "	\"MsBank\" "
//                    + "WHERE \"MsBank\".\"modify_date\" > ?;";
            String sql = "SELECT \"MsBank\".* FROM \"MsBank\" where \"TipeTransfer\" = 'Online' ORDER BY \"NamaBank\"::CHAR ASC";
            stPgsql = connPgsql.prepareStatement(sql);
//            stPgsql.setTimestamp(1, Timestamp.valueOf(tgl));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                obj = new JSONObject();
                obj.put("idBank", rsPgsql.getString("id_bank"));
                obj.put("namaBank", rsPgsql.getString("NamaBank"));
                obj.put("kodeBank", rsPgsql.getString("kodebank").trim());
                jArr.add(obj);
            }
            if (jArr.equals("[]")) {
                jobjResp.put("ACK", "NOK");
                jobjResp.put("listBank", jArr);
            } else {
                jobjResp.put("ACK", "OK");
                jobjResp.put("listBank", jArr);
            }

        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        //</editor-fold>
        return jobjResp.toString();
    }

    @Override
    public int getIdSupplier(JSONObject jsonReq) {
        int idSupplier = 0;
        //<editor-fold defaultstate="collapsed" desc="query getIdSupplier">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String sql = "select b.id_supplier"
                    + " from \"MsFee\" a, \"MsFeeSupplier\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and a.id_tipeaplikasi = ?"
                    + " and a.id_tipetransaksi = ?"
                    + " and b.id_operator = ?"
                    + " and a.\"FlagActive\" = TRUE"
                    + " and b.\"FlagActive\" = TRUE";
            stPgsql = connPgsql.prepareStatement(sql);
            stPgsql.setLong(1, Long.parseLong(jsonReq.get("id_TipeAplikasi").toString()));
            stPgsql.setLong(2, Long.parseLong(jsonReq.get("ID_TipeTransaksi").toString()));
            stPgsql.setLong(3, Long.parseLong(jsonReq.get("id_Operator").toString()));
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idSupplier = rsPgsql.getInt("id_supplier");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return idSupplier;
    }

    @Override
    public String getMinPayment(String id_tipetransaksi) {
        String minPay = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);

            String sql = "SELECT value_config FROM tb_config WHERE key_config = 'MIN_TRX' and id_tipetransaksi = ? ";

            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            stPgsqlLb.setLong(1, Long.parseLong(id_tipetransaksi));
            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                minPay = rsPgsqlLb.getString("value_config");
            }

        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return minPay;
    }

    @Override
    public String getMaxPayment(String id_tipetransaksi) {
        String maxPay = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);

            String sql = "SELECT value_config FROM tb_config WHERE key_config = 'MAX_TRX' and id_tipetransaksi = ? ";

            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            stPgsqlLb.setLong(1, Long.parseLong(id_tipetransaksi));
            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                maxPay = rsPgsqlLb.getString("value_config");
            }

        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return maxPay;
    }

    @Override
    public String getNotifMinPay() {
        String notifMinPay = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);

            String sql = "SELECT wording_content FROM \"MsWordingContent\" WHERE wording_content_name = 'NOTIF_MIN_T2B'";

            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                notifMinPay = rsPgsqlLb.getString("wording_content");
            }

        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return notifMinPay;
    }

    @Override
    public String getNotifMaxPay() {
        String notifMaxPay = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);

            String sql = "SELECT wording_content FROM \"MsWordingContent\" WHERE wording_content_name = 'NOTIF_MAX_T2B'";

            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                notifMaxPay = rsPgsqlLb.getString("wording_content");
            }

        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return notifMaxPay;
    }

    @Override
    public boolean cekStatus(String username) {
        boolean status = false;
        int id_statusverifikasi = 0;
        //<editor-fold defaultstate="collapsed" desc="query cekStatus">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select * from \"MsAgentAccount\" "
                    + "where id_agentaccount = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                id_statusverifikasi = rsPgsql.getInt("id_accountstatus");
            }

            if (id_statusverifikasi == 1 || id_statusverifikasi == 2 || id_statusverifikasi == 3) {
                status = true;
            } else {
                status = false;
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public boolean cekNorek(String noRekening, String kodeBank, String tipe) {
        String sql = "";
        String idMemberAcc = "";
        boolean status = true;
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query cekNorek">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
            //get id_memberaccount
            if (tipe.equalsIgnoreCase("member")) {
                sql = "WITH kode_bank AS ( " + " SELECT " + "  id_bank " + " FROM " + "  \"MsBank\" " + " WHERE "
                        + "  kodebank = ? " + ") SELECT " + " COUNT (d.id_blacklist) AS jumlah "
                        + "FROM " + " kode_bank b, " + " \"MsBlackList\" d " + "WHERE " + " b.id_bank = d.id_bank "
                        + "AND \"lower\"(d.account_type) = 'receiver' " + "AND d.account_number = ? "
                        + "AND d.id_tipetransaksi = 6 " + "AND d.flagactive = 't';";
            } else {
                sql = "WITH kode_bank AS ( " + " SELECT " + "  id_bank " + " FROM " + "  \"MsBank\" " + " WHERE "
                        + "  kodebank = ? " + ") SELECT " + " COUNT (d.id_blacklist) AS jumlah "
                        + "FROM " + " kode_bank b, " + " \"MsBlackList\" d " + "WHERE " + " b.id_bank = d.id_bank "
                        + "AND \"lower\"(d.account_type) = 'receiver' " + "AND d.account_number = ? "
                        + "AND d.id_tipetransaksi = 30 " + "AND d.flagactive = 't';";
            }
            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            stPgsqlLb.setString(1, kodeBank);
            stPgsqlLb.setString(2, noRekening);

            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                count = rsPgsqlLb.getInt("jumlah");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        return status;
    }

    @Override
    public JSONObject prosesInq(JSONObject jsonReq, JSONObject jsonResp, int idSupplier) {
        JSONObject ThirdParty = new JSONObject();
        String BeneficiaryAccount = jsonReq.get("BeneficiaryAccount").toString();
//        String BeneficiaryName = jsonReq.get("BeneficiaryName").toString();
        String Kode_Bank = jsonReq.get("kodeBank").toString();
//        String Nama_Bank = jsonReq.get("namaBank").toString();
//        String SenderName = jsonReq.get("senderName").toString();
//        String SenderCellular = jsonReq.get("senderCellular").toString();
//        String TransferIssue = jsonReq.get("transferIssue").toString();
        int Nominal = Integer.parseInt(jsonReq.get("nominal").toString());
        String gen_id = "TMN" + jsonReq.get("idtrx").toString();
//        String ktp = jsonReq.get("ktp").toString();
        String username = jsonReq.get("username").toString();

        int BiayaCetak = 0;
        String namaPengirim = "";
        String noKtp = "";
        String wilayah = "";
        String kota = "";
        String alamatPengirim = "";
        String sandiDatiPengirim = "";
        String description = "tt";
        String date = this.utilities.getDate("yyyy-MM-dd HH:mm:ss");
//        String sandiDatiPenerima = jsonReq.containsKey("sandiDati") ? jsonReq.get("sandiDati").toString() : "0300";
        //<editor-fold defaultstate="collapsed" desc="query prosesInq">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
            String sql = "SELECT  "
                    + "\"MsInformasiAgent\".\"Alamat\", "
                    + "\"MsInformasiAgent\".\"Nama\",  "
                    + "\"MsInformasiAgent\".\"NomorKTP\", "
                    + "\"MsKota\".\"NamaKota\", "
                    + "\"MsKecamatan\".\"NamaKecamatan\", "
                    + "\"MsKota\".\"SandiDATI\"  "
                    + "FROM  "
                    + "\"MsInformasiAgent\"  "
                    + "JOIN \"MsKelurahan\" ON \"MsInformasiAgent\".id_kelurahan = \"MsKelurahan\".id_kelurahan  "
                    + "JOIN \"MsKecamatan\" ON \"MsKelurahan\".id_kecamatan = \"MsKecamatan\".id_kecamatan  "
                    + "JOIN \"MsKota\" ON \"MsKecamatan\".id_kota = \"MsKota\".id_kota  "
                    + "WHERE  "
                    + "\"MsInformasiAgent\".id_agent = ? AND \"MsInformasiAgent\".id_tipeinformasiagent = 1 ";

            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            stPgsqlLb.setString(1, this.getID_Agent(username));

            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                alamatPengirim = rsPgsqlLb.getString("Alamat");
                sandiDatiPengirim = rsPgsqlLb.getString("SandiDATI");
                namaPengirim = rsPgsqlLb.getString("Nama");
                noKtp = rsPgsqlLb.getString("NomorKTP");
                wilayah = rsPgsqlLb.getString("NamaKecamatan");
                kota = rsPgsqlLb.getString("NamaKota");
            }

            if ((alamatPengirim != null && !alamatPengirim.isEmpty()) || (sandiDatiPengirim != null && !sandiDatiPengirim.isEmpty()) || (namaPengirim != null && !namaPengirim.isEmpty()) || (noKtp != null && !noKtp.isEmpty()) || (wilayah != null && !wilayah.isEmpty()) || (kota != null && !kota.isEmpty())) {
                int biaya = this.biayaTransferBank(Nominal);
                //System.out.println("biaya ===== " + biaya);

                if (username.startsWith("0")) {
                    BiayaCetak = biaya;
                } else {
                    BiayaCetak = biaya;
                }

                int harga_cetak = BiayaCetak + Nominal;

                boolean aray[] = this.getTB_Config(jsonReq.get("username").toString(), harga_cetak + "");

                boolean MAX_COUNT_DAY = aray[0];
                boolean MAX_DAY = aray[1];
//                boolean MAX_TRX = aray[2];
//                boolean MIN_TRX = aray[3];
                String minPayment = this.getMinPayment("6");
                String maxPayment = this.getMaxPayment("6");
                String notifMinPay = this.getNotifMinPay();
                String notifMaxPay = this.getNotifMaxPay();

                String[] config = this.getConfigCairkanSaldo(username, "NON_SAMSUNG");

                String max_count_day = config[0];
                String max_day = config[1];
                String max_trx = config[2];
                String min_trx = config[3];

                Locale locale = new Locale("id", "ID");
                NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
                String moneyString;

                if (Nominal >= Integer.parseInt(minPayment)) {
                    if (Nominal <= Integer.parseInt(maxPayment)) {
                        if (MAX_COUNT_DAY) {
                            if (MAX_DAY) {
                                //INQ
                                String Signature = this.getSignature(BeneficiaryAccount + "witamidevel" + gen_id + Kode_Bank);

                                // DO Inquiry Process
                                ThirdParty.put("type", "INQUIRY");
                                ThirdParty.put("TrxId", gen_id);
                                ThirdParty.put("SenderAccount", username);
                                ThirdParty.put("SenderSeluler", username);
                                ThirdParty.put("SenderName", namaPengirim.length() < 30 ? namaPengirim : namaPengirim.substring(0, 30));
                                ThirdParty.put("BeneficiaryAccount", BeneficiaryAccount);
                                ThirdParty.put("BeneficiaryCity", sandiDatiPengirim);
                                ThirdParty.put("BeneficiaryName", namaPengirim.length() < 30 ? namaPengirim : namaPengirim.substring(0, 30));
                                ThirdParty.put("Description", description);
                                ThirdParty.put("Amount", Nominal);
                                ThirdParty.put("ReceiverBankCode", Kode_Bank);
                                ThirdParty.put("AccountId", "witamidevel");
                                ThirdParty.put("datetime", date);
                                ThirdParty.put("Signature", Signature);
//                                    ThirdParty.put("SenderSeluler", SenderCellular);

                                ThirdParty.put("SenderKtp", noKtp);
                                ThirdParty.put("SenderAddress", alamatPengirim);
//                                    ThirdParty.put("BeneficiaryCity", sandiDatiPenerima);
//                                    ThirdParty.put("SenderCity", sandiDatiPengirim);
                                ThirdParty.put("SenderRegion", wilayah);
                                ThirdParty.put("SenderCity", sandiDatiPengirim);

                                String inquiry = this.prosesInquiry(ThirdParty.toString(), idSupplier); //eta anu ka prod na eh ka url prod , ok
//                                String inquiry = "{\"ResponseCode\":\"200\", \"ACK\":\"OK\",\"MSG\":\"000000~~Approved\", "
//                                        + "\"ResponseSourceName\":\"PT WITAMI TUNAI MANDIRI\", \"ResponseDestinationAcc\":\"0580767842\","
//                                        + "\"ResponseDestinationName\":\"LIBRI MONICA PRICILLA\",\"ResponseDestinationIdBank\":\"014\","
//                                        + "\"ResponseDestinationBankName\":\"BCA\",\"ResponseTerminalId\":\"1R3131382E3926\","
//                                        + "\"ResponseMsgKey\":\"211c50da\"}";
                                JSONParser jsonParser = new JSONParser();
                                JSONObject jsonObject = (JSONObject) jsonParser.parse(inquiry);
                                String httpStat = jsonObject.get("ResponseCode").toString();
                                if (httpStat.equals("200")) {
                                    String ACK = (String) jsonObject.get("ACK");
                                    if (ACK.equals("OK") || ACK.equals("SUSPECT")) {
                                        String BeneficiaryName = jsonObject.get("ResponseDestinationName").toString();
                                        String message = jsonObject.get("ResponseMsgKey").toString();
                                        String bank = jsonObject.get("ResponseDestinationBankName").toString();
                                        BeneficiaryAccount = jsonObject.get("ResponseDestinationAcc").toString();
                                        String status = "Approved";

                                        jsonResp.put("messageKey", jsonObject.get("ResponseMsgKey").toString());
                                        jsonResp.put("hargaCetak", harga_cetak);
                                        jsonResp.put("biayaTransferBank", BiayaCetak);
                                        jsonResp.put("nominalTransfer", Nominal);
                                        jsonResp.put("namaTujuan", BeneficiaryName);
                                        jsonResp.put("nomorRekeningTujuan", BeneficiaryAccount);
                                        jsonResp.put("namaBank", bank);
                                        jsonResp.put("Status", status);
                                        jsonResp.put("TrxId", gen_id);
//                                            jsonResp.put("Berita", TransferIssue);
                                        jsonResp.put("ResponseMsgKey", message);
                                        jsonResp.put("namaPengirim", namaPengirim);
//                                            jsonResp.put("hpPengirim", SenderCellular);
                                        jsonResp.put("Signature", Signature);
                                        jsonResp.put("ID", "inquiry");
                                        jsonResp.put("ACK", "OK");

                                    } else {
                                        jsonResp.put("ACK", "NOK");
                                        jsonResp.put("pesan", jsonObject.get("MSG"));
                                    }
                                } else {
                                    jsonResp.put("ACK", "NOK");
                                    jsonResp.put("pesan", jsonObject.get("MSG"));
                                }
                            } else {
                                moneyString = formatter.format(Integer.parseInt(max_day));

                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Anda sudah mencapai limit " + moneyString + " per hari");
                            }
                        } else {
                            jsonResp.put("ACK", "NOK");
                            jsonResp.put("pesan", "Anda sudah mencapai limit " + max_count_day + "x transaksi transfer per hari");
                        }
                    } else {
                        moneyString = formatter.format(Integer.parseInt(max_trx));

                        jsonResp.put("ACK", "NOK");
                        jsonResp.put("pesan", "Maksimal nominal per transaksi " + moneyString);
                    }
                } else {
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", notifMinPay);
                }
            } else {
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Transaksi tidak dapat diproses. Silahkan hubungi Customer Service True Money untuk melengkapi data Anda");
            }

        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        return jsonResp;
    }

    public String getID_Agent(String username) {
        String id_agent = "";
        //<editor-fold defaultstate="collapsed" desc="query getID_MemberNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \"MsAgentAccount\".id_agent "
                    + "FROM public.\"MsAgentAccount\" "
                    + "WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, username);

            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                id_agent = rsPgsql.getString("id_agent");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return id_agent;
    }

    public boolean[] getTB_Config(String idaccount, String nominal) {
        boolean hasil[] = new boolean[5];

        //<editor-fold defaultstate="collapsed" desc="query getTB_Config">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        String idAgentAcc = "";
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);

            //get id_memberaccount
            /*String getIdMember = "SELECT id_memberaccount FROM \"MsMemberAccount\" WHERE \"Handphone\" = ?";
            stPgsqlLb = connPgsqlLb.prepareStatement(getIdMember);
            stPgsqlLb.setString(1, idaccount);

            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                idMemberAcc = rsPgsqlLb.getString("id_memberaccount");
            }*/

            idAgentAcc = idaccount;

            String sql = "";
            if (idaccount.startsWith("0")) {
                sql = "WITH  "
                        + " detail AS ( "
                        + "	SELECT "
                        + "		\"count\"(*) as hitung, "
                        + "		COALESCE ( "
                        + "			\"sum\"(\"Nominal\"::INT) + ?, 0 "
                        + "		) AS total "
                        + "	FROM "
                        + "		\"TrTransaksi\" "
                        + "	WHERE "
                        + "		id_tipetransaksi = 6 "
                        + "	AND id_memberaccount = ? "
                        + "	AND \"TimeStamp\"::DATE = now()::DATE "
                        + "	AND \"upper\"(\"StatusTRX\") <> 'GAGAL' "
                        + "), "
                        + " max_count_day AS ( "
                        + "	SELECT "
                        + "		value_config AS max_count_day "
                        + "	FROM "
                        + "		tb_config "
                        + "	WHERE "
                        + "		id_tipetransaksi = 6 "
                        + "	AND key_config = 'MAX_COUNT_DAY' "
                        + "), "
                        + " max_day AS ( "
                        + "	SELECT "
                        + "		value_config AS max_day "
                        + "	FROM "
                        + "		tb_config "
                        + "	WHERE "
                        + "		id_tipetransaksi = 6 "
                        + "	AND key_config = 'MAX_DAY' "
                        + "), "
                        + " max_trx AS ( "
                        + "	SELECT "
                        + "		value_config AS max_trx "
                        + "	FROM "
                        + "		tb_config "
                        + "	WHERE "
                        + "		id_tipetransaksi = 6 "
                        + "	AND key_config = 'MAX_TRX' "
                        + "), "
                        + " min_trx AS ( "
                        + "	SELECT "
                        + "		value_config AS min_trx "
                        + "	FROM "
                        + "		tb_config "
                        + "	WHERE "
                        + "		id_tipetransaksi = 6 "
                        + "	AND key_config = 'MIN_TRX' "
                        + ")  "
                        + "SELECT "
                        + "	detail.hitung < max_count_day.max_count_day::INT AS maxcountday, "
                        + "	detail.total <= max_day.max_day::INT AS maxday, ? <= max_trx.max_trx::INT AS amountmaxday, ? >= min_trx.min_trx::INT AS amountminday "
                        + "FROM "
                        + "	max_count_day, "
                        + "	max_trx, "
                        + "	min_trx, "
                        + "	max_day, "
                        + "	detail";

                stPgsqlLb = connPgsqlLb.prepareStatement(sql);
                stPgsqlLb.setInt(1, Integer.parseInt(nominal));
                stPgsqlLb.setString(2, this.getID_Agent(idaccount));
                stPgsqlLb.setInt(3, Integer.parseInt(nominal));
                stPgsqlLb.setInt(4, Integer.parseInt(nominal));

                rsPgsqlLb = stPgsqlLb.executeQuery();

                while (rsPgsqlLb.next()) {
                    hasil[0] = rsPgsqlLb.getBoolean("maxcountday"); // MAX_COUNT_DAY
                    hasil[1] = rsPgsqlLb.getBoolean("maxday"); // MAX_DAY
                    hasil[2] = rsPgsqlLb.getBoolean("amountmaxday"); // MAX_TRX
                    hasil[3] = rsPgsqlLb.getBoolean("amountminday"); // MIN_TRX
                }
            } else {
                sql = "WITH agentacc AS ( " + "	SELECT " + "		*, ? AS nom " + "	FROM "
                        + "		\"MsAgentAccount\" " + "	WHERE " + "		id_agentaccount = ? "
                        + "), " + " detail AS ( " + "	SELECT " + "		\"count\" (id_transaksi) AS hitung, "
                        + "		COALESCE ( " + "			\"sum\" (\"Nominal\" :: INT) + ?, "
                        + "			'0' " + "		) AS total " + "	FROM " + "		\"TrTransaksi\" " + "	WHERE "
                        + "		id_tipetransaksi = 30 " + "	AND id_agentaccount = ? "
                        + "	AND \"TimeStamp\" :: DATE = now() :: DATE "
                        + "AND \"StatusTRX\" in ('SUKSES', 'PENDING', 'OPEN')), " + " max_count_day AS ( "
                        + "	SELECT " + "		value_config AS max_count_day " + "	FROM " + "		tb_config "
                        + "	WHERE " + "		id_tipetransaksi = 30 " + "	AND key_config = 'MAX_COUNT_DAY' "
                        + "), " + " max_day AS ( " + "	SELECT " + "		value_config AS max_day " + "	FROM "
                        + "		tb_config " + "	WHERE " + "		id_tipetransaksi = 30 "
                        + "	AND key_config = 'MAX_DAY' " + "), " + " max_trx AS ( " + "	SELECT "
                        + "		value_config AS max_trx " + "	FROM " + "		tb_config " + "	WHERE "
                        + "		id_tipetransaksi = 30 " + "	AND key_config = 'MAX_TRX' " + "), "
                        + " max_trx_samsung AS ( " + "	SELECT " + "		value_config AS max_trx " + "	FROM "
                        + "		tb_config " + "	WHERE " + "		id_tipetransaksi = 30 "
                        + "	AND key_config = 'MAX_TRX_SAMSUNG' " + "), " + " min_trx_samsung AS ( " + "	SELECT "
                        + "		value_config AS max_trx " + "	FROM " + "		tb_config " + "	WHERE "
                        + "		id_tipetransaksi = 30 " + "	AND key_config = 'MIN_TRX_SAMSUNG' " + "), "
                        + "max_day_samsung AS ( " + "	SELECT " + "		value_config AS max_day_samsung "
                        + "	FROM " + "		tb_config " + "	WHERE " + "		id_tipetransaksi = 30 "
                        + "	AND key_config = 'MAX_DAY_SAMSUNG' " + "), " + " min_trx AS ( " + "	SELECT "
                        + "		value_config AS max_trx " + "	FROM " + "		tb_config " + "	WHERE "
                        + "		id_tipetransaksi = 30 " + "	AND key_config = 'MIN_TRX' " + ") SELECT "
                        + "	CASE WHEN agentacc.id_dealer = 9 THEN 't' ELSE 'f' END AS \"isSamsung\", "
                        + "	detail.hitung < max_count_day.max_count_day :: INTEGER AS maxcountday, "
                        + "   CASE WHEN agentacc.id_dealer = 9 THEN detail.total <= max_day_samsung.max_day_samsung :: INTEGER ELSE detail.total <= max_day.max_day :: INTEGER END AS maxday, "
                        + "	CASE WHEN agentacc.id_dealer = 9 THEN agentacc.nom <= max_trx_samsung.max_trx :: INTEGER ELSE agentacc.nom <= max_trx.max_trx :: INTEGER END AS amountmaxday, "
                        + "	CASE WHEN agentacc.id_dealer = 9 THEN agentacc.nom >= min_trx_samsung.max_trx :: INTEGER ELSE agentacc.nom >= min_trx.max_trx :: INTEGER END AS amountminday "
                        + "FROM " + "	detail, " + "	max_count_day, " + "	max_trx, " + "	max_trx_samsung, "
                        + "	min_trx, " + "	min_trx_samsung, " + "   max_day_samsung, " + "	max_day, "
                        + "	agentacc";

                stPgsqlLb = connPgsqlLb.prepareStatement(sql);
                stPgsqlLb.setInt(1, Integer.parseInt(nominal));
                stPgsqlLb.setString(2, idAgentAcc);
                stPgsqlLb.setInt(3, Integer.parseInt(nominal));
                stPgsqlLb.setString(4, idAgentAcc);

                rsPgsqlLb = stPgsqlLb.executeQuery();

                while (rsPgsqlLb.next()) {
                    hasil[0] = rsPgsqlLb.getBoolean("maxcountday"); // MAX_COUNT_DAY
                    hasil[1] = rsPgsqlLb.getBoolean("maxday"); // MAX_DAY
                    hasil[2] = rsPgsqlLb.getBoolean("amountmaxday"); // MAX_TRX
                    hasil[3] = rsPgsqlLb.getBoolean("amountminday"); // MIN_TRX
                    hasil[4] = rsPgsqlLb.getBoolean("isSamsung"); // IS_SAMSUNG
                }
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return hasil;
    }

    public int biayaTransferBank(int Nominal) {
        int biaya = 0;
        //<editor-fold defaultstate="collapsed" desc="query biayaTransferBank">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
            String sql = "SELECT " + "	\"MsFeeRange\".* " + "FROM " + "	\"MsFeeRange\" "
                    + "JOIN \"MsFee\" ON \"MsFee\".id_fee = \"MsFeeRange\".id_fee "
                    + "WHERE ? <= \"MsFeeRange\".\"RangeMax\" " + "AND ? >= \"MsFeeRange\".\"RangeMin\" "
                    + "AND \"MsFeeRange\".\"FlagActive\" = 't' " + "AND \"MsFee\".id_tipetransaksi = 6 "
                    + "AND \"MsFee\".id_tipeaplikasi = 3;";
            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            stPgsqlLb.setInt(1, Nominal);
            stPgsqlLb.setInt(2, Nominal);

            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                biaya = rsPgsqlLb.getInt("Nominal"); // MEMBER
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return biaya;
    }

    public String[] getConfigCairkanSaldo(String idaccount, String key_config) throws SQLException, Exception {
        String value_config[] = new String[4];

        //<editor-fold defaultstate="collapsed" desc="query biayaTransferBank">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);

            String sql = "";
            if (idaccount.startsWith("0")) {
                sql = "WITH detail1 AS ( " + "	SELECT " + "		value_config AS \"MAX_TRX\" " + "	FROM "
                        + "		tb_config " + "	WHERE " + "		key_config = 'MAX_TRX' "
                        + "AND id_tipetransaksi = 6 " + "), " + " detail2 AS ( " + "	SELECT "
                        + "		value_config AS \"MAX_DAY\" " + "	FROM " + "		tb_config " + "	WHERE "
                        + "		key_config = 'MAX_DAY' " + "AND id_tipetransaksi = 6 " + "), " + " detail3 AS ( "
                        + "	SELECT " + "		value_config AS \"MAX_COUNT_DAY\" " + "	FROM " + "		tb_config "
                        + "	WHERE " + "		key_config = 'MAX_COUNT_DAY' " + "AND id_tipetransaksi = 6 " + "), "
                        + " detail4 AS ( " + "	SELECT " + "		value_config AS \"MIN_TRX\" " + "	FROM "
                        + "		tb_config " + "	WHERE " + "		key_config = 'MIN_TRX' "
                        + "AND id_tipetransaksi = 6 " + ") SELECT " + "	detail1.\"MAX_TRX\", "
                        + "	detail2.\"MAX_DAY\", " + "	detail3.\"MAX_COUNT_DAY\", " + "	detail4.\"MIN_TRX\" "
                        + "FROM " + "	detail1, " + "	detail2, " + "	detail3, " + "	detail4";

                stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            } else {
                String max, min, max_day = "";
                if (key_config.equalsIgnoreCase("SAMSUNG")) {
                    max = "MAX_TRX_SAMSUNG";
                    min = "MIN_TRX_SAMSUNG";
                    max_day = "MAX_DAY_SAMSUNG";
                } else {
                    max = "MAX_TRX";
                    min = "MIN_TRX";
                    max_day = "MAX_DAY";
                }

                sql = "WITH detail1 AS ( " + "	SELECT " + "		value_config AS \"MAX_TRX\" " + "	FROM "
                        + "		tb_config " + "	WHERE " + "		key_config = ? "
                        + "AND id_tipetransaksi = 30 " + "), " + " detail2 AS ( " + "	SELECT "
                        + "		value_config AS \"MAX_DAY\" " + "	FROM " + "		tb_config " + "	WHERE "
                        + "		key_config = ? " + "AND id_tipetransaksi = 30 " + "), "
                        + " detail3 AS ( " + "	SELECT " + "		value_config AS \"MAX_COUNT_DAY\" " + "	FROM "
                        + "		tb_config " + "	WHERE " + "		key_config = 'MAX_COUNT_DAY' "
                        + "AND id_tipetransaksi = 30 " + "), " + " detail4 AS ( " + "	SELECT "
                        + "		value_config AS \"MIN_TRX\" " + "	FROM " + "		tb_config " + "	WHERE "
                        + "		key_config = ? " + "AND id_tipetransaksi = 30 " + ") SELECT "
                        + "	detail1.\"MAX_TRX\", " + "	detail2.\"MAX_DAY\", " + "	detail3.\"MAX_COUNT_DAY\", "
                        + "	detail4.\"MIN_TRX\" " + "FROM " + "	detail1, " + "	detail2, " + "	detail3, "
                        + "	detail4";

                stPgsqlLb = connPgsqlLb.prepareStatement(sql);
                stPgsqlLb.setString(1, max);
                stPgsqlLb.setString(2, max_day);
                stPgsqlLb.setString(3, min);
            }

            rsPgsqlLb = stPgsqlLb.executeQuery();

            while (rsPgsqlLb.next()) {
                value_config[0] = rsPgsqlLb.getString("MAX_COUNT_DAY"); // MAX_COUNT_DAY
                value_config[1] = rsPgsqlLb.getString("MAX_DAY"); // MAX_DAY
                value_config[2] = rsPgsqlLb.getString("MAX_TRX"); // MAX_TRX
                value_config[3] = rsPgsqlLb.getString("MIN_TRX"); // MIN_TRX
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>

        return value_config;
    }

    public String getSignature(String string) {
        String hasil = "";
        try {

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(string.getBytes());

            byte byteData[] = md.digest();

            // convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            hasil = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public String prosesInquiry(String data, int idSupplier) {
        String response = "";
        String url = "";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'alto_transfer';";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

//            response = curl_post_inq(idSupplier.getUrl(), data, false);
        response = this.utilities.curlPost(url, data, false).toString();

        return response;
    }

    @Override
    public int gettrxid(String trxid) {
        int count = 0;
        //<editor-fold defaultstate="collapsed" desc="query gettrxid">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "select count(*) from \"TrTransaksi\" where id_trx = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return count;
    }

    @Override
    public String[] getStatusTrx(String trxid) {
        String status[] = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query getStatusTrx">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT"
                    + "  \"TrTransaksi\".\"CustomerReference\", \"TrTransaksi\".\"NoHPTujuan\", \"TrTransaksi\".\"NoRekTujuan\","
                    + "  \"TrTransaksi\".\"StatusTRX\", \"TrTransaksi\".\"id_pelanggan\" FROM"
                    + "  public.\"TrTransaksi\""
                    + "  WHERE \"TrTransaksi\".id_trx = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, trxid);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                status[0] = rsPgsql.getString("StatusTRX");
                status[1] = rsPgsql.getString("CustomerReference");
                status[2] = rsPgsql.getString("NoHPTujuan");
                status[3] = rsPgsql.getString("NoRekTujuan");
                status[4] = rsPgsql.getString("id_pelanggan");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return status;
    }

    @Override
    public String[] StatusRekeningAgentNonEDC(String username) {
        String[] statusRekMember = new String[5];
        //<editor-fold defaultstate="collapsed" desc="query StatusRekeningAgentNonEDC">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            String query = "SELECT \n" + "\"MsAccountStatus\".\"NamaAccountStatus\" "
                    + ",\"MsAgentAccount\".\"LastBalance\" ,\"MsAgentAccount\".\"StatusKartu\" "
                    + ",\"MsAgentAccount\".id_agentaccount ,\"MsAgentAccount\".\"PIN\" "
                    + " FROM public.\"MsAgentAccount\", public.\"MsAccountStatus\" WHERE "
                    + "\"MsAgentAccount\".id_accountstatus = \"MsAccountStatus\".id_accountstatus "
                    + "AND \"MsAgentAccount\".\"id_agentaccount\" = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, username);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                statusRekMember[0] = rsPgsql.getString("NamaAccountStatus");
                statusRekMember[1] = rsPgsql.getString("LastBalance");
                statusRekMember[2] = rsPgsql.getString("StatusKartu");
                statusRekMember[3] = rsPgsql.getString("id_agentaccount");
                statusRekMember[4] = this.encDec.decryptDbVal(rsPgsql.getString("PIN"), this.entityConfig.getEncKeyDb());
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return statusRekMember;
    }

    @Override
    public boolean CekIsEmptyTransfer(String nORek, int nominal, String idaccount) {
        boolean cek = false;
        int count = 0;
        String idAgentAcc = "";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            //get id_memberaccount
            /*String getIdMember = "SELECT id_memberaccount FROM \"MsMemberAccount\" WHERE \"Handphone\" = ?";
            stPgsql = connPgsql.prepareStatement(getIdMember);
            stPgsql.setString(1, idaccount);

            rsPgsql = stPgsql.executeQuery();

            while (rsPgsql.next()) {
                idMemberAcc = rsPgsql.getString("id_memberaccount");
            }*/

            idAgentAcc = idaccount;

            String query = "SELECT \"count\"(*) from \"TrTransaksi\" WHERE \"NoRekTujuan\" = ? "
                    + "and \"Nominal\" = ? " + " AND \"TimeStamp\"::DATE = now()::DATE "
                    + "AND (id_memberaccount = ? OR id_agentaccount = ?)"
                    + "AND \"upper\"(\"StatusTRX\") in ('SUKSES', 'OPEN', 'PENDING')";

            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, nORek);
            stPgsql.setString(2, String.valueOf(nominal));
            stPgsql.setString(3, idAgentAcc);
            stPgsql.setString(4, idAgentAcc);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                count = rsPgsql.getInt("count");
            }

            if (count == 0) {
                cek = true;
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return cek;
    }

    @Override
    public String isSaldoCukup(String idAccount, int total) {
        String hasil = ""; // cukup = false;
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        //String idAccount = jsonReq.get("username").toString();
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String q = "SELECT COALESCE(\"LastBalance\", 0) AS saldo FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
            if (q.trim().length() != 0) {
                long saldo = 0;
                stPgsql = connPgsql.prepareStatement(q);
                stPgsql.setString(1, idAccount);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    saldo = rsPgsql.getLong("saldo");
                }
                //System.out.println("saldo ===== " + saldo);
                if (saldo >= total) {
                    hasil = "00"; // = true;
                } else {
                    hasil = "01";
                }
            }
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                s = Throwables.getStackTraceAsString(ex1);
                controllerLog.logErrorWriter(s);
            }
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return hasil; //cukup;
    }

    @Override
    public JSONObject prosesTransfer(JSONObject jsonReq, JSONObject jsonResp, String ID_Agent, String[] com) {
        int id_tipetransaksi = 6;
        String NoKartu = null;
        int Nominal = 0;
        String Kode_Bank = null;
        String NORek = null;
        String verifikasi = "PIN";
        String SenderName = null;
        String namaPenerima = null;
        String keteranganPenerima = "";
        String messageKey = null;
        int BiayaTransferBank = 0;
        int Hargacetak = 0;
        String PIN = null;
        String gen_id = null;
        String ktp = null;

        NoKartu = jsonReq.get("username").toString();
        Nominal = Integer.parseInt(jsonReq.get("nominal").toString());
        Kode_Bank = jsonReq.get("kodeBank").toString();
        NORek = jsonReq.get("BeneficiaryAccount").toString();
        SenderName = jsonReq.get("senderName").toString();
        namaPenerima = jsonReq.get("BeneficiaryName").toString();
//        keteranganPenerima = jsonReq.get("transferIssue").toString()
        messageKey = jsonReq.get("messageKey").toString();
        BiayaTransferBank = Integer.parseInt(jsonReq.get("biayaTransferBank").toString());
        Hargacetak = Integer.parseInt(jsonReq.get("hargaCetak").toString());
        PIN = jsonReq.get("PIN").toString();
        gen_id = "TMN" + jsonReq.get("idtrx").toString();
//        ktp = jsonReq.get("ktp").toString();
        int total = Hargacetak + BiayaTransferBank;
        int totNominal = Nominal + BiayaTransferBank;

        int msSupplier = this.getIdSupplier(jsonReq);
        int fee[] = this.getFee(3, 6, 36, 0, "biasa");
        JSONObject ThirdParty = new JSONObject();

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);
            connPgsql.setAutoCommit(false);

            String deduct = this.deductBalanceAgent(NoKartu, String.valueOf(totNominal));

            if (deduct.equalsIgnoreCase("OK")) {

                String idtrxsupp = null;
                if (String.valueOf(msSupplier).equalsIgnoreCase("19")) {
                    idtrxsupp = gen_id;
                }

                // Update ke Tabel Transaksi
                String Keterangan = "Transfer ke " + this.getNamaBank(Kode_Bank) + " " + NORek
                        + " atas nama " + namaPenerima.toUpperCase() + " sebesar Rp " + Nominal;
                Keterangan = Keterangan.replace("'", "''");
                String info = "PENDING";
                String cetakTransaksi = "INSERT INTO \"TrTransaksi\"( " + "id_tipetransaksi,\"Nominal\",  "
                        + "\"StatusTRX\",\"StatusDeduct\",\"id_trx\",\"OpsVerifikasi\",\"Keterangan\",  "
                        + "\"TimeStamp\",id_tipeaplikasi,\"TypeTRX\",\"Total\",\"NoRekTujuan\",id_agentaccount,"
                        + "\"StatusRefund\", \"StatusKomisi\", id_bank, \"Biaya\" " + ",\"Nomorktp\" , id_supplier, id_operator,"
                        + "\"StatusDana\", id_trxsupplier, hargabeli, hargajual) "
                        + "VALUES('" + 6 + "',?,?,?,?,?,?,now(),3,'WITHDRAW',?,?,?,'NOK','PENDING',?,?,?,?,36,'DEDUCT',?,?,?);";

                stPgsql = connPgsql.prepareStatement(cetakTransaksi, Statement.RETURN_GENERATED_KEYS);
                stPgsql.setString(1, String.valueOf(Nominal));
                stPgsql.setString(2, info);
                stPgsql.setString(3, deduct);
                stPgsql.setString(4, gen_id);
                stPgsql.setString(5, verifikasi);
                stPgsql.setString(6, Keterangan);
                stPgsql.setInt(7, Hargacetak);
                stPgsql.setString(8, NORek);
                stPgsql.setString(9, ID_Agent);
                stPgsql.setInt(10, Integer.parseInt(this.getidbank(Kode_Bank)));
                stPgsql.setString(11, String.valueOf(BiayaTransferBank));
                stPgsql.setString(12, ktp);
                stPgsql.setInt(13, msSupplier);
                stPgsql.setString(14, idtrxsupp);
                stPgsql.setInt(15, (fee[1] + Nominal));
                stPgsql.setInt(16, Hargacetak);
                int hasil = stPgsql.executeUpdate();

                String idtransaksi = null;
                if (hasil > 0) {
                    rsPgsql = stPgsql.getGeneratedKeys();
                    while (rsPgsql.next()) {
                        idtransaksi = rsPgsql.getString(1);
                    }
                }

                String sqlInsertMsTMNStock = "INSERT"
                        + " INTO \"TrStock\" (\"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                        + " VALUES (?,now(),'0','Agent',?,'0',?,'WITHDRAW',?,'0');";
                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                stPgsql.setLong(1, this.getLastBalanceAgent(ID_Agent));
                stPgsql.setInt(2, Hargacetak);
                stPgsql.setString(3, ID_Agent);
                stPgsql.setLong(4, Long.parseLong(idtransaksi));
                int status = stPgsql.executeUpdate();

                String inquiry = "{\"ResponseCode\":\"200\",\"ACK\":\"OK\"}";
//                String inquiry = "{\"ResponseCode\":\"200\",\"ACK\":\"NOK\"}";
                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject = (JSONObject) jsonParser.parse(inquiry);
                String httpStat = jsonObject.get("ResponseCode").toString();
                if (httpStat.equals("200")) {
                    String ACK = jsonObject.get("ACK").toString();
                    if (ACK.equals("OK") || ACK.equals("SUSPECT")) {
                        String signature = this.getSignature(NORek + "" + gen_id + "" + Kode_Bank + messageKey);
                        String date = this.utilities.getDate("yyyy-MM-dd HH:mm:ss");
                        ThirdParty = new JSONObject();
                        ThirdParty.put("type", "TRANSFERONLINE");
                        ThirdParty.put("TrxId", gen_id);
                        ThirdParty.put("AccountId", ID_Agent);
                        ThirdParty.put("datetime", date);
                        ThirdParty.put("MsgKey", messageKey);
                        ThirdParty.put("Signature", signature);
                        ThirdParty.put("note", keteranganPenerima);
                        ThirdParty.put("BeneficiaryCity", this.getCity(this.getID_Agent(NoKartu)));
                        ThirdParty.put("SenderCity", this.getCity(this.getID_Agent(NoKartu)));

                        inquiry = this.prosesTrf(ThirdParty.toString(), msSupplier);
                        jsonParser = new JSONParser();
                        jsonObject = (JSONObject) jsonParser.parse(inquiry);

                        httpStat = jsonObject.get("ResponseCode").toString();

                        if (httpStat.equals("200")) {
                            ACK = jsonObject.get("ACK").toString();
                            if (ACK.equals("OK")) {
                                String updateTransaksi = "UPDATE \"TrTransaksi\" SET \"StatusRefund\"='NOK', \"StatusKomisi\"='OK',"
                                        + "\"Biaya\" = ? , \"StatusTRX\"='SUKSES', \"StatusDana\"='DEDUCT', "
                                        + "timestampupdatetrx = now() WHERE id_trx = ? ";
                                stPgsql = connPgsql.prepareStatement(updateTransaksi);
                                stPgsql.setInt(1, BiayaTransferBank);
                                stPgsql.setString(2, gen_id);
                                status = stPgsql.executeUpdate();

                                //true
                                int nominalTrue = Nominal + fee[1];
                                sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                        + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                        + " VALUES (123456, now(), 0, 'True', ?, 0, 1, 'DEPOSIT', ?, 0);";
                                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                                stPgsql.setLong(1, nominalTrue);
                                stPgsql.setLong(2, Long.parseLong(idtransaksi));
                                stPgsql.executeUpdate();

                                int margin = 0;
                                double komAgent = 0, komTrue = 0;

                                //calculate commission for Agent & True
                                margin = BiayaTransferBank - fee[1];
                                komTrue = (Double.parseDouble(com[0]) * margin) / 100;
                                komAgent = (Double.parseDouble(com[1]) * margin) / 100;

                                // insert tempkomisi
                                //System.out.println("komisi agent " + komAgent);
                                //<editor-fold defaultstate="collapsed" desc="query insert tempkomisi">
                                String tempkomisi = "INSERT INTO \"TempKomisi\" "
                                        + " (\"id_trx\", \"Agent\", \"True\", \"Status\", \"StatusTransaksi\") "
                                        + " VALUES (?, ?, ?, 'OK', 'OK')";
                                stPgsql = connPgsql.prepareStatement(tempkomisi);
                                stPgsql.setString(1, idtransaksi);
                                stPgsql.setFloat(2, (int) Math.ceil(komAgent));
                                stPgsql.setFloat(3, (int) Math.floor(komTrue));
                                stPgsql.executeUpdate();
                                //</editor-fold>

                                // komisi agent
                                //<editor-fold defaultstate="collapsed" desc="query komisi agent">
                                String hitungkomisi = null;
                                String sql = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                                stPgsql = connPgsql.prepareStatement(sql);
                                stPgsql.setString(1, ID_Agent);
                                stPgsql.executeQuery();

                                hitungkomisi = "UPDATE \"MsAgentAccount\" SET  \"LastBalance\" = \"LastBalance\" + ? WHERE id_agentaccount = ?;";
                                stPgsql = connPgsql.prepareStatement(hitungkomisi);
                                stPgsql.setLong(1, (int) Math.ceil(komAgent));
                                stPgsql.setString(2, ID_Agent);
                                stPgsql.executeUpdate();
                                //</editor-fold>
                                // update TMNStock agent
                                //<editor-fold defaultstate="collapsed" desc="query update TMNStock agent">
                                //get lastbalance agent
                                String lb = "";
                                String getAmount = "SELECT \"MsAgentAccount\".\"LastBalance\" FROM public.\"MsAgentAccount\" WHERE \"MsAgentAccount\".\"id_agentaccount\" = ?;";

                                stPgsql = connPgsql.prepareStatement(getAmount);
                                stPgsql.setString(1, ID_Agent);
                                rsPgsql = stPgsql.executeQuery();
                                while (rsPgsql.next()) {
                                    lb = rsPgsql.getString("LastBalance");
                                }

                                if (lb == null && lb.isEmpty()) {
                                    lb = "0";
                                }

                                sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                        + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\",  "
                                        + " \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                        + " VALUES (?, now(), 0, 'Agent', ?, ?, ?, 'DEPOSIT', ?, 0);";
                                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                                stPgsql.setLong(1, Long.parseLong(lb));
                                stPgsql.setLong(2, (int) Math.ceil(komAgent));
                                stPgsql.setLong(3, Long.parseLong(String.valueOf((int) Math.ceil(komAgent))));
                                stPgsql.setString(4, ID_Agent);
                                stPgsql.setLong(5, Long.parseLong(idtransaksi));
                                stPgsql.executeUpdate();
                                //</editor-fold>

                                //komisi true
                                //nominalTrue = BiayaTransferBank - fee[1];
                                sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                        + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                        + " VALUES (123456, now(), ?, 'True', ?, 0, 1, 'DEPOSIT', ?, 0);";
                                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);
                                stPgsql.setLong(1, (int) Math.ceil(komTrue));
                                stPgsql.setLong(2, (int) Math.ceil(komTrue));
                                stPgsql.setLong(3, Long.parseLong(idtransaksi));
                                stPgsql.executeUpdate();

                                //update TMNStock supplier
                                int nominalSupplier = Nominal + fee[1];
                                sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                        + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                        + " VALUES (123456, now(), 0, 'Supplier', ?, 0, ?, 'WITHDRAW', ?, 0);";
                                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);

                                stPgsql.setLong(1, nominalSupplier);
                                stPgsql.setInt(2, fee[0]);
                                stPgsql.setLong(3, Long.parseLong(idtransaksi));
                                stPgsql.executeUpdate();

                                String nominal = NumberFormat.getInstance().format(Nominal);

                                String pesan = "Transfer ke " + this.getNamaBank(Kode_Bank) + " " + NORek
                                        + " atas nama " + namaPenerima.toUpperCase() + " sebesar Rp" + nominal + " pada "
                                        + sfd.format(new java.util.Date()) + " SUKSES";

                                jsonResp.put("id_Trx", gen_id);
                                jsonResp.put("NORek", NORek);
                                jsonResp.put("namabank", this.getNamaBank(Kode_Bank));
                                jsonResp.put("namapenerima", namaPenerima);
                                jsonResp.put("nominal", Nominal);
                                jsonResp.put("admin", BiayaTransferBank);
                                jsonResp.put("total", Hargacetak);
                                jsonResp.put("lastBalance", this.getLastBalanceAgent(ID_Agent));
                                jsonResp.put("pesan", Keterangan);
                                jsonResp.put("idMember", ID_Agent);
                                jsonResp.put("idAgent", ID_Agent);
                                jsonResp.put("ID", "transfer");
                                jsonResp.put("ACK", "OK");
                                jsonResp.put("timestamp", this.utilities.getDate().toString());
                                jsonResp.put("statusTRX", "SUKSES");

                                int saldoawal = Hargacetak + Integer.parseInt(jsonResp.get("lastBalance").toString());
                                jsonResp.put("saldoawal", saldoawal);
                                //insert notif
                                //<editor-fold defaultstate="collapsed" desc="query insert notif">
                                sql = "INSERT INTO \"Notifikasi\" "
                                        + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                        + " VALUES (?, ?, ?,'FALSE',now())";
                                stPgsql = connPgsql.prepareStatement(sql);
                                stPgsql.setString(1, pesan);
                                stPgsql.setString(2, ID_Agent);
                                stPgsql.setString(3, idtransaksi);
                                stPgsql.executeUpdate();

                                String smsScript = null;
                                //<editor-fold defaultstate="collapsed" desc="query kirim sms">
                                sql = "select"
                                        + "        this_.id,"
                                        + "        this_.account_type,"
                                        + "        this_.created_by,"
                                        + "        this_.flag_active,"
                                        + "        this_.id_tipeaplikasi,"
                                        + "        this_.id_tipetransaksi,"
                                        + "        this_.sms_script,"
                                        + "        this_.timestamp,"
                                        + "        this_.trx_status "
                                        + "    from"
                                        + "        public.\"MsSMSNotification\" this_ "
                                        + "    where"
                                        + "        this_.id_tipetransaksi=? "
                                        + "        and this_.id_tipeaplikasi=? "
                                        + "        and this_.account_type=? "
                                        + "        and this_.trx_status=?";
                                stPgsql = connPgsql.prepareStatement(sql);
                                stPgsql.setLong(1, 6);
                                stPgsql.setLong(2, 3);
                                stPgsql.setString(3, "AGENT");
                                stPgsql.setString(4, "SUKSES");
                                rsPgsql = stPgsql.executeQuery();
                                while (rsPgsql.next()) {
                                    smsScript = rsPgsql.getString("sms_script");
                                }
                                if (smsScript != null) {

                                    pesan = String.format(smsScript, this.getNamaBank(Kode_Bank), NORek, namaPenerima.toUpperCase(), nominal, sfd.format(new Date()));

                                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                                    new SmsHelper().sent(uuid, NoKartu, pesan, "T2B", entityConfig);
                                }

                                //</editor-fold>
                            } else if (ACK.equals("SUSPECT")) {
                                String pesan = "Transaksi Anda sedang diproses";

                                jsonResp.put("id_Trx", gen_id);
                                jsonResp.put("NORek", NORek);
                                jsonResp.put("namabank", this.getNamaBank(Kode_Bank));
                                jsonResp.put("namapenerima", namaPenerima);
                                jsonResp.put("nominal", Nominal);
                                jsonResp.put("admin", BiayaTransferBank);
                                jsonResp.put("total", Hargacetak);
                                jsonResp.put("lastBalance", this.getLastBalanceAgent(ID_Agent));
                                jsonResp.put("pesan", pesan);
                                jsonResp.put("ID", "transfer");
                                jsonResp.put("ACK", "OK");
                                jsonResp.put("timestamp", this.utilities.getDate().toString());
                                jsonResp.put("statusTRX", "PENDING");
                            } else {

                                //<editor-fold defaultstate="collapsed" desc="query refundMember">
                                String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                                stPgsql = connPgsql.prepareStatement(sql1);
                                stPgsql.setString(1, ID_Agent);
                                //stPgsql.setString(2, ID_Agent);
                                rsPgsql = stPgsql.executeQuery();

                                String sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + Hargacetak + " "
                                        + "WHERE id_agentaccount = ? "; //OR \"Handphone\"= ? ";
                                stPgsql = connPgsql.prepareStatement(sql);
                                stPgsql.setString(1, ID_Agent);
                                //stPgsql.setString(2, ID_Agent);
                                stPgsql.executeUpdate();
                                //</editor-fold>

                                sql = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK',"
                                        + "\"StatusDana\" = 'REFUND', \"StatusDeduct\" = 'NOK', \"StatusKomisi\" = 'NOK',"
                                        + "timestamprefund = now() " + "WHERE \"id_trx\" = ?";
                                stPgsql = connPgsql.prepareStatement(sql);
                                stPgsql.setString(1, gen_id);
                                hasil = stPgsql.executeUpdate();

                                //member refund
                                sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                                        + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                                        + " VALUES (?, now(), 0, 'Agent', ?, 0, ?, 'REFUND', ?, 0);";
                                stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);

                                stPgsql.setLong(1, this.getLastBalanceAgent(ID_Agent) + Hargacetak);
                                stPgsql.setInt(2, Hargacetak);
                                stPgsql.setLong(3, Long.parseLong(ID_Agent));
                                stPgsql.setLong(4, Long.parseLong(idtransaksi));
                                stPgsql.executeUpdate();
                                String pesan = "Transaksi Anda gagal dengan nomor transaksi: " + gen_id;
                                //insert notif
                                //<editor-fold defaultstate="collapsed" desc="query insert notif">
                                sql = "INSERT INTO \"Notifikasi\" "
                                        + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                                        + " VALUES (?, ?, ?,'FALSE',now())";
                                stPgsql = connPgsql.prepareStatement(sql);
                                stPgsql.setString(1, pesan);
                                stPgsql.setString(2, ID_Agent);
                                stPgsql.setString(3, idtransaksi);
                                stPgsql.executeUpdate();
                                //</editor-fold>

                                jsonResp.put("id_Trx", gen_id);
                                jsonResp.put("ACK", "NOK");
                                jsonResp.put("pesan", "Transaksi Anda gagal");
                                jsonResp.put("statusTRX", "GAGAL");
                            }
                        } else {
//                            String updateTransaksi = "UPDATE \"TrTransaksi\" "
//                                    + "SET \"StatusTRX\" = 'PENDING', \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'PENDING' "
//                                    + " WHERE id_trx = '" + gen_id + "'";
//                            stPgsql = connPgsql.prepareStatement(updateTransaksi);
//                            hasil = stPgsql.executeUpdate();
//
//                            String updateTemp = "UPDATE \"TempKomisi\" "
//                                    + "SET \"Status\" = 'PENDING', \"StatusTransaksi\" = 'PENDING' "
//                                    + "WHERE \"id_trx\" = '" + gen_id + "'";
//                            stPgsql = connPgsql.prepareStatement(updateTemp);
//                            stPgsql.executeUpdate();
//
//                            String pesan = "Transaksi Anda sedang diproses";
//
//                            jsonResp.put("id_Trx", gen_id);
//                            jsonResp.put("pesan", pesan);
//                            jsonResp.put("timestamp", utilities.getDate().toString());
//                            jsonResp.put("statusTRX", "PENDING");
//                            jsonResp.put("ACK", "OK");
                            String pesan = "Transaksi Anda sedang diproses";

                            jsonResp.put("id_Trx", gen_id);
                            jsonResp.put("pesan", pesan);
                            jsonResp.put("NORek", NORek);
                            jsonResp.put("namabank", this.getNamaBank(Kode_Bank));
                            jsonResp.put("namapenerima", namaPenerima);
                            jsonResp.put("nominal", Nominal);
                            jsonResp.put("admin", BiayaTransferBank);
                            jsonResp.put("total", Hargacetak);
                            jsonResp.put("lastBalance", this.getLastBalanceAgent(ID_Agent));
                            jsonResp.put("timestamp", utilities.getDate().toString());
                            jsonResp.put("statusTRX", "PENDING");
                            jsonResp.put("ACK", "OK");

//                            String pesan = "Transaksi Anda gagal dengan nomor transaksi: " + gen_id;
//                            jsonResp.put("id_Trx", gen_id);
//                            jsonResp.put("ACK", "NOK");
//                            jsonResp.put("pesan", "Transaksi Anda gagal");
//                            jsonResp.put("statusTRX", "GAGAL");
                        }

                    } else {
                        // update tr transaksi
                        String updateTransaksi = "UPDATE \"TrTransaksi\" "
                                + "SET \"StatusTRX\" = 'PENDING', \"StatusRefund\" = 'NOK', \"StatusKomisi\" = 'PENDING' "
                                + " WHERE id_trx = '" + gen_id + "'";
                        stPgsql = connPgsql.prepareStatement(updateTransaksi);
                        hasil = stPgsql.executeUpdate();

                        String updateTemp = "UPDATE \"TempKomisi\" "
                                + "SET \"Status\" = 'PENDING', \"StatusTransaksi\" = 'PENDING' "
                                + "WHERE \"id_trx\" = '" + gen_id + "'";
                        stPgsql = connPgsql.prepareStatement(updateTemp);
                        stPgsql.executeUpdate();

                        String pesan = "Transaksi Anda sedang diproses";

                        jsonResp.put("id_Trx", gen_id);
                        jsonResp.put("pesan", pesan);
                        jsonResp.put("NORek", NORek);
                        jsonResp.put("namabank", this.getNamaBank(Kode_Bank));
                        jsonResp.put("namapenerima", namaPenerima);
                        jsonResp.put("nominal", Nominal);
                        jsonResp.put("admin", BiayaTransferBank);
                        jsonResp.put("total", Hargacetak);
                        jsonResp.put("lastBalance", this.getLastBalanceAgent(ID_Agent));
                        jsonResp.put("timestamp", utilities.getDate().toString());
                        jsonResp.put("statusTRX", "PENDING");
                        jsonResp.put("ACK", "OK");
                    }
                } else {
                    //<editor-fold defaultstate="collapsed" desc="query refundMember">
                    String sql1 = "SELECT * FROM \"MsAgentAccount\" WHERE id_agentaccount = ? FOR UPDATE";
                    stPgsql = connPgsql.prepareStatement(sql1);
                    stPgsql.setString(1, ID_Agent);
                    //stPgsql.setString(2, ID_Agent);
                    rsPgsql = stPgsql.executeQuery();

                    String sql = "UPDATE \"MsAgentAccount\" " + "SET \"LastBalance\" = \"LastBalance\" + " + Hargacetak + " "
                            + "WHERE id_agentaccount = ? ";
                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.setString(1, ID_Agent);
                    //stPgsql.setString(2, ID_Agent);
                    stPgsql.executeUpdate();
                    //</editor-fold>

                    sql = "UPDATE \"TrTransaksi\" SET \"StatusTRX\" = 'GAGAL', \"StatusRefund\" = 'OK',"
                            + "\"StatusDana\" = 'REFUND', \"StatusDeduct\" = 'NOK', \"StatusKomisi\" = 'NOK',"
                            + "timestamprefund = now() " + "WHERE \"id_trx\" = ?";
                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.setString(1, gen_id);
                    hasil = stPgsql.executeUpdate();

                    //member refund
                    sqlInsertMsTMNStock = "INSERT INTO \"TrStock\"( "
                            + " \"LastBalance\", \"TimeStamp\", \"True\", \"StokType\", \"Nominal\", \"Agent\", id_stock, \"TypeTrx\", id_transaksi, \"Dealer\")"
                            + " VALUES (?, now(), 0, 'Agent', ?, 0, ?, 'REFUND', ?, 0);";
                    stPgsql = connPgsql.prepareStatement(sqlInsertMsTMNStock);

                    stPgsql.setLong(1, this.getLastBalanceAgent(ID_Agent) + Hargacetak);
                    stPgsql.setInt(2, Hargacetak);
                    stPgsql.setLong(3, Long.parseLong(ID_Agent));
                    stPgsql.setLong(4, Long.parseLong(idtransaksi));
                    stPgsql.executeUpdate();

                    String pesan = "Transaksi Anda gagal dengan nomor transaksi: " + gen_id;
                    //insert notif
                    //<editor-fold defaultstate="collapsed" desc="query insert notif">
                    sql = "INSERT INTO \"Notifikasi\" "
                            + "(\"DetailNotifikasi\", id_account, id_trx, \"StatusRead\", \"TimeStamp\")"
                            + " VALUES (?, ?, ?,'FALSE',now())";
                    stPgsql = connPgsql.prepareStatement(sql);
                    stPgsql.setString(1, pesan);
                    stPgsql.setString(2, ID_Agent);
                    stPgsql.setString(3, idtransaksi);
                    stPgsql.executeUpdate();
                    //</editor-fold>
                    jsonResp.put("id_Trx", gen_id);
                    jsonResp.put("ACK", "NOK");
                    jsonResp.put("pesan", "Transaksi Anda gagal");
                    jsonResp.put("statusTRX", "GAGAL");
                }
                connPgsql.commit();

            } else {
                jsonResp.put("id_Trx", gen_id);
                jsonResp.put("ACK", "NOK");
                jsonResp.put("pesan", "Transaksi Anda gagal");
            }
            connPgsql.commit();
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
            try {
                connPgsql.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (ParseException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return jsonResp;
    }

    @Override
    public String[] getComission(int idTipeTransaksi, int idTipeAplikasi, int idOperator) {
        String[] comission = new String[3];
        //<editor-fold defaultstate="collapsed" desc="query getComission">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPosgre = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connPosgre = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String sqlGetRangeComission = "select * from \"MsFeeSupplier\" a, \"MsFee\" b"
                    + " where a.id_fee = b.id_fee"
                    + " and b.id_tipetransaksi = ?"
                    + " and b.id_tipeaplikasi = ?"
                    + " and a.id_operator = ?"
                    + " and a.\"FlagActive\" = true";

            ps = connPosgre.prepareStatement(sqlGetRangeComission);
            ps.setLong(1, idTipeTransaksi);
            ps.setLong(2, idTipeAplikasi);
            ps.setLong(3, idOperator);
            rs = ps.executeQuery();
            if (rs.next()) {
                comission[0] = rs.getString("TrueFeeNom");
                comission[1] = rs.getString("AgentCommNom");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connPosgre != null) {
                    connPosgre.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return comission;
    }

    public String deductBalanceAgent(String idAccount, String nominal) {
        String Return = "";
        int lastBalance = 0;

        //<editor-fold defaultstate="collapsed" desc="query deductBalanceMember">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
            String sql = "SELECT \"LastBalance\" FROM \"MsAgentAccount\" WHERE \"id_agentaccount\"= ? FOR UPDATE;";
            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            stPgsqlLb.setString(1, idAccount);
            //stPgsqlLb.setString(2, idAccount);
            rsPgsqlLb = stPgsqlLb.executeQuery();
            while (rsPgsqlLb.next()) {
                lastBalance = rsPgsqlLb.getInt("LastBalance");
            }

            if (lastBalance < Integer.parseInt(nominal)) {
                Return = "NOK";

                return Return;
            }

            String query = "UPDATE \"MsAgentAccount\" SET \"LastBalance\"= \"LastBalance\" - ? "
                    + " WHERE \"id_agentaccount\"= ?;";
            stPgsqlLb = connPgsqlLb.prepareStatement(query);
            stPgsqlLb.setInt(1, Integer.parseInt(nominal));
            stPgsqlLb.setString(2, idAccount);
            //stPgsqlLb.setString(3, idAccount);
            int status = stPgsqlLb.executeUpdate();

            if (status == 1) {
                Return = "OK";
            } else {
                Return = "NOK";
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return Return;
    }

    public Long getLastBalanceAgent(String idAgentAcc) {
        long lastBalance = 0;
        //<editor-fold defaultstate="collapsed" desc="query getLastBalanceMember">
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
            String query = "select \"LastBalance\" from \"MsAgentAccount\" "
                    + "where id_agentaccount = ?";
            stPgsqlLb = connPgsqlLb.prepareStatement(query);
            stPgsqlLb.setString(1, idAgentAcc);
            rsPgsqlLb = stPgsqlLb.executeQuery();
            while (rsPgsqlLb.next()) {
                lastBalance = rsPgsqlLb.getLong("LastBalance");
            }
        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        //</editor-fold>
        return lastBalance;
    }

    public String getidbank(String kodebank) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        String idbank = "";
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String query = "SELECT \"MsBank\".id_bank FROM \"MsBank\" WHERE kodebank = ?;";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, kodebank);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idbank = rsPgsql.getString("id_bank");
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return idbank;
    }

    public int[] getFee(int idtipefee, int idtipetransaksi, int idoperator, int nominal, String tipe) {
        int[] fee = new int[4];

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        String idbank = "";
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            if (idtipefee == 3) {
                String query = "SELECT " + "b.\"HargaCetak\", " + "b.\"HargaCetakMember\", " + "b.\"HargaBeli\", "
                        + "b.id_supplier " + "FROM " + "\t\"MsFeeSupplier\" AS b "
                        + "INNER JOIN \"MsFee\" d ON b.id_fee = d.id_fee " + "WHERE " + "\td.id_tipeaplikasi = 3 "
                        + "AND d.id_tipetransaksi = ? AND b.id_operator = ? AND b.\"FlagActive\" = TRUE";
                stPgsql = connPgsql.prepareStatement(query);
                stPgsql.setInt(1, idtipetransaksi);
                stPgsql.setInt(2, idoperator);
                rsPgsql = stPgsql.executeQuery();
                while (rsPgsql.next()) {
                    fee[0] = rsPgsql.getInt("id_supplier");
                    fee[1] = rsPgsql.getInt("HargaBeli");
                    fee[2] = rsPgsql.getInt("HargaCetak");
                    fee[3] = rsPgsql.getInt("HargaCetakMember");
                }
                if ((fee[1] + "" != null && !String.valueOf(fee[1]).isEmpty())
                        || (fee[2] + "" != null && !String.valueOf(fee[2]).isEmpty())
                        || fee[3] + "" != null && !String.valueOf(fee[3]).isEmpty()) {
                } else {
                    fee[1] = 0;
                    fee[2] = 0;
                    fee[3] = 0;
                }
            } else if (idtipefee == 1) {
                String getFee = "SELECT \"Nominal\" FROM \"MsFee\" WHERE id_tipetransaksi = ? AND id_tipeaplikasi=3";
                stPgsql = connPgsql.prepareStatement(getFee);
                stPgsql.setInt(1, idtipetransaksi);
                rsPgsql = stPgsql.executeQuery();

                while (rsPgsql.next()) {
                    fee[0] = rsPgsql.getInt("Nominal");
                }
                if (fee[0] + "" != null && !String.valueOf(fee[0]).isEmpty()) {
                } else {
                    fee[0] = 0;
                }
            } else if (idtipefee == 2) {
                String sqlGetFee = null;

                if (tipe.equalsIgnoreCase("samsung")) {
                    sqlGetFee = "SELECT " + "	\"MsFeeRange\".* " + "FROM " + "	\"MsFeeRange\" "
                            + "JOIN \"MsFee\" ON \"MsFeeRange\".id_fee = \"MsFee\".id_fee "
                            + "JOIN \"MsTipeTransaksi\" ON \"MsFee\".id_tipetransaksi = \"MsTipeTransaksi\".id_tipetransaksi "
                            + "WHERE " + "	\"MsFee\".id_tipetransaksi = ? "
                            + "AND \"MsFee\".id_tipeaplikasi = 3 " + ""
                            + "AND ? >= \"MsFeeRange\".\"RangeMin\" " + ""
                            + "AND ? <= \"MsFeeRange\".\"RangeMax\" " + "AND \"MsFeeRange\".\"FlagActive\" = 't' "
                            + "AND \"MsFee\".\"FlagActive\" = 't' " + "ORDER BY " + "	\"Nominal\" DESC";

                } else {
                    sqlGetFee = "SELECT " + "	\"MsFeeRange\".* " + "FROM " + "	\"MsFeeRange\" "
                            + "JOIN \"MsFee\" ON \"MsFeeRange\".id_fee = \"MsFee\".id_fee "
                            + "JOIN \"MsTipeTransaksi\" ON \"MsFee\".id_tipetransaksi = \"MsTipeTransaksi\".id_tipetransaksi "
                            + "WHERE " + "	\"MsFee\".id_tipetransaksi = ? "
                            + "AND \"MsFee\".id_tipeaplikasi = 3 "
                            + "AND ? >= \"MsFeeRange\".\"RangeMin\" " + "AND ? "
                            + "<= \"MsFeeRange\".\"RangeMax\" " + "AND \"MsFeeRange\".\"FlagActive\" = 't' "
                            + "AND \"MsFee\".\"FlagActive\" = 't' " + "ORDER BY " + "	\"Nominal\" ASC";

                }

                stPgsql = connPgsql.prepareStatement(sqlGetFee);
                stPgsql.setInt(1, idtipetransaksi);
                stPgsql.setInt(2, nominal);
                stPgsql.setInt(3, nominal);

                rsPgsql = stPgsql.executeQuery();

                while (rsPgsql.next()) {
                    fee[0] = rsPgsql.getInt("Nominal");
                }
                if (fee[0] + "" != null && !String.valueOf(fee[0]).isEmpty()) {
                } else {
                    fee[0] = 0;
                }
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return fee;
    }

    public String getCity(String NoKartu) {
        String idCity = "";
        DatabaseUtilitiesPgsql databaseUtilitiesPgsqlLb = new DatabaseUtilitiesPgsql();
        Connection connPgsqlLb = null;
        PreparedStatement stPgsqlLb = null;
        ResultSet rsPgsqlLb = null;
        try {
            connPgsqlLb = databaseUtilitiesPgsqlLb.getConnection(this.entityConfig);
            String sql = "SELECT id_kota FROM \"MsInformasiAgent\" WHERE \"id_agent\" = ? ";

            stPgsqlLb = connPgsqlLb.prepareStatement(sql);
            stPgsqlLb.setString(1, NoKartu);
            rsPgsqlLb = stPgsqlLb.executeQuery();
            while (rsPgsqlLb.next()) {
                idCity = rsPgsqlLb.getString("id_kota");
            }

        } catch (ParserConfigurationException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (SQLException ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } catch (Exception ex) {
            String s = Throwables.getStackTraceAsString(ex);
            controllerLog.logErrorWriter(s);
        } finally {
            try {
                if (rsPgsqlLb != null) {
                    rsPgsqlLb.close();
                }
                if (stPgsqlLb != null) {
                    stPgsqlLb.close();
                }
                if (connPgsqlLb != null) {
                    connPgsqlLb.close();
                }
            } catch (SQLException ex) {
                String s = Throwables.getStackTraceAsString(ex);
                controllerLog.logErrorWriter(s);
            }
        }
        return idCity;
    }

    public String getNamaBank(String kode_Bank) {
        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        String idbank = "";
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String query = "SELECT \"NamaBank\" FROM \"MsBank\" WHERE kodebank = ?";
            stPgsql = connPgsql.prepareStatement(query);
            stPgsql.setString(1, kode_Bank);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                idbank = rsPgsql.getString("NamaBank");
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return idbank;
    }

    public String prosesTrf(String data, int msSupplier) {
        String response = "";
        String url = "";

        DatabaseUtilitiesPgsql databaseUtilitiesPgsql = new DatabaseUtilitiesPgsql();
        Connection connPgsql = null;
        PreparedStatement stPgsql = null;
        ResultSet rsPgsql = null;
        try {
            connPgsql = databaseUtilitiesPgsql.getConnection(this.entityConfig);

            String query = "SELECT parameter_value FROM \"ms_parameter\" where \"parameter_name\" = 'alto_transfer';";
            stPgsql = connPgsql.prepareStatement(query);
            rsPgsql = stPgsql.executeQuery();
            while (rsPgsql.next()) {
                url = rsPgsql.getString("parameter_value");
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rsPgsql != null) {
                    rsPgsql.close();
                }
                if (stPgsql != null) {
                    stPgsql.close();
                }
                if (connPgsql != null) {
                    connPgsql.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ImpleTransferBank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        JSONObject jObjCurl = new JSONObject();
        jObjCurl.put("msSupplier", msSupplier);
        jObjCurl.put("data", data);

        int idLog = utilities.insertLog(url, jObjCurl);
        response = this.utilities.curlPost(url, data, false).toString();

        utilities.updateLog(idLog, response);
        JSONParser jSONParser = new JSONParser();
        JSONObject jsonRespCurl = new JSONObject();
        try {
            jsonRespCurl = (JSONObject) jSONParser.parse(response);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return response;
    }

}
