package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcAgen46 {

    public String getRegister(JSONObject jsonReq);

    String getKelurahan(JSONObject jsonReq);

    String[] StatusRekeningAgentNonEDC(String username);

    String getOtpTarikTunai(JSONObject jsonReq);

    String getSetorTunai(JSONObject jsonReq);

    String checkAkun(JSONObject jsonReq);

    String getTarik(JSONObject jsonReq);
}
