package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcAsuransiAllianz {

    String getKelurahan(JSONObject jsonReq);

    String getProduk(JSONObject jsonReq);

    public int gettrxid(String idtrx);

    public String[] getStatusTrx(String idtrx);

    public boolean CekIsDuplicate(String idPelanggan, String idOperator, String username);

    public String[] StatusRekeningAgentNonEDC(String username);

    public int[] getComission(int idOperator, int idDenom);

    public String isSaldoCukup(String username, int parseInt);

    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, int[] com);

    boolean checkTenor(String ktp, String companyId);
}
