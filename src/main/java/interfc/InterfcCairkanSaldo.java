package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcCairkanSaldo {

    JSONObject getMsSupplier(JSONObject jsonReq);

    JSONObject getCustDetil(JSONObject jsonReq);

    boolean[] getTbConfig(String username, String nominal);

    String[] getConfigCairkanSaldo(String username, String samsung);

    int[] getFee(int idtipefee, int idtipetransaksi, int idoperator, int nominal, String tipe);

    boolean cekNorek(String noRekening, String kodeBank, String agent);

    String prosesInquiry(String s, JSONObject jObjMsSupplier);

    int getTrxId(String gen_id);

    String[] getStatusTrx(String gen_id);

    boolean cekIsEmptyTransfer(String noRek, int nominal, String noAgen);

    String prosesBayar(JSONObject jsonReq, int[] fee, JSONObject jObjMsSupplier);
}
