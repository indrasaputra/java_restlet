package interfc;

import org.json.simple.JSONObject;

public interface InterfcComplaint {
    String getListTipeTransaksi();
    String saveComplaint(final JSONObject jsonReq);
}
