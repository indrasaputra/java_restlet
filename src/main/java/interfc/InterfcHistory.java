package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcHistory {

    public String getHistory(JSONObject jsonReq);
}
