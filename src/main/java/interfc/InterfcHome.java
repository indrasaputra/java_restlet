package interfc;

import org.json.simple.JSONObject;
import entity.EntityConfig;

/**
 *
 * @author hasan
 */
public interface InterfcHome {

    public String getLastBalance(JSONObject objJson);

    String getPoint(JSONObject jsonReq);

    public String getIklan(JSONObject jsonReq);
}
