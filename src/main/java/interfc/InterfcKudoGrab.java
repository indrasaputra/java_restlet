package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcKudoGrab {

    String getKota(JSONObject jsonReq);

    String getBank(JSONObject jsonReq);

    String customerProfileList(JSONObject jsonReq);

    String createApplication(JSONObject jsonReq);

    String phoneNumberVerification(JSONObject jsonReq);

    String resendPhoneNumberVerification(JSONObject jsonReq);

    String isiKtp(JSONObject jsonReq);

    String isiSim(JSONObject jsonReq);

    String isiStnk(JSONObject jsonReq);

    String isiDeclaration(JSONObject jsonReq);

    String isiBank(JSONObject jsonReq);

    String uploadFileKudoGrab(JSONObject jsonReq);

    String completeData(JSONObject jsonReq);

    String checkStatusData(JSONObject jsonReq);
}
