package interfc;

import org.json.simple.JSONObject;
import entity.EntityConfig;

/**
 *
 * @author hasan
 */
public interface InterfcLogin {

    public String getSignInToken(JSONObject objJson);

    public JSONObject requestToken(JSONObject jsonReq);

    public void blockAccount(JSONObject jsonReq);

    public JSONObject changeDevice(JSONObject jsonReq);

    public void setFcmToken(JSONObject jsonReq);
}
