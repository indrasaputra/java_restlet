package interfc;

import org.json.simple.JSONObject;

/**
 * Created by Fino Indrasaputra on 03/07/2018.
 */
public interface InterfcMember {

    public String getInqIsiSaldo(JSONObject jsonReq);

    public String prosesIsiSaldo(JSONObject jsonReq);
}
