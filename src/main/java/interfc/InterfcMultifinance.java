package interfc;

import org.json.simple.JSONObject;

/**
 * Created by Fino Indrasaputra on 04/06/2018.
 */
public interface InterfcMultifinance {

    String getKelurahan(JSONObject jsonReq);

    String getProduk(JSONObject jsonReq);

    public int gettrxid(String idtrx);

    public String[] getStatusTrx(String idtrx);

    public boolean CekIsDuplicate(String idPelanggan, String idOperator, String username);

    public String[] StatusRekeningAgentNonEDC(String username);

    public int[] getComission(int parseInt);

    public String isSaldoCukup(String username, int parseInt);

    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, int[] com);

    boolean checkTenor(String ktp, String companyId);

}
