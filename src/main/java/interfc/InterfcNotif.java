package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcNotif {

    public String getNotif(JSONObject jsonReq);
}
