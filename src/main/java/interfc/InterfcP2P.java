package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcP2P {

    JSONObject getCommision();

    boolean checkBalance(JSONObject jsonReq, JSONObject jObjInfoFee);

    JSONObject getInfoAgent(String idAgentAccount);

    long deductBalance(String idAgentAccount, String nominal);

    String insertTrTransaksi(JSONObject jsonReq, JSONObject jObjAgentToAgentData);

    void insertTrStockOpen(String idTransaksi, long lastBalanceAfterDeductNominal, String nominal,  JSONObject jsonReq, JSONObject jObjAgentToAgentData);

    long transfer(JSONObject jsonReq, JSONObject jObjAgentToAgentData);

    void updateTransaksi(String idTransaksi, JSONObject jsonReq, JSONObject jObjAgentToAgentData);

    void insertTrStockClose(String idTransaksi, long lastBalanceAfterTransaction, JSONObject jsonReq, JSONObject jObjAgentToAgentData);
}
