package interfc;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hasan
 */
public interface InterfcPembayaranKomunitas {

    public String getKomunitas(JSONObject jsonReq);

    String getInq(JSONObject jsonReq);

    String[] StatusRekeningAgentNonEDC(String username);

    String getPay(JSONObject jsonReq);

    void sendSms(JSONObject jsonReq, JSONObject respObj);
}
