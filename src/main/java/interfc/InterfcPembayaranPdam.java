package interfc;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcPembayaranPdam {    

    public String getKodeArea(JSONObject jsonReq);

    public String tipeOperator(String idoperator);

    public String getProductCode(String idoperator, int idSupplier);

    public String getUrl(String string);

    public String curlPost(String url, String toString, boolean b);

    public int gettrxid(String idtrx);

    public String[] getStatusTrx(String idtrx);

    public boolean CekIsEmptyPostpaid(String idPelanggan, String idOperator, String username);

    public String[] StatusRekeningAgentNonEDC(String username);

    public String[] getComission(int idTipeTransaksi, int idTipeAplikasi, int idOperator);

    public String isSaldoCukup(String username, int parseInt);

    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, String[] com);

    public int getIdSupplier(JSONObject jsonReq);
    
}
