package interfc;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcPembayaranPln {
    
    public String getTipeOperator(String idoperator);
    
    public String getUrl(String string);

    public String getProductCode(String idoperator, int idSupplier);

    public String curlPost(String url, String toString, boolean b);

    public int getTrxId(String idtrx);

    public String[] getStatusTrx(String idtrx);

    public boolean CekIsEmptyPostpaid(String idpelanggan, String idoperator, String username);

    public String[] StatusRekeningAgentNonEDC(String username);

    public int[] getComission(int parseInt);

    public String isSaldoCukup(String id_agentaccount, int parseInt);

    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] statrekmember, int[] com);

    public int getIdSupplier(JSONObject jsonReq);
    
}
