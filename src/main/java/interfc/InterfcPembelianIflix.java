package interfc;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hasan
 */
public interface InterfcPembelianIflix {

    public String getDenom(JSONObject jsonReq);

    int getCountTrxByIdtrx(String idTrx);

    String getStatusTrxByIdtrx(JSONObject jsonReq);

    int checkDuplicateTransaction(JSONObject jsonReq);

    ArrayList<HashMap<String,Object>> getMsAgentAccountDetil(JSONObject jsonReq);

    ArrayList<HashMap<String,Object>> getDetilHarga(JSONObject jsonReq);

    String getUrl();

    JSONObject getPay(JSONObject jsonReq);
}
