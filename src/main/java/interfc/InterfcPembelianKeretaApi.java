package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcPembelianKeretaApi {

    public String getStasiun(JSONObject jsonReq);

    String getSchedule(JSONObject jsonReq);

    String getInq(JSONObject jsonReq);

    String setManualSeat(JSONObject jsonReq);

    String getCancelBook(JSONObject jsonReq);

    String getPay(JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, String[] com);

    int gettrxid(String idtrx);

    String[] getStatusTrx(String idtrx);

    String[] StatusRekeningAgentNonEDC(String username);

    String[] getComission(int idTipeTransaksi, int idTipeAplikasi, int idOperator);

    String isSaldoCukup(String username, int total);

    String getSeatMap(JSONObject jsonReq);
}
