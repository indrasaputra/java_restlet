package interfc;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcPembelianPaketData {

    public String getDenom(JSONObject jsonReq);

    public String bayarPulsaPrabayar(JSONObject jsonReq);

    public int getCountIdTrx(JSONObject jsonReq);

    public String getStatusTrxByIdtrx(JSONObject jsonReq);

    public int checkDuplicateTransaction(JSONObject jsonReq);

    public ArrayList<HashMap<String, Object>> getMsAgentAccountDetil(JSONObject jsonReq);

    public ArrayList<HashMap<String, Object>> getDetilHarga(JSONObject jsonReq);

    public JSONObject deductBalance(JSONObject jsonReq);

    public void insertTrTransTrStock(JSONObject jsonReq, ArrayList<HashMap<String, Object>> row);

    public void updateStatusAccount(JSONObject jsonReq);

    public void hitApiSwitcing(JSONObject jsonReq);
}
