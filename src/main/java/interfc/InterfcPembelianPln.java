package interfc;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcPembelianPln {

    public String getDenom(JSONObject jsonReq);

    public ArrayList<HashMap<String, Object>> getIDAgenKomisi(JSONObject jsonReq);

    public String getURL(String param);

    public int insertLog(JSONObject jsonReq);

    public String curlPost(String url, JSONObject jsonReq, boolean b);

    public void updateLog(String resp, int idLog);

    public String bayarPlnPrabayar(JSONObject jsonReq);

    JSONArray getDetilharga(String idOperator, String nominal);
}
