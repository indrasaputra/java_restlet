package interfc;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author hasan
 */
public interface InterfcPembelianStreaming {

    public String getProvider(JSONObject jsonReq);

    public String getNominal(JSONObject jsonReq);

    public String bayarIflix(JSONObject jsonReq);

    public int getCountTrxByIdtrx(String idTrx);

    public String getStatusTrxByIdtrx(JSONObject jsonReq);

    public ArrayList<HashMap<String, Object>> getMsAgentAccountDetil(JSONObject jsonReq);

    public ArrayList<HashMap<String, Object>> getDetilHarga(JSONObject jsonReq);

    public JSONObject deductBalance(JSONObject jsonReq);

    public void insertTrTransTrStock(JSONObject jsonReq, ArrayList<HashMap<String, Object>> row);

    public void updateStatusAccount(JSONObject jsonReq);

    public void hitApiSwitcing(JSONObject jsonReq);
}
