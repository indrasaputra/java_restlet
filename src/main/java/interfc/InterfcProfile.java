package interfc;

import org.json.simple.JSONObject;
import entity.EntityConfig;

/**
 *
 * @author hasan
 */
public interface InterfcProfile {

    public String getProfile(JSONObject objJson);

    public String[] StatusRekeningAgent(String username);

    public String changePin(String username, String pinBaru);

    public String requestToken(String requestBody);

    public String validasiTokenReguler(String username, String otp);

    public void updatePin(String username, String pinEncrypt);
}
