package interfc;

import org.json.simple.JSONObject;

/**
 * Created by Fino Indrasaputra on 11/07/2018.
 */
public interface InterfcQurban {

    public String getKelurahan(JSONObject jsonObject);

    public String getProduk(JSONObject jsonReq);

    public int gettrxid(String trxid);

    public String[] getStatusTrx(String trxid);

    public boolean CekIsDuplicate(String idPelanggan, String id_operator, String idaccount);

    public String[] StatusRekeningAgentNonEDC(String NoKartu);

    public int[] getComission(int idOperator, int idDenom);

    public String isSaldoCukup(String idAccount, int total);

    public JSONObject prosesBayar(JSONObject jsonReq, JSONObject jsonResp, String[] rekeningAgentNonEDC, int[] com);
}
