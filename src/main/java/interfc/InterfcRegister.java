package interfc;

import org.json.simple.JSONObject;
import entity.EntityConfig;

/**
 *
 * @author hasan
 */
public interface InterfcRegister {

    public String getReferral(JSONObject objJson);

    public String getKelurahan(JSONObject objJson);

    public String getOtp(JSONObject jsonReq);

    public String getRegister(JSONObject jsonReq);

}
