package interfc;

import org.json.simple.JSONObject;

/**
 *
 * @author hasan
 */
public interface InterfcTransfer {

    String getInq(JSONObject jsonReq);

    String[] StatusRekeningAgentNonEDC(String username);

    String getPay(JSONObject jsonReq);

    void sendSms(JSONObject jsonReq, JSONObject respObj);

    String getInqTerimaUang(JSONObject jsonReq);

    String getPayTerimaUang(JSONObject jsonReq);
}
