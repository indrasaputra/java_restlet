package interfc;

import org.json.simple.JSONObject;

/**
 * Created by Fino Indrasaputra on 20/07/2018.
 */
public interface InterfcTransferBank {

    public String getBank();

    public int getIdSupplier(JSONObject jsonReq);

    public String getMinPayment(String id_tipetransaksi);

    public String getMaxPayment(String id_tipetransaksi);

    public String getNotifMinPay();

    public String getNotifMaxPay();

    public boolean cekStatus(String username);

    public boolean cekNorek(String noRekening, String kodeBank, String tipe);

    public JSONObject prosesInq(JSONObject jsonReq, JSONObject jsonResp, int idSupplier);

    public int gettrxid(String trxid);

    public String[] getStatusTrx(String trxid);

    public String[] StatusRekeningAgentNonEDC(String username);

    public boolean CekIsEmptyTransfer(String nORek, int nominal, String idaccount);

    public String isSaldoCukup(String idAccount, int total);

    public JSONObject prosesTransfer(JSONObject jsonReq, JSONObject jsonResp, String ID_Member, String[] com);

    public String[] getComission(int idTipeTransaksi, int idTipeAplikasi, int idOperator);
}
